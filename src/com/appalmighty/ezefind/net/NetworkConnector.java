package com.appalmighty.ezefind.net;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

import com.appalmighty.ezefind.net.NetworkConstant.Api;
import com.appalmighty.ezefind.net.NetworkConstant.Base;

public class NetworkConnector {

	public static final String TAG = NetworkConnector.class.getSimpleName();

	public static String loginAuthentication(List<NameValuePair> nameValuePair) {
		String url = null;
		url = Base.API + Api.LOGIN;

		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;

	}

	public static String LoginVerification(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.API + Api.LOGINVERIFY;
		Log.e("url", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String registerUser(List<NameValuePair> nameValuePair) {
		String url = null;
		url = Base.API + Api.REGISTER;

		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;

	}

	public static String forgotPassword(List<NameValuePair> nameValuePair) {
		String url = null;
		url = Base.API + Api.RESET;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;

	}

	public static String PackageForm(List<NameValuePair> nameValuePair) {
		String url = null;
		url = Base.PACKAGEAPI + Api.PACKAGEDATA;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;

	}

	public static String PackageDetail(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.PACKAGE;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String PackageLocation(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.PACKAGELOCATION;
		Log.e("url location", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String Distance(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.DISTANCE;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String PackageDetail1(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.CREATEPACKAGEDATA;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String Test(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.TEST;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetPackageItem(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.GETPACKAGEDATAITEM;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String DeletPackageItem(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.DELETEPACKAGEDATAITEM;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String UpdatePackageItem(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.UPDATEPACKAGEDATAITEM;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetPackage(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.GETALLPACKAGES;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String InviteUser(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.API + Api.INVITEUSER;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetSingleItem(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.SINGLEITEMINFO;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetSinglePackage(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.GETSINGLEPACKAGEDETAIL;
		Log.e("url filter", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetPicklist(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PICKLIST + Api.PICKLIST;
		Log.e("url picklist", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String AddPicklist(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PICKLIST + Api.ADDPICKLIST;
		Log.e("url add picklist", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String CreatePickList(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PICKLIST + Api.CREATEPICKLIST;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetPicklistDetail(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PICKLIST + Api.PICKLISTDETAIL;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String DeletePicklist(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PICKLIST + Api.DELETEPICKLIST;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String deletePackageData(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.DELETEPACKAGEDATA;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String deletePackage(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.DELETEPACKAGE;
		Log.e("url delete package---", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String PasswordChange(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.API + Api.PASSWORDCHANGE;
		Log.e("url---", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String UpdateProfile(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.API + Api.PROFILEUPDATE;
		Log.e("url update Profile---", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetProfile(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.API + Api.GETPROFILE;
		Log.e("url get Profile---", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String AllMember(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.API + Api.ALLMEMBER;
		Log.e("urli member", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String Selectedmember(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.API + Api.SELECTEDMEMBER;
		Log.e("url", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String Deletemember(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.API + Api.DELETESELECTEDMEMBER;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GETCategory(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.API + Api.GETCATOGARY;
		Log.e("url", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String CreateCategory(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.API + Api.CREATECATOGARY;
		Log.e("url create", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String Deleteattchment(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.PACKAGEAPI + Api.DELETEATTACHMENT;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetAllData(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.REPORT + Api.GETALLDATA;
		Log.e("url all", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetItemData(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.REPORT + Api.GETITEMDATA;
		Log.e("url item", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetItemFilterData(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.REPORT + Api.GETITEMFILTERDATA;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetAllMemeber(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.REPORT + Api.GETALLMEMEBER;
		Log.e("url all member", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetAllMemeberReport(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.REPORT + Api.GETMEMEBERSREPORT;
		Log.e("url all member report", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetSingle_Memeber_filter(
			List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.REPORT + Api.SINGLE_MEMEBER_FILTER;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetMemeberReportFilter(
			List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.REPORT + Api.MEMEBERS_REPORT_FILTER;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetCatagoryData(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.REPORT + Api.GETCATAGORYDATA;
		Log.e("url category", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetLocationData(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.REPORT + Api.GETLOCATIONDATA;
		Log.e("url loc", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetDateData(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.REPORT + Api.GETDATEDATA;
		Log.e("url date", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GetValueData(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.REPORT + Api.GETVALUEDATA;
		Log.e("url value", "" + url);
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String GETLocation(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = Base.API + Api.GETALLLOCATION;
		String jString = null;
		try {
			jString = SocketConnection.postJSONObject(url, nameValuePair);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return jString;
	}

	public static String DeleteCategory(String UserId, String CategoryId) {
		// TODO Auto-generated method stub
		String url = "http://beta.brstdev.com/yiiezefind/frontend/index.php/graph/delete-category";

		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("UserId", UserId));
		pairs.add(new BasicNameValuePair("CategoryId", CategoryId));

		String result = null;
		try {
			result = SocketConnection.postJSONObject(url, pairs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

}
