package com.appalmighty.ezefind.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.util.Log;

public class SocketConnection {

	public static final String TAG = SocketConnection.class.getSimpleName();
	public static final int HTTP_SESSION_TIME_OUT = (int) (10 * 1000);

	public static String getJSONObject(String url) throws IOException {
		InputStream iStream = null;
		String jString = null;

		HttpResponse response = null;
		// Making HTTP request
		try {
			// defaultHttpClient
			HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams,
					HTTP_SESSION_TIME_OUT);
			HttpConnectionParams
					.setSoTimeout(httpParams, HTTP_SESSION_TIME_OUT);
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);

			response = httpclient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			iStream = entity.getContent();
			jString = convertStreamToString(iStream);
		} catch (ConnectTimeoutException e) {
			Log.e("connection error time OUT", "ok" + e.getMessage().toString());
		} catch (SocketTimeoutException ste) {

			Log.e("Timeout Exception: ", ste.toString());

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e(TAG, "" + e.getMessage());
		}

		return jString;

	}

	public static String postJSONObject(String url,
			List<NameValuePair> namevaluepairs) throws IOException {
		InputStream iStream = null;
		String jString = null;

		HttpResponse response = null;
		try {
			// defaultHttpClient
			HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams,
					HTTP_SESSION_TIME_OUT);
			HttpConnectionParams
					.setSoTimeout(httpParams, HTTP_SESSION_TIME_OUT);
			HttpClient httpclient = new DefaultHttpClient(httpParams);
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(new UrlEncodedFormEntity(namevaluepairs));
			// HttpResponse response = null;
			response = httpclient.execute(httpPost);

			HttpEntity entity = response.getEntity();
			iStream = entity.getContent();
			jString = convertStreamToString(iStream);
		} catch (ConnectTimeoutException e){
			Log.e("connection error time OUT", "ok");
			jString = "error";

		} catch (SocketTimeoutException ste) {

			Log.e("Timeout Exception: ", ste.getMessage());
			jString = "error";
		} catch (UnsupportedEncodingException e) {
			Log.e("UnsupportedEncodingException: ", e.getMessage());
			jString = "error";
		} catch (ClientProtocolException e) {
			Log.e("ClientProtocolException ", e.getMessage());
			jString = "error";
		} catch (IOException e) {
			Log.e("IOException: ", e.getMessage());
			jString = "error";
		}
		if (!jString.equals("")) {
			return jString;
		} else {
			jString = "error";
			return jString;

		}
	}

	public static String convertStreamToString(InputStream iS)
			throws IOException {
		String response = null;
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					iS, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "");
			}
			response = sb.toString();

		} catch (Exception e) {
			Log.e(TAG + "Buffer Error",
					"Error converting result " + e.toString());
		} finally {
			if (iS != null) {
				iS.close();
			}

		}
		return response;

	}

}
