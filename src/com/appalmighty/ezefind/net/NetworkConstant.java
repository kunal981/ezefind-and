package com.appalmighty.ezefind.net;

public class NetworkConstant {

	public class Base {
		public static final String HOST = "http://beta.brstdev.com/yiiezefind/frontend/index.php";
		public static final String API = HOST + "/users/";
		public static final String PACKAGEAPI = HOST + "/package/";
		public static final String PICKLIST = HOST + "/picklists/";
		public static final String REPORT = HOST + "/graph/";

	}

	public class Api {

		// Login API
		public static final String LOGIN = "login";
		public static final String LOGINVERIFY = "login-access";
		public static final String REGISTER = "registration";
		public static final String RESET = "password-reset";

		// Package API
		public static final String PACKAGE = "createpackage";
		public static final String CREATEPACKAGEDATA = "create-packagedata";
		public static final String DELETEPACKAGE = "delete-package";
		public static final String PACKAGEDATA = "update-packagedata";
		public static final String DELETEPACKAGEDATA = "delete-packagedata";
		public static final String DELETEATTACHMENT = "deleteattachment";
		public static final String PACKAGEDATAITEM = "adddataitems";
		public static final String GETPACKAGEDATAITEM = "getpackagesdataitems";
		public static final String DELETEPACKAGEDATAITEM = "delete-item";
		public static final String UPDATEPACKAGEDATAITEM = "updatedataitem";
		public static final String SINGLEITEMINFO = "info-item";
		public static final String GETALLPACKAGES = "get-packages";
		public static final String GETSINGLEPACKAGEDETAIL = "getpackages-details";
		public static final String PACKAGELOCATION = "packagedata-location";
		public static final String DISTANCE = "distance";
		public static final String TEST = "test";
		public static final String INVITEUSER = "invite-pacakge";
		public static final String DELETE_CATEGORY = "invite-pacakge";

		// PICKLIST API
		public static final String CREATEPICKLIST = "createpicklist";
		public static final String PICKLIST = "get-picklist";
		public static final String ADDPICKLIST = "addpicklist-items";
		public static final String PICKLISTDETAIL = "picklist-detail";
		public static final String DELETEPICKLIST = "delete-picklist";

		// Setting API
		public static final String PASSWORDCHANGE = "change-password";
		public static final String PROFILEUPDATE = "update-profile";
		public static final String GETPROFILE = "profile";
		public static final String ALLMEMBER = "get-members";
		public static final String SELECTEDMEMBER = "member-detail";
		public static final String DELETESELECTEDMEMBER = "member-detail";

		// Catogary API
		public static final String GETCATOGARY = "getcategory";
		public static final String CREATECATOGARY = "create-category";

		// Report Section API
		public static final String GETALLDATA = "all";
		public static final String GETITEMDATA = "items";
		public static final String GETITEMFILTERDATA = "items-filter";
		public static final String GETALLMEMEBER = "members";
		public static final String SINGLE_MEMEBER_FILTER = "members-filter";
		public static final String GETMEMEBERSREPORT = "members-report";
		public static final String MEMEBERS_REPORT_FILTER = "members-report-filter";
		public static final String GETCATAGORYDATA = "category";
		public static final String GETLOCATIONDATA = "location";
		public static final String GETDATEDATA = "date";
		public static final String GETVALUEDATA = "value";
		public static final String GETALLLOCATION = "getlocation";

	}

}
