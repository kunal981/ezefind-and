package com.appalmighty.ezefind.setting.fgmt;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.Modal.I_MemberModal;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class SettingFragmentAllMember extends Fragment implements
		OnClickListener {

	Button buttonBack, btnhome;
	Fragment fragment = null;
	List<I_MemberModal> listmemberModal;
	MyAdapter adapter;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	private SharedPreferences preferences;
	String UserId;
	ListView Lv;
	I_MemberModal memberModal;
	TextView txtInfo;
	String memberId, id, id_;

	public static SettingFragmentAllMember newInstance() {
		SettingFragmentAllMember fragment = new SettingFragmentAllMember();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.layout_all_imember,
				container, false);
		Lv = (ListView) rootView.findViewById(R.id.AllI_member);
		buttonBack = (Button) rootView.findViewById(R.id.id_btn_back);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		txtInfo = (TextView) rootView.findViewById(R.id.txt_no_members);
		buttonBack.setOnClickListener(this);
		btnhome.setOnClickListener(this);
		listmemberModal = new ArrayList<I_MemberModal>();

		preferences = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = preferences.getString(AppConstant.KEY_USER_ID, "");

		Lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				memberModal = listmemberModal.get(position);
				fragment = SettingFragmentMemberDetail.newInstance();
				id_ = memberModal.getMemberId();
				Log.e("id trans", "" + id_);
				switchToFragment(fragment);
			}
		});

		internetcheck = new ConnectionDetector(getActivity());
		isInternetPresent = internetcheck.isConnectingToInternet();

		if (isInternetPresent) {
			new AsyncAllMemberTask().execute(UserId);

		} else {

		}
		return rootView;
	}

	private void switchToFragment(Fragment fragment) {

		FragmentManager fm = getFragmentManager();
		Bundle savedInstanceState = new Bundle();
		savedInstanceState.putString("memberID", id_);
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
				R.anim.slide_in_left, R.anim.slide_out_right);
		fragment.setArguments(savedInstanceState);
		ft.replace(R.id.container, fragment);
		ft.addToBackStack(null);
		ft.commit();

	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.id_btn_back:
			getActivity().onBackPressed();
			break;
		case R.id.id_button_home:
			getActivity().finish();
			Intent intent = new Intent(getActivity(), MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			break;
		}

	}

	public class AsyncAllMemberTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {
				ViewUtil.hideProgressDialog();
				if (result.contains("true")) {
					Log.e("Response i member", result);

					listmemberModal = updateUI(result);
					if (listmemberModal != null) {
						adapter = new MyAdapter(getActivity(), listmemberModal);
						Lv.setAdapter(adapter);
					}

				} else {
					ViewUtil.showAlertDialog(getActivity(), "",
							"Members not found", false);
				}
			}

		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));

			String result = NetworkConnector.AllMember(parameter);
			return result;
		}

	}

	class MyAdapter extends BaseAdapter {
		private LayoutInflater inflater = null;
		private Context mContext;

		List<I_MemberModal> listmemberModal = new ArrayList<I_MemberModal>();
		DisplayImageOptions options;

		public MyAdapter(Context mContext, List<I_MemberModal> listmemberModal) {
			super();

			this.mContext = mContext;
			this.listmemberModal = listmemberModal;
			options = setupImageLoader();
		}

		@Override
		public int getCount() {

			return listmemberModal.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return listmemberModal.get(arg0);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			final ViewHolder holder;
			if (inflater == null) {
				inflater = (LayoutInflater) getActivity().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE);

			}
			if (convertView == null) {
				convertView = inflater
						.inflate(R.layout.custom_all_member, null);

				holder = new ViewHolder();
				holder.textView = (TextView) convertView
						.findViewById(R.id.usersname);
				holder.imageView = (ImageView) convertView
						.findViewById(R.id.imageView_user);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			memberModal = listmemberModal.get(position);
			id = memberModal.getMemberId();

			String username = memberModal.getName();
			if (username != null) {
				holder.textView.setText(username);

			}

			String getImage = memberModal.getProfileImage();

			if (getImage != null) {
				ImageLoader.getInstance().displayImage(getImage,
						holder.imageView, options);
			}

			return convertView;
		}

	}

	class ViewHolder {
		TextView textView;
		ImageView imageView;
	}

	/**
	 * @param result
	 * @return
	 */
	public List<I_MemberModal> updateUI(String result) {
		// TODO Auto-generated method stub
		listmemberModal = new ArrayList<I_MemberModal>();
		try {
			JSONObject jsonObject = new JSONObject(result);
			JSONArray jsonArray = jsonObject.getJSONArray("message");
			Log.e("jsonArray", "" + jsonArray);

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonInnerObject = jsonArray.getJSONObject(i);
				I_MemberModal memberModal = new I_MemberModal();
				String name = jsonInnerObject.getString("Name");
				String profile_pic = jsonInnerObject.getString("ProfileImage");
				memberId = jsonInnerObject.getString("MemberId");
				if (name != null) {
					memberModal.setName(name);

				}
				if (profile_pic != null) {
					memberModal.setProfileImage(profile_pic);

				}
				if (memberId != null) {
					memberModal.setMemberId(memberId);

				}

				listmemberModal.add(memberModal);

			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return listmemberModal;

	}

	private DisplayImageOptions setupImageLoader() {
		return new DisplayImageOptions.Builder().cacheInMemory(true)
				.showImageOnLoading(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.no_image_icon)
				.showImageOnFail(R.drawable.no_image_icon).cacheOnDisk(true)
				.considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1)).build();

	}
}
