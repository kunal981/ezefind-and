package com.appalmighty.ezefind.setting.fgmt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;

public class SettingFragmentNetwork extends Fragment implements OnClickListener {

	private Button buttonBack, btnhome;
	private TextView txttext;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;

	public static SettingFragmentNetwork newInstance() {
		SettingFragmentNetwork fragment = new SettingFragmentNetwork();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(
				R.layout.fragment_layout_setting_network, container, false);
		buttonBack = (Button) rootView.findViewById(R.id.id_btn_back);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		txttext = (TextView) rootView.findViewById(R.id.id_text);
		buttonBack.setOnClickListener(this);
		btnhome.setOnClickListener(this);
		internetcheck = new ConnectionDetector(getActivity());
		isInternetPresent = internetcheck.isConnectingToInternet();

		if (isInternetPresent) {
			txttext.setText("CONNECTED");
		} else {

		}

		return rootView;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.id_btn_back:
			getActivity().onBackPressed();
			break;
		case R.id.id_button_home:
			getActivity().finish();
			Intent intent = new Intent(getActivity(), MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			break;
		}

	}
}
