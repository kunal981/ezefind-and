package com.appalmighty.ezefind.setting.fgmt;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.setting.act.SettingActivity;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.appalmighty.ezefind.view.ChoosePopup;

public class SettingFragmentPassword extends Fragment implements
		OnClickListener {
	public static final int RESULT_OK = -1;
	public static final int REQUEST_AUDIO_TO_TEXT_CURRENT = 301;
	public static final int REQUEST_AUDIO_TO_TEXT_NEW = 302;

	public static final int REQUEST_AUDIO_TO_TEXT_CONFIRM = 303;
	private Button buttonBack, btnhome;
	private TextView update;
	private EditText edt_curentpsd, edt_newpsd, edt_confirmpsd;
	String current, New, confirm, typingvalue;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	private SharedPreferences preferences;
	String UserId;

	public static SettingFragmentPassword newInstance() {
		SettingFragmentPassword fragment = new SettingFragmentPassword();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(
				R.layout.fragment_layout_setting_password, container, false);
		buttonBack = (Button) rootView.findViewById(R.id.id_btn_back);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		edt_curentpsd = (EditText) rootView.findViewById(R.id.edt_current_pwd);
		edt_newpsd = (EditText) rootView.findViewById(R.id.edt_new_pwd);
		edt_confirmpsd = (EditText) rootView.findViewById(R.id.edt_retype_pwd);
		update = (TextView) rootView.findViewById(R.id.id_Done);
		btnhome.setOnClickListener(this);
		buttonBack.setOnClickListener(this);
		update.setOnClickListener(this);
		preferences = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);

		UserId = preferences.getString(AppConstant.KEY_USER_ID, "");

		internetcheck = new ConnectionDetector(getActivity());
		edt_curentpsd.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					typingvalue = preferences.getString(
							AppConstant.KEY_ENTRYTYPE, "");
					Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						edt_curentpsd.setInputType(InputType.TYPE_NULL);

						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_CURRENT);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		edt_newpsd.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					typingvalue = preferences.getString(
							AppConstant.KEY_ENTRYTYPE, "");
					Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						edt_newpsd.setInputType(InputType.TYPE_NULL);

						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_NEW);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		edt_confirmpsd.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					typingvalue = preferences.getString(
							AppConstant.KEY_ENTRYTYPE, "");
					Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						edt_confirmpsd.setInputType(InputType.TYPE_NULL);

						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_CONFIRM);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		edt_curentpsd.setOnClickListener(this);
		edt_newpsd.setOnClickListener(this);
		edt_confirmpsd.setOnClickListener(this);
		return rootView;
	}

	protected void startRecognition(Activity mainActivity, int RequestCode) {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
		startActivityForResult(intent, RequestCode);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		/*
		 * if (requestCode == 100) {
		 */
		switch (requestCode) {
		case REQUEST_AUDIO_TO_TEXT_CURRENT:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				edt_curentpsd.setText(text.get(0));
			}
			break;
		case REQUEST_AUDIO_TO_TEXT_NEW:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				edt_newpsd.setText(text.get(0));
			}
			break;
		case REQUEST_AUDIO_TO_TEXT_CONFIRM:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				edt_confirmpsd.setText(text.get(0));
			}
			break;

		}
	}

	protected boolean validatePassword() {
		if (current.equals("")) {
			ViewUtil.showAlertDialog(getActivity(), "ALERT",
					"Please enter your current password", true);
			return false;
		}
		if (New.equals("")) {
			ViewUtil.showAlertDialog(getActivity(), "ALERT",
					"Please enter your new password", true);
			return false;
		}
		if (confirm.trim().equals("")) {
			ViewUtil.showAlertDialog(getActivity(), "ALERT",
					"Please retype your new password", true);
			return false;
		}
		if (New.trim().equals(confirm.trim())) {
			ViewUtil.showAlertDialog(getActivity(), "ALERT",
					"Password mismatch", true);
			return true;
		} else {
			ViewUtil.showAlertDialog(getActivity(), "ALERT",
					"Password mismatch", false);
		}

		return true;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.id_btn_back:
			getActivity().onBackPressed();
			break;
		case R.id.id_button_home:
			getActivity().finish();
			Intent intent = new Intent(getActivity(), MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			break;
		case R.id.id_Done:
			current = edt_curentpsd.getText().toString();
			New = edt_newpsd.getText().toString();
			confirm = edt_confirmpsd.getText().toString();

			isInternetPresent = internetcheck.isConnectingToInternet();
			if (validatePassword()) {
				if (isInternetPresent) {
					new AsyncChangePasswordTask().execute(current, New, UserId);

				} else {
					ViewUtil.showAlertDialog(getActivity(), "Error",
							"No Internet Connection", true);
				}
			}
			break;
		case R.id.edt_current_pwd:
			typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE, "");
			Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				edt_curentpsd.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					edt_curentpsd.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(),
							REQUEST_AUDIO_TO_TEXT_CURRENT);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				edt_curentpsd.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;
		case R.id.edt_new_pwd:
			typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE, "");
			Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				edt_newpsd.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					edt_newpsd.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_NEW);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				edt_newpsd.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;
		case R.id.edt_retype_pwd:
			typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE, "");
			Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				edt_confirmpsd.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					edt_confirmpsd.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(),
							REQUEST_AUDIO_TO_TEXT_CONFIRM);

				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				edt_confirmpsd.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;

		}

	}

	public class AsyncChangePasswordTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response", result);
							ViewUtil.showAlertDialog(getActivity(), "SUCCESS",
									"Successfully Changed Password", true);
							edt_curentpsd.setText("");
							edt_newpsd.setText("");
							edt_confirmpsd.setText("");
							Intent intentSetting = new Intent(getActivity(),
									SettingActivity.class);
							startActivity(intentSetting);
							// getActivity().overridePendingTransition(R.anim.fade_in,
							// R.anim.fade_out);
							ViewUtil.hideProgressDialog();

						} else {
							Log.e("Response", result);

							String Server_message = jObj.getString("message");

							ViewUtil.hideProgressDialog();
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									Server_message, true);

						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("OldPassword", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("NewPassword", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[2])));
			String result = NetworkConnector.PasswordChange(parameter);
			return result;
		}

	}
}
