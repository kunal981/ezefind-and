package com.appalmighty.ezefind.setting.fgmt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;

public class SettingFragmentSaveEntry extends Fragment implements
		OnClickListener {

	private Button buttonBack, btnhome;
	private ImageView text, audio, forboth;
	private TextView txttext, txtaudio, txtboth;

	public static SettingFragmentSaveEntry newInstance() {
		SettingFragmentSaveEntry fragment = new SettingFragmentSaveEntry();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(
				R.layout.fragment_layout_setting_save_entry, container, false);
		buttonBack = (Button) rootView.findViewById(R.id.id_btn_back);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		txttext = (TextView) rootView.findViewById(R.id.id_text);
		txtaudio = (TextView) rootView.findViewById(R.id.id_audio);
		txtboth = (TextView) rootView.findViewById(R.id.id_audio_text_both);
		text = (ImageView) rootView.findViewById(R.id.id_chk_text);
		audio = (ImageView) rootView.findViewById(R.id.id_btn_audio);
		forboth = (ImageView) rootView.findViewById(R.id.id_btn_audio_text_b);
		btnhome.setOnClickListener(this);
		buttonBack.setOnClickListener(this);
		txttext.setOnClickListener(this);
		txtaudio.setOnClickListener(this);
		txtboth.setOnClickListener(this);

		return rootView;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.id_btn_back:
			getActivity().onBackPressed();
			break;
		case R.id.id_button_home:
			getActivity().finish();
			Intent intent = new Intent(getActivity(), MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			break;
		case R.id.id_text:
			text.setVisibility(View.VISIBLE);
			audio.setVisibility(View.INVISIBLE);
			forboth.setVisibility(View.INVISIBLE);
			break;
		case R.id.id_audio:
			text.setVisibility(View.INVISIBLE);
			audio.setVisibility(View.VISIBLE);
			forboth.setVisibility(View.INVISIBLE);
			break;
		case R.id.id_audio_text_both:
			text.setVisibility(View.INVISIBLE);
			audio.setVisibility(View.INVISIBLE);
			forboth.setVisibility(View.VISIBLE);
			break;
		}

	}
}
