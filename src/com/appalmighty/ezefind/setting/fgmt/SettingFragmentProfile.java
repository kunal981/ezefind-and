package com.appalmighty.ezefind.setting.fgmt;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.appalmighty.ezefind.view.ChoosePopup;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class SettingFragmentProfile extends Fragment implements OnClickListener {
	public static final int RESULT_OK = -1;
	public static final int REQUEST_AUDIO_TO_TEXT_FIRST = 301;
	public static final int REQUEST_AUDIO_TO_TEXT_LAST = 302;

	public static final int REQUEST_AUDIO_TO_TEXT_ADDRESS = 303;
	public static final int REQUEST_AUDIO_TO_TEXT_CITY = 304;
	public static final int REQUEST_AUDIO_TO_TEXT_STATE = 305;
	public static final int REQUEST_AUDIO_TO_TEXT_ZIP = 306;
	private Button buttonBack, btnhome;
	DisplayImageOptions displayImageOptions;
	private SharedPreferences preferences;
	ImageView profileimage;
	EditText firstname, lastname, address, state, city, zipcode;
	String first, last, addre, State, City, zip, UserId, typingvalue;
	TextView update;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;

	public static SettingFragmentProfile newInstance() {
		SettingFragmentProfile fragment = new SettingFragmentProfile();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(
				R.layout.fragment_layout_setting_profile, container, false);
		buttonBack = (Button) rootView.findViewById(R.id.id_btn_back);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		profileimage = (ImageView) rootView.findViewById(R.id.id_image_profile);
		update = (TextView) rootView.findViewById(R.id.id_edit);
		firstname = (EditText) rootView.findViewById(R.id.id_edt_name);
		lastname = (EditText) rootView.findViewById(R.id.id_edt_l_name);
		address = (EditText) rootView.findViewById(R.id.id_edt_address_name);
		state = (EditText) rootView.findViewById(R.id.id_edt_state);
		city = (EditText) rootView.findViewById(R.id.id_edt_city);
		zipcode = (EditText) rootView.findViewById(R.id.id_edt_zip);
		displayImageOptions = setupImageLoader();
		btnhome.setOnClickListener(this);
		buttonBack.setOnClickListener(this);
		update.setOnClickListener(this);
		internetcheck = new ConnectionDetector(getActivity());
		preferences = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);

		UserId = preferences.getString(AppConstant.KEY_USER_ID, "");
		typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE, "");
		isInternetPresent = internetcheck.isConnectingToInternet();

		if (isInternetPresent) {

			new AsyncGetProfileTask().execute(UserId);
		} else {

		}
		firstname.setOnClickListener(this);
		lastname.setOnClickListener(this);
		address.setOnClickListener(this);
		state.setOnClickListener(this);
		city.setOnClickListener(this);
		zipcode.setOnClickListener(this);
		firstname.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					typingvalue = preferences.getString(
							AppConstant.KEY_ENTRYTYPE, "");
					Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						firstname.setInputType(InputType.TYPE_NULL);

						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_FIRST);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		lastname.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					typingvalue = preferences.getString(
							AppConstant.KEY_ENTRYTYPE, "");
					Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						lastname.setInputType(InputType.TYPE_NULL);

						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_LAST);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		address.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					typingvalue = preferences.getString(
							AppConstant.KEY_ENTRYTYPE, "");
					Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						address.setInputType(InputType.TYPE_NULL);

						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_ADDRESS);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		city.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					typingvalue = preferences.getString(
							AppConstant.KEY_ENTRYTYPE, "");
					Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						city.setInputType(InputType.TYPE_NULL);

						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_CITY);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		state.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					typingvalue = preferences.getString(
							AppConstant.KEY_ENTRYTYPE, "");
					Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						state.setInputType(InputType.TYPE_NULL);

						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_STATE);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		zipcode.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					typingvalue = preferences.getString(
							AppConstant.KEY_ENTRYTYPE, "");
					Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						zipcode.setInputType(InputType.TYPE_NULL);

						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_ZIP);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		return rootView;
	}

	protected void startRecognition(Activity mainActivity, int RequestCode) {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
		startActivityForResult(intent, RequestCode);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		/*
		 * if (requestCode == 100) {
		 */
		switch (requestCode) {
		case REQUEST_AUDIO_TO_TEXT_FIRST:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				firstname.setText(text.get(0));
			}
			break;
		case REQUEST_AUDIO_TO_TEXT_LAST:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				lastname.setText(text.get(0));
			}
			break;
		case REQUEST_AUDIO_TO_TEXT_ADDRESS:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				address.setText(text.get(0));
			}
			break;
		case REQUEST_AUDIO_TO_TEXT_CITY:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				city.setText(text.get(0));
			}
			break;
		case REQUEST_AUDIO_TO_TEXT_STATE:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				state.setText(text.get(0));
			}
			break;
		case REQUEST_AUDIO_TO_TEXT_ZIP:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				zipcode.setText(text.get(0));
			}
			break;
		}
	}

	public DisplayImageOptions setupImageLoader() {
		return new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1)).build();

	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.id_btn_back:
			getActivity().onBackPressed();
			break;
		case R.id.id_button_home:
			getActivity().finish();
			Intent intent = new Intent(getActivity(), MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			break;
		case R.id.id_edit:
			first = firstname.getText().toString();
			last = lastname.getText().toString();
			addre = address.getText().toString();
			State = state.getText().toString();
			City = city.getText().toString();
			zip = zipcode.getText().toString();

			if (isInternetPresent) {

				new AsyncUpdateProfileTask().execute(first, last, addre, State,
						City, zip, UserId);
			} else {

			}
			break;
		case R.id.id_edt_name:
			typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE, "");
			Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				firstname.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					firstname.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_FIRST);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				firstname.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;
		case R.id.id_edt_l_name:
			typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE, "");
			Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				lastname.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					lastname.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_LAST);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				lastname.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;
		case R.id.id_edt_address_name:
			typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE, "");
			Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				address.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					address.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(),
							REQUEST_AUDIO_TO_TEXT_ADDRESS);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				address.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;
		case R.id.id_edt_state:
			typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE, "");
			Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				state.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					state.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_STATE);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				state.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;
		case R.id.id_edt_city:
			typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE, "");
			Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				city.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					city.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_CITY);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				city.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;
		case R.id.id_edt_zip:
			typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE, "");
			Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				zipcode.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					zipcode.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_ZIP);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				zipcode.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;

		}

	}

	public class AsyncUpdateProfileTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				JSONObject jObj = new JSONObject(result);

				if (jObj.getBoolean("success")) {
					Log.e("Response update", result);

					firstname.setText(first);
					lastname.setText(last);
					address.setText(addre);
					state.setText(State);
					city.setText(City);
					zipcode.setText(zip);

					ViewUtil.showAlertDialog(getActivity(), "Success",
							"Your Data is Updated Successfully", true);
					ViewUtil.hideProgressDialog();

				} else {
					Log.e("Response", result);

					String Server_message = jObj.getString("message");
					ViewUtil.hideProgressDialog();

					ViewUtil.showAlertDialog(getActivity(), "Update Failed",
							Server_message, true);

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("FirstName", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("LastName", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("Address", String
					.valueOf(params[2])));
			parameter.add(new BasicNameValuePair("State", String
					.valueOf(params[3])));

			parameter.add(new BasicNameValuePair("City", String
					.valueOf(params[4])));
			parameter.add(new BasicNameValuePair("ZipCode", String
					.valueOf(params[5])));
			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[6])));

			Log.e("parameterupdate", "" + parameter);
			String result = NetworkConnector.UpdateProfile(parameter);
			return result;
		}

	}

	public class AsyncGetProfileTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response profile", result);
							JSONObject phone = jObj.getJSONObject("message");
							firstname.setText(phone.getString("FirstName"));
							lastname.setText(phone.getString("LastName"));
							address.setText(phone.getString("Address"));
							state.setText(phone.getString("State"));
							city.setText(phone.getString("City"));
							zipcode.setText(phone.getString("ZipCode"));
							ImageLoader.getInstance().displayImage(
									"http://beta.brstdev.com/yiiezefind"
											+ phone.getString("State"),
									profileimage);
							// ViewUtil.showAlertDialog(getActivity(),
							// "Success",
							// "Your Data is Updated Successfully", true);
							ViewUtil.hideProgressDialog();

						} else {
							Log.e("Response", result);

							String Server_message = jObj.getString("message");
							ViewUtil.hideProgressDialog();

							ViewUtil.showAlertDialog(getActivity(),
									"Update Failed", Server_message, true);

						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));

			Log.e("paramete profile", "" + parameter);
			String result = NetworkConnector.GetProfile(parameter);
			return result;
		}

	}
}
