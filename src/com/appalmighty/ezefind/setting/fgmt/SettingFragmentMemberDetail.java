package com.appalmighty.ezefind.setting.fgmt;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.Modal.I_MemberModal;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.setting.fgmt.SettingFragmentAllMember.MyAdapter;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class SettingFragmentMemberDetail extends Fragment implements
		OnClickListener {

	private Button buttonBack, btnhome;
	private TextView txtaudio, txtboth, txt_Address, txt_Date, txtUserName;
	private ImageView audio, forboth, delete, imgUser;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	private SharedPreferences preferences;
	String UserId, memberid;
	DisplayImageOptions options;

	public static SettingFragmentMemberDetail newInstance() {
		SettingFragmentMemberDetail fragment = new SettingFragmentMemberDetail();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.layout_single_member,
				container, false);
		buttonBack = (Button) rootView.findViewById(R.id.id_btn_back);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		imgUser = (ImageView) rootView.findViewById(R.id.imageView1);
		delete = (ImageView) rootView.findViewById(R.id.delete_imemeber);
		txtaudio = (TextView) rootView.findViewById(R.id.id_audio);
		txt_Address = (TextView) rootView.findViewById(R.id.textAddress);
		txtUserName = (TextView) rootView.findViewById(R.id.textUsername);
		txt_Date = (TextView) rootView.findViewById(R.id.textDateAdded);
		txtboth = (TextView) rootView.findViewById(R.id.id_audio_text_both);
		audio = (ImageView) rootView.findViewById(R.id.id_btn_audio);
		forboth = (ImageView) rootView.findViewById(R.id.id_btn_audio_text_b);
		buttonBack.setOnClickListener(this);
		delete.setOnClickListener(this);
		btnhome.setOnClickListener(this);
		txtaudio.setOnClickListener(this);
		txtboth.setOnClickListener(this);

		preferences = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = preferences.getString(AppConstant.KEY_USER_ID, "");

		internetcheck = new ConnectionDetector(getActivity());
		isInternetPresent = internetcheck.isConnectingToInternet();

		options = setupImageLoader();

		if (getArguments() != null) {
			Bundle data = this.getArguments();
			memberid = data.getString("memberID");

		}

		if (isInternetPresent) {
			new AsyncMemberDetailTask().execute(UserId, memberid);

		} else {

		}
		return rootView;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.id_btn_back:
			getActivity().onBackPressed();
			break;

		case R.id.id_button_home:
			getActivity().finish();
			Intent intent = new Intent(getActivity(), MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			break;
		case R.id.id_audio:

			audio.setVisibility(View.VISIBLE);
			forboth.setVisibility(View.INVISIBLE);
			break;
		case R.id.id_audio_text_both:

			audio.setVisibility(View.INVISIBLE);
			forboth.setVisibility(View.VISIBLE);
			break;
		case R.id.delete_imemeber:
			if (isInternetPresent) {
				new AsyncDeleteMemberDetailTask().execute(UserId, "memberid");

			} else {

			}
			break;

		}
	}

	public class AsyncDeleteMemberDetailTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				JSONObject jObj = new JSONObject(result);

				if (jObj.getBoolean("success")) {
					Log.e("Response", result);
					getActivity().onBackPressed();
					ViewUtil.hideProgressDialog();

				} else {
					Log.e("Response", result);

					JSONObject phone = jObj.getJSONObject("message");
					String Server_message = phone.getString("Account");
					ViewUtil.hideProgressDialog();
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							Server_message, true);

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("MemberId", String
					.valueOf(params[1])));

			String result = NetworkConnector.Deletemember(parameter);
			return result;
		}

	}

	public class AsyncMemberDetailTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.e("Response", result);
			try {

				if (isInternetPresent) {
					ViewUtil.hideProgressDialog();
					if (result.contains("true")) {
						updateUI(result);
					} else {
						ViewUtil.showAlertDialog(getActivity(), "",
								"Members not found", false);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("MemberId", String
					.valueOf(params[1])));
			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.Selectedmember(parameter);
			return result;
		}

	}

	/**
	 * @param result
	 * @return
	 */
	public void updateUI(String result) {
		// TODO Auto-generated method stub
		try {
			JSONObject jsonObject = new JSONObject(result);
			Log.e("jsonObject", "" + jsonObject);

			JSONObject jsonInnerObject = jsonObject.getJSONObject("message");
			String added = jsonInnerObject.getString("Added");
			String viewed = jsonInnerObject.getString("Viewed");
			String address = jsonInnerObject.getString("Address");
			String profilePic = jsonInnerObject.getString("ProfileImage");
			String name = jsonInnerObject.getString("Name");

			if (added != null) {

				txt_Date.setText(added);

			}
			if (viewed != null) {

			}
			if (address != null) {

				txt_Address.setText(address);
			}

			if (name != null) {

				txtUserName.setText(name);
			}

			if (profilePic != null) {

				ImageLoader.getInstance().displayImage(profilePic, imgUser,
						options);
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private DisplayImageOptions setupImageLoader() {
		return new DisplayImageOptions.Builder().cacheInMemory(true)
				.showImageOnLoading(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.no_image_icon)
				.showImageOnFail(R.drawable.no_image_icon).cacheOnDisk(true)
				.considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1)).build();

	}
}
