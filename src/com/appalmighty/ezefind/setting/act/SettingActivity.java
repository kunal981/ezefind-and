package com.appalmighty.ezefind.setting.act;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.newentry.act.PickGoActivity;
import com.appalmighty.ezefind.setting.fgmt.SettingFragmentAccount;
import com.appalmighty.ezefind.setting.fgmt.SettingFragmentAllMember;
import com.appalmighty.ezefind.setting.fgmt.SettingFragmentEntry;
import com.appalmighty.ezefind.setting.fgmt.SettingFragmentNetwork;
import com.appalmighty.ezefind.setting.fgmt.SettingFragmentPassword;
import com.appalmighty.ezefind.setting.fgmt.SettingFragmentProfile;
import com.appalmighty.ezefind.setting.fgmt.SettingFragmentSaveEntry;
import com.appalmighty.ezefind.util.AppConstant;

public class SettingActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_activity_setting);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, SettingFragment.newInstance())
					.commit();
		}

	}

	public static class SettingFragment extends Fragment implements
			OnClickListener {

		RelativeLayout imageButtonProfile, imageButtonAcc, imageButtonEntry,
				imageButtonNetwork, imageButtonSaveSetting,
				imagebuttonPassword, imageButtonmemeber, ImageButtonpicklist;
		Button btnhome;
		TextView network, entrytype;
		Boolean isInternetPresent = false;
		ConnectionDetector internetcheck;
		SharedPreferences sharedpreferences;
		String typingvalue;

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_layout_setting_main, container, false);
			imageButtonProfile = (RelativeLayout) rootView
					.findViewById(R.id.rl_profile);
			imageButtonAcc = (RelativeLayout) rootView
					.findViewById(R.id.rl_account);
			imageButtonEntry = (RelativeLayout) rootView
					.findViewById(R.id.rl_entrysetting);
			imageButtonNetwork = (RelativeLayout) rootView
					.findViewById(R.id.rl_network);
			imageButtonSaveSetting = (RelativeLayout) rootView
					.findViewById(R.id.rl_setting);
			imagebuttonPassword = (RelativeLayout) rootView
					.findViewById(R.id.rl_password);
			imageButtonmemeber = (RelativeLayout) rootView
					.findViewById(R.id.rl_member);
			ImageButtonpicklist = (RelativeLayout) rootView
					.findViewById(R.id.rl_picklist);
			btnhome = (Button) rootView.findViewById(R.id.id_button_home);
			network = (TextView) rootView
					.findViewById(R.id.id_network_selected);
			entrytype = (TextView) rootView
					.findViewById(R.id.id_entry_selected);
			sharedpreferences = getActivity().getSharedPreferences(
					AppConstant.KEY_APP, Context.MODE_PRIVATE);
			typingvalue = sharedpreferences.getString(
					AppConstant.KEY_ENTRYTYPE, "");

			imageButtonProfile.setOnClickListener(this);
			imageButtonAcc.setOnClickListener(this);
			imageButtonEntry.setOnClickListener(this);
			imageButtonNetwork.setOnClickListener(this);
			imageButtonSaveSetting.setOnClickListener(this);
			imagebuttonPassword.setOnClickListener(this);
			imageButtonmemeber.setOnClickListener(this);
			ImageButtonpicklist.setOnClickListener(this);
			btnhome.setOnClickListener(this);
			internetcheck = new ConnectionDetector(getActivity());
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {
				network.setText("CONNECTED");
			} else {
				network.setText("NOT CONNECTED");
			}
			Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("0")) {
				entrytype.setText("TEXT");
			} else if (typingvalue.equals("1")) {
				entrytype.setText("AUDIO ONLY");
			} else {
				entrytype.setText("TEXT AND AUDIO");
			}
			return rootView;
		}

		public static Fragment newInstance() {
			SettingFragment fragment = new SettingFragment();
			return fragment;
		}

		@Override
		public void onClick(View v) {
			int id = v.getId();
			Fragment fragment = null;
			switch (id) {
			case R.id.rl_profile:
				fragment = SettingFragmentProfile.newInstance();
				switchToFragment(fragment);
				break;
			case R.id.rl_account:
				fragment = SettingFragmentAccount.newInstance();
				switchToFragment(fragment);
				break;
			case R.id.rl_entrysetting:
				fragment = SettingFragmentEntry.newInstance();
				switchToFragment(fragment);
				break;
			case R.id.rl_network:
				fragment = SettingFragmentNetwork.newInstance();
				switchToFragment(fragment);
				break;
			case R.id.rl_setting:
				fragment = SettingFragmentSaveEntry.newInstance();
				switchToFragment(fragment);
				break;
			case R.id.rl_member:
				fragment = SettingFragmentAllMember.newInstance();
				switchToFragment(fragment);
				break;
			case R.id.rl_picklist:
				Intent intent1 = new Intent(getActivity(), PickGoActivity.class);
				intent1.putExtra("Pack_go", "pick_list");
				startActivity(intent1);
				getActivity().overridePendingTransition(R.anim.fade_in,
						R.anim.fade_out);
				getActivity().finish();
				break;
			case R.id.rl_password:
				fragment = SettingFragmentPassword.newInstance();
				switchToFragment(fragment);
				break;
			case R.id.id_button_home:
				getActivity().finish();
				Intent intent = new Intent(getActivity(), MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				break;

			default:
				break;
			}

		}

		private void switchToFragment(Fragment fragment) {

			FragmentManager fm = getFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			ft.setCustomAnimations(R.anim.slide_in_right,
					R.anim.slide_out_left, R.anim.slide_in_left,
					R.anim.slide_out_right);
			ft.replace(R.id.container, fragment);
			ft.addToBackStack(null);
			ft.commit();

		}
	}

}
