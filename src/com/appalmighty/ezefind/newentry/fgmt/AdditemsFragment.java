package com.appalmighty.ezefind.newentry.fgmt;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.DataBase.DatasourceHandler;
import com.appalmighty.ezefind.DataBase.NewEntryDataBase;
import com.appalmighty.ezefind.login.act.LoginActivity;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.net.NetworkConstant.Api;
import com.appalmighty.ezefind.net.NetworkConstant.Base;
import com.appalmighty.ezefind.newentry.act.PostAPi;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.appalmighty.ezefind.util.Helper.ToastUi;
import com.appalmighty.ezefind.view.ChoosePopup;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class AdditemsFragment extends Fragment implements OnClickListener {

	public static final int REQUEST_AUDIO_TO_TEXT_TITLE = 301;
	public static final int REQUEST_AUDIO_TO_TEXT_DESC = 303;

	public static final int REQUEST_TAKE_PHOTO = 100;
	public static final int REQUEST_TAKE_PHOTO_ATTACHMENT = 101;
	public static final int REQUEST_TAKE_GALLERY = 200;
	public static final int RESULT_OK = -1;
	public static final int REQUEST_AUDIO_TO_TEXT_VALUE = 304;
	MediaRecorder myAudioRecorder = null;
	MediaPlayer myMediaPlayer = null;
	ListView listview_items;
	ListAdapter adapter1;
	Button btn_add, backButton, btnhome, btnLogout;
	RelativeLayout relative_addItems;
	ImageView img_save, img_edit, img_cancel;
	private LinearLayout layout_addItem_box_audio;
	private LinearLayout layout_addItem_box_text;
	private int editItemMode = 0; // 0 for text by default 1 for audio
	private TextView textAudioTitle, textAudioDesc, textAudioValue, textdate,
			textadd, txtattachment, txtquantity;
	private EditText textTitle, textDesc, textValue;
	private ImageView imageButtonAttachement, imageButtonQuantity, imageshirt;
	String quanti = "1";

	public static int countQuantity = 1;
	String outputFile = null;
	private File destination = null;
	private File destination1 = null;
	String AUDIO_RECORDER_FOLDER = "eze-find";
	ArrayList<String> Allfile;
	ArrayList<String> Allfile1;
	ArrayList<String> Title;
	ArrayList<String> Itemid;
	ArrayList<String> valueArraylist;
	ArrayList<String> Description;
	ArrayList<String> Image;
	ArrayList<String> ImageType;
	ArrayList<Integer> Attachments;
	ArrayList<String> Quntities;
	String UserId, packageid, packagedataid;
	SharedPreferences shared;
	File file1, file2, file3, file4, file5;
	String imagemean = null, title, description, itemvalue;
	Button quantity;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	DatasourceHandler DATABASE;
	int clickedvalue;
	// DisplayImageOptions displayImageOptions;
	Cursor c;
	String typingvalue;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.activity_add_items,
				container, false);
		listview_items = (ListView) rootView
				.findViewById(R.id.listview_add_items);
		Intent intent = getActivity().getIntent();

		txtattachment = (TextView) rootView.findViewById(R.id.attachment);
		txtquantity = (TextView) rootView.findViewById(R.id.quantity);

		textdate = (TextView) rootView.findViewById(R.id.txt_date);
		textadd = (TextView) rootView.findViewById(R.id.txtaddress);
		btn_add = (Button) rootView.findViewById(R.id.btn_addMore);
		img_edit = (ImageView) rootView.findViewById(R.id.img_edit);
		img_save = (ImageView) rootView.findViewById(R.id.img_btn_save);
		img_cancel = (ImageView) rootView.findViewById(R.id.img_btn_cancel);
		backButton = (Button) rootView.findViewById(R.id.back_button);
		textTitle = (EditText) rootView.findViewById(R.id.edt_shirts_name);
		textDesc = (EditText) rootView.findViewById(R.id.edt_desc);
		textValue = (EditText) rootView.findViewById(R.id.edt_value_name);
		btnLogout = (Button) rootView.findViewById(R.id.btn_logout);
		relative_addItems = (RelativeLayout) rootView
				.findViewById(R.id.relative_layout);
		// layout_addItem_box_audio = (LinearLayout) rootView
		// .findViewById(R.id.linear_layout_mode_audio);
		layout_addItem_box_text = (LinearLayout) rootView
				.findViewById(R.id.linear_layout_mode_text);
		imageButtonAttachement = (ImageView) rootView
				.findViewById(R.id.imageButtonAttachment);
		imageButtonQuantity = (ImageView) rootView
				.findViewById(R.id.imageButtonQuantity);
		imageshirt = (ImageView) rootView.findViewById(R.id.img_shirts);
		btn_add.setOnClickListener(this);
		img_edit.setOnClickListener(this);
		img_save.setOnClickListener(this);
		img_cancel.setOnClickListener(this);
		textTitle.setOnClickListener(this);
		textDesc.setOnClickListener(this);
		textValue.setOnClickListener(this);
		// textAudioTitle.setOnClickListener(this);
		// textAudioValue.setOnClickListener(this);
		// textAudioDesc.setOnClickListener(this);
		imageButtonQuantity.setOnClickListener(this);
		imageButtonAttachement.setOnClickListener(this);
		btnLogout.setOnClickListener(this);
		imageshirt.setOnClickListener(this);
		Allfile = new ArrayList<String>();
		Allfile1 = new ArrayList<String>();
		Title = new ArrayList<String>();
		Itemid = new ArrayList<String>();
		Description = new ArrayList<String>();
		Image = new ArrayList<String>();
		valueArraylist = new ArrayList<String>();
		ImageType = new ArrayList<String>();
		Attachments = new ArrayList<Integer>();
		Quntities = new ArrayList<String>();
		// openAudioModeEditbox();
		layout_addItem_box_text.setVisibility(View.VISIBLE);
		DATABASE = new DatasourceHandler(getActivity());
		internetcheck = new ConnectionDetector(getActivity());
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = shared.getString(AppConstant.KEY_USER_ID, "");

		textadd.setText(shared.getString(AppConstant.KEY_Address, ""));

		packageid = shared.getString(AppConstant.KEY_PackageId, "");
		packagedataid = shared.getString(AppConstant.KEY_PackageDataId, "");

		textdate.setText(shared.getString(AppConstant.KEY_Date, ""));
		btnhome = (Button) rootView.findViewById(R.id.id_button_home_);

		isInternetPresent = internetcheck.isConnectingToInternet();

		adapter1 = new ListAdapter(getActivity(), Title, Description, Image,
				Attachments, Quntities, ImageType, Itemid, valueArraylist);
		listview_items.setAdapter(adapter1);
		if (isInternetPresent) {
			new AsyncGetitemTask().execute(packageid, packagedataid, UserId);

		} else {
			c = DATABASE.getItem(packageid, packagedataid, UserId);

			if (c.getCount() != 0) {

				while (c.moveToNext()) {
					Itemid.add(c.getString(c
							.getColumnIndex(NewEntryDataBase.KEY_ITEMID)));
					Title.add(c.getString(4));
					Description.add(c.getString(5));
					Quntities.add(c.getString(6));
					Attachments.add(Integer.parseInt(c.getString(7)));
					Log.e("oncreate", "" + c.getString(7));
					Image.add(c.getString(8));
					ImageType.add(c.getString(9));

				}
				adapter1.notifyDataSetChanged();

			} else {
				Toast.makeText(getActivity(), "No Data Found", 2000).show();
			}
		}
		btnhome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().finish();
				Intent intent = new Intent(getActivity(), MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);

			}
		});
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		btnLogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				// set title
				alertDialogBuilder.setTitle("LOGOUT");

				// set dialog message
				alertDialogBuilder
						.setMessage("Are you sure for logout")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										SharedPreferences settings = getActivity()
												.getSharedPreferences(
														AppConstant.KEY_APP,
														Context.MODE_PRIVATE);
										settings.edit().clear().commit();
										getActivity().finish();
										Intent intent_home = new Intent(
												getActivity(),
												LoginActivity.class);
										startActivity(intent_home);

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		});
		textTitle.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				Log.e("focus", "focus:" + hasFocus);
				if (hasFocus) {
					typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE,
							"");
					// Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						textTitle.setInputType(InputType.TYPE_NULL);
						getActivity()
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_TITLE);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		textDesc.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				// Log.e("focus", "focus:"+hasFocus);
				if (hasFocus) {
					typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE,
							"");
					// Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						textDesc.setInputType(InputType.TYPE_NULL);
						getActivity()
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_DESC);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		textValue.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				// Log.e("focus", "focus:"+hasFocus);
				if (hasFocus) {
					typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE,
							"");
					// Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						textValue.setInputType(InputType.TYPE_NULL);
						getActivity()
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_VALUE);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		return rootView;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	class ListAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;
		ArrayList<String> adtitle = new ArrayList<String>();
		ArrayList<String> aditemid = new ArrayList<String>();
		ArrayList<String> addescription = new ArrayList<String>();
		ArrayList<String> adimage = new ArrayList<String>();
		ArrayList<Integer> adattachments = new ArrayList<Integer>();
		ArrayList<String> adquntities = new ArrayList<String>();
		ArrayList<String> adimageType = new ArrayList<String>();
		ArrayList<String> value_ = new ArrayList<String>();

		DisplayImageOptions displayImageOptions;

		public ListAdapter(Activity applicationContext,
				ArrayList<String> title, ArrayList<String> description,
				ArrayList<String> image, ArrayList<Integer> attachments,
				ArrayList<String> quntities, ArrayList<String> imageType,
				ArrayList<String> itemid, ArrayList<String> valueArraylist) {
			// TODO Auto-generated constructor stub
			this.activity = applicationContext;
			adtitle = title;
			addescription = description;
			adimage = image;
			adattachments = attachments;
			adquntities = quntities;
			adimageType = imageType;
			aditemid = itemid;
			value_ = valueArraylist;
			displayImageOptions = setupImageLoader();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub

			return aditemid.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return aditemid.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder holder;
			BitmapFactory.Options options = null;
			File image = null;
			holder = new ViewHolder();
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.fragment_on_open_listitems, null);

				holder.itemimage = (ImageView) convertView
						.findViewById(R.id.img_shirts);
				holder.itemtitle = (TextView) convertView
						.findViewById(R.id.txt_shirts_name);
				holder.itemdesc = (TextView) convertView
						.findViewById(R.id.txt_shirts_description);
				holder.totlattachment = (TextView) convertView
						.findViewById(R.id.Totalattachment);
				holder.totalquantity = (TextView) convertView
						.findViewById(R.id.ToatalQuantity);
				holder.txtValue = (TextView) convertView
						.findViewById(R.id.txt_value);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			try {

				holder.itemtitle.setText(adtitle.get(position));
				holder.itemdesc.setText(addescription.get(position));
				holder.totlattachment.setText(String.valueOf(adattachments
						.get(position)));
				holder.totalquantity.setText(String.valueOf(adquntities
						.get(position)));

				holder.txtValue.setText("$" + " " + value_.get(position));
				Log.e("adtitle.get(position)", "" + adtitle.get(position));
				if (adimageType.get(position).equals("64")) {

					image = new File(adimage.get(position));
					Log.e("image", "" + image);
					options = new BitmapFactory.Options();
					options.inSampleSize = 8;

					Bitmap bitmap = BitmapFactory.decodeFile(
							image.getAbsolutePath(), options);
					holder.itemimage.setImageBitmap(bitmap);

				} else {

					ImageLoader.getInstance().displayImage(
							String.valueOf(adimage.get(position)),
							holder.itemimage);
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return convertView;

		}

		class ViewHolder {
			ImageView itemimage;
			TextView itemtitle, itemdesc, totlattachment, totalquantity,
					txtValue;
		}

		public Bitmap ConvertToImage(String image) {
			try {
				byte[] imageAsBytes = Base64.decode(image.getBytes(),
						Base64.DEFAULT);

				Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
						imageAsBytes.length);

				return bitmap;
			} catch (Exception e) {
				return null;
			}
		}

		public DisplayImageOptions setupImageLoader() {
			return new DisplayImageOptions.Builder().cacheInMemory(true)
					.cacheOnDisk(true).considerExifParams(true)
					.displayer(new RoundedBitmapDisplayer(1)).build();

		}
	}

	protected void openTypeModePopWindow() {
		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_option_text_mode, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

		ImageView btnDismiss = (ImageView) popupView
				.findViewById(R.id.btn_close);
		btnDismiss.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				popupWindow.dismiss();
			}
		});
		Button audioMode = (Button) popupView.findViewById(R.id.btn_mode_audio);
		audioMode.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				editItemMode = 1;
				// openAudioModeEditbox();
				popupWindow.dismiss();
			}
		});
		Button textmode = (Button) popupView.findViewById(R.id.btn_mode_text);
		textmode.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				editItemMode = 0;
				openTextModeEditbox();
				popupWindow.dismiss();
			}
		});

		popupWindow.showAtLocation(layout_addItem_box_audio,
				Gravity.CENTER_HORIZONTAL, 0, 0);

	}

	// protected void openAudioModeEditbox() {
	// // layout_addItem_box_audio.setVisibility(View.VISIBLE);
	// layout_addItem_box_text.setVisibility(View.GONE);
	// }

	protected void openTextModeEditbox() {
		// layout_addItem_box_audio.setVisibility(View.GONE);
		layout_addItem_box_text.setVisibility(View.VISIBLE);
	}

	private void publishQuantityPopupWindow() {
		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(R.layout.popup_layout_quantity,
				null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

		Button incrementOp = (Button) popupView
				.findViewById(R.id.btn_increment);
		quantity = (Button) popupView.findViewById(R.id.btn_quantity);
		Button decrementOp = (Button) popupView
				.findViewById(R.id.btn_decrement);

		countQuantity = 1;
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		popupWindow.setTouchInterceptor(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub

				if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					txtquantity.setText(String.valueOf(countQuantity));
					popupWindow.dismiss();
					return true;
				}
				return false;
			}
		});
		incrementOp.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				countQuantity = countQuantity + 1;
				quantity.setText(String.valueOf(countQuantity));
				quanti = String.valueOf(countQuantity);
				txtquantity.setText(String.valueOf(countQuantity));
			}
		});
		decrementOp.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (countQuantity != 1)
					countQuantity = countQuantity - 1;
				txtquantity.setText(String.valueOf(countQuantity));
				quantity.setText(String.valueOf(countQuantity));
				quanti = String.valueOf(countQuantity);
			}
		});
		quantity.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();

			}
		});
		popupWindow.showAtLocation(layout_addItem_box_text, Gravity.CENTER, 0,
				0);

		// popupWindow.showAsDropDown(imageButtonAttachement, 0, 0);

	}

	private void openItemAttachMenu() {
		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_attachement, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, true);

		Button optionphotoVideo = (Button) popupView
				.findViewById(R.id.btn_photo_video);
		final Button optionphotoAudio = (Button) popupView
				.findViewById(R.id.btn_audio);
		Button optionBarcode = (Button) popupView
				.findViewById(R.id.btn_barcode);
		ImageView btnClose = (ImageView) popupView.findViewById(R.id.btn_close);

		optionphotoVideo.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				openCameraOptionDailog();
				popupWindow.dismiss();

			}
		});
		optionphotoAudio.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				openAudioOptionDailog();
				popupWindow.dismiss();

			}
		});
		optionBarcode.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				ToastUi.print(getActivity(), "Coming soon");

			}
		});
		btnClose.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();

			}
		});
		popupWindow.showAtLocation(layout_addItem_box_text, Gravity.CENTER, 0,
				0);

		// popupWindow.showAsDropDown(imageButtonAttachement, 0, 0);
	}

	private void openAudioOptionDailog() {

		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_audio_recording, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, true);
		ImageView btnClose = (ImageView) popupView.findViewById(R.id.btn_close);
		ImageView btnSave = (ImageView) popupView
				.findViewById(R.id.id_btn_save);
		ImageView btnDel = (ImageView) popupView.findViewById(R.id.id_btn_del);
		final Chronometer focus = (Chronometer) popupView
				.findViewById(R.id.chronometer1);

		final TextView duration = (TextView) popupView
				.findViewById(R.id.duration);
		TextView txtDate = (TextView) popupView
				.findViewById(R.id.text_current_date);
		final ImageView btn_play_pause = (ImageView) popupView
				.findViewById(R.id.id_btn_record);
		final ImageView btn_play_record = (ImageView) popupView
				.findViewById(R.id.imageView1);

		final Handler seekHandler = new Handler();

		final SeekBar seekBar = (SeekBar) popupView.findViewById(R.id.seek_bar);
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate2 = df2.format(c.getTime());

		txtDate.setText(formattedDate2);

		btn_play_pause.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!btn_play_pause.isSelected()) {
					// to start audio recording

					initMediaRecorder();
					start(v);

				} else {
					// to stop audio recording
					stop(v);
				}

			}

			private void stop(View v) {
				myAudioRecorder.stop();
				myAudioRecorder.release();
				focus.stop();

				myAudioRecorder = null;
				btn_play_pause.setSelected(false);
				Toast.makeText(getActivity(), "Audio recorded successfully",
						Toast.LENGTH_LONG).show();
			}

			private void start(View v) {
				try {
					myAudioRecorder.prepare();
					myAudioRecorder.start();
					btn_play_pause.setSelected(true);
					Toast.makeText(getActivity(), "Recording started",
							Toast.LENGTH_LONG).show();
					focus.start();
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					btn_play_pause.setSelected(false);
					Toast.makeText(getActivity(), "Recording falied",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					btn_play_pause.setSelected(false);
					Toast.makeText(getActivity(), "Recording falied",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}

			}

			private void initMediaRecorder() {
				myAudioRecorder = new MediaRecorder();
				myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
				myAudioRecorder
						.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
				myAudioRecorder
						.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
				outputFile = getFilename();
				myAudioRecorder.setOutputFile(outputFile);
			}

			private String getFilename() {
				String filepath = Environment.getExternalStorageDirectory()
						.getPath();
				File file = new File(filepath, AUDIO_RECORDER_FOLDER);
				if (!file.exists()) {
					file.mkdirs();
				}
				String path = file.getAbsolutePath() + "/"
						+ System.currentTimeMillis() + ".3gp";

				Log.e("audioo", "" + path);
				Allfile.add(path);
				txtattachment.setText("" + Allfile.size());
				return path;
			}
		});

		btn_play_record.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!btn_play_record.isSelected()) {

					startPlaying();

				} else {
					stopPlayingRecord();

				}

			}

			private void stopPlayingRecord() {
				myMediaPlayer.stop();
				myMediaPlayer.release();
				myMediaPlayer = null;
				btn_play_record.setSelected(false);
			}

			private void startPlaying() {
				// TODO Auto-generated method stub

				if (outputFile != null && !outputFile.equals("")) {
					if (myMediaPlayer == null) {
						myMediaPlayer = new MediaPlayer();
					}

					try {
						myMediaPlayer.setDataSource(outputFile);// Write your
						// location

						myMediaPlayer.prepare();
						myMediaPlayer.start();

						seekBar.setMax(myMediaPlayer.getDuration());
						int mediaDuration = myMediaPlayer.getDuration();
						int mediaPosition = myMediaPlayer.getCurrentPosition();
						double timeRemaining = mediaDuration - mediaPosition;
						duration.setText(String.format(
								"%d min, %d sec",
								TimeUnit.MILLISECONDS
										.toMinutes((long) timeRemaining),
								TimeUnit.MILLISECONDS
										.toSeconds((long) timeRemaining)
										- TimeUnit.MINUTES
												.toSeconds(TimeUnit.MILLISECONDS
														.toMinutes((long) timeRemaining))));

						btn_play_record.setSelected(true);
						seekUpdation();
					} catch (Exception e) {
						e.printStackTrace();
						Toast.makeText(getActivity(), "File not exist",
								Toast.LENGTH_SHORT).show();
					}

				} else {
					Toast.makeText(getActivity(), "File not exist",
							Toast.LENGTH_SHORT).show();
				}

			}

			public void seekUpdation() {
				if (myMediaPlayer != null) {
					seekBar.setProgress(myMediaPlayer.getCurrentPosition());
					seekHandler.postDelayed(run, 1000);
				} else {
					seekBar.setProgress(0);
				}
			}

			Runnable run = new Runnable() {
				@Override
				public void run() {
					seekUpdation();
				}
			};

		});

		btnClose.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (myAudioRecorder != null) {
					myAudioRecorder.stop();
					myAudioRecorder.release();
					focus.stop();
					myAudioRecorder = null;
				}
				if (myMediaPlayer != null) {
					myMediaPlayer.stop();
					myMediaPlayer.release();
					myMediaPlayer = null;
				}
				popupWindow.dismiss();

			}
		});
		btnSave.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();

				Toast.makeText(getActivity(), "Successfully Save the file",
						Toast.LENGTH_SHORT).show();

			}
		});
		btnDel.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				deleteFile();

			}

			private void deleteFile() {
				if (outputFile != null && !outputFile.equals("")) {
					File file = new File(outputFile);
					if (file != null) {
						if (file.exists()) {
							file.delete();
							Allfile.remove(outputFile);
							Toast.makeText(getActivity(),
									"Audio Deleted successfully",
									Toast.LENGTH_SHORT).show();
						} else {
							Toast.makeText(getActivity(), "File not exit ",
									Toast.LENGTH_SHORT).show();
						}
					}

				} else {
					Toast.makeText(getActivity(), "File not exit ",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		popupWindow.showAtLocation(layout_addItem_box_text, Gravity.CENTER, 0,
				0);
	}

	private void openCameraOptionDailog() {

		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_caputre_selection, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		Button camera = (Button) popupView
				.findViewById(R.id.id_button_take_camera);
		Button gallery = (Button) popupView.findViewById(R.id.id_button_save);
		camera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(intent, 101);
				popupWindow.dismiss();

			}
		});
		gallery.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				intent.setType("image/*");
				startActivityForResult(
						Intent.createChooser(intent, "Select File"), 200);
				popupWindow.dismiss();

			}
		});
		ImageView btnClose = (ImageView) popupView.findViewById(R.id.btn_close);
		btnClose.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();

			}
		});
		popupWindow.showAtLocation(layout_addItem_box_text, Gravity.CENTER, 0,
				0);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int ID = v.getId();
		clickedvalue = v.getId();
		switch (ID) {

		case R.id.edt_shirts_name:

			// TODO Auto-generated method stub
			typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE, "");
			Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				textTitle.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					textTitle.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_VALUE);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				textTitle.setInputType(InputType.TYPE_CLASS_TEXT);
			}

			break;
		case R.id.edt_desc:

			// TODO Auto-generated method stub
			typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE, "");
			// Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				textDesc.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					textDesc.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_VALUE);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				textDesc.setInputType(InputType.TYPE_CLASS_TEXT);
			}

			break;
		case R.id.edt_value_name:

			// TODO Auto-generated method stub
			typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE, "");
			// Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				textValue.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					textValue.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_VALUE);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				textValue.setInputType(InputType.TYPE_CLASS_TEXT);
			}

			break;
		case R.id.btn_logout:

			// TODO Auto-generated method stub

			AlertDialog.Builder alertDialogBuilder2 = new AlertDialog.Builder(
					getActivity());
			// set title
			alertDialogBuilder2.setTitle("LOGOUT");

			// set dialog message
			alertDialogBuilder2
					.setMessage("Are you sure for logout")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									SharedPreferences settings = getActivity()
											.getSharedPreferences(
													AppConstant.KEY_APP,
													Context.MODE_PRIVATE);
									settings.edit().clear().commit();
									getActivity().finish();
									Intent intent_home = new Intent(
											getActivity(), LoginActivity.class);
									startActivity(intent_home);

								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									dialog.cancel();
								}
							});

			// create alert dialog
			AlertDialog alertDialog2 = alertDialogBuilder2.create();

			// show it
			alertDialog2.show();

			break;
		case R.id.btn_addMore:

			// openTypeModePopWindow();

			// if (layout_addItem_box_audio.getVisibility() == View.VISIBLE) {
			// title = textAudioTitle.getText().toString();
			// description = textAudioDesc.getText().toString();
			// itemvalue = textAudioValue.getText().toString();
			// countQuantity = 1;
			// imageshirt.setImageResource(R.drawable.img_take_photo);
			// } else {
			try {

				title = textTitle.getText().toString();
				description = textDesc.getText().toString();
				itemvalue = textValue.getText().toString().replace("$", "")
						.trim();
				countQuantity = 1;

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// }

			if (title.equals("") && description.equals("")) {

				ViewUtil.showAlertDialog(getActivity(), "Error",
						"Please Enter Title Or Description", true);
			} else if (imagemean != null) {
				isInternetPresent = internetcheck.isConnectingToInternet();

				if (isInternetPresent) {

					Allfile1.clear();
					for (int z = 1; z < Allfile.size(); z++) {
						File mFile = new File(Allfile.get(z));

						Allfile1.add(mFile.getAbsolutePath());

					}
					try {
						btn_add.setVisibility(View.VISIBLE);
						// relative_addItems
						// .setVisibility(View.GONE);

						// textAudioValue.setText("");
						// textAudioTitle.setText("");
						// textAudioDesc.setText("");
					} catch (Exception e) {
						// TODO: handle exception
						Log.e("Crash", "" + e.getMessage());
					}
					new UploadMediaFile().execute();

				} else {
					/*
					 * imageshirt.setImageResource(R.drawable.img_take_photo);
					 * textTitle.setText(""); textDesc.setText("");
					 * textValue.setText("$ "); txtattachment.setText("0");
					 * txtquantity.setText("1");
					 */
					int sixzw = Allfile.size();

					if (title.equals("") && description.equals("")) {

					} else if (imagemean != null) {

						long ITEMID = DATABASE.InsertPackage_Data_items(UserId,
								packageid, packagedataid, title, description,
								String.valueOf(quanti), sixzw,
								String.valueOf(destination), "64", itemvalue);

						Log.e("ALL", "" + Allfile.size());
						Log.e("ITEMID", "" + ITEMID);
						if (Allfile.size() == 0) {

						} else if (Allfile.size() == 1) {
							DATABASE.InsertItemAttachment(
									String.valueOf(ITEMID), Allfile.get(0),
									"0", "0", "0", "0",
									String.valueOf(Allfile.size()));
						} else if (Allfile.size() == 2) {
							DATABASE.InsertItemAttachment(
									String.valueOf(ITEMID), Allfile.get(0),
									Allfile.get(1), "0", "0", "0",
									String.valueOf(Allfile.size()));
						} else if (Allfile.size() == 3) {
							DATABASE.InsertItemAttachment(
									String.valueOf(ITEMID), Allfile.get(0),
									Allfile.get(1), Allfile.get(2), "0", "0",
									String.valueOf(Allfile.size()));
						} else if (Allfile.size() == 4) {
							DATABASE.InsertItemAttachment(
									String.valueOf(ITEMID), Allfile.get(0),
									Allfile.get(1), Allfile.get(2),
									Allfile.get(3), "0",
									String.valueOf(Allfile.size()));
						} else if (Allfile.size() == 5) {
							DATABASE.InsertItemAttachment(
									String.valueOf(ITEMID), Allfile.get(0),
									Allfile.get(1), Allfile.get(2),
									Allfile.get(3), Allfile.get(4),
									String.valueOf(Allfile.size()));
						}
						Itemid.add(String.valueOf(ITEMID));
						ImageType.add("64");
						Image.add(String.valueOf(destination));
						Title.add(title);
						Description.add(description);
						int size = Allfile.size();
						Attachments.add(Allfile.size());
						Log.e("Attachments", "" + Attachments);
						Quntities.add(String.valueOf(quanti));
						adapter1.notifyDataSetChanged();
						btn_add.setVisibility(View.VISIBLE);
						// relative_addItems
						// .setVisibility(View.GONE);

						// textAudioTitle.setText("");
						// textAudioDesc.setText("");
						quanti = "1";

						Allfile.clear();
					} else {
						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
								getActivity());

						// set title
						alertDialogBuilder.setTitle("Provide Image");

						alertDialogBuilder.setIcon(R.drawable.fail);
						// set dialog message
						alertDialogBuilder
								.setMessage("Please Provide Image")
								.setCancelable(false)
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												// if
												// this
												// button
												// is
												// clicked,
												// close
												// current
												// activity
												dialog.cancel();

											}
										});

						AlertDialog alertDialog = alertDialogBuilder.create();

						alertDialog.show();
					}
				}
			} else {
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());

				// set title
				alertDialogBuilder.setTitle("Provide Image");

				alertDialogBuilder.setIcon(R.drawable.fail);
				// set dialog message
				alertDialogBuilder
						.setMessage("Please Provide Image")
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();

									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}

			break;
		case R.id.img_edit:

			EdititemsFragment openContainerFragment = new EdititemsFragment();
			FragmentManager fm = getFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			ft.setCustomAnimations(R.anim.slide_in_right,
					R.anim.slide_out_left, R.anim.slide_in_left,
					R.anim.slide_out_right);
			Bundle bundle = new Bundle();
			bundle.putString("Address", textadd.getText().toString());
			bundle.putString("PackageId", packageid);

			bundle.putString("Packagedata", packagedataid);
			openContainerFragment.setArguments(bundle);
			ft.replace(R.id.container, openContainerFragment);
			ft.addToBackStack(null);
			ft.commit();

			break;
		case R.id.img_btn_cancel:

			// TODO Auto-generated method stub

			isInternetPresent = internetcheck.isConnectingToInternet();
			AlertDialog.Builder alertDialogBuilder1 = new AlertDialog.Builder(
					getActivity());

			// set title
			alertDialogBuilder1.setTitle("Cancel");

			// set dialog message
			alertDialogBuilder1
					.setMessage("Are you sure want to Cancel")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									// if (isInternetPresent) {
									getActivity().finish();
									Intent intent = new Intent(getActivity(),
											MainActivity.class);
									intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
											| Intent.FLAG_ACTIVITY_SINGLE_TOP
											| Intent.FLAG_ACTIVITY_NEW_TASK);
									startActivity(intent);
									// new AsyncDeletePackageDataTask()
									// .execute(packageid, UserId);
									//
									// } else {
									//
									// long Package = DATABASE.DeletePackage(
									// packageid, UserId);
									//
									// long PackageData = DATABASE
									// .DeletePackagedata(packageid,
									// UserId);
									//
									// long PackageDataitem = DATABASE
									// .DeletePackagedataitem(
									// packageid, UserId);
									// ;
									//
									// }
									dialog.cancel();

								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.cancel();
								}
							});

			// create alert dialog
			AlertDialog alertDialog1 = alertDialogBuilder1.create();

			// show it
			alertDialog1.show();

			break;
		case R.id.img_btn_save:

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					getActivity());
			// set title
			alertDialogBuilder.setTitle("ALERT");

			// set dialog message
			alertDialogBuilder
					.setMessage("Are you sure for save")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									title = textTitle.getText().toString();

									description = textDesc.getText().toString();

									itemvalue = textValue.getText().toString()
											.replace("$", "").trim();
									countQuantity = 1;
									imageshirt
											.setImageResource(R.drawable.img_take_photo);

									/*
									 * if (title.equals("") &&
									 * description.equals("")) {
									 * 
									 * ViewUtil.showAlertDialog( getActivity(),
									 * "Error",
									 * "Please Enter Title Or Description",
									 * true); } else
									 */

									if (imagemean != null) {
										isInternetPresent = internetcheck
												.isConnectingToInternet();

										if (isInternetPresent) {
											try {
												Allfile1.clear();
												for (int z = 1; z < Allfile
														.size(); z++) {
													File mFile = new File(
															Allfile.get(z));

													Allfile1.add(mFile
															.getAbsolutePath());
													Log.e("All: ", ""
															+ Allfile1);

												}

												btn_add.setVisibility(View.VISIBLE);
												// relative_addItems
												// .setVisibility(View.GONE);
												/*
												 * textTitle.setText("");
												 * textDesc.setText("");
												 * textValue.setText("$ ");
												 */
												// textAudioTitle.setText("");
												// textAudioDesc.setText("");

												new UploadMediaFile().execute();

											} catch (Exception e) {
												Log.e("crash",
														"" + e.getMessage());
											}
										} else {
											int sixzw = Allfile.size();

											if (title.equals("")
													&& description.equals("")) {

											} else if (imagemean != null) {
												Log.e("wbjvj",
														""
																+ String.valueOf(destination));
												long ITEMID = DATABASE
														.InsertPackage_Data_items(
																UserId,
																packageid,
																packagedataid,
																title,
																description,
																String.valueOf(quanti),
																sixzw,
																String.valueOf(destination),
																"64", itemvalue);

												Log.e("ALL",
														"" + Allfile.size());
												Log.e("ITEMID", "" + ITEMID);
												if (Allfile.size() == 0) {

												} else if (Allfile.size() == 1) {
													DATABASE.InsertItemAttachment(
															String.valueOf(ITEMID),
															Allfile.get(0),
															"0",
															"0",
															"0",
															"0",
															String.valueOf(Allfile
																	.size()));
												} else if (Allfile.size() == 2) {
													DATABASE.InsertItemAttachment(
															String.valueOf(ITEMID),
															Allfile.get(0),
															Allfile.get(1),
															"0",
															"0",
															"0",
															String.valueOf(Allfile
																	.size()));
												} else if (Allfile.size() == 3) {
													DATABASE.InsertItemAttachment(
															String.valueOf(ITEMID),
															Allfile.get(0),
															Allfile.get(1),
															Allfile.get(2),
															"0",
															"0",
															String.valueOf(Allfile
																	.size()));
												} else if (Allfile.size() == 4) {
													DATABASE.InsertItemAttachment(
															String.valueOf(ITEMID),
															Allfile.get(0),
															Allfile.get(1),
															Allfile.get(2),
															Allfile.get(3),
															"0",
															String.valueOf(Allfile
																	.size()));
												} else if (Allfile.size() == 5) {
													DATABASE.InsertItemAttachment(
															String.valueOf(ITEMID),
															Allfile.get(0),
															Allfile.get(1),
															Allfile.get(2),
															Allfile.get(3),
															Allfile.get(4),
															String.valueOf(Allfile
																	.size()));
												}
												Itemid.add(String
														.valueOf(ITEMID));
												ImageType.add("64");
												Image.add(String
														.valueOf(destination));
												Title.add(title);
												Description.add(description);
												int size = Allfile.size();
												Attachments.add(Allfile.size());
												Quntities.add(String
														.valueOf(quanti));
												adapter1.notifyDataSetChanged();
												btn_add.setVisibility(View.VISIBLE);
												// relative_addItems
												// .setVisibility(View.GONE);
												textTitle.setText("");
												textDesc.setText("");
												// textAudioTitle.setText("");
												// textAudioDesc.setText("");
												quanti = "1";

											} else {
												AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
														getActivity());

												// set title
												alertDialogBuilder
														.setTitle("Provide Image");

												alertDialogBuilder
														.setIcon(R.drawable.fail);
												// set dialog message
												alertDialogBuilder
														.setMessage(
																"Please Provide Image")
														.setCancelable(false)
														.setPositiveButton(
																"OK",
																new DialogInterface.OnClickListener() {
																	public void onClick(
																			DialogInterface dialog,
																			int id) {
																		// if
																		// this
																		// button
																		// is
																		// clicked,
																		// close
																		// current
																		// activity
																		dialog.cancel();

																	}
																});

												AlertDialog alertDialog = alertDialogBuilder
														.create();

												alertDialog.show();
											}
										}
									} else {
										AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
												getActivity());

										// set title
										alertDialogBuilder
												.setTitle("Provide Image");

										alertDialogBuilder
												.setIcon(R.drawable.fail);
										// set dialog message
										alertDialogBuilder
												.setMessage(
														"Please Provide Image")
												.setCancelable(false)
												.setPositiveButton(
														"OK",
														new DialogInterface.OnClickListener() {
															public void onClick(
																	DialogInterface dialog,
																	int id) {

																dialog.cancel();

															}
														});

										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder
												.create();

										// show it
										alertDialog.show();
									}

								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									dialog.cancel();
								}
							});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

			break;
		// case R.id.edt_shirts_name_audio:
		// if (isSpeechRecognitionActivityPresented(getActivity()) == true) {
		// // if yes � running recognition
		// startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_TITLE);
		// } else {
		// // if no, then showing notification to install Voice Search
		// Toast.makeText(
		// getActivity(),
		// "In order to activate speech recognition you must install Google Voice Search",
		// Toast.LENGTH_LONG).show();
		// // start installing process
		// installGoogleVoiceSearch(getActivity());
		// }
		// break;
		// case R.id.edt_desc_audio:
		// if (isSpeechRecognitionActivityPresented(getActivity()) == true) {
		// // if yes � running recognition
		// startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_DESC);
		// } else {
		// // if no, then showing notification to install Voice Search
		// Toast.makeText(
		// getActivity(),
		// "In order to activate speech recognition you must install Google Voice Search",
		// Toast.LENGTH_LONG).show();
		// // start installing process
		// installGoogleVoiceSearch(getActivity());
		// }
		// break;
		// case R.id.edt_value_audio:
		// if (isSpeechRecognitionActivityPresented(getActivity()) == true) {
		// // if yes � running recognition
		// startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_VALUE);
		// } else {
		// // if no, then showing notification to install Voice Search
		// Toast.makeText(
		// getActivity(),
		// "In order to activate speech recognition you must install Google Voice Search",
		// Toast.LENGTH_LONG).show();
		// // start installing process
		// installGoogleVoiceSearch(getActivity());
		// }
		// break;
		case R.id.imageButtonQuantity:
			publishQuantityPopupWindow();
			break;
		case R.id.imageButtonAttachment:
			if (Allfile.size() < 5) {
				openItemAttachMenu();
			} else
				Toast.makeText(getActivity(),
						"You Have Already Insert 5 Attachement", 3000).show();
			break;
		case R.id.img_shirts:
			selectImage();
			break;
		default:
			break;
		}

	}

	private void selectImage() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
				ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		destination = new File(Environment.getExternalStorageDirectory(),
				System.currentTimeMillis() + ".jpg");
		Log.e("dest attach", "" + destination);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));

		startActivityForResult(intent, 100);
	}

	protected void startRecognition(Activity mainActivity, int RequestCode) {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
		startActivityForResult(intent, RequestCode);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		/*
		 * if (requestCode == 100) {
		 */
		switch (requestCode) {
		case REQUEST_AUDIO_TO_TEXT_TITLE:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				textTitle.setText(text.get(0));
			}
			break;
		case REQUEST_AUDIO_TO_TEXT_DESC:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				textDesc.setText(text.get(0));
			}
			break;
		case REQUEST_AUDIO_TO_TEXT_VALUE:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				textValue.setText(text.get(0));
			}
			break;

		// public String convert_image(Bitmap bitmap) {
		// ByteArrayOutputStream stream = new ByteArrayOutputStream();
		//
		// bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); // compress
		// to
		//
		// byte[] byte_arr = stream.toByteArray();
		//
		// String ba1 = BaseConvert.encodeBytes(byte_arr);
		// return ba1;
		// }

		case REQUEST_TAKE_PHOTO:
			// && null != data

			int rotate = 0;
			if (resultCode == RESULT_OK && !destination.equals("")) {
				ExifInterface exif;
				try {
					exif = new ExifInterface(destination.getAbsolutePath());

					int orientation = exif.getAttributeInt(
							ExifInterface.TAG_ORIENTATION,
							ExifInterface.ORIENTATION_NORMAL);
					Log.e("orientation", "" + orientation);

					switch (orientation) {
					case ExifInterface.ORIENTATION_ROTATE_270:
						rotate = 270;
						break;
					case ExifInterface.ORIENTATION_ROTATE_180:
						rotate = 180;
						break;
					case ExifInterface.ORIENTATION_ROTATE_90:
						rotate = 90;
						break;
					}
					BitmapFactory.Options options;

					options = new BitmapFactory.Options();
					options.inSampleSize = 8;

					Bitmap bitmap = BitmapFactory.decodeFile(
							destination.getAbsolutePath(), options);
					ByteArrayOutputStream bytes = new ByteArrayOutputStream();
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
					byte[] bytearray = bytes.toByteArray();
					// RotateBitmap(bitmap, 90);

					imageshirt.setImageBitmap(RotateBitmap(bitmap, rotate));

					if (bitmap.equals("")) {

					} else {
						imagemean = "image";
					}
					destination1 = new File(
							Environment.getExternalStorageDirectory(),
							System.currentTimeMillis() + ".jpg");
					ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();
					RotateBitmap(bitmap, rotate).compress(
							Bitmap.CompressFormat.PNG, 100, bytes1);
					byte[] bytearray1 = bytes1.toByteArray();
					FileOutputStream fo;

					Log.e("destination", "" + destination1);
					destination1.createNewFile();

					fo = new FileOutputStream(destination1);
					fo.write(bytes1.toByteArray());
					Log.e("fo", "" + fo);
					fo.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
			deleteRecursive(destination);
			break;
		case REQUEST_TAKE_PHOTO_ATTACHMENT:
			if (resultCode == RESULT_OK && null != data) {
				Bitmap thumbnail1 = (Bitmap) data.getExtras().get("data");
				ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();
				thumbnail1.compress(Bitmap.CompressFormat.JPEG, 100, bytes1);

				File destination1 = new File(
						Environment.getExternalStorageDirectory(),
						System.currentTimeMillis() + ".jpg");

				Allfile.add(destination1.toString());
				txtattachment.setText("" + Allfile.size());
				FileOutputStream fos;
				try {
					destination1.createNewFile();
					fos = new FileOutputStream(destination1);
					fos.write(bytes1.toByteArray());

					fos.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			break;
		case REQUEST_TAKE_GALLERY:
			if (resultCode == RESULT_OK && null != data) {
				Uri selectedImageUri = data.getData();
				String[] projection = { MediaColumns.DATA };
				Cursor cursor = getActivity().managedQuery(selectedImageUri,
						projection, null, null, null);
				int column_index = cursor
						.getColumnIndexOrThrow(MediaColumns.DATA);
				cursor.moveToFirst();

				String selectedImagePath = cursor.getString(column_index);
				Log.e("selectedImagePath", "" + selectedImagePath);
				Allfile.add(selectedImagePath);
				txtattachment.setText("" + Allfile.size());

			}
			break;

		}

	}

	private void deleteRecursive(File destination2) {
		// TODO Auto-generated method stub
		destination2.delete();
	}

	public static Bitmap RotateBitmap(Bitmap source, float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
				source.getHeight(), matrix, true);
	}

	/**
	 * Checks availability of speech recognizing Activity
	 * 
	 * @param callerActivity
	 *            � Activity that called the checking
	 * @return true � if Activity there available, false � if Activity is absent
	 */
	private static boolean isSpeechRecognitionActivityPresented(
			Activity callerActivity) {
		try {
			// getting an instance of package manager
			PackageManager pm = callerActivity.getPackageManager();
			// a list of activities, which can process speech recognition Intent
			List activities = pm.queryIntentActivities(new Intent(
					RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);

			if (activities.size() != 0) { // if list not empty
				return true; // then we can recognize the speech
			}
		} catch (Exception e) {

		}

		return false; // we have no activities to recognize the speech
	}

	/**
	 * Asking the permission for installing Google Voice Search. If permission
	 * granted � sent user to Google Play
	 * 
	 * @param callerActivity
	 *            � Activity, that initialized installing
	 */
	private static void installGoogleVoiceSearch(final Activity ownerActivity) {

		// creating a dialog asking user if he want
		// to install the Voice Search
		Dialog dialog = new AlertDialog.Builder(ownerActivity)
				.setMessage(
						"For recognition it�s necessary to install Google Voice Search") // dialog
																							// message
				.setTitle("Install Voice Search from Google Play?") // dialog
																	// header
				.setPositiveButton("Install",
						new DialogInterface.OnClickListener() { // confirm
							// button

							// Install Button click handler
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								try {
									// creating an Intent for opening
									// applications page in Google Play
									// Voice Search package name:
									// com.google.android.voicesearch
									Intent intent = new Intent(
											Intent.ACTION_VIEW,
											Uri.parse("market://details?id=com.google.android.voicesearch"));
									// setting flags to avoid going in
									// application history (Activity call stack)
									intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY
											| Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
									// sending an Intent
									ownerActivity.startActivity(intent);
								} catch (Exception ex) {
									// if something going wrong
									// doing nothing
								}
							}
						})

				.setNegativeButton("Cancel", null) // cancel button
				.create();

		dialog.show(); // showing dialog
	}

	/**
	 * Upload Image & Video filesss
	 * */
	private class UploadMediaFile extends AsyncTask<String, Integer, String> {
		@Override
		protected void onPreExecute() {
			// setting progress bar to zero

			ViewUtil.showProgressDialog(getActivity());

			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {

			return uploadFile();

		}

		private String uploadFile() {
			String responseString = null;

			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(Base.PACKAGEAPI
					+ Api.PACKAGEDATAITEM);
			// HttpPost httppost = new HttpPost(Base.PACKAGEAPI + Api.TEST);
			try {

				PostAPi entity = new PostAPi(getActivity());
				try {

					if (Allfile1.size() == 0) {

						Allfile1.clear();

						entity.addPart("PackageId", new StringBody(packageid));
						entity.addPart("PackageDataId", new StringBody(
								packagedataid));

						entity.addPart("Quantity",
								new StringBody(String.valueOf(quanti)));
						entity.addPart("Title", new StringBody(title));
						entity.addPart("Description", new StringBody(
								description));
						entity.addPart("Image", new FileBody(destination1));
						entity.addPart("Value", new StringBody(itemvalue));

						entity.addPart("UserId", new StringBody(UserId));
						
					} else if (Allfile1.size() == 1) {
						file1 = new File(Allfile1.get(0));
						Log.e("file1", "" + file1);
						Log.e("Allfile1 size", "" + Allfile1.size());
						Allfile1.clear();
						// Adding file data to http body

						entity.addPart("image1", new FileBody(file1));
						entity.addPart("PackageId", new StringBody(packageid));
						entity.addPart("PackageDataId", new StringBody(
								packagedataid));
						entity.addPart("Quantity",
								new StringBody(String.valueOf(quanti)));
						entity.addPart("Title", new StringBody(title));
						entity.addPart("Description", new StringBody(
								description));
						entity.addPart("Value", new StringBody(itemvalue));
						entity.addPart("Image", new FileBody(destination1));
						entity.addPart("UserId", new StringBody(UserId));
						
						Log.e("Image", "" + destination1);
						Log.e("packageid", "" + packageid);
						Log.e("packagedataid", "" + packagedataid);
						Log.e("quanti", "" + String.valueOf(quanti));
						Log.e("description", "" + description);
						Log.e("UserId", "" + UserId);
						Log.e("Value", "" + itemvalue);
						Log.e("image1", "" + file1);

					} else if (Allfile1.size() == 2) {

						file1 = new File(Allfile1.get(0));
						file2 = new File(Allfile1.get(1));
						Log.e("file1", "" + file1);
						Log.e("file2 ", "" + file2);
						Allfile1.clear();
						// Adding file data to http body

						entity.addPart("image1", new FileBody(file1));

						entity.addPart("image2", new FileBody(file2));
						entity.addPart("PackageId", new StringBody(packageid));
						entity.addPart("PackageDataId", new StringBody(
								packagedataid));
						entity.addPart("Value", new StringBody(itemvalue));
						entity.addPart("Quantity",
								new StringBody(String.valueOf(quanti)));
						entity.addPart("Title", new StringBody(title));
						entity.addPart("Description", new StringBody(
								description));
						entity.addPart("Image", new FileBody(destination1));
						entity.addPart("UserId", new StringBody(UserId));

					} else if (Allfile1.size() == 3) {
						file1 = new File(Allfile1.get(0));
						file2 = new File(Allfile1.get(1));
						file3 = new File(Allfile1.get(2));

						Allfile1.clear();

						entity.addPart("image1", new FileBody(file1));

						entity.addPart("image2", new FileBody(file2));

						entity.addPart("image3", new FileBody(file3));
						entity.addPart("PackageId", new StringBody(packageid));
						entity.addPart("PackageDataId", new StringBody(
								packagedataid));
						entity.addPart("Quantity",
								new StringBody(String.valueOf(quanti)));
						entity.addPart("Value", new StringBody(itemvalue));
						entity.addPart("Title", new StringBody(title));
						entity.addPart("Description", new StringBody(
								description));
						entity.addPart("Image", new FileBody(destination1));
						entity.addPart("UserId", new StringBody(UserId));

					} else if (Allfile1.size() == 4) {
						file1 = new File(Allfile1.get(0));
						file2 = new File(Allfile1.get(1));
						file3 = new File(Allfile1.get(2));
						file4 = new File(Allfile1.get(3));

						Allfile1.clear();

						entity.addPart("image1", new FileBody(file1));

						entity.addPart("image2", new FileBody(file2));

						entity.addPart("image3", new FileBody(file3));

						entity.addPart("image4", new FileBody(file4));
						entity.addPart("PackageId", new StringBody(packageid));
						entity.addPart("PackageDataId", new StringBody(
								packagedataid));
						entity.addPart("Value", new StringBody(itemvalue));
						entity.addPart("Quantity",
								new StringBody(String.valueOf(quanti)));
						entity.addPart("Title", new StringBody(title));
						entity.addPart("Description", new StringBody(
								description));
						entity.addPart("Image", new FileBody(destination1));
						entity.addPart("UserId", new StringBody(UserId));

					} else if (Allfile1.size() == 5) {
						file1 = new File(Allfile1.get(0));
						file2 = new File(Allfile1.get(1));
						file3 = new File(Allfile1.get(2));
						file4 = new File(Allfile1.get(3));
						file5 = new File(Allfile1.get(4));
						Allfile1.clear();
						entity.addPart("image1", new FileBody(file1));

						entity.addPart("image2", new FileBody(file2));

						entity.addPart("image3", new FileBody(file3));

						entity.addPart("image4", new FileBody(file4));

						entity.addPart("image5", new FileBody(file5));
						entity.addPart("Value", new StringBody(itemvalue));
						entity.addPart("PackageId", new StringBody(packageid));
						entity.addPart("PackageDataId", new StringBody(
								packagedataid));
						entity.addPart("Quantity",
								new StringBody(String.valueOf(quanti)));
						entity.addPart("Title", new StringBody(title));
						entity.addPart("Description", new StringBody(
								description));
						entity.addPart("Image", new FileBody(destination1));
						entity.addPart("UserId", new StringBody(UserId));

					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				httppost.setEntity(entity);
				// file.delete();
				// Making server call
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity r_entity = response.getEntity();

				int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode == 200) {
					// Server response
					Log.e("statusCode", "" + statusCode);

					responseString = EntityUtils.toString(r_entity);
				} else {
					Log.e("else", "" + statusCode);
					responseString = "Error occurred! Http Status Code: "
							+ statusCode;
				}

			} catch (ClientProtocolException e) {
				Log.e("Client", "" + e.toString());
				responseString = e.toString();
			} catch (IOException e) {
				Log.e("Io", "" + e.toString());
				responseString = e.toString();
			}

			return responseString;

		}

		@Override
		protected void onPostExecute(String result) {

			super.onPostExecute(result);
			Log.e("", "Response from server: " + result);

			JSONObject jObj;
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response add more", result);
							JSONObject phone = jObj.getJSONObject("message");
							String image = "http://beta.brstdev.com/yiiezefind"
									+ phone.getString("Image");
							/*
							 * DATABASE.InsertPackage_Data_items(UserId,
							 * packageid, packagedataid, title, description,
							 * quantity .getText().toString(), Allfile.size(),
							 * image, "url");
							 */
							imageshirt
									.setImageResource(R.drawable.img_take_photo);
							textTitle.setText("");
							textDesc.setText("");
							textValue.setText("$ ");
							txtattachment.setText("0");
							txtquantity.setText("1");

							Itemid.add(phone.getString("ItemId"));
							imagemean = "";

							ImageType.add("url");
							Image.add(image);
							Title.add(title);

							Description.add(description);
							int size = Allfile.size();
							Attachments.add(size);
							Log.e("quanti", "" + quanti);
							Quntities.add(quanti);
							quanti = "1";
							Allfile.clear();
							ViewUtil.hideProgressDialog();
							Log.e("after", "" + Image.size());
							// adapter1.notifyDataSetChanged();
							if (clickedvalue == R.id.img_btn_save) {
								getActivity().finish();
								Intent intent = new Intent(getActivity(),
										MainActivity.class);
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
										| Intent.FLAG_ACTIVITY_SINGLE_TOP
										| Intent.FLAG_ACTIVITY_NEW_TASK);
								startActivity(intent);
							} else {

							}

						} else {
							ViewUtil.showAlertDialog(getActivity(), "Error",
									"Data Not Uploaded", false);
							// Toast.makeText(getActivity(),
							// "Data Not Uploaded",
							// Toast.LENGTH_LONG).show();
							ViewUtil.hideProgressDialog();
						}

					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public class AsyncGetitemTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response Async", result);

							JSONArray array = jObj.getJSONArray("message");
							for (int i = 0; i < array.length(); i++) {
								JSONObject object = array.getJSONObject(i);
								String item_id = object.getString("ItemId");
								if (item_id != null) {
									Itemid.add(item_id);
								}

								String image = "http://beta.brstdev.com/yiiezefind"
										+ object.getString("Image");
								if (image != null) {
									Image.add(image);
								}

								String quantity = object.getString("Quantity");
								if (quantity != null) {
									Quntities.add(quantity);
								}
								int attachment = object
										.getInt("AttachmentCount");
								Attachments.add(attachment);

								String title_ = object.getString("Title");
								if (title_ != null) {
									Title.add(title_);
								}

								String desc = object.getString("Description");
								if (desc != null) {
									Description.add(desc);

								}

								ImageType.add("url");

								String value = object.getString("Value");
								Log.e("value", "" + value);
								if (value != null) {
									valueArraylist.add(value);

								}

							}
							Log.e("Title", "" + Title);

							Log.e("Description", "" + Description);
							Log.e("Itemid", "" + Itemid);
							Log.e("before", "" + Image.size());

							adapter1.notifyDataSetChanged();

							ViewUtil.hideProgressDialog();
						}

						else {
							Log.e("Response add items", result);

							String Server_message = jObj.getString("message");
							Toast.makeText(getActivity(), Server_message, 2000)
									.show();
							ViewUtil.hideProgressDialog();
							// ViewUtil.showAlertDialog(getActivity(), "Error",
							// Server_message, true);

						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("PackageId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("PackageDataId", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[2])));

			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.GetPackageItem(parameter);
			return result;
		}

	}

	public class AsyncDeletePackageDataTask extends
			AsyncTask<String, Void, String> {
		List<NameValuePair> parameter = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							getActivity().finish();
							Intent intent = new Intent(getActivity(),
									MainActivity.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_SINGLE_TOP
									| Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(intent);
							ViewUtil.hideProgressDialog();

						} else {
							Log.e("Response", result);

							String Server_message = jObj.getString("Account");
							ViewUtil.hideProgressDialog();
							ViewUtil.showAlertDialog(getActivity(),
									"SomeThing Wrong", Server_message, true);

						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("PackageId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[1])));
			String result = NetworkConnector.deletePackage(parameter);
			return result;
		}

	}

}
