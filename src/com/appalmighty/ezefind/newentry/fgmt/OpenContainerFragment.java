package com.appalmighty.ezefind.newentry.fgmt;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.DataBase.DatasourceHandler;
import com.appalmighty.ezefind.login.act.LoginActivity;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class OpenContainerFragment extends Fragment {
	private GridviewAdapter1 adapter;
	private GridView gridView;
	Button backbutton, btnhome, btnLogout;
	ImageView btnDone, imgNewEntry;
	ImageButton imageButton_filter;
	RelativeLayout layout_filter;
	String title;
	TextView textViewTitle, textaddress;
	String UserId, packageid, packagedata;
	SharedPreferences shared;

	ArrayList<String> Title;
	ArrayList<String> Itemid;
	ArrayList<String> Image;
	ArrayList<String> ImageType;
	ArrayList<Integer> Attachments;
	ArrayList<String> Quntities;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	DatasourceHandler DATABASE;
	Cursor itemcount;

	public static OpenContainerFragment newInstance() {
		OpenContainerFragment fragment = new OpenContainerFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_open_container,
				container, false);
		title = getArguments().getString("Title_Open");
		gridView = (GridView) rootView
				.findViewById(R.id.id_gridview_open_container);
		backbutton = (Button) rootView.findViewById(R.id.back_button);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		btnLogout = (Button) rootView.findViewById(R.id.btn_logout);
		imageButton_filter = (ImageButton) rootView
				.findViewById(R.id.img_filter);

		textaddress = (TextView) rootView.findViewById(R.id.txtaddress);
		Itemid = new ArrayList<String>();
		Title = new ArrayList<String>();
		Image = new ArrayList<String>();
		ImageType = new ArrayList<String>();
		Attachments = new ArrayList<Integer>();
		Quntities = new ArrayList<String>();
		layout_filter = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter);
		textViewTitle = (TextView) rootView.findViewById(R.id.txt_title_header);
		textViewTitle.setText(title);
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = shared.getString(AppConstant.KEY_USER_ID, "");
		packageid = getArguments().getString("Packageid");
		packagedata = getArguments().getString("packagedataid");
		textaddress.setText(getArguments().getString("address") + "/"
				+ getArguments().getString("type"));
		internetcheck = new ConnectionDetector(getActivity());
		DATABASE = new DatasourceHandler(getActivity());
		adapter = new GridviewAdapter1(getActivity(),
				OpenContainerFragment.this.Itemid,
				OpenContainerFragment.this.Title,
				OpenContainerFragment.this.Image,
				OpenContainerFragment.this.Attachments,
				OpenContainerFragment.this.Quntities, ImageType);
		gridView.setAdapter(adapter);
		isInternetPresent = internetcheck.isConnectingToInternet();
		if (isInternetPresent) {

			new AsyncGetAllitemTask().execute(UserId, packageid, packagedata);
		} else {

			Cursor c = DATABASE.getAllPackageItem(UserId, packageid,
					packagedata);
			Log.e("", "" + UserId + "---" + packageid + "----" + packagedata);
			if (c.getCount() != 0) {

				while (c.moveToNext()) {

					OpenContainerFragment.this.Itemid.add(c.getString(3));
					OpenContainerFragment.this.Title.add(c.getString(4));
					OpenContainerFragment.this.Image.add(c.getString(8));
					OpenContainerFragment.this.Attachments.add(Integer
							.parseInt(c.getString(7)));
					OpenContainerFragment.this.Quntities.add(c.getString(6));
					ImageType.add("64");
				}

			} else {
				ViewUtil.showAlertDialog(getActivity(), "Error",
						"No Item found", true);
			}
			adapter.notifyDataSetChanged();
		}
		btnLogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				// set title
				alertDialogBuilder.setTitle("LOGOUT");

				// set dialog message
				alertDialogBuilder
						.setMessage("Are you sure for logout")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										SharedPreferences settings = getActivity()
												.getSharedPreferences(
														AppConstant.KEY_APP,
														Context.MODE_PRIVATE);
										settings.edit().clear().commit();
										getActivity().finish();
										Intent intent_home = new Intent(
												getActivity(),
												LoginActivity.class);
										startActivity(intent_home);

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			}
		});
		backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		imageButton_filter.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// layout_filter.setVisibility(View.VISIBLE);
			}
		});

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				// Toast.makeText(getActivity(), "Clicked", Toast.LENGTH_SHORT)
				// .show();
				Log.e("clicked", "Hello");
			}
		});
		btnhome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().finish();
				Intent intent = new Intent(getActivity(), MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		/*
		 * btnDone.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub layout_filter.setVisibility(View.GONE);
		 * 
		 * } }); imgNewEntry.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub layout_filter.setVisibility(View.GONE);
		 * 
		 * } });
		 */
		return rootView;
	}

	class GridviewAdapter1 extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;
		ArrayList<String> adtitle = new ArrayList<String>();
		ArrayList<String> adimage = new ArrayList<String>();
		ArrayList<String> adimagetype = new ArrayList<String>();
		ArrayList<String> aditemid = new ArrayList<String>();
		ArrayList<Integer> adattachments = new ArrayList<Integer>();
		ArrayList<String> adquntities = new ArrayList<String>();
		DisplayImageOptions displayImageOptions;

		public GridviewAdapter1(Activity activity, ArrayList<String> itemid,
				ArrayList<String> title, ArrayList<String> image,
				ArrayList<Integer> attachments, ArrayList<String> quntities,
				ArrayList<String> imageType) {
			this.activity = activity;
			adtitle = title;
			adimage = image;
			aditemid = itemid;
			adattachments = attachments;
			adquntities = quntities;
			adimagetype = imageType;
			displayImageOptions = setupImageLoader();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return adtitle.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return adtitle.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			BitmapFactory.Options options;
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.fragment_open_container_griditems, null);
				ImageView imageView = (ImageView) convertView
						.findViewById(R.id.img_clothes);
				TextView title1 = (TextView) convertView
						.findViewById(R.id.txt_jacket);
				TextView attachment = (TextView) convertView
						.findViewById(R.id.txtattachment);
				TextView quantity = (TextView) convertView
						.findViewById(R.id.txtquantity);
				Log.e("att", "" + adattachments.size());
				attachment.setText(String.valueOf(adattachments.get(position)));
				quantity.setText(adquntities.get(position));
				title1.setText(adtitle.get(position));
				if (adimagetype.get(position).equals("64")) {

					File image = new File(adimage.get(position));
					Log.e("image", "" + image);
					options = new BitmapFactory.Options();
					options.inSampleSize = 8;

					Bitmap bitmap = BitmapFactory.decodeFile(
							image.getAbsolutePath(), options);
					imageView.setImageBitmap(bitmap);

				} else {
					ImageLoader.getInstance().displayImage(
							String.valueOf(adimage.get(position)), imageView);
				}

				imageView.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ContainerDetailsFragment openContainerFragment = new ContainerDetailsFragment();
						FragmentManager fm = getFragmentManager();
						FragmentTransaction ft = fm.beginTransaction();
						ft.setCustomAnimations(R.anim.slide_in_right,
								R.anim.slide_out_left, R.anim.slide_in_left,
								R.anim.slide_out_right);
						Bundle bundle = new Bundle();
						bundle.putString("Title_Open", getArguments()
								.getString("Title_Open"));
						bundle.putString("itemid", aditemid.get(position));

						openContainerFragment.setArguments(bundle);
						ft.replace(R.id.container, openContainerFragment);
						ft.addToBackStack(null);
						ft.commit();

					}
				});

			}
			return convertView;
		}

		public Bitmap ConvertToImage(String image) {
			try {
				byte[] imageAsBytes = Base64.decode(image.getBytes(),
						Base64.DEFAULT);

				Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
						imageAsBytes.length);

				return bitmap;
			} catch (Exception e) {
				return null;
			}
		}

		public DisplayImageOptions setupImageLoader() {
			return new DisplayImageOptions.Builder().cacheInMemory(true)
					.cacheOnDisk(true).considerExifParams(true)
					.displayer(new RoundedBitmapDisplayer(1)).build();

		}
	}

	public class AsyncGetAllitemTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response open container", result);

							JSONArray array = jObj.getJSONArray("message");
							for (int i = 0; i < array.length(); i++) {
								JSONObject object = array.getJSONObject(i);
								String Itemid = object.getString("ItemId");
								String Title = object.getString("Title");

								String Image = "http://beta.brstdev.com/yiiezefind"
										+ object.getString("Image");

								String quantity = object.getString("Quantity");
								int attachment = object
										.getInt("AttachmentCount");

								if (Itemid != null) {
									OpenContainerFragment.this.Itemid
											.add(Itemid);
								}

								if (Title != null) {
									OpenContainerFragment.this.Title.add(Title);
								}

								if (Image != null) {
									OpenContainerFragment.this.Image.add(Image);
								}

								if (quantity != null) {
									OpenContainerFragment.this.Quntities
											.add(quantity);
								}

								OpenContainerFragment.this.Attachments
										.add(attachment);

								OpenContainerFragment.this.ImageType.add("url");
							}

							adapter.notifyDataSetChanged();
							ViewUtil.hideProgressDialog();
						}

						else {
							String Server_message = jObj.getString("message");

							ViewUtil.hideProgressDialog();
							ViewUtil.showAlertDialog(getActivity(), "Error",
									Server_message, true);

						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("PackageId", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("PackageDataId", String
					.valueOf(params[2])));

			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.GetPackageItem(parameter);
			return result;
		}

	}

}
