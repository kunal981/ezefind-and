package com.appalmighty.ezefind.newentry.fgmt;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.DataBase.DatasourceHandler;
import com.appalmighty.ezefind.login.act.LoginActivity;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.appalmighty.ezefind.view.ChoosePopup;
import com.appalmighty.ezefind.viewinventory.fgmt.OnStartPickListFragment;

public class StartPackingFragment extends Fragment {
	private Button btnStart, btnLogout;
	private Button btnhome;
	private TextView txtTitle;
	private EditText title;
	private EditText startDate;
	private EditText startTime;
	private EditText address;
	public static final int RESULT_OK = -1;
	public static final int REQUEST_AUDIO_TO_TEXT_TITLE = 301;
	public static final int REQUEST_AUDIO_TO_TEXT_ADDRESS = 302;
	static final int DATE_DIALOG_ID = 999;

	public static final String TAG = StartPackingFragment.class.getName();
	public static final String KEY = "key";
	String key;
	String UserId, typingvalue, itemid;
	SharedPreferences shared;
	Date oneWayTripDate, oneWayTripTime;
	LinearLayout add;
	long name;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	DatasourceHandler DATABASE;
	private SQLiteDatabase database;

	public static StartPackingFragment newInstance(String key) {
		StartPackingFragment fragment = new StartPackingFragment();
		Bundle bundle = new Bundle();
		bundle.putString(KEY, key);

		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_start_packing,
				container, false);
		txtTitle = (TextView) rootView.findViewById(R.id.txt_title_);
		title = (EditText) rootView.findViewById(R.id.edt_title);
		btnStart = (Button) rootView.findViewById(R.id.btn_start);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		startDate = (EditText) rootView.findViewById(R.id.edt_start_date);
		startTime = (EditText) rootView.findViewById(R.id.edt_start_timer);
		add = (LinearLayout) rootView.findViewById(R.id.r_layoutAddress);
		address = (EditText) rootView.findViewById(R.id.edt_address);
		btnLogout = (Button) rootView.findViewById(R.id.btn_logout);
		intHeaderTitle();
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);

		UserId = shared.getString(AppConstant.KEY_USER_ID, "");
		itemid = shared.getString(AppConstant.KEY_ItemId, "");
		DATABASE = new DatasourceHandler(getActivity());
		internetcheck = new ConnectionDetector(getActivity());
		title.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE, "");
				Log.e("typingvalue", "" + typingvalue);
				if (typingvalue.equals("2")) {
					title.setInputType(InputType.TYPE_NULL);
					ChoosePopup.openTypeModePopWindow(getActivity());
				} else if (typingvalue.equals("1")) {
					if (ChoosePopup
							.isSpeechRecognitionActivityPresented(getActivity()) == true) {
						// if yes � running recognition
						title.setInputType(InputType.TYPE_NULL);
						startRecognition(getActivity(),
								REQUEST_AUDIO_TO_TEXT_TITLE);
					} else {
						// if no, then showing notification to install Voice
						// Search
						Toast.makeText(
								getActivity(),
								"In order to activate speech recognition you must install Google Voice Search",
								Toast.LENGTH_LONG).show();
						// start installing process
						ChoosePopup.installGoogleVoiceSearch(getActivity());
					}
				} else {
					title.setInputType(InputType.TYPE_CLASS_TEXT);
				}
			}
		});
		title.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE,
							"");
					Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						title.setInputType(InputType.TYPE_NULL);

						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_TITLE);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});

		address.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE, "");
				Log.e("typingvalue", "" + typingvalue);
				if (typingvalue.equals("2")) {
					address.setInputType(InputType.TYPE_NULL);
					ChoosePopup.openTypeModePopWindow(getActivity());
				} else if (typingvalue.equals("1")) {
					if (ChoosePopup
							.isSpeechRecognitionActivityPresented(getActivity()) == true) {
						// if yes � running recognition
						address.setInputType(InputType.TYPE_NULL);
						startRecognition(getActivity(),
								REQUEST_AUDIO_TO_TEXT_ADDRESS);
					} else {
						// if no, then showing notification to install Voice
						// Search
						Toast.makeText(
								getActivity(),
								"In order to activate speech recognition you must install Google Voice Search",
								Toast.LENGTH_LONG).show();
						// start installing process
						ChoosePopup.installGoogleVoiceSearch(getActivity());
					}
				} else {
					address.setInputType(InputType.TYPE_CLASS_TEXT);
				}
			}
		});
		address.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE,
							"");
					Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						title.setInputType(InputType.TYPE_NULL);
						getActivity()
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_TITLE);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});

		btnLogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				// set title
				alertDialogBuilder.setTitle("LOGOUT");

				// set dialog message
				alertDialogBuilder
						.setMessage("Are you sure for logout")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										SharedPreferences settings = getActivity()
												.getSharedPreferences(
														AppConstant.KEY_APP,
														Context.MODE_PRIVATE);
										settings.edit().clear().commit();
										getActivity().finish();
										Intent intent_home = new Intent(
												getActivity(),
												LoginActivity.class);
										startActivity(intent_home);

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			}
		});
		btnhome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().finish();
				Intent intent = new Intent(getActivity(), MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		setTimer();

		addClickListener();
		return rootView;
	}

	protected void startRecognition(Activity mainActivity, int RequestCode) {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
		startActivityForResult(intent, RequestCode);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		/*
		 * if (requestCode == 100) {
		 */
		switch (requestCode) {
		case REQUEST_AUDIO_TO_TEXT_TITLE:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				title.setText(text.get(0));
			}
			break;
		case REQUEST_AUDIO_TO_TEXT_ADDRESS:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				address.setText(text.get(0));
			}
			break;
		}
	}

	private void addClickListener() {

		btnStart.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				isInternetPresent = internetcheck.isConnectingToInternet();

				SimpleDateFormat input = new SimpleDateFormat("MM-dd-yyyy");
				SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
				try {
					oneWayTripDate = input
							.parse(startDate.getText().toString()); // parse
					// input

				} catch (ParseException e) {
					e.printStackTrace();
				}

				SimpleDateFormat input1 = new SimpleDateFormat("hh:mm a");
				SimpleDateFormat output1 = new SimpleDateFormat("HH:mm:ss");
				try {
					oneWayTripTime = input1.parse(startTime.getText()
							.toString()); // parse
					// input

				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (isInternetPresent) {

					Log.e("date", "" + output.format(oneWayTripDate));
					Log.e("time", "" + output1.format(oneWayTripTime));
					if (txtTitle.getText().toString().equals("START PACKING")
							&& validateUser()) {
						new AsyncPackageTask().execute(title.getText()
								.toString(), output.format(oneWayTripDate),
								output1.format(oneWayTripTime), address
										.getText().toString(), UserId,
								"Package");
						/*
						 * DATABASE.InsertPackage(title.getText().toString(),
						 * startDate.getText().toString(), startTime
						 * .getText().toString(), address .getText().toString(),
						 * UserId);
						 */
					} else if (txtTitle.getText().toString()
							.equals("START INVENTORY")
							&& validateUser()) {
						new AsyncPackageTask().execute(title.getText()
								.toString(), output.format(oneWayTripDate),
								output1.format(oneWayTripTime), address
										.getText().toString(), UserId,
								"Inventory");
						/*
						 * DATABASE.InsertPackage(title.getText().toString(),
						 * startDate.getText().toString(), startTime
						 * .getText().toString(), address .getText().toString(),
						 * UserId);
						 */
					} else if (txtTitle.getText().toString()
							.equals("START NEW PICK LIST")) {
						if (title.getText().toString().equals("")) {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"Please Enter Picklist Title", false);

						} else {
							new AsyncCreatePickListTask().execute(title
									.getText().toString(), output
									.format(oneWayTripDate), output1
									.format(oneWayTripTime), "Package", UserId,
									itemid);
						}
					}
				} else if (txtTitle.getText().toString()
						.equals("START PACKING")
						&& !isInternetPresent && validateUser()) {

					name = DATABASE.InsertPackage(title.getText().toString(),
							output.format(oneWayTripDate), output1
									.format(oneWayTripTime), address.getText()
									.toString(), UserId, "Package");

					if (name > 0) {

						NewEntryMenuFragment fragment = new NewEntryMenuFragment();
						FragmentManager fm = getFragmentManager();
						FragmentTransaction ft = fm.beginTransaction();
						ft.setCustomAnimations(R.anim.slide_in_right,
								R.anim.slide_out_left, R.anim.slide_in_left,
								R.anim.slide_out_right);
						Bundle bundle = new Bundle();

						bundle.putString("Title", title.getText().toString());
						bundle.putString("Date", startDate.getText().toString());
						bundle.putString("Packageid", String.valueOf(name));
						bundle.putString("Typed", "Package");
						fragment.setArguments(bundle);
						ft.replace(R.id.container, fragment);
						ft.addToBackStack(null);
						ft.commit();
						Editor editor = shared.edit();

						editor.putString(AppConstant.KEY_PackageId,
								String.valueOf(name));
						editor.putString(AppConstant.KEY_Date,
								output.format(oneWayTripDate));

						editor.putString(AppConstant.KEY_Address, address
								.getText().toString());

						editor.commit();
						title.setText("");
						address.setText("");
					} else {
						Toast.makeText(getActivity(), "Data Not Inserted", 2000)
								.show();
					}

				} else if (txtTitle.getText().toString()
						.equals("START INVENTORY")
						&& !isInternetPresent) {

					name = DATABASE.InsertPackage(title.getText().toString(),
							output.format(oneWayTripDate), output1
									.format(oneWayTripTime), address.getText()
									.toString(), UserId, "Inventory");

					if (name > 0) {

						NewEntryMenuFragment fragment = new NewEntryMenuFragment();
						FragmentManager fm = getFragmentManager();
						FragmentTransaction ft = fm.beginTransaction();
						ft.setCustomAnimations(R.anim.slide_in_right,
								R.anim.slide_out_left, R.anim.slide_in_left,
								R.anim.slide_out_right);
						Bundle bundle = new Bundle();

						bundle.putString("Title", title.getText().toString());
						bundle.putString("Date", startDate.getText().toString());
						bundle.putString("Packageid", String.valueOf(name));
						bundle.putString("Typed", "Inventory");
						fragment.setArguments(bundle);
						ft.replace(R.id.container, fragment);
						ft.addToBackStack(null);
						ft.commit();
						Editor editor = shared.edit();

						editor.putString(AppConstant.KEY_PackageId,
								String.valueOf(name));
						editor.putString(AppConstant.KEY_Date,
								output.format(oneWayTripDate));

						editor.putString(AppConstant.KEY_Address, address
								.getText().toString());

						editor.commit();
						title.setText("");
						address.setText("");
					} else {
						Toast.makeText(getActivity(), "Data Not Inserted", 2000)
								.show();
					}

				}

				else if (txtTitle.getText().toString()
						.equals("START NEW PICK LIST")
						&& !isInternetPresent) {
					if (title.getText().toString().equals("")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"Please Enter PickList Title", false);
					} else {

						name = DATABASE.InsertPicklist(title.getText()
								.toString(), output.format(oneWayTripDate),
								output1.format(oneWayTripTime), UserId, shared
										.getString(
												AppConstant.KEY_SelectedType,
												""));
						if (name > 0) {
							long check1 = DATABASE.InsertPicklistitem(String
									.valueOf(name), UserId, itemid,
									shared.getString(
											AppConstant.KEY_SelectedType, ""),
									output.format(oneWayTripDate));
							Log.e("check", "" + check1);

							OnStartPickListFragment fragment = new OnStartPickListFragment();
							FragmentManager fm = getFragmentManager();
							FragmentTransaction ft = fm.beginTransaction();
							ft.setCustomAnimations(R.anim.slide_in_right,
									R.anim.slide_out_left,
									R.anim.slide_in_left,
									R.anim.slide_out_right);

							Bundle bundle = new Bundle();

							bundle.putString("Picklistid", String.valueOf(name));

							fragment.setArguments(bundle);
							ft.replace(R.id.container, fragment);
							ft.addToBackStack(null);
							ft.commit();
						} else {
							ViewUtil.showAlertDialog(getActivity(), "Error",
									"PickList Not Created", true);
						}
					}
				}
			}
		});

		startDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				DialogFragment picker = new DatePickerFragment();
				picker.show(getFragmentManager(), "datePicker");
			}

		});
		startTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				TimePickerFragment picker = new TimePickerFragment();
				picker.show(getFragmentManager(), "timePicker");
			}

		});

	}

	private boolean validateUser() {

		if (isEmptyTitle()) {
			ViewUtil.showAlertDialog(getActivity(), "Title",
					"Please enter Package Name", true);
			return false;
		}

		if (isAddressEmpty()) {
			ViewUtil.showAlertDialog(getActivity(), "Address",
					"Please enter Address", true);
			return false;
		}

		return true;
	}

	private boolean isAddressEmpty() {
		// TODO Auto-generated method stub
		if (address.getText().toString().trim().equals("")) {
			return true;
		}
		return false;
	}

	private boolean isEmptyTitle() {
		// TODO Auto-generated method stub
		if (title.getText().toString().trim().equals("")) {
			return true;
		}
		return false;
	}

	private void setTimer() {

		Date today = Calendar.getInstance().getTime();

		setDate(today);
		setTime(today);

	}

	private void setDate(Date today) {
		// Create an instance of SimpleDateFormat used for formatting
		// the string representation of date (month/day/year)

		DateFormat df = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault());

		// Get the date today using Calendar object.
		// Using DateFormat format method we can create a string
		// representation of a date with the defined format.

		String reportDate = df.format(today);
		startDate.setText(reportDate);

	}

	private void setTime(Date today) {

		DateFormat timeFormat = new SimpleDateFormat("hh:mm a",
				Locale.getDefault());

		Log.e("Formater", timeFormat.format(today));
		startTime.setText(timeFormat.format(today));

	}

	private void intHeaderTitle() {
		// TODO Auto-generated method stub
		String key = getArguments().getString(KEY);
		if (key.equals("pack_go")) {
			txtTitle.setText("START PACKING");
		} else if (key.equals("inventory")) {
			txtTitle.setText("START INVENTORY");
		} else if (key.equals("pick_list")) {
			txtTitle.setText("START NEW PICK LIST");
			add.setVisibility(View.GONE);

		}
	}

	public class DatePickerFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker

			DatePickerDialog d = new DatePickerDialog(getActivity(), this,
					year, month, day);
			DatePicker dp = d.getDatePicker();
			dp.setMinDate(c.getTimeInMillis());
			return d;
			// Create a new instance of DatePickerDialog and return it
			// return new DatePickerDialog(getActivity(), this, year, month,
			// day);
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			Calendar c = Calendar.getInstance();
			c.set(year, month, day);
			Date date = c.getTime();
			setDate(date);
		}
	}

	public class TimePickerFragment extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int hour = c.get(Calendar.HOUR);
			int minute = c.get(Calendar.MINUTE);

			// Create a new instance of DatePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute,
					false);
		}

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			c.set(year, month, day, hourOfDay, minute);
			setTime(c.getTime());
		}
	}

	public class AsyncPackageTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				key = getArguments().getString(KEY);
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);
						Log.e("postexecute", "" + key);
						if (jObj != null) {
							if (jObj.getBoolean("success")
									&& key.equals("pack_go")) {
								Log.e("Response start packing", result);
								title.setText("");
								address.setText("");
								JSONObject phone = jObj
										.getJSONObject("message");
								String packageid = phone.getString("PackageId");
								String StartDate = phone.getString("StartDate");
								String StartTime = phone.getString("StartTime");
								String Address = phone.getString("Address");

								SimpleDateFormat input = new SimpleDateFormat(
										"yyyy-MM-dd");
								SimpleDateFormat output = new SimpleDateFormat(
										"dd-MM-yyyy");
								try {
									oneWayTripDate = input.parse(StartDate);
									Log.e("StartDate",
											"" + output.format(oneWayTripDate));
								} catch (ParseException e) {
									e.printStackTrace();
								}
								Editor editor = shared.edit();
								editor.putString(AppConstant.KEY_PackageId,
										packageid);
								editor.putString(AppConstant.KEY_Date,
										output.format(oneWayTripDate));
								editor.putString(AppConstant.KEY_Time,
										StartTime);
								editor.putString(AppConstant.KEY_Address,
										Address);

								editor.commit();
								NewEntryMenuFragment fragment = new NewEntryMenuFragment();
								FragmentManager fm = getFragmentManager();
								FragmentTransaction ft = fm.beginTransaction();
								ft.setCustomAnimations(R.anim.slide_in_right,
										R.anim.slide_out_left,
										R.anim.slide_in_left,
										R.anim.slide_out_right);
								Bundle bundle = new Bundle();

								bundle.putString("Title", title.getText()
										.toString());
								bundle.putString("Date", startDate.getText()
										.toString());
								bundle.putString("Typed", "Package");

								fragment.setArguments(bundle);
								ft.replace(R.id.container, fragment);
								ft.addToBackStack(null);
								ft.commit();
								ViewUtil.hideProgressDialog();
								Log.e("", "last");
							} else if (jObj.getBoolean("success")
									&& key.equals("inventory")) {
								Log.e("Response", result);
								JSONObject phone = jObj
										.getJSONObject("message");
								String packageid = phone.getString("PackageId");
								String StartDate = phone.getString("StartDate");
								String StartTime = phone.getString("StartTime");
								String Address = phone.getString("Address");
								SimpleDateFormat input = new SimpleDateFormat(
										"yyyy-MM-dd");
								SimpleDateFormat output = new SimpleDateFormat(
										"dd-MM-yyyy");
								try {
									oneWayTripDate = input.parse(StartDate); // parse
																				// input
									// tripDate.setText(output.format(oneWayTripDate));
									// // format output
									Log.e("StartDate",
											"" + output.format(oneWayTripDate));
								} catch (ParseException e) {
									e.printStackTrace();
								}

								Log.e("StartTime", "" + StartTime);
								Log.e("Address", "" + Address);
								Editor editor = shared.edit();
								editor.putString(AppConstant.KEY_PackageId,
										packageid);
								editor.putString(AppConstant.KEY_Date,
										output.format(oneWayTripDate));
								editor.putString(AppConstant.KEY_Time,
										StartTime);
								editor.putString(AppConstant.KEY_Address,
										Address);

								editor.commit();
								NewEntryMenuFragment fragment = new NewEntryMenuFragment();
								FragmentManager fm = getFragmentManager();
								FragmentTransaction ft = fm.beginTransaction();
								ft.setCustomAnimations(R.anim.slide_in_right,
										R.anim.slide_out_left,
										R.anim.slide_in_left,
										R.anim.slide_out_right);
								Bundle bundle = new Bundle();

								bundle.putString("Title", title.getText()
										.toString());
								bundle.putString("Date", startDate.getText()
										.toString());
								bundle.putString("Typed", "Inventory");
								fragment.setArguments(bundle);
								ft.replace(R.id.container, fragment);
								ft.addToBackStack(null);
								ft.commit();
								ViewUtil.hideProgressDialog();
							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");

								ViewUtil.hideProgressDialog();
								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);

							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("Title", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("StartDate", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("StartTime", String
					.valueOf(params[2])));
			parameter.add(new BasicNameValuePair("Address", String
					.valueOf(params[3])));
			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[4])));
			parameter.add(new BasicNameValuePair("Typed", String
					.valueOf(params[5])));
			Log.e("parameter on start pack", "" + parameter);
			String result = NetworkConnector.PackageDetail(parameter);
			return result;
		}

	}

	public class AsyncCreatePickListTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						key = getArguments().getString(KEY);
						JSONObject jObj = new JSONObject(result);
						Log.e("postexecute", "" + key);
						if (jObj != null) {
							if (jObj.getBoolean("success")
									&& key.equals("pick_list")) {
								Log.e("Response", result);

								JSONObject phone = jObj
										.getJSONObject("message");
								String packageid = phone
										.getString("PicklistId");

								SimpleDateFormat input = new SimpleDateFormat(
										"yyyy-MM-dd");
								SimpleDateFormat output = new SimpleDateFormat(
										"dd-MM-yyyy");
								try {
									oneWayTripDate = input.parse(startDate
											.getText().toString()); // parse
																	// input

									Log.e("StartDate",
											"" + output.format(oneWayTripDate));
								} catch (ParseException e) {
									e.printStackTrace();
								}

								Editor editor = shared.edit();
								editor.putString(AppConstant.KEY_PackageId,
										packageid);
								editor.putString(AppConstant.KEY_ItemId, "");
								editor.putString(AppConstant.KEY_Date,
										output.format(oneWayTripDate));

								editor.commit();
								/*
								 * Intent intent1 = new Intent(getActivity(),
								 * PickGoActivity.class);
								 * intent1.putExtra("Pack_go", "pick_list");
								 * startActivity(intent1);
								 * getActivity().overridePendingTransition
								 * (R.anim.fade_in, R.anim.fade_out);
								 * getActivity().finish();
								 */
								OnStartPickListFragment fragment = new OnStartPickListFragment();
								FragmentManager fm = getFragmentManager();
								FragmentTransaction ft = fm.beginTransaction();
								ft.setCustomAnimations(R.anim.slide_in_right,
										R.anim.slide_out_left,
										R.anim.slide_in_left,
										R.anim.slide_out_right);
								Bundle bundle = new Bundle();

								bundle.putString("Picklistid", packageid);
								fragment.setArguments(bundle);
								ft.replace(R.id.container, fragment);
								ft.addToBackStack(null);
								ft.commit();
								ViewUtil.hideProgressDialog();
							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");

								ViewUtil.hideProgressDialog();
								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);

							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("Title", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("StartDate", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("StartTime", String
					.valueOf(params[2])));
			parameter.add(new BasicNameValuePair("Type", String
					.valueOf(params[3])));
			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[4])));
			parameter.add(new BasicNameValuePair("ItemId", String
					.valueOf(params[5])));
			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.CreatePickList(parameter);
			return result;
		}

	}
}
