package com.appalmighty.ezefind.newentry.fgmt;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.DataBase.DatasourceHandler;
import com.appalmighty.ezefind.DataBase.NewEntryDataBase;
import com.appalmighty.ezefind.location.MapActivity;
import com.appalmighty.ezefind.login.act.LoginActivity;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.appalmighty.ezefind.view.ChoosePopup;
import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class NewEntryFormFragment extends Fragment implements OnClickListener {

	private static final int GALLERY_IMAGE_REQUEST_CODE = 99;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_GALLERY = 2;
	public static final int RESULT_OK = -1;
	public static final int REQUEST_AUDIO_TO_TEXT_DESCRIPTION = 301;
	public static final int REQUEST_AUDIO_TO_TEXT_VALUE = 302;

	private static final String IMAGE_DIRECTORY_NAME = "ezeFind";
	private Uri fileUri;
	private File mediaStorageDir;
	private File mediaFile;
	DatasourceHandler DATABASE;
	private static final int ZBAR_SCANNER_REQUEST = 0;
	public static final String KEY = "key";
	private TextView textItemNumber, textBarcodeNumber, textdate, textadd;
	// TextView Of AddCatogary
	private TextView textshirt, textpant, textelectronic, textcoat;
	// ImageView Of Caution
	private ImageView imgfragile, imgsharp, imgliquid, imgheavy;
	private ImageView imageBox, imageBag, imageSingle, imageBin;
	private ImageView imgBtnCancel, imgBtnSave, imageProfile, imgAddItems,
			imgEditItems;

	private Button buttonScan, buttonOtherBarcode, buttonScanOk,
			buttonScanCancel, buttonSaveProfile, buttonAddProfile,
			buttonAddCateGory, buttonBack, buttonBackprofile, buttoncolorpick,
			btnLogout;
	private EditText profileName, edtDescription, edtPrice;
	private Button buttonClose, buttonLocationClose;
	private LinearLayout yes, no, valuelayout, colorlayout;
	private RelativeLayout rLocPlace, rLocShelf, rLocDraw, rLocNear, rLocGPS,
			rLocAddnew;

	private RelativeLayout containerBarcode, containerBarcodeScan,
			containerAddProfile, containerAddCategory, containerCaution,
			containerLocation, containerpopup, containercolor;
	// Layout Of Color Picker
	private RelativeLayout rred, ryellow, rpurple, rgreen, rblue;
	private Button editBarCode, editBarCode2, editProfile, editCategory,
			editCaution, editLocation, btnhome;

	public static final String KEY_ARG = "key";

	public String barCode, barcode2, color;

	private String title, totalbag, package_type, imageValue, userid, type,
			barcodeValue, category_id, description, caution, location,
			location_type, platform, price, value;

	private SharedPreferences preferences;
	private EditText edtBag;
	private String UserId;
	private String packageid;
	private String packagedataid;
	private String number;
	private String date;
	private String address;
	private String profile;
	private int callingbarcode = 0;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	DisplayImageOptions displayImageOptions;
	ListView catogarylist;
	MyAdapter adapet;
	ArrayList<String> list;
	String typingvalue, back = "0";

	public static NewEntryFormFragment newInstance(String string) {
		NewEntryFormFragment fragment = new NewEntryFormFragment();
		Bundle bundle = new Bundle();
		bundle.putString(KEY_ARG, string);
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(
				R.layout.fragment_new_entry_bag_layout, container, false);
		preferences = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);

		textItemNumber = (TextView) rootView
				.findViewById(R.id.id_text_item_number);
		textdate = (TextView) rootView.findViewById(R.id.txt_date);
		textadd = (TextView) rootView.findViewById(R.id.textaddrerss);
		imageBox = (ImageView) rootView.findViewById(R.id.id_image_box);
		imageBag = (ImageView) rootView.findViewById(R.id.id_image_bag);
		imageSingle = (ImageView) rootView
				.findViewById(R.id.id_image_single_item);
		imgAddItems = (ImageView) rootView.findViewById(R.id.img_add);
		imgEditItems = (ImageView) rootView.findViewById(R.id.img_edit);
		imageBin = (ImageView) rootView.findViewById(R.id.id_image_bin);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home_);
		imgBtnCancel = (ImageView) rootView.findViewById(R.id.img_btn_cancel);
		imgBtnSave = (ImageView) rootView.findViewById(R.id.img_btn_save);
		btnLogout = (Button) rootView.findViewById(R.id.btn_logout);
		colorlayout = (LinearLayout) rootView
				.findViewById(R.id.linear_layout_colorpicker);
		valuelayout = (LinearLayout) rootView
				.findViewById(R.id.linear_layout_value);
		btnLogout.setOnClickListener(this);
		imgBtnCancel.setOnClickListener(this);
		imgBtnSave.setOnClickListener(this);
		imgAddItems.setOnClickListener(this);
		imgEditItems.setOnClickListener(this);

		editBarCode = (Button) rootView.findViewById(R.id.edt_barcode);
		editBarCode2 = (Button) rootView.findViewById(R.id.edt_barcode_other);
		editProfile = (Button) rootView.findViewById(R.id.edt_member);
		buttoncolorpick = (Button) rootView.findViewById(R.id.edt_colorpicker);
		edtDescription = (EditText) rootView.findViewById(R.id.edt_description);
		edtPrice = (EditText) rootView.findViewById(R.id.edt_price);
		edtBag = (EditText) rootView.findViewById(R.id.edt_bag);
		editCategory = (Button) rootView.findViewById(R.id.edt_category);
		editCaution = (Button) rootView.findViewById(R.id.edt_caution);
		editLocation = (Button) rootView.findViewById(R.id.edt_location);
		buttonBack = (Button) rootView.findViewById(R.id.back_button_);
		list = new ArrayList<String>();
		edtDescription.setOnClickListener(this);
		edtPrice.setOnClickListener(this);
		internetcheck = new ConnectionDetector(getActivity());
		isInternetPresent = internetcheck.isConnectingToInternet();
		containerBarcode = (RelativeLayout) rootView
				.findViewById(R.id.container_barcode);
		containerBarcodeScan = (RelativeLayout) rootView
				.findViewById(R.id.container_id_scan);
		containerAddProfile = (RelativeLayout) rootView
				.findViewById(R.id.container_add_profile);
		containerAddCategory = (RelativeLayout) rootView
				.findViewById(R.id.container_add_category);
		containerCaution = (RelativeLayout) rootView
				.findViewById(R.id.container_caution);
		containerLocation = (RelativeLayout) rootView
				.findViewById(R.id.container_location);
		containerpopup = (RelativeLayout) rootView
				.findViewById(R.id.container_pop);
		containercolor = (RelativeLayout) rootView
				.findViewById(R.id.container_color);
		DATABASE = new DatasourceHandler(getActivity());
		preferences = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = preferences.getString(AppConstant.KEY_USER_ID, "");
		packageid = preferences.getString(AppConstant.KEY_PackageId, "");
		packagedataid = preferences
				.getString(AppConstant.KEY_PackageDataId, "");
		number = preferences.getString(AppConstant.KEY_PackageNumber, "");
		date = preferences.getString(AppConstant.KEY_Date, "");

		textadd.setText(preferences.getString(AppConstant.KEY_Address, ""));
		textdate.setText(date);
		edtBag.setText(getArguments().getString(KEY_ARG) + "    " + number);
		editProfile.setText(preferences
				.getString(AppConstant.KEY_USER_NAME, ""));
		Log.e("bhj", "" + getArguments().getString(KEY_ARG));
		if (getArguments().getString(KEY_ARG).equals("Bag")) {
			colorlayout.setVisibility(View.VISIBLE);
		} else if (getArguments().getString(KEY_ARG).equals("Single Item")) {
			valuelayout.setVisibility(View.VISIBLE);
			imgAddItems.setClickable(false);
			imgEditItems.setClickable(false);
		}
		buttoncolorpick.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				containercolor.setVisibility(View.VISIBLE);

			}
		});
		edtDescription.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				// Log.e("focus", "focus:"+hasFocus);
				if (hasFocus) {
					typingvalue = preferences.getString(
							AppConstant.KEY_ENTRYTYPE, "");
					// Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						edtDescription.setInputType(InputType.TYPE_NULL);
						getActivity()
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_DESCRIPTION);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		edtPrice.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (hasFocus) {
					typingvalue = preferences.getString(
							AppConstant.KEY_ENTRYTYPE, "");
					// Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						edtPrice.setInputType(InputType.TYPE_NULL);
						getActivity()
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_VALUE);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}
			}
		});
		initHeaderItem();

		addClickListerToInputs();

		initBarcodeContainerComp(rootView);
		initProfileContainerComp(rootView);
		initCategoryContainerComp(rootView);
		initCautionContainer(rootView);
		initLocationContainer(rootView);
		initPopUpcontainer(rootView);
		initColorcontainer(rootView);

		btnhome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					new AsyncDeletePackageDataTask().execute(packageid,
							packagedataid, UserId);

					getActivity().onBackPressed();

				} else {

					long name = DATABASE.DeletePackageData(UserId, packageid,
							packagedataid);
					if (name > 0) {
						getActivity().onBackPressed();
					} else {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"Data not Deleted", true);
					}
				}
				getActivity().finish();
				Intent intent = new Intent(getActivity(), MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);

			}
		});

		buttonBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showAlertDialog(getActivity(), "Cancel",
						"Are you sure to cancel", false);

			}
		});
		rootView.setFocusableInTouchMode(true);
		rootView.requestFocus();
		rootView.setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub

				if (keyCode == KeyEvent.KEYCODE_BACK && back.equals("0")) {

					back = "1";
					showAlertDialog(getActivity(), "Cancel",
							"Are you sure to cancel", false);
					return true;

				} else {
					return false;
				}

			}
		});
		return rootView;
	}

	private void initProfileContainerComp(View rootView) {
		buttonSaveProfile = (Button) rootView.findViewById(R.id.btn_save);
		imageProfile = (ImageView) rootView.findViewById(R.id.img_userProfile);
		buttonAddProfile = (Button) rootView.findViewById(R.id.btn_addphoto);
		profileName = (EditText) rootView.findViewById(R.id.edt_entername);
		buttonBackprofile = (Button) rootView.findViewById(R.id.back_button);
		displayImageOptions = setupImageLoader();
		profileName.setText(preferences
				.getString(AppConstant.KEY_USER_NAME, ""));
		profileName.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				// TODO Auto-generated method stub
				if (hasFocus) {
					typingvalue = preferences.getString(
							AppConstant.KEY_ENTRYTYPE, "");
					// Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						profileName.setInputType(InputType.TYPE_NULL);
						getActivity()
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_VALUE);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}

			}
		});
		profileName.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE,
						"");
				// Log.e("typingvalue", "" + typingvalue);
				if (typingvalue.equals("2")) {
					profileName.setInputType(InputType.TYPE_NULL);
					ChoosePopup.openTypeModePopWindow(getActivity());
				} else if (typingvalue.equals("1")) {
					if (ChoosePopup
							.isSpeechRecognitionActivityPresented(getActivity()) == true) {
						// if yes � running recognition
						profileName.setInputType(InputType.TYPE_NULL);
						startRecognition(getActivity(),
								REQUEST_AUDIO_TO_TEXT_DESCRIPTION);
					} else {
						// if no, then showing notification to install Voice
						// Search
						Toast.makeText(
								getActivity(),
								"In order to activate speech recognition you must install Google Voice Search",
								Toast.LENGTH_LONG).show();
						// start installing process
						ChoosePopup.installGoogleVoiceSearch(getActivity());
					}
				} else {
					profileName.setInputType(InputType.TYPE_CLASS_TEXT);
				}
			}
		});
		ImageLoader.getInstance().displayImage(
				String.valueOf(preferences.getString(AppConstant.KEY_USERIMAGE,
						"")), imageProfile);
		buttonSaveProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				editProfile.setText(profileName.getText().toString());
				containerAddProfile.setVisibility(View.GONE);
			}
		});
		buttonAddProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				selectImage();
			}
		});
		buttonBackprofile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				containerAddProfile.setVisibility(View.GONE);
			}
		});

	}

	public DisplayImageOptions setupImageLoader() {
		return new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1)).build();

	}

	private void initBarcodeContainerComp(View rootView) {

		buttonScan = (Button) rootView.findViewById(R.id.button_scan);
		buttonScanOk = (Button) rootView.findViewById(R.id.button_scan_ok);
		buttonScanCancel = (Button) rootView.findViewById(R.id.button_cancel);
		textBarcodeNumber = (TextView) rootView
				.findViewById(R.id.text_barcode_number);
		buttonScan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerBarcode.setVisibility(View.GONE);
				containerBarcodeScan.setVisibility(View.VISIBLE);
				buttonScanOk.setText("Scan");
				buttonScanOk.setTag("Scan");
			}
		});
		buttonScanCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerBarcodeScan.setVisibility(View.GONE);
			}
		});
		buttonScanOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (v.getTag().toString().toLowerCase(Locale.getDefault())
						.equals("scan")) {
					launchScanner(v);
				} else if (v.getTag().toString()
						.toLowerCase(Locale.getDefault()).equals("ok")) {
					if (callingbarcode == 1) {
						editBarCode.setText(barCode);
					} else if (callingbarcode == 2) {
						editBarCode2.setText(barCode);
					}
					containerBarcodeScan.setVisibility(View.GONE);
				}

			}
		});

	}

	private void initBarcode2ContainerComp(View rootView) {

		buttonOtherBarcode = (Button) rootView
				.findViewById(R.id.edt_barcode_other);
		buttonOtherBarcode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerBarcode.setVisibility(View.GONE);
				containerBarcodeScan.setVisibility(View.VISIBLE);
			}
		});
		buttonScanOk = (Button) rootView.findViewById(R.id.button_scan_ok);
		buttonScanCancel = (Button) rootView.findViewById(R.id.button_cancel);
		textBarcodeNumber = (TextView) rootView
				.findViewById(R.id.text_barcode_number);
		buttonScanCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerBarcodeScan.setVisibility(View.GONE);
			}
		});
		buttonScanOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (v.getTag().toString().toLowerCase(Locale.getDefault())
						.equals("scan")) {
					launchScanner(v);
				} else if (v.getTag().toString()
						.toLowerCase(Locale.getDefault()).equals("ok")) {
					editBarCode.setText(barCode);
					containerBarcodeScan.setVisibility(View.GONE);
				}

			}
		});

	}

	private void initCategoryContainerComp(View rootView) {

		catogarylist = (ListView) rootView.findViewById(R.id.Catogarylist);
		buttonAddCateGory = (Button) rootView
				.findViewById(R.id.id_button_add_category);

		catogarylist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				containerAddCategory.setVisibility(View.GONE);
				editCategory.setText((String) parent
						.getItemAtPosition(position));
			}
		});

		buttonAddCateGory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// containerBarcode.setVisibility(View.GONE);
				containerAddCategory.setVisibility(View.GONE);
				addOtherCategoryWindow();
			}
		});

	}

	private void initCautionContainer(View rootView) {
		imgfragile = (ImageView) rootView.findViewById(R.id.img_fragile);
		imgsharp = (ImageView) rootView.findViewById(R.id.img_sharp);
		imgliquid = (ImageView) rootView.findViewById(R.id.img_liquid);
		imgheavy = (ImageView) rootView.findViewById(R.id.img_heavy);
		buttonClose = (Button) rootView.findViewById(R.id.back_close);
		imgfragile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				containerCaution.setVisibility(View.GONE);
				editCaution.setText("Fragile");
			}
		});
		imgsharp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				containerCaution.setVisibility(View.GONE);
				editCaution.setText("Sharp");
			}
		});
		imgliquid.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				containerCaution.setVisibility(View.GONE);
				editCaution.setText("Liquid");
			}
		});
		imgheavy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				containerCaution.setVisibility(View.GONE);
				editCaution.setText("Heavy");
			}
		});

		buttonClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerCaution.setVisibility(View.GONE);
			}
		});

	}

	private void initLocationContainer(View rootView) {

		buttonLocationClose = (Button) rootView
				.findViewById(R.id.location_close);
		rLocPlace = (RelativeLayout) (rootView).findViewById(R.id.id_rel_place);
		rLocShelf = (RelativeLayout) (rootView).findViewById(R.id.id_rel_shelf);
		rLocDraw = (RelativeLayout) (rootView).findViewById(R.id.id_rel_draw);
		rLocNear = (RelativeLayout) (rootView).findViewById(R.id.id_rel_near);
		rLocGPS = (RelativeLayout) (rootView).findViewById(R.id.id_rel_gps);
		rLocAddnew = (RelativeLayout) (rootView)
				.findViewById(R.id.id_rel_add_new);

		rLocPlace.setOnClickListener(this);
		rLocShelf.setOnClickListener(this);
		rLocDraw.setOnClickListener(this);
		rLocNear.setOnClickListener(this);
		rLocGPS.setOnClickListener(this);
		rLocAddnew.setOnClickListener(this);

		buttonLocationClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerLocation.setVisibility(View.GONE);
			}
		});

	}

	private void initPopUpcontainer(View rootView) {
		yes = (LinearLayout) rootView.findViewById(R.id.linearYes);
		no = (LinearLayout) rootView.findViewById(R.id.linearlayoutNo);
		yes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				barCode = editBarCode.getText().toString();
				category_id = editCategory.getText().toString();
				description = edtDescription.getText().toString();
				caution = editCaution.getText().toString();
				location = editLocation.getText().toString();
				barcode2 = editBarCode2.getText().toString();
				price = edtPrice.getText().toString();
				color = buttoncolorpick.getText().toString();
				profile = editProfile.getText().toString();
				Editor editor = preferences.edit();
				editor.putString(AppConstant.KEY_CATEGORY, category_id);
				editor.putString(AppConstant.KEY_LOCATION, location);
				editor.commit();
				if (isInternetPresent) {
					saveNewEntry();
					containerpopup.setVisibility(View.GONE);
				} else {
					if (validateForm()) {
						Log.e("QUERY_PACKAGEDATA", ""
								+ NewEntryDataBase.QUERY_PACKAGEDATA);

						String typed = "Package";
						long name = DATABASE.UpdatePackageData(UserId,
								packageid, getArguments().getString(KEY_ARG),
								packagedataid, color, caution, description,
								category_id, barCode, price, barcode2,
								location, typed, profile);

						if (name > 0) {
							getFragmentManager().popBackStack();
							AdditemsFragment openContainerFragment = new AdditemsFragment();
							FragmentManager fm = getFragmentManager();
							FragmentTransaction ft = fm.beginTransaction();
							ft.setCustomAnimations(R.anim.slide_in_right,
									R.anim.slide_out_left,
									R.anim.slide_in_left,
									R.anim.slide_out_right);

							ft.replace(R.id.container, openContainerFragment);
							ft.addToBackStack(null);
							ft.commit();

							edtBag.setText("");
							edtDescription.setText("");
							edtPrice.setText("");
							editBarCode.setText("");
							editCategory.setText("");
							editCaution.setText("");
							editLocation.setText("");
							editBarCode2.setText("");
							buttoncolorpick.setText("");
							editProfile.setText("");

						} else
							ViewUtil.showAlertDialog(getActivity(), "Error",
									"Data Not Inserted", true);

						Log.e("return", "" + name);
					}

					containerpopup.setVisibility(View.GONE);
				}
			}
		});
		no.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				containerpopup.setVisibility(View.GONE);
			}
		});
	}

	private void initColorcontainer(View rootView) {
		rred = (RelativeLayout) rootView.findViewById(R.id.l_color_red);
		ryellow = (RelativeLayout) rootView.findViewById(R.id.l_color_yellow);
		rpurple = (RelativeLayout) rootView.findViewById(R.id.l_color_purple);
		rgreen = (RelativeLayout) rootView.findViewById(R.id.l_color_green);
		rblue = (RelativeLayout) rootView.findViewById(R.id.l_color_blue);
		Button btnClose = (Button) rootView.findViewById(R.id.btnclose);
		rred.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				buttoncolorpick.setText("Red");
				containercolor.setVisibility(View.GONE);
			}
		});
		ryellow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				buttoncolorpick.setText("Yellow");
				containercolor.setVisibility(View.GONE);
			}
		});
		rpurple.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				buttoncolorpick.setText("Purple");
				containercolor.setVisibility(View.GONE);

			}
		});
		rgreen.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				buttoncolorpick.setText("Green");
				containercolor.setVisibility(View.GONE);

			}
		});
		rblue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				buttoncolorpick.setText("Blue");
				containercolor.setVisibility(View.GONE);

			}
		});

		btnClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containercolor.setVisibility(View.GONE);
			}
		});
	}

	protected void addOtherCategoryWindow() {
		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_add_category, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		final EditText edtnewcatogary = (EditText) popupView
				.findViewById(R.id.edt_entername);
		Button btnDismiss = (Button) popupView
				.findViewById(R.id.id_button_save);
		btnDismiss.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				Log.e("catterrr", "" + edtnewcatogary.getText().toString());
				editCategory.setText(edtnewcatogary.getText().toString());
				if (isInternetPresent) {
					new AsyncuplaodcataogaryTask().execute(UserId,
							edtnewcatogary.getText().toString());
				} else {

					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet connection", false);
				}
				popupWindow.dismiss();
				containerAddCategory.setVisibility(View.GONE);
			}
		});

		popupWindow.showAtLocation(containerAddCategory, Gravity.CENTER, 0, 0);

	}

	private void addLocationPopuWindow(String string) {
		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(R.layout.popup_layout_draw_,
				null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

		Button btnDismiss = (Button) popupView.findViewById(R.id.back_button);
		Button btnSave = (Button) popupView.findViewById(R.id.btn_save);
		final TextView textHeader = (TextView) popupView
				.findViewById(R.id.txt_title);
		final EditText input = (EditText) popupView.findViewById(R.id.edt_text);
		textHeader.setText(string);
		input.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				// TODO Auto-generated method stub
				// Log.e("focus", "focus:"+hasFocus);
				if (hasFocus) {
					typingvalue = preferences.getString(
							AppConstant.KEY_ENTRYTYPE, "");
					// Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						input.setInputType(InputType.TYPE_NULL);
						getActivity()
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_DESCRIPTION);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}

			}
		});
		input.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE,
						"");
				// Log.e("typingvalue", "" + typingvalue);
				if (typingvalue.equals("2")) {
					input.setInputType(InputType.TYPE_NULL);
					ChoosePopup.openTypeModePopWindow(getActivity());
				} else if (typingvalue.equals("1")) {
					if (ChoosePopup
							.isSpeechRecognitionActivityPresented(getActivity()) == true) {
						// if yes � running recognition
						input.setInputType(InputType.TYPE_NULL);
						startRecognition(getActivity(),
								REQUEST_AUDIO_TO_TEXT_VALUE);
					} else {
						// if no, then showing notification to install Voice
						// Search
						Toast.makeText(
								getActivity(),
								"In order to activate speech recognition you must install Google Voice Search",
								Toast.LENGTH_LONG).show();
						// start installing process
						ChoosePopup.installGoogleVoiceSearch(getActivity());
					}
				} else {
					input.setInputType(InputType.TYPE_CLASS_TEXT);
				}

			}
		});
		btnDismiss.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();
				containerLocation.setVisibility(View.GONE);
			}
		});
		btnSave.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				editLocation.setText(input.getText().toString());
				containerLocation.setVisibility(View.GONE);
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (input.getText().toString().equals("")) {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"Please Provide Location.", true);
				}

				else if (isInternetPresent) {
					new AsyncLocationTask().execute(packageid, packagedataid,
							input.getText().toString(), textHeader.getText()
									.toString(), UserId);
					popupWindow.dismiss();
				} else if (input.getText().toString() != null) {

					long row = DATABASE.InsertLocation(packageid,
							packagedataid, input.getText().toString(),
							textHeader.getText().toString(), UserId, "null",
							"null", "null", "null", "null");
					Log.e("row", "" + row);
					if (row > 0) {
						popupWindow.dismiss();
					}
				}

			}
		});

		popupWindow.showAtLocation(containerLocation, Gravity.CENTER, 0, 0);
	}

	public class AsyncLocationTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response save location", result);

							ViewUtil.hideProgressDialog();

						} else {
							Log.e("Response", result);

							JSONObject phone = jObj.getJSONObject("message");
							String Server_message = phone.getString("Account");
							ViewUtil.hideProgressDialog();
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									Server_message, true);

						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("PackageId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("PackageDataId", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("Value", String
					.valueOf(params[2])));
			parameter.add(new BasicNameValuePair("Type", String
					.valueOf(params[3])));

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[4])));
			Log.e("parameter save", "" + parameter);
			String result = NetworkConnector.PackageLocation(parameter);
			return result;
		}

	}

	public class AsyncGetcataogaryTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			list.clear();
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response", result);
							JSONArray array = jObj.getJSONArray("message");
							Log.e("array", "" + array);
							for (int i = 0; i < array.length(); i++) {

								list.add(array.get(i).toString().toUpperCase());
							}
							Log.e("list:", "" + list);
							Log.e("listsize:", "" + list.size());
							adapet = new MyAdapter(getActivity(), list);
							catogarylist.setAdapter(adapet);
							ViewUtil.hideProgressDialog();

						} else {
							Log.e("Response", result);

							String Server_message = jObj.getString("message");
							ViewUtil.hideProgressDialog();
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									Server_message, true);
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));
			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.GETCategory(parameter);
			return result;
		}

	}

	public class AsyncuplaodcataogaryTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response-create", result);

							ViewUtil.hideProgressDialog();
							adapet = new MyAdapter(getActivity(), list);
							catogarylist.setAdapter(adapet);
						} else {
							Log.e("Response", result);

							String Server_message = jObj.getString("message");
							ViewUtil.hideProgressDialog();
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									Server_message, true);

						}
					}
				}
				ViewUtil.showAlertDialog(getActivity(), "ERROR",
						"No Internet Connection", false);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("Category", String
					.valueOf(params[1])));
			Log.e("parameter create", "" + parameter);
			String result = NetworkConnector.CreateCategory(parameter);
			return result;
		}

	}

	private void addClickListerToInputs() {
		editBarCode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				callingbarcode = 1;
				containerBarcode.setVisibility(View.VISIBLE);

			}
		});
		editBarCode2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				callingbarcode = 2;
				containerBarcode.setVisibility(View.VISIBLE);

			}
		});
		editProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerAddProfile.setVisibility(View.VISIBLE);

			}
		});
		editCategory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isInternetPresent) {
					new AsyncGetcataogaryTask().execute(UserId);

				} else {
					// ViewUtil.showAlertDialog(getActivity(), "ERROR",
					// "No Internet connection", false)
					list.clear();
					list.add("PANNTS");

					list.add("SHIRT");
					list.add("COAT");
					list.add("ELECTRONICS");

					adapet = new MyAdapter(getActivity(), list);
					catogarylist.setAdapter(adapet);
				}
				containerAddCategory.setVisibility(View.VISIBLE);

			}
		});
		editCaution.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerCaution.setVisibility(View.VISIBLE);

			}
		});
		editLocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				containerLocation.setVisibility(View.VISIBLE);

			}
		});

	}

	public class MyAdapter extends BaseAdapter {
		private LayoutInflater inflater;
		private Activity activity;
		ArrayList<String> text = new ArrayList<String>();

		public MyAdapter(FragmentActivity fragmentActivity,
				ArrayList<String> list) {
			// TODO Auto-generated constructor stub
			activity = fragmentActivity;
			text = list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return text.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return text.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final ViewHolder holder;
			if (convertView == null) {

				convertView = inflater.inflate(R.layout.adapterview, null);
				holder = new ViewHolder();
				holder.name = (TextView) convertView
						.findViewById(R.id.txt_item);
				holder.imgButtondelete = (ImageButton) convertView
						.findViewById(R.id.img_delete_button);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {
				holder.name.setId(position);
				holder.name.setText(text.get(position));
			} catch (Exception e) {
				e.printStackTrace();
			}

			holder.imgButtondelete
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.e("position", "" + position);
							Log.e("UserId", "" + UserId);
							Log.e("packageid", "" + packageid);

							new AsyncDeleteCategory()
									.execute(UserId, packageid);
						}
					});
			return convertView;
		}

	}

	class ViewHolder {

		private TextView name;
		private ImageButton imgButtondelete;

	}

	private void initHeaderItem() {

		String key = getArguments().getString(KEY_ARG);

		switch (key) {
		case AppConstant.KEY_BAG:
			textItemNumber.setText(getArguments().getString(KEY_ARG) + ""
					+ number);
			imageBag.setVisibility(View.VISIBLE);
			break;
		case AppConstant.KEY_BOX:
			textItemNumber.setText(getArguments().getString(KEY_ARG) + ""
					+ number);
			imageBox.setVisibility(View.VISIBLE);

			break;
		case AppConstant.KEY_SINGLE_ITEM:
			textItemNumber.setText(getArguments().getString(KEY_ARG) + ""
					+ number);
			imageSingle.setVisibility(View.VISIBLE);
			break;
		case AppConstant.KEY_BIN:
			textItemNumber.setText(getArguments().getString(KEY_ARG) + ""
					+ number);
			imageBin.setVisibility(View.VISIBLE);
			break;

		default:
			break;
		}

	}

	public void launchScanner(View v) {
		if (isCameraAvailable()) {
			Intent intent = new Intent(getActivity(), ZBarScannerActivity.class);
			startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
		} else {
			Toast.makeText(getActivity(), "Rear Facing Camera Unavailable",
					Toast.LENGTH_SHORT).show();
		}
	}

	public boolean isCameraAvailable() {
		PackageManager pm = getActivity().getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	protected void selectImage() {

		final CharSequence[] options = { "Take Photo", "Choose from Gallery",
				"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Add Photo!");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals("Take Photo")) {
					captureImage();

				} else if (options[item].equals("Choose from Gallery")) {
					chooseFromGallery();

				} else if (options[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	protected void chooseFromGallery() {

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_GALLERY);
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		intent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.name());
		startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
	}

	protected void captureImage() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

	}

	/**
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/**
	 * returning image / video
	 */
	private File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHOCO_IMG_" + timeStamp + ".jpg");
		} else if (type == MEDIA_TYPE_GALLERY) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHOCO_IMG_GAL_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 2;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		Log.d("Insample size", inSampleSize + "");
		return inSampleSize;
	}

	private void previewCapturedImage() {
		// TODO Auto-generated method stub
		// bimatp factory
		int rotate = 0;
		ExifInterface exif;
		try {
			exif = new ExifInterface(mediaStorageDir.getAbsolutePath());
			Log.e("mediaStorageDir", "" + mediaStorageDir.getAbsolutePath());

			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			Log.e("orientation", "" + orientation);
			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;
			}
			BitmapFactory.Options options = new BitmapFactory.Options();

			options.inJustDecodeBounds = true;

			// downsizing image as it throws OutOfMemory Exception for larger
			// images
			BitmapFactory.decodeFile(fileUri.getPath(), options);

			// options.inSampleSize = 8;

			int imageHeight = options.outHeight;
			int imageWidth = options.outWidth;
			String imageType = options.outMimeType;

			options.inSampleSize = calculateInSampleSize(options, 250, 250);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;

			Bitmap bitMap = BitmapFactory
					.decodeFile(fileUri.getPath(), options);

			int imageHeight_a = options.outHeight;
			int imageWidth_1 = options.outWidth;

			imageProfile.setImageBitmap(RotateBitmap(bitMap, rotate));

		} catch (Exception e) {
			// TODO: handle exception
		}

		deleteRecursive(mediaStorageDir);

	}

	public static Bitmap RotateBitmap(Bitmap source, float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
				source.getHeight(), matrix, true);
	}

	private void previewGalleryImage(Intent data) {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		// downsizing image as it throws OutOfMemory Exception for larger
		// images
		// options.inSampleSize = 8;
		Uri selectedImage = data.getData();
		String[] filePathColumn = { MediaStore.Images.Media.DATA };

		if (getActivity().getContentResolver() != null) {
			Cursor cursor = getActivity().getContentResolver().query(
					selectedImage, filePathColumn, null, null, null);

			if (cursor != null) {
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String filePath = cursor.getString(columnIndex);
				cursor.close();

				BitmapFactory.decodeFile(filePath, options);
				// bitmap = MediaStore.Images.Media.getBitmap(
				// this.getContentResolver(), data.getData());

				options.inSampleSize = calculateInSampleSize(options, 250, 250);

				// Decode bitmap with inSampleSize set
				options.inJustDecodeBounds = false;

				Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

				int imageHeight_a = options.outHeight;
				int imageWidth_1 = options.outWidth;

				imageProfile.setImageBitmap(bitmap);
				deleteRecursive(mediaStorageDir);
			} else {
				Toast.makeText(getActivity(),
						"Unable to locate image .. Please try again",
						Toast.LENGTH_LONG).show();
			}

		} else {
			Toast.makeText(getActivity(),
					"Unable to locate image .. Please try again",
					Toast.LENGTH_LONG).show();
		}

	}

	public static void deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory()) {
			for (File child : fileOrDirectory.listFiles()) {
				deleteRecursive(child);
			}
		}

		boolean status = fileOrDirectory.delete();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case REQUEST_AUDIO_TO_TEXT_DESCRIPTION:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				edtDescription.setText(text.get(0));
			}
			break;
		case REQUEST_AUDIO_TO_TEXT_VALUE:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				edtPrice.setText(text.get(0));
			}
			break;
		case ZBAR_SCANNER_REQUEST:
			if (resultCode == Activity.RESULT_OK) {
				// Toast.makeText(
				// getActivity(),
				// "Scan Result = "
				// + data.getStringExtra(ZBarConstants.SCAN_RESULT),
				// Toast.LENGTH_SHORT).show();
				if (data != null) {
					barCode = data.getStringExtra(ZBarConstants.SCAN_RESULT);
					textBarcodeNumber.setText(barCode);
					buttonScanOk.setText("Ok");
					buttonScanOk.setTag("Ok");
				}

			} else if (resultCode == Activity.RESULT_CANCELED && data != null) {
				String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
				if (!TextUtils.isEmpty(error)) {
					Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT)
							.show();
				}
			}
			break;
		case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				// successfully captured the image
				// display it in image view
				previewCapturedImage();
			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getActivity(), "User cancelled image capture",
						Toast.LENGTH_SHORT).show();
			} else {
				// failed to capture image
				Toast.makeText(getActivity(), "Sorry! Failed to capture image",
						Toast.LENGTH_SHORT).show();
			}
			break;
		case GALLERY_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				// video successfully recorded
				// preview the recorded video

				previewGalleryImage(data);

			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled recording
				Toast.makeText(getActivity(),
						"User cancelled Gallery selection", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to record video
				Toast.makeText(getActivity(), "Sorry! Process Failed",
						Toast.LENGTH_SHORT).show();
			}
			break;
		}

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (AppConstant.BACKHANDLE.equals("0")) {
			AppConstant.BACKHANDLE = "0";
		} else {
			AppConstant.BACKHANDLE = "0";
			editLocation.setText(MapActivity.name);
		}

	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.id_rel_place:
			addLocationPopuWindow("Place");

			break;
		case R.id.id_rel_shelf:
			addLocationPopuWindow("Shelf");

			break;
		case R.id.id_rel_draw:
			addLocationPopuWindow("Draw");

			break;
		case R.id.id_rel_near:
			addLocationPopuWindow("Near");

			break;
		case R.id.id_rel_gps:
			containerLocation.setVisibility(View.GONE);
			Intent intent = new Intent(getActivity(), MapActivity.class);
			intent.putExtra("item", edtBag.getText().toString());
			startActivity(intent);
			break;
		case R.id.id_rel_add_new:
			addLocationPopuWindow("Add new");

			break;
		case R.id.img_btn_cancel:
			showAlertDialog(getActivity(), "Cancel", "Are you sure to cancel",
					false);

			break;
		case R.id.img_btn_save:

			/* saveNewEntry(); */
			containerpopup.setVisibility(View.VISIBLE);

			break;
		case R.id.btn_logout:
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					getActivity());
			// set title
			alertDialogBuilder.setTitle("LOGOUT");

			// set dialog message
			alertDialogBuilder
					.setMessage("Are you sure for logout")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									SharedPreferences settings = getActivity()
											.getSharedPreferences(
													AppConstant.KEY_APP,
													Context.MODE_PRIVATE);
									settings.edit().clear().commit();
									getActivity().finish();
									Intent intent_home = new Intent(
											getActivity(), LoginActivity.class);
									startActivity(intent_home);

								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									dialog.cancel();
								}
							});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

			break;
		case R.id.img_add:
			getFragmentManager().popBackStack();
			AdditemsFragment openContainerFragment = new AdditemsFragment();
			FragmentManager fm = getFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			ft.setCustomAnimations(R.anim.slide_in_right,
					R.anim.slide_out_left, R.anim.slide_in_left,
					R.anim.slide_out_right);

			ft.replace(R.id.container, openContainerFragment);
			ft.addToBackStack(null);
			ft.commit();

			break;
		case R.id.img_edit:
			getFragmentManager().popBackStack();
			EdititemsFragment edititemsFragment = new EdititemsFragment();
			FragmentManager fme = getFragmentManager();
			FragmentTransaction fte = fme.beginTransaction();
			fte.setCustomAnimations(R.anim.slide_in_right,
					R.anim.slide_out_left, R.anim.slide_in_left,
					R.anim.slide_out_right);

			fte.replace(R.id.container, edititemsFragment);
			fte.addToBackStack(null);
			fte.commit();
			break;
		case R.id.edt_description:
			typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE, "");
			// Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				edtDescription.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					edtDescription.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(),
							REQUEST_AUDIO_TO_TEXT_DESCRIPTION);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				edtDescription.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;
		case R.id.edt_price:
			typingvalue = preferences.getString(AppConstant.KEY_ENTRYTYPE, "");
			// Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				edtPrice.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					edtPrice.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_VALUE);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				edtPrice.setInputType(InputType.TYPE_CLASS_TEXT);
			}

			break;
		default:
			break;
		}

	}

	protected void startRecognition(Activity mainActivity, int RequestCode) {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
		startActivityForResult(intent, RequestCode);
	}

	private void saveNewEntry() {

		List<NameValuePair> parameter = null;

		if (validateForm()) {
			// containerpopup.setVisibility(View.VISIBLE);

			parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(UserId)));
			parameter.add(new BasicNameValuePair("PackageId", String
					.valueOf(packageid)));
			parameter.add(new BasicNameValuePair("PackageDataId", String
					.valueOf(packagedataid)));
			parameter.add(new BasicNameValuePair("Caution", String
					.valueOf(caution)));
			parameter
					.add(new BasicNameValuePair("Color", String.valueOf(color)));
			parameter.add(new BasicNameValuePair("Category", String
					.valueOf(category_id)));
			parameter.add(new BasicNameValuePair("Description", String
					.valueOf(description)));
			parameter.add(new BasicNameValuePair("BarCode", String
					.valueOf(barCode)));
			parameter
					.add(new BasicNameValuePair("Value", String.valueOf(price)));
			parameter.add(new BasicNameValuePair("MemberName", String
					.valueOf(profile)));
			Log.e("parameter save entry", "" + parameter);

			new AsyncSavePackageFormTask(parameter).execute();
		}
	}

	private boolean validateForm() {

		if (barCode.equals("")) {
			ViewUtil.showAlertDialog(getActivity(), "Barcode",
					"Please scan a barcode", true);
			return false;

		}

		else if (color.equals("")
				&& getArguments().getString(KEY_ARG).equals("Bag")) {

			ViewUtil.showAlertDialog(getActivity(), "Color",
					"Please select color", true);
			return false;

		}

		else if (profile.equals("")) {
			ViewUtil.showAlertDialog(getActivity(), "Profile",
					"Please enter name", true);
			return false;
		} else if (category_id.equals("")) {
			ViewUtil.showAlertDialog(getActivity(), "Category",
					"Please select category", true);
			return false;
		} else if (description.equals("")) {
			ViewUtil.showAlertDialog(getActivity(), "Description",
					"Please enter description", true);
			return false;
		} else if (caution.equals("")) {
			ViewUtil.showAlertDialog(getActivity(), "Caution",
					"Please select a caution", true);
			return false;
		} else if (location.equals("")) {
			ViewUtil.showAlertDialog(getActivity(), "Location",
					"Please select location", true);
			return false;
		} else if (price.equals("")
				&& getArguments().getString(KEY_ARG).equals("Single Item")) {
			ViewUtil.showAlertDialog(getActivity(), "Price",
					"Please enter price", true);
			return false;
		}

		return true;
	}

	public class AsyncSavePackageFormTask extends
			AsyncTask<String, Void, String> {

		List<NameValuePair> parameter = null;

		public AsyncSavePackageFormTask(List<NameValuePair> parameter) {
			// TODO Auto-generated constructor stubthis
			this.parameter = parameter;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response save entry", result);
							ViewUtil.hideProgressDialog();
							if (getArguments().getString(KEY_ARG).equals(
									"Single Item")) {
								String message = jObj.getString("message");
								ViewUtil.showAlertDialog(getActivity(), "",
										"Single item successfully saved ", true);
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
										getActivity());
								alertDialogBuilder
										.setMessage(message)
										.setCancelable(false)
										.setPositiveButton(
												"OK",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														// if this button is
														// clicked, close
														// current activity
														dialog.cancel();
														getActivity().finish();
														Intent intent = new Intent(
																getActivity(),
																MainActivity.class);
														intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
																| Intent.FLAG_ACTIVITY_SINGLE_TOP
																| Intent.FLAG_ACTIVITY_NEW_TASK);
														startActivity(intent);
													}
												});

								// create alert dialog
								AlertDialog alertDialog = alertDialogBuilder
										.create();

								// show it
								alertDialog.show();

								// show alert dialog
							} else {
								getFragmentManager().popBackStack();
								AdditemsFragment openContainerFragment = new AdditemsFragment();
								FragmentManager fm = getFragmentManager();
								FragmentTransaction ft = fm.beginTransaction();
								ft.setCustomAnimations(R.anim.slide_in_right,
										R.anim.slide_out_left,
										R.anim.slide_in_left,
										R.anim.slide_out_right);

								ft.replace(R.id.container,
										openContainerFragment);
								ft.addToBackStack(null);
								ft.commit();

								/*
								 * DATABASE.UpdatePackageData(UserId, packageid,
								 * getArguments().getString(KEY_ARG),
								 * packagedataid, color, caution, description,
								 * category_id, barCode, price, barcode2,
								 * location,"Package");
								 */
								edtBag.setText("");
								edtDescription.setText("");
								edtPrice.setText("");
								editBarCode.setText("");
								editCategory.setText("");
								editCaution.setText("");
								editLocation.setText("");
								editBarCode2.setText("");
								buttoncolorpick.setText("");
							}
						} else {
							Log.e("Response", result);
							String Server_message = jObj.getString("message");
							ViewUtil.showAlertDialog(getActivity(), "Error",
									Server_message, true);
							ViewUtil.hideProgressDialog();
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			String result = NetworkConnector.PackageForm(parameter);

			return result;
		}

	}

	public void showAlertDialog(Activity context, String title, String message,
			boolean error) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder
				.setMessage(message)
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								dialog.cancel();

								isInternetPresent = internetcheck
										.isConnectingToInternet();
								if (isInternetPresent) {
									new AsyncDeletePackageDataTask().execute(
											packageid, packagedataid, UserId);

									getActivity().onBackPressed();

								} else {

									long name = DATABASE.DeletePackageData(
											UserId, packageid, packagedataid);
									if (name > 0) {
										getActivity().onBackPressed();
									} else {
										ViewUtil.showAlertDialog(getActivity(),
												"ERROR", "Data not Deleted",
												true);
									}
								}
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public class AsyncDeletePackageDataTask extends
			AsyncTask<String, Void, String> {
		List<NameValuePair> parameter = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response", result);
							ViewUtil.hideProgressDialog();

						} else {
							Log.e("Response", result);

							String Server_message = jObj.getString("message");
							ViewUtil.hideProgressDialog();
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									Server_message, true);

						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("PackageId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("PackageDataId", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[2])));
			String result = NetworkConnector.deletePackageData(parameter);
			return result;
		}

	}

	public class AsyncDeleteCategory extends AsyncTask<String, Integer, String> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result = NetworkConnector.DeleteCategory(params[0],
					params[1]);

			Log.e("result delete---->>", "" + result);
			return result;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			ViewUtil.hideProgressDialog();
		}

	}
}
