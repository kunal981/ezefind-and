package com.appalmighty.ezefind.newentry.fgmt;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.DataBase.DatasourceHandler;
import com.appalmighty.ezefind.login.act.LoginActivity;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.net.NetworkConstant.Api;
import com.appalmighty.ezefind.net.NetworkConstant.Base;
import com.appalmighty.ezefind.newentry.act.PostAPi;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class EdititemsFragment extends Fragment {
	ListView listview_edit;
	public static final int RESULT_OK = -1;
	ListAdapter adapteredit;
	Button backButton, btnLogout, btnhome;
	ArrayList<String> arrayList;
	RelativeLayout rel_popup;
	LinearLayout linearYes, linearNo;
	ImageView imgbtnsave, imgbtnadd;
	ImageButton imgbtnCancel;
	DatasourceHandler Database;
	Cursor mCursor;
	ArrayList<String> Title;
	ArrayList<String> Itemid;
	ArrayList<String> Description;
	ArrayList<String> Image;
	ArrayList<Integer> Attachments;
	ArrayList<String> valueArraylist;
	ArrayList<String> Quntities;
	ArrayList<String> ImageType;
	String UserId, packageid, packagedataid, itemid;
	SharedPreferences shared;
	String image, title, description, headertitle;
	TextView textdate, texttime, textadd;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	DatasourceHandler DATABASE;
	Cursor c;
	File destination;
	int getposition;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.activity_edit_items,
				container, false);
		backButton = (Button) rootView.findViewById(R.id.back_button);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home_);
		Intent intent = getActivity().getIntent();
		textadd = (TextView) rootView.findViewById(R.id.txtaddress);
		textdate = (TextView) rootView.findViewById(R.id.txt_date);
		texttime = (TextView) rootView.findViewById(R.id.txt_time);
		btnLogout = (Button) rootView.findViewById(R.id.btn_logout);
		listview_edit = (ListView) rootView
				.findViewById(R.id.listview_edit_items);
		rel_popup = (RelativeLayout) rootView.findViewById(R.id.container_pop);
		linearYes = (LinearLayout) rootView.findViewById(R.id.linearYes);
		linearNo = (LinearLayout) rootView.findViewById(R.id.linearlayoutNo);
		imgbtnCancel = (ImageButton) rootView.findViewById(R.id.imgbtn_cancel);
		imgbtnsave = (ImageView) rootView.findViewById(R.id.img_btn_save);
		imgbtnadd = (ImageView) rootView.findViewById(R.id.img_add);
		Itemid = new ArrayList<String>();
		Title = new ArrayList<String>();
		Description = new ArrayList<String>();
		Image = new ArrayList<String>();
		ImageType = new ArrayList<String>();
		Attachments = new ArrayList<Integer>();
		Quntities = new ArrayList<String>();
		valueArraylist = new ArrayList<String>();
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = shared.getString(AppConstant.KEY_USER_ID, "");
		Log.e("UserId", "" + UserId);
		textdate.setText(shared.getString(AppConstant.KEY_Date, ""));
		texttime.setText(shared.getString(AppConstant.KEY_Time, ""));

		DATABASE = new DatasourceHandler(getActivity());
		internetcheck = new ConnectionDetector(getActivity());
		adapteredit = new ListAdapter(getActivity(), Title, Description, Image,
				Itemid, Attachments, Quntities, ImageType, valueArraylist);

		listview_edit.setAdapter(adapteredit);
		Log.e("KEY_Address", "" + shared.getString(AppConstant.KEY_Address, ""));
		headertitle = shared.getString(AppConstant.KEY_Address, "");
		textadd.setText(shared.getString(AppConstant.KEY_Address, ""));
		packageid = shared.getString(AppConstant.KEY_PackageId, "");
		packagedataid = shared.getString(AppConstant.KEY_PackageDataId, "");

		isInternetPresent = internetcheck.isConnectingToInternet();
		if (isInternetPresent) {
			new AsyncGetitemTask().execute(packageid, packagedataid, UserId);

		} else {
			c = DATABASE.getItem(packageid, packagedataid, UserId);

			if (c.getCount() != 0) {
				Log.e("c.coount", "" + c.getCount());

				while (c.moveToNext()) {

					ImageType.add(c.getString(9));
					Title.add(c.getString(4));
					Description.add(c.getString(5));
					Image.add(c.getString(8));
					Log.e("oncreae", "" + c.getString(8));
					Attachments.add(Integer.parseInt(c.getString(7)));
					Quntities.add(c.getString(6));
					Itemid.add(c.getString(3));

				}
				adapteredit.notifyDataSetChanged();

			}
		}

		ContainerDetailsFragment entryFragment = new ContainerDetailsFragment();

		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		imgbtnadd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Intent intent_additems = new Intent(getActivity(),
				// AddItemsActivity.class);
				// startActivity(intent_additems);
				AdditemsFragment openContainerFragment = new AdditemsFragment();
				FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.setCustomAnimations(R.anim.slide_in_right,
						R.anim.slide_out_left, R.anim.slide_in_left,
						R.anim.slide_out_right);
				// Bundle bundle = new Bundle();
				// bundle.putString("Title_Open", titlekey);
				// bundle.putString("Packageid", Packageid1.get(position));
				// bundle.putString("packagedataid",
				// Packagedataid1.get(position));
				// bundle.putString("type", type1.get(position));
				// bundle.putString("loction", location1.get(position));
				// bundle.putString("value", value1.get(position));
				// bundle.putString("address", addressheader.getText()
				// .toString());
				// openContainerFragment.setArguments(bundle);
				ft.replace(R.id.container, openContainerFragment);
				ft.addToBackStack(null);
				ft.commit();
			}
		});
		imgbtnsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// rel_popup.setVisibility(View.VISIBLE);

			}
		});
		btnLogout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// rel_popup.setVisibility(View.VISIBLE);
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				// set title
				alertDialogBuilder.setTitle("LOGOUT");

				// set dialog message
				alertDialogBuilder
						.setMessage("Are you sure for logout")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										SharedPreferences settings = getActivity()
												.getSharedPreferences(
														AppConstant.KEY_APP,
														Context.MODE_PRIVATE);
										settings.edit().clear().commit();
										getActivity().finish();
										Intent intent_home = new Intent(
												getActivity(),
												LoginActivity.class);
										startActivity(intent_home);

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		});

		imgbtnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rel_popup.setVisibility(View.GONE);
			}
		});

		btnhome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().finish();
				Intent intent = new Intent(getActivity(), MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);

			}
		});
		return rootView;
	}

	class ListAdapter extends BaseAdapter {

		private LayoutInflater inflater;
		ArrayList<String> adtitle = new ArrayList<String>();
		ArrayList<String> addescription = new ArrayList<String>();
		ArrayList<String> adimage = new ArrayList<String>();
		ArrayList<String> aditemid = new ArrayList<String>();
		ArrayList<Integer> adattachments = new ArrayList<Integer>();
		ArrayList<String> adquntities = new ArrayList<String>();
		ArrayList<String> adimageType = new ArrayList<String>();
		ArrayList<String> value_ = new ArrayList<String>();
		DisplayImageOptions displayImageOptions;
		Context context;
		String selitemid, itemtitle, itemdes, itemimag;

		boolean my = false;

		public ListAdapter(Context context, ArrayList<String> title,
				ArrayList<String> date, ArrayList<String> image,
				ArrayList<String> itemid, ArrayList<Integer> attachments,
				ArrayList<String> quntities, ArrayList<String> imageType,
				ArrayList<String> valueArraylist) {

			adtitle = title;
			addescription = date;
			adimage = image;
			aditemid = itemid;
			adattachments = attachments;
			adquntities = quntities;
			adimageType = imageType;
			value_ = valueArraylist;
			displayImageOptions = setupImageLoader();
			this.context = context;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return adtitle.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return adtitle.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			final ViewHolder holder;
			BitmapFactory.Options options;
			if (inflater == null)
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.activity_edititems_list, null);
				holder = new ViewHolder();

				holder.parentLayout = (RelativeLayout) convertView
						.findViewById(R.id.relative_parent_);
				holder.btnOpen = (Button) convertView
						.findViewById(R.id.btn_open_zip_);
				holder.btnQuantity = (Button) convertView
						.findViewById(R.id.btn_quantity_);
				holder.btnEdit = (Button) convertView
						.findViewById(R.id.btn_edit_);
				holder.btnDelete = (Button) convertView
						.findViewById(R.id.btn_delete);
				holder.edtTitle = (EditText) convertView
						.findViewById(R.id.edit_shirts_name);

				holder.edtDesc = (EditText) convertView
						.findViewById(R.id.edt_descriptions);
				holder.imgIcon = (ImageView) convertView
						.findViewById(R.id.img_shirts__);
				holder.layout_edittexts = (LinearLayout) convertView
						.findViewById(R.id.linear_layout_e);
				holder.txtTitle = (TextView) convertView
						.findViewById(R.id.tv_shirts_name);
				holder.txtDesc = (TextView) convertView
						.findViewById(R.id.tv_description);
				holder.totlattachment = (TextView) convertView
						.findViewById(R.id.Totalattachment);
				holder.totalquantity = (TextView) convertView
						.findViewById(R.id.ToatalQuantity);
				holder.txtValue = (TextView) convertView
						.findViewById(R.id.txt_edit_value);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.txtTitle.setText(adtitle.get(position));
			holder.txtDesc.setText(addescription.get(position));
			holder.totlattachment.setText(String.valueOf(adattachments
					.get(position)));
			holder.totalquantity.setText(adquntities.get(position));
			holder.txtValue.setText("$" + " " + value_.get(position));

			if (adimageType.get(position).equals("64")) {

				File image = new File(adimage.get(position));
				Log.e("image", "" + image);
				options = new BitmapFactory.Options();
				options.inSampleSize = 8;

				Bitmap bitmap = BitmapFactory.decodeFile(
						image.getAbsolutePath(), options);
				holder.imgIcon.setImageBitmap(bitmap);

			} else {
				ImageLoader.getInstance().displayImage(
						String.valueOf(adimage.get(position)), holder.imgIcon);
			}

			// holder.btnOpen.setVisibility(View.VISIBLE);
			// holder.btnQuantity.setVisibility(View.VISIBLE);
			// holder.btnEdit.setVisibility(View.GONE);
			// holder.btnDelete.setVisibility(View.GONE);
			// holder.edtTitle.setSelected(false);
			// holder.edtDesc.setSelected(false);
			// holder.imgIcon.setSelected(false);

			// holder.parentLayout
			// .setOnLongClickListener(new OnLongClickListener() {
			//
			// @Override
			// public boolean onLongClick(View v) {
			// // TODO Auto-generated method stub
			// holder.btnOpen.setVisibility(View.GONE);
			// holder.btnQuantity.setVisibility(View.GONE);
			// holder.btnEdit.setVisibility(View.VISIBLE);
			// holder.btnDelete.setVisibility(View.VISIBLE);
			// return false;
			// }
			// });
			holder.imgIcon.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (holder.btnEdit.getVisibility() == View.VISIBLE) {
						getposition = position;
						selectImage();
					} else {

						ContainerDetailsFragment openContainerFragment = new ContainerDetailsFragment();
						FragmentManager fm = getFragmentManager();
						FragmentTransaction ft = fm.beginTransaction();
						ft.setCustomAnimations(R.anim.slide_in_right,
								R.anim.slide_out_left, R.anim.slide_in_left,
								R.anim.slide_out_right);
						Bundle bundle = new Bundle();

						bundle.putString("itemid", aditemid.get(position));

						openContainerFragment.setArguments(bundle);
						ft.replace(R.id.container, openContainerFragment);
						ft.addToBackStack(null);
						ft.commit();

					}

				}
			});
			holder.parentLayout.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					holder.btnOpen.setVisibility(View.GONE);
					holder.btnQuantity.setVisibility(View.GONE);
					holder.btnEdit.setVisibility(View.VISIBLE);
					holder.btnDelete.setVisibility(View.VISIBLE);
					holder.layout_edittexts.setVisibility(View.VISIBLE);
					holder.totalquantity.setVisibility(View.GONE);
					holder.totlattachment.setVisibility(View.GONE);

				}
			});

			holder.btnEdit.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// if (my == false) {
					// holder.btnEdit.setText("Save");
					// holder.edtTitle.setText(adtitle.get(position));
					// holder.edtDesc.setText(addescription.get(position));
					// holder.edtTitle.setVisibility(View.VISIBLE);
					// holder.edtDesc.setVisibility(View.VISIBLE);
					// holder.txtDesc.setVisibility(View.GONE);
					// holder.txtTitle.setVisibility(View.GONE);
					// holder.edtTitle.setSelected(true);
					// holder.edtDesc.setSelected(true);
					// holder.imgIcon.setSelected(true);
					// // image
					//
					// my = true;
					// } else if (my == true) {
					// rel_popup.setVisibility(View.VISIBLE);
					// holder.btnEdit.setText("Edit");
					// holder.edtTitle.setVisibility(View.GONE);
					// holder.edtDesc.setVisibility(View.GONE);
					// holder.txtDesc.setVisibility(View.VISIBLE);
					// holder.txtTitle.setVisibility(View.VISIBLE);
					// holder.edtTitle.setSelected(false);
					// holder.edtDesc.setSelected(false);
					// holder.imgIcon.setSelected(false);
					// holder.btnOpen.setVisibility(View.VISIBLE);
					// holder.btnQuantity.setVisibility(View.VISIBLE);
					// holder.btnEdit.setVisibility(View.GONE);
					// holder.btnDelete.setVisibility(View.GONE);
					// holder.layout_edittexts.setVisibility(View.GONE);
					// title = holder.edtTitle.getText().toString();
					// description = holder.edtDesc.getText().toString();
					// // image = adimage.get(position);
					//
					// holder.edtTitle.setText(title);
					// holder.edtDesc.setText(description);
					//
					// getposition = position;
					// ByttonAction("Save", aditemid.get(position), position);
					//
					// my = false;
					// }
					ContainerDetailsFragment openContainerFragment = new ContainerDetailsFragment();
					FragmentManager fm = getFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					ft.setCustomAnimations(R.anim.slide_in_right,
							R.anim.slide_out_left, R.anim.slide_in_left,
							R.anim.slide_out_right);
					Bundle bundle = new Bundle();
					bundle.putString("Title_Open",
							getArguments().getString("Title_Open"));
					bundle.putString("itemid", aditemid.get(position));

					openContainerFragment.setArguments(bundle);
					ft.replace(R.id.container, openContainerFragment);
					ft.addToBackStack(null);
					ft.commit();

				}
			});

			holder.btnDelete.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					selitemid = aditemid.get(position);
					itemtitle = adtitle.get(position);
					itemdes = addescription.get(position);
					itemimag = adimage.get(position);
					rel_popup.setVisibility(View.VISIBLE);
					ByttonAction("Delete", aditemid.get(position), position);

				}
			});

			return convertView;
		}

		protected void ByttonAction(final String string, final String string2,
				final int position) {
			// TODO Auto-generated method stub

			linearYes.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					/*
					 * linearYes.setSelected(true); linearNo.setSelected(false);
					 */
					if (string.equals("Save")) {
						// if (isInternetPresent) {
						// if (title.equals("") || description.equals("")) {
						//
						// ViewUtil.showAlertDialog(getActivity(),
						// "Error",
						// "Please Enter Title Or Description",
						// true);
						// } else {
						// itemid = string2;
						//
						// new AsyncUpdatePackageItemTask().execute();
						// // new AsyncUpdatePackageItemTask().execute(
						// // packageid, packagedataid, title,
						// // description, image, UserId, string2);
						// }
						// } else {
						//
						//
						//
						//
						// }
						//
						// rel_popup.setVisibility(View.INVISIBLE);
					} else {
						if (isInternetPresent) {

							new AsyncDeleteitemTask().execute(packageid,
									packagedataid, UserId, string2);
						} else {

							try {
								DATABASE.DeletePackageDataitem(UserId,
										packageid, packagedataid,
										aditemid.get(position));
								DATABASE.DeletePicklistitem(
										aditemid.get(position), UserId);
								adtitle.remove(position);
								addescription.remove(position);
								adimage.remove(position);
								notifyDataSetChanged();

							} catch (Exception e) {

								e.printStackTrace();
							}
						}

						rel_popup.setVisibility(View.INVISIBLE);

					}

				}
			});
			linearNo.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					linearYes.setSelected(false);
					linearNo.setSelected(true);
					rel_popup.setVisibility(View.INVISIBLE);
				}
			});

		}

		protected void selectImage() {
			// TODO Auto-generated method stub
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			destination = new File(Environment.getExternalStorageDirectory(),
					System.currentTimeMillis() + ".jpg");
			intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
			startActivityForResult(intent, 100);
		}

		public DisplayImageOptions setupImageLoader() {
			return new DisplayImageOptions.Builder().cacheInMemory(true)
					.cacheOnDisk(true).considerExifParams(true)
					.displayer(new RoundedBitmapDisplayer(1)).build();

		}

		// public class AsyncUpdatePackageItemTask extends
		// AsyncTask<String, Void, String> {
		// ViewHolder holder;
		//
		// @Override
		// protected void onPreExecute() {
		// super.onPreExecute();
		// ViewUtil.showProgressDialog(getActivity());
		// }
		//
		// @Override
		// protected void onPostExecute(String result) {
		// super.onPostExecute(result);
		// try {
		//
		// JSONObject jObj = new JSONObject(result);
		//
		// if (jObj.getBoolean("success")) {
		// Log.e("Response", result);
		// adtitle.set(getposition, title);
		// addescription.set(getposition, description);
		// if (image != null) {
		// adimage.set(getposition, image);
		// adimageType.set(getposition, "64");
		// } else {
		// adimage.set(getposition, adimage.get(getposition));
		// adimageType.set(getposition, "url");
		// }
		// ViewUtil.hideProgressDialog();
		// adapteredit.notifyDataSetChanged();
		// } else {
		// Log.e("Response", result);
		//
		// JSONObject phone = jObj.getJSONObject("message");
		// String Server_message = phone.getString("Account");
		// ViewUtil.hideProgressDialog();
		//
		// /*
		// * ViewUtil.showAlertDialog(getActivity(),
		// * "Login Failed", Server_message, true);
		// */
		//
		// }
		// } catch (JSONException e) {
		// e.printStackTrace();
		// }
		// }
		//
		// @Override
		// protected String doInBackground(String... params) {
		//
		// List<NameValuePair> parameter = new LinkedList<NameValuePair>();
		// parameter.add(new BasicNameValuePair("PackageId", String
		// .valueOf(params[0])));
		// parameter.add(new BasicNameValuePair("PackageDataId", String
		// .valueOf(params[1])));
		// parameter.add(new BasicNameValuePair("Title", String
		// .valueOf(params[2])));
		// parameter.add(new BasicNameValuePair("Description", String
		// .valueOf(params[3])));
		//
		// parameter.add(new BasicNameValuePair("Image", String
		// .valueOf(params[4])));
		// parameter.add(new BasicNameValuePair("UserId", String
		// .valueOf(params[5])));
		// parameter.add(new BasicNameValuePair("ItemId", String
		// .valueOf(params[6])));
		//
		// Log.e("parameter", "" + parameter);
		// String result = NetworkConnector.UpdatePackageItem(parameter);
		// return result;
		// }
		//
		// }

		private class AsyncUpdatePackageItemTask extends
				AsyncTask<String, Integer, String> {
			@Override
			protected void onPreExecute() {
				// setting progress bar to zero

				ViewUtil.showProgressDialog(getActivity());

				super.onPreExecute();
			}

			@Override
			protected String doInBackground(String... params) {

				return uploadFile();

			}

			private String uploadFile() {
				String responseString = null;

				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(Base.PACKAGEAPI
						+ Api.UPDATEPACKAGEDATAITEM);
				// HttpPost httppost = new HttpPost(Base.PACKAGEAPI + Api.TEST);
				Log.e("htt", "" + httppost);
				try {

					PostAPi entity = new PostAPi(getActivity());
					try {
						Log.e("destination", "" + destination);
						entity.addPart("PackageId", new StringBody(packageid));
						Log.e("packageid", "" + packageid);
						entity.addPart("PackageDataId", new StringBody(
								packagedataid));
						Log.e("packagedataid", "" + packagedataid);
						entity.addPart("Title", new StringBody(title));
						Log.e("title", "" + title);
						entity.addPart("Description", new StringBody(
								description));
						Log.e("description", "" + description);

						entity.addPart("UserId", new StringBody(UserId));

						Log.e("UserId", "" + UserId);
						entity.addPart("ItemId", new StringBody(itemid));

						Log.e("itemid", "" + itemid);
						entity.addPart("Image", new FileBody(destination));
						Log.e("destination", "" + destination);
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}

					httppost.setEntity(entity);
					// file.delete();
					// Making server call
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity r_entity = response.getEntity();

					int statusCode = response.getStatusLine().getStatusCode();
					if (statusCode == 200) {
						// Server response
						responseString = EntityUtils.toString(r_entity);
					} else {
						Log.e("else", "" + statusCode);
						responseString = "Error occurred! Http Status Code: "
								+ statusCode;
					}

				} catch (ClientProtocolException e) {
					Log.e("Client", "" + e.toString());
					responseString = e.toString();
				} catch (IOException e) {
					Log.e("Io", "" + e.toString());
					responseString = e.toString();
				}

				return responseString;

			}

			@Override
			protected void onPostExecute(String result) {

				super.onPostExecute(result);
				Log.e("", "Response from server: " + result);

				try {
					isInternetPresent = internetcheck.isConnectingToInternet();
					if (isInternetPresent) {
						if (result.equals("error")) {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"SomeThing Wrong", false);
							ViewUtil.hideProgressDialog();
						} else {

							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("Response", result);

								adtitle.set(getposition, title);
								addescription.set(getposition, description);
								JSONObject phone = jObj
										.getJSONObject("message");

								adimage.set(getposition,
										phone.getString("Image"));
								adimageType.set(getposition, "url");

								// adimage.set(getposition,
								// phone.getString("Image"));
								// adimageType.set(getposition, "url");

								ViewUtil.hideProgressDialog();
								adapteredit.notifyDataSetChanged();
							} else {
								Log.e("Response", result);
								String Server_message = jObj
										.getString("message");

								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);
								ViewUtil.hideProgressDialog();

								/*
								 * ViewUtil.showAlertDialog(getActivity(),
								 * "Login Failed", Server_message, true);
								 */

							}
						}
					} else {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"No Internet Connection", false);
						ViewUtil.hideProgressDialog();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

		}

		public class AsyncDeleteitemTask extends
				AsyncTask<String, Void, String> {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				ViewUtil.showProgressDialog(getActivity());
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {
					isInternetPresent = internetcheck.isConnectingToInternet();
					if (isInternetPresent) {
						if (result.equals("error")) {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"SomeThing Wrong", false);
							ViewUtil.hideProgressDialog();
						} else {

							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("Response", result);
								adtitle.remove(getposition);
								addescription.remove(getposition);
								adimage.remove(getposition);
								aditemid.remove(getposition);
								adapteredit.notifyDataSetChanged();
								ViewUtil.hideProgressDialog();
							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");

								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);
								ViewUtil.hideProgressDialog();

							}
						}
					} else {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"No Internet Connection", false);
						ViewUtil.hideProgressDialog();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			protected String doInBackground(String... params) {

				List<NameValuePair> parameter = new LinkedList<NameValuePair>();

				parameter.add(new BasicNameValuePair("PackageId", String
						.valueOf(params[0])));
				parameter.add(new BasicNameValuePair("PackageDataId", String
						.valueOf(params[1])));
				parameter.add(new BasicNameValuePair("UserId", String
						.valueOf(params[2])));
				parameter.add(new BasicNameValuePair("ItemId", String
						.valueOf(params[3])));
				Log.e("parameter", "" + parameter);
				String result = NetworkConnector.DeletPackageItem(parameter);
				return result;
			}

		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// ViewHolder holder = null;
		Log.e("file", "" + Uri.fromFile(destination));
		if (resultCode == RESULT_OK) {

			try {

				Log.e("gee", "" + destination);
				Image.set(getposition, String.valueOf(destination));
				ImageType.set(getposition, "64");
				adapteredit.notifyDataSetChanged();
			} catch (Exception e) {
				Log.e("Crash", "" + e.getMessage());
			}

		}
	}

	class ViewHolder {
		Button btnOpen, btnQuantity, btnEdit, btnDelete;
		RelativeLayout parentLayout;
		LinearLayout layout_edittexts;
		EditText edtTitle, edtDesc;
		TextView txtTitle, txtDesc, totlattachment, totalquantity, txtValue;
		ImageView imgIcon;

	}

	public class AsyncGetitemTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response", result);

							JSONArray array = jObj.getJSONArray("message");
							for (int i = 0; i < array.length(); i++) {
								JSONObject object = array.getJSONObject(i);

								String quantity = object.getString("Quantity");
								int attachment = object
										.getInt("AttachmentCount");
								ImageType.add("url");
								Itemid.add(object.getString("ItemId"));
								Title.add(object.getString("Title"));
								Description
										.add(object.getString("Description"));
								Image.add("http://beta.brstdev.com/yiiezefind"
										+ object.getString("Image"));
								Attachments.add(attachment);
								Quntities.add(quantity);
								String value = object.getString("Value");
								Log.e("value", "" + value);
								if (value != null) {
									valueArraylist.add(value);

								}

							}
							adapteredit.notifyDataSetChanged();

							ViewUtil.hideProgressDialog();
						}

						else {
							Log.e("Response", result);

							String Server_message = jObj.getString("message");
							Toast.makeText(getActivity(), Server_message, 2000)
									.show();
							ViewUtil.hideProgressDialog();
							ViewUtil.showAlertDialog(getActivity(), "Error",
									Server_message, true);

						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("PackageId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("PackageDataId", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[2])));

			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.GetPackageItem(parameter);
			return result;
		}

	}

}
