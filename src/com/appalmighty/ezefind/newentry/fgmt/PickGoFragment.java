package com.appalmighty.ezefind.newentry.fgmt;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.DataBase.DatasourceHandler;
import com.appalmighty.ezefind.DataBase.NewEntryDataBase;
import com.appalmighty.ezefind.login.act.LoginActivity;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class PickGoFragment extends Fragment {

	public static final String TAG = PickGoFragment.class.getName();

	private ListView listView;
	private ListAdapter adapter;
	private Button btn_start_new, btnhome, btnLogout;
	// private boolean clicked = true;
	private TextView txtTitle;
	public static final String KEY = "key";
	String key, strKey;
	DatasourceHandler Database;
	Cursor mCursor, v;

	ArrayList<String> Title;
	ArrayList<String> Dates;
	ArrayList<String> size;
	ArrayList<String> profile;
	ArrayList<String> itemimgeType;
	ArrayList<String> itemimgeType1;
	ArrayList<String> itemimgeType2;
	ArrayList<String> itemimges;
	ArrayList<String> itemimges1;
	ArrayList<String> itemimges2;
	ArrayList<String> packageid;
	ArrayList<String> Address;
	String UserId, itemid, Type;
	SharedPreferences shared;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	DatasourceHandler DATABASE;
	int check = 10000, getposition;
	String PackageDataidsss;
	Cursor itemidpic;

	public PickGoFragment() {
	}

	public static PickGoFragment newInstance(String key) {
		PickGoFragment fragment = new PickGoFragment();
		Bundle bundle = new Bundle();
		bundle.putString(KEY, key);
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_home_pack_go,
				container, false);
		System.gc();
		btn_start_new = (Button) rootView
				.findViewById(R.id.id_button_start_new_package);
		listView = (ListView) rootView.findViewById(R.id.listView_items);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		txtTitle = (TextView) rootView.findViewById(R.id.txt_title_);
		btnLogout = (Button) rootView.findViewById(R.id.btn_logout);
		Title = new ArrayList<String>();
		Dates = new ArrayList<String>();
		size = new ArrayList<String>();
		profile = new ArrayList<String>();
		itemimgeType = new ArrayList<String>();
		itemimgeType1 = new ArrayList<String>();
		itemimgeType2 = new ArrayList<String>();
		itemimges = new ArrayList<String>();
		itemimges1 = new ArrayList<String>();
		itemimges2 = new ArrayList<String>();
		packageid = new ArrayList<String>();
		Address = new ArrayList<String>();

		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = shared.getString(AppConstant.KEY_USER_ID, "");
		itemid = shared.getString(AppConstant.KEY_ItemId, "");
		internetcheck = new ConnectionDetector(getActivity());
		intHeaderTitle();
		intType();
		adapter = new ListAdapter(getActivity(), packageid, Title, Dates, size,
				profile, itemimges, itemimges1, itemimges2, Address,
				itemimgeType, itemimgeType1, itemimgeType2);
		listView.setAdapter(adapter);

		btnLogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				// set title
				alertDialogBuilder.setTitle("LOGOUT");

				// set dialog message
				alertDialogBuilder
						.setMessage("Are you sure for logout")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										SharedPreferences settings = getActivity()
												.getSharedPreferences(
														AppConstant.KEY_APP,
														Context.MODE_PRIVATE);
										settings.edit().clear().commit();
										getActivity().finish();
										Intent intent_home = new Intent(
												getActivity(),
												LoginActivity.class);
										startActivity(intent_home);

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			}
		});
		isInternetPresent = internetcheck.isConnectingToInternet();

		if (isInternetPresent) {
			if (txtTitle.getText().toString().equals("Pack&Go Log")) {
				new AsyncGetPackageTask().execute(UserId, "Package");
			} else if (txtTitle.getText().toString().equals("Pick List Log")) {
				new AsyncGetPickListTask().execute(UserId);
			} else if (txtTitle.getText().toString().equals("Inventory Log")) {
				new AsyncGetPackageTask().execute(UserId, "Inventory");
			}

		} else {
			DATABASE = new DatasourceHandler(getActivity());

			if (txtTitle.getText().toString().equals("Pack&Go Log")) {
				mCursor = DATABASE.viewData(UserId, "Package");
				Log.e("ViewDatapick", "" + mCursor.getCount());
				if (mCursor.getCount() != 0) {

					while (mCursor.moveToNext()) {

						packageid
								.add(mCursor.getString(mCursor
										.getColumnIndex(NewEntryDataBase.KEY_PACKAGEID)));
						v = DATABASE
								.GetDataCount(
										UserId,
										mCursor.getString(mCursor
												.getColumnIndex(NewEntryDataBase.KEY_PACKAGEID)));

						Log.e("GetDatCountpick", "" + v.getCount());
						if (v.getCount() >= 1) {
							if (v.moveToFirst()) {

							}
						} else if (v.getCount() == 0) {
							itemimgeType.add("url");
							itemimgeType1.add("url");
							itemimgeType2.add("url");

							itemimges
									.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
							itemimges1
									.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
							itemimges2
									.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
						}

						size.add(String.valueOf(v.getCount()));
						Title.add(mCursor.getString(mCursor
								.getColumnIndex(NewEntryDataBase.KEY_TITLE)));
						Address.add(mCursor.getString(mCursor
								.getColumnIndex(NewEntryDataBase.KEY_ADDRESS)));
						SimpleDateFormat input = new SimpleDateFormat(
								"yyyy-MM-dd");
						SimpleDateFormat output = new SimpleDateFormat(
								"dd-MM-yyyy");

						Date oneWayTripDate;
						try {
							oneWayTripDate = input
									.parse(mCursor.getString(mCursor
											.getColumnIndex(NewEntryDataBase.KEY_STARTDATE)));
							Dates.add(output.format(oneWayTripDate));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						profile.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");

						if (v.getCount() != 0 && v.getCount() >= 1) {
							Cursor cursor = DATABASE
									.getAllPackageItem(
											UserId,
											mCursor.getString(mCursor
													.getColumnIndex(NewEntryDataBase.KEY_PACKAGEIDdata)),
											v.getString(v
													.getColumnIndex(NewEntryDataBase.KEY_PACKAGEDATAIDdata)));
							Log.e("getAllPackageItempick",
									"" + cursor.getCount());
							if (cursor.getCount() == 0) {
								itemimgeType.add("url");
								itemimgeType1.add("url");
								itemimgeType2.add("url");

								itemimges
										.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
								itemimges1
										.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
								itemimges2
										.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
							}
							if (cursor.getCount() != 0) {
								if (cursor.moveToFirst()) {

									if (cursor.getCount() == 1) {
										Log.e("1", "1");
										itemimgeType.add("64");

										itemimges
												.add(cursor.getString(cursor
														.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));
										itemimgeType1.add("url");
										itemimges1
												.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
										itemimgeType2.add("url");
										itemimges2
												.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
									}
								}
								if (cursor.getCount() == 2) {
									Log.e("2", "2");
									itemimgeType.add("64");
									itemimges
											.add(cursor.getString(cursor
													.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));

									while (cursor.moveToNext()) {

										itemimgeType1.add("64");
										itemimges1
												.add(cursor.getString(cursor
														.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));
									}

									itemimgeType2.add("url");
									itemimges2
											.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");

								}

								if (cursor.getCount() >= 3) {

									Log.e("3", "3");
									itemimgeType.add("64");
									itemimgeType1.add("64");
									itemimgeType2.add("64");
									itemimges
											.add(cursor.getString(cursor
													.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));
									while (cursor.moveToNext()) {
										if (check == 10000) {
											itemimges1
													.add(cursor.getString(cursor
															.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));
											check = 10001;
										} else if (10001 == check) {
											itemimges2
													.add(cursor.getString(cursor
															.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));
											check = 10000;
										} else {

										}

									}

								}
							}
						}
					}

					mCursor.close();
				} else {
					ViewUtil.showAlertDialog(getActivity(), "Error",
							"No Data Found", true);
				}
			}

			else if (txtTitle.getText().toString().equals("Inventory Log")) {
				mCursor = DATABASE.viewData(UserId, "Inventory");

				if (mCursor.getCount() != 0) {

					while (mCursor.moveToNext()) {
						// check=0;
						packageid
								.add(mCursor.getString(mCursor
										.getColumnIndex(NewEntryDataBase.KEY_PACKAGEID)));
						v = DATABASE
								.GetDataCount(
										UserId,
										mCursor.getString(mCursor
												.getColumnIndex(NewEntryDataBase.KEY_PACKAGEID)));

						if (v.getCount() == 1) {
							if (v.moveToFirst()) {

							}
						} else {
						}

						size.add(String.valueOf(v.getCount()));
						Title.add(mCursor.getString(mCursor
								.getColumnIndex(NewEntryDataBase.KEY_TITLE)));
						Address.add(mCursor.getString(mCursor
								.getColumnIndex(NewEntryDataBase.KEY_ADDRESS)));
						SimpleDateFormat input = new SimpleDateFormat(
								"yyyy-MM-dd");
						SimpleDateFormat output = new SimpleDateFormat(
								"dd-MM-yyyy");

						Date oneWayTripDate;
						try {
							oneWayTripDate = input
									.parse(mCursor.getString(mCursor
											.getColumnIndex(NewEntryDataBase.KEY_STARTDATE)));
							Dates.add(output.format(oneWayTripDate));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						profile.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");

						if (v.getCount() != 0) {
							Cursor cursor = DATABASE
									.getAllPackageItem(
											UserId,
											mCursor.getString(mCursor
													.getColumnIndex(NewEntryDataBase.KEY_PACKAGEIDdata)),
											v.getString(v
													.getColumnIndex(NewEntryDataBase.KEY_PACKAGEDATAIDdata)));

							if (cursor.getCount() == 0) {
								itemimgeType.add("url");
								itemimgeType1.add("url");
								itemimgeType2.add("url");

								itemimges
										.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
								itemimges1
										.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
								itemimges2
										.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
							}
							if (cursor.getCount() != 0) {
								if (cursor.moveToFirst()) {

									if (cursor.getCount() == 1) {
										Log.e("1", "1");
										itemimgeType.add("64");

										itemimges
												.add(cursor.getString(cursor
														.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));
										itemimgeType1.add("url");
										itemimges1
												.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
										itemimgeType2.add("url");
										itemimges2
												.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
									}
								}
								if (cursor.getCount() == 2) {
									Log.e("2", "2");
									itemimgeType.add("64");
									itemimges
											.add(cursor.getString(cursor
													.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));

									while (cursor.moveToNext()) {

										itemimgeType1.add("64");
										itemimges1
												.add(cursor.getString(cursor
														.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));
									}

									itemimgeType2.add("url");
									itemimges2
											.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");

								}

								if (cursor.getCount() >= 3) {

									Log.e("3", "3");
									itemimgeType.add("64");
									itemimgeType1.add("64");
									itemimgeType2.add("64");
									itemimges
											.add(cursor.getString(cursor
													.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));
									while (cursor.moveToNext()) {
										if (check == 10000) {
											itemimges1
													.add(cursor.getString(cursor
															.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));
											check = 10001;
										} else if (10001 == check) {
											itemimges2
													.add(cursor.getString(cursor
															.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));
											check = 10000;
										} else {

										}

									}

								}
							}
						} else {
							itemimgeType.add("url");
							itemimgeType1.add("url");
							itemimgeType2.add("url");

							itemimges
									.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
							itemimges1
									.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
							itemimges2
									.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");

						}
					}

					mCursor.close();
				} else {
					ViewUtil.showAlertDialog(getActivity(), "Error",
							"No Data Found", true);
				}
			} else if (txtTitle.getText().toString().equals("Pick List Log")) {
				mCursor = DATABASE.viewPicklistData(UserId);

				if (mCursor.getCount() != 0) {

					while (mCursor.moveToNext()) {
						Title.add(mCursor.getString(mCursor
								.getColumnIndex(NewEntryDataBase.KEY_PICKLISTTITLE)));
						Address.add("null");
						SimpleDateFormat input = new SimpleDateFormat(
								"yyyy-MM-dd");
						SimpleDateFormat output = new SimpleDateFormat(
								"dd-MM-yyyy");

						Date oneWayTripDate;
						try {
							oneWayTripDate = input
									.parse(mCursor.getString(mCursor
											.getColumnIndex(NewEntryDataBase.KEY_PICKLISTSTARTDATE)));
							Dates.add(output.format(oneWayTripDate));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						packageid
								.add(mCursor.getString(mCursor
										.getColumnIndex(NewEntryDataBase.KEY_PICKLISTID)));
						Cursor v = DATABASE
								.GetPicklistitemcount(
										UserId,
										mCursor.getString(mCursor
												.getColumnIndex(NewEntryDataBase.KEY_PICKLISTID)));
						size.add(String.valueOf(v.getCount()));
						itemimgeType.add("url");
						itemimgeType1.add("url");
						itemimgeType2.add("url");

						itemimges
								.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
						itemimges1
								.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
						itemimges2
								.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
						if (v.getCount() != 0) {
							while (v.moveToNext()) {
								itemidpic = DATABASE
										.getItemPic(
												UserId,
												v.getString(v
														.getColumnIndex(NewEntryDataBase.KEY_ADDEDitemid)));

							}
						}

					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "Error",
							"No Data Found", true);
				}
			} else {
				ViewUtil.showAlertDialog(getActivity(), "Error",
						"No Data Found", true);
			}
			Log.e("itemimgeType", "" + itemimgeType);
			Log.e("itemimges1", "" + itemimges1);
			Log.e("itemimges2", "" + itemimges2);
			adapter.notifyDataSetChanged();
		}

		/*listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				if (txtTitle.getText().toString().equals("Pack&Go Log")) {
					NewEntryMenuFragment fragment = new NewEntryMenuFragment();
					FragmentManager fm = getFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					ft.setCustomAnimations(R.anim.slide_in_right,
							R.anim.slide_out_left, R.anim.slide_in_left,
							R.anim.slide_out_right);
					Bundle bundle = new Bundle();
					bundle.putString("Packageid", packageid.get(position));
					bundle.putString("Typed", "Package");
					fragment.setArguments(bundle);
					ft.replace(R.id.container, fragment);
					ft.addToBackStack(null);
					ft.commit();
					Editor editor = shared.edit();
					editor.putString(AppConstant.KEY_Date, Dates.get(position));

					editor.commit();
				} else if (txtTitle.getText().toString()
						.equals("Inventory Log")) {
					NewEntryMenuFragment fragment = new NewEntryMenuFragment();
					FragmentManager fm = getFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					ft.setCustomAnimations(R.anim.slide_in_right,
							R.anim.slide_out_left, R.anim.slide_in_left,
							R.anim.slide_out_right);
					Bundle bundle = new Bundle();
					bundle.putString("Packageid", packageid.get(position));
					Log.e("package", "" + packageid.get(position));
					bundle.putString("Typed", "Inventory");
					fragment.setArguments(bundle);
					ft.replace(R.id.container, fragment);
					ft.addToBackStack(null);
					ft.commit();
					Editor editor = shared.edit();
					editor.putString(AppConstant.KEY_Date, Dates.get(position));

					editor.commit();

				} else if (txtTitle.getText().toString()
						.equals("Pick List Log")) {
					OnStartPickListFragment fragment = new OnStartPickListFragment();
					FragmentManager fm = getFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					ft.setCustomAnimations(R.anim.slide_in_right,
							R.anim.slide_out_left, R.anim.slide_in_left,
							R.anim.slide_out_right);
					Bundle bundle = new Bundle();
					bundle.putString("Picklistid", packageid.get(position));
					fragment.setArguments(bundle);
					ft.replace(R.id.container, fragment);
					ft.addToBackStack(null);
					ft.commit();
					Editor editor = shared.edit();
					editor.putString(AppConstant.KEY_Date, Dates.get(position));

					editor.commit();
				}
			}
		});*/
		btnhome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().finish();
				Intent intent = new Intent(getActivity(), MainActivity.class);
				startActivity(intent);
			}
		});
		btn_start_new.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				StartPackingFragment fragment = new StartPackingFragment();

				FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.setCustomAnimations(R.anim.slide_in_right,
						R.anim.slide_out_left, R.anim.slide_in_left,
						R.anim.slide_out_right);
				ft.replace(R.id.container, fragment);
				Bundle bundle = new Bundle();
				bundle.putString(KEY, key);
				fragment.setArguments(bundle);
				ft.addToBackStack(null);
				ft.commit();
			}
		});

		return rootView;
	}

	private void intType() {
		// TODO Auto-generated method stub
		key = getArguments().getString(KEY);
		if (key.equals("pack_go")) {
			Editor editor = shared.edit();
			editor.putString(AppConstant.KEY_SelectedType, "Package");
			editor.commit();
		} else if (key.equals("inventory")) {
			Editor editor = shared.edit();
			editor.putString(AppConstant.KEY_SelectedType, "Inventory");
			editor.commit();
		}

	}

	private void intHeaderTitle() {
		// TODO Auto-generated method stub
		key = getArguments().getString(KEY);
		if (key.equals("pack_go")) {
			txtTitle.setText("Pack&Go Log");
			strKey = "View: Pack&Go Packages";
		} else if (key.equals("inventory")) {
			txtTitle.setText("Inventory Log");
			strKey = "View:Inventory";
		} else if (key.equals("pick_list")) {
			txtTitle.setText("Pick List Log");
			Type = shared.getString(AppConstant.KEY_SelectedType, "");
		} else if (key.equals("start_new_pick")) {
			txtTitle.setText("START NEW PICK LIST");
		}

	}

	class ListAdapter extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;
		ArrayList<String> title1 = new ArrayList<String>();
		ArrayList<String> date1 = new ArrayList<String>();
		ArrayList<String> size1 = new ArrayList<String>();
		ArrayList<String> packageid1 = new ArrayList<String>();
		ArrayList<String> profile1 = new ArrayList<String>();
		ArrayList<String> itemimagetype = new ArrayList<String>();
		ArrayList<String> itemimagetype1 = new ArrayList<String>();
		ArrayList<String> itemimagetype2 = new ArrayList<String>();
		ArrayList<String> itemimage1 = new ArrayList<String>();
		ArrayList<String> itemimage2 = new ArrayList<String>();
		ArrayList<String> itemimage3 = new ArrayList<String>();
		ArrayList<String> address1 = new ArrayList<String>();
		String title, date, size2, packageid, profile, itemimag, itemimag1,
				itemimag2;
		DisplayImageOptions displayImageOptions;

		public ListAdapter(Activity activity, ArrayList<String> packageid,
				ArrayList<String> title, ArrayList<String> dates,
				ArrayList<String> size, ArrayList<String> profile,
				ArrayList<String> itemimges, ArrayList<String> itemimges1,
				ArrayList<String> itemimges2, ArrayList<String> address,
				ArrayList<String> itemimgeType,
				ArrayList<String> itemimgeType1, ArrayList<String> itemimgeType2) {
			this.activity = activity;
			title1 = title;
			date1 = dates;
			size1 = size;
			packageid1 = packageid;
			profile1 = profile;
			itemimage1 = itemimges;
			itemimage2 = itemimges1;
			itemimage3 = itemimges2;
			address1 = address;
			itemimagetype = itemimgeType;
			itemimagetype1 = itemimgeType1;
			itemimagetype2 = itemimgeType2;
			displayImageOptions = setupImageLoader();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub

			return title1.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return title1.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			BitmapFactory.Options options;

			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final ViewHolder holder;
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.fragment_home_pack_go_listitems, null);
				holder = new ViewHolder();

				holder.layout_items = (LinearLayout) convertView
						.findViewById(R.id.linear_layout_items);
				holder.layout_dialog = (LinearLayout) convertView
						.findViewById(R.id.linear_layout_dialogopen);
				// holder.userimage = (ImageView) convertView
				// .findViewById(R.id.img_userImage);

				holder.imgshirt1 = (ImageView) convertView
						.findViewById(R.id.img_shirt1);
				holder.imgshirt2 = (ImageView) convertView
						.findViewById(R.id.img_shirt2);
				holder.imgshirt3 = (ImageView) convertView
						.findViewById(R.id.img_shirt3);
				holder.imgOpen = (ImageView) convertView
						.findViewById(R.id.img_open);
				holder.imgadd = (ImageView) convertView
						.findViewById(R.id.img_add);
				holder.imgshare = (ImageView) convertView
						.findViewById(R.id.img_share);

				holder.imgdelete = (ImageView) convertView
						.findViewById(R.id.img_delete);

				holder.relative_popUp = (RelativeLayout) convertView
						.findViewById(R.id.rel_dialog_popup);
				holder.rel_rowItems = (RelativeLayout) convertView
						.findViewById(R.id.rel_items_row);
				holder.imgInvite = (ImageView) convertView
						.findViewById(R.id.img_invite);
				holder.count = (TextView) convertView
						.findViewById(R.id.txt_item_count);
				holder.titlename = (TextView) convertView
						.findViewById(R.id.id_txt_username);
				holder.date = (TextView) convertView
						.findViewById(R.id.txt_date);
				holder.mail = (Button) convertView
						.findViewById(R.id.btn_mail_share);
				holder.message = (Button) convertView
						.findViewById(R.id.btn_message_share);
				holder.yahoo = (Button) convertView
						.findViewById(R.id.btn_yahoo_share);
				holder.google = (Button) convertView
						.findViewById(R.id.btn_google_share);
				holder.clould = (Button) convertView
						.findViewById(R.id.btn_cloud_share);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			if (itemimagetype.get(position).equals("64")) {
				Iterator iterator = itemimage1.iterator();
				while (iterator.hasNext()) {

					File image = new File(iterator.next().toString());
					Log.e("image", "" + image);
					options = new BitmapFactory.Options();
					options.inSampleSize = 8;

					Bitmap bitmap = BitmapFactory.decodeFile(
							image.getAbsolutePath(), options);
					holder.imgshirt1.setImageBitmap(bitmap);

				}

			} else {

				ImageLoader.getInstance().displayImage(
						String.valueOf(itemimage1.get(position)),
						holder.imgshirt1);

			}

			if (itemimagetype1.get(position).equals("64")) {
				Iterator iterator = itemimage2.iterator();
				while (iterator.hasNext()) {

					File image = new File(iterator.next().toString());
					Log.e("image", "" + image);
					options = new BitmapFactory.Options();
					options.inSampleSize = 8;

					Bitmap bitmap = BitmapFactory.decodeFile(
							image.getAbsolutePath(), options);
					holder.imgshirt2.setImageBitmap(bitmap);

				}

			} else {

				ImageLoader.getInstance().displayImage(
						String.valueOf(itemimage2.get(position)),
						holder.imgshirt2);

			}
			if (itemimagetype2.get(position).equals("64")) {
				Iterator iterator = itemimage3.iterator();
				while (iterator.hasNext()) {

					File image = new File(iterator.next().toString());
					Log.e("image", "" + image);
					options = new BitmapFactory.Options();
					options.inSampleSize = 8;

					Bitmap bitmap = BitmapFactory.decodeFile(
							image.getAbsolutePath(), options);
					holder.imgshirt3.setImageBitmap(bitmap);

				}

			} else {

				ImageLoader.getInstance().displayImage(
						String.valueOf(itemimage3.get(position)),
						holder.imgshirt3);

			}
			holder.imgshirt1.setId(position);
			holder.imgshirt2.setId(position);
			holder.imgshirt3.setId(position);
			holder.count.setId(position);
			holder.titlename.setId(position);
			holder.date.setId(position);
			holder.count.setText(size1.get(position));
			holder.titlename.setText(title1.get(position));
			holder.date.setText(date1.get(position));
			// ImageLoader.getInstance().displayImage(
			// String.valueOf(profile1.get(position)), holder.userimage);

			holder.layout_items.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					holder.layout_dialog.setVisibility(View.VISIBLE);
					holder.layout_items.setVisibility(View.GONE);
					if (txtTitle.getText().toString().equals("Pick List Log")) {
						holder.imgadd.setVisibility(View.VISIBLE);
						holder.imgshare.setVisibility(View.VISIBLE);
						holder.imgOpen.setVisibility(View.GONE);
						holder.imgInvite.setVisibility(View.GONE);
					}

				}
			});
			holder.imgadd.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					getposition = position;
					if (isInternetPresent) {
						new AsyncAddPickListTask().execute(
								packageid1.get(position), UserId, itemid, Type);
					} else {

						Log.e("itemid piock", "" + itemid);
						Cursor test = DATABASE.checkingduplicate(
								packageid1.get(position), UserId, itemid, Type);

						Log.e("test", "" + test.getCount());
						if (test.getCount() == 0) {
							long check = DATABASE.InsertPicklistitem(
									packageid1.get(position), UserId, itemid,
									Type, currentdate());
							Log.e("check", "" + check);
							if (check > 0) {
								Log.e("fd", "" + check);
								size1.set(getposition, String.valueOf(Integer
										.parseInt(size1.get(position)) + 1));

								Toast.makeText(getActivity(),
										"Item Succesfully Added",
										Toast.LENGTH_LONG).show();
							}
						} else {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"Item Already Exist", true);

						}
					}
					adapter.notifyDataSetChanged();
				}
			});
			holder.imgOpen.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if (txtTitle.getText().toString().equals("Pack&Go Log")) {
						OpenNewEntryFragment entryFragment = new OpenNewEntryFragment();
						FragmentManager fm = getFragmentManager();
						FragmentTransaction ft = fm.beginTransaction();
						ft.setCustomAnimations(R.anim.slide_in_right,
								R.anim.slide_out_left, R.anim.slide_in_left,
								R.anim.slide_out_right);
						Bundle bundle = new Bundle();
						bundle.putString("Title_Open", strKey);
						bundle.putString("Packedid", packageid1.get(position));
						bundle.putString("Address", address1.get(position));
						bundle.putString("Typed", "Package");
						entryFragment.setArguments(bundle);
						ft.replace(R.id.container, entryFragment);
						ft.addToBackStack(null);
						ft.commit();
						Editor editor = shared.edit();

						editor.putString(AppConstant.KEY_TITLEOPEN, strKey);

						editor.commit();
					}

					else if (txtTitle.getText().toString()
							.equals("Inventory Log")) {
						OpenNewEntryFragment entryFragment = new OpenNewEntryFragment();
						FragmentManager fm = getFragmentManager();
						FragmentTransaction ft = fm.beginTransaction();
						ft.setCustomAnimations(R.anim.slide_in_right,
								R.anim.slide_out_left, R.anim.slide_in_left,
								R.anim.slide_out_right);
						Bundle bundle = new Bundle();
						bundle.putString("Title_Open", strKey);
						bundle.putString("Packedid", packageid1.get(position));
						bundle.putString("Address", address1.get(position));
						bundle.putString("Typed", "Inventory");
						entryFragment.setArguments(bundle);
						ft.replace(R.id.container, entryFragment);
						ft.addToBackStack(null);
						ft.commit();
						Editor editor = shared.edit();

						editor.putString(AppConstant.KEY_TITLEOPEN, strKey);

						editor.commit();
					}

				}
			});
			holder.imgdelete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					getposition = position;
					isInternetPresent = internetcheck.isConnectingToInternet();
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							getActivity());

					// set title
					alertDialogBuilder.setTitle("Delete");

					// set dialog message
					alertDialogBuilder
							.setMessage("Are you sure want to delete")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {

											if (isInternetPresent) {
												if (txtTitle
														.getText()
														.toString()
														.equals("Pick List Log")) {
													new AsyncDeletePickListTask().execute(
															packageid1
																	.get(position),
															UserId);
												} else {
													new AsyncDeletePackageDataTask().execute(
															packageid1
																	.get(position),
															UserId);
												}
											} else {

												if (txtTitle
														.getText()
														.toString()
														.equals("Pick List Log")) {
													long picklist = DATABASE.DeletePicklist(
															packageid1
																	.get(position),
															UserId);
													long picklistitem = DATABASE
															.DeletePicklistitem(
																	packageid1
																			.get(position),
																	UserId);

													title1.remove(getposition);
													date1.remove(getposition);
													size1.remove(getposition);
													packageid1
															.remove(getposition);

													itemimage1
															.remove(getposition);
													itemimage2
															.remove(getposition);
													itemimage3
															.remove(getposition);

												} else {

													long Package = DATABASE.DeletePackage(
															packageid1
																	.get(position),
															UserId);

													long PackageData = DATABASE.DeletePackagedata(
															packageid1
																	.get(position),
															UserId);

													long PackageDataitem = DATABASE
															.DeletePackagedataitem(
																	packageid1
																			.get(position),
																	UserId);

													profile1.remove(getposition);
													title1.remove(getposition);
													date1.remove(getposition);
													size1.remove(getposition);
													packageid1
															.remove(getposition);

													itemimage1
															.remove(getposition);
													itemimage2
															.remove(getposition);
													itemimage3
															.remove(getposition);
												}
												//adapter.notifyDataSetChanged();
											}
											dialog.cancel();

										}
									})
							.setNegativeButton("No",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											dialog.cancel();
										}
									});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();

				}
			});
			holder.imgshare.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent sharingIntent = new Intent(
							android.content.Intent.ACTION_SEND);
					sharingIntent.setType("text/plain");
					sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Sharing");
					sharingIntent.putExtra(Intent.EXTRA_TEXT,
							"Title:  " + Title.get(position) + ""
									+ "Total Item :" + size.get(position));
					startActivity(Intent.createChooser(sharingIntent, "SHARE"));
				}
			});
			holder.imgInvite.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					InviteContactFragment contactFragment = new InviteContactFragment();
					FragmentManager fm = getFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					ft.setCustomAnimations(R.anim.slide_in_right,
							R.anim.slide_out_left, R.anim.slide_in_left,
							R.anim.slide_out_right);
					Log.e("Invite", "" + packageid1.get(position));
					Bundle bundle = new Bundle();
					bundle.putString("Packageid", packageid1.get(position));
					if (txtTitle.getText().toString().equals("Pack&Go Log")) {
						bundle.putString("Typed", "Package");
					} else if (txtTitle.getText().toString()
							.equals("Inventory Log")) {
						bundle.putString("Typed", "Inventory");
					}

					contactFragment.setArguments(bundle);
					ft.replace(R.id.container, contactFragment);
					ft.addToBackStack(null);
					ft.commit();
				}
			});

			// For Share Click For Ending Point See next comment

			holder.mail.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					Intent sendIntent = new Intent(Intent.ACTION_VIEW);
					sendIntent.setType("plain/text");
					sendIntent.setData(Uri.parse(""));
					sendIntent.setClassName("com.google.android.gm",
							"com.google.android.gm.ComposeActivityGmail");
					sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Sharing");
					sendIntent.putExtra(Intent.EXTRA_TEXT,
							"Title:  " + Title.get(position) + ""
									+ "Total Item :" + size.get(position));
					startActivity(sendIntent);
				}
			});
			holder.message.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent smsVIntent = new Intent(Intent.ACTION_VIEW);
					smsVIntent.setType("vnd.android-dir/mms-sms");
					smsVIntent.putExtra("sms_body",
							"Title:  " + Title.get(position) + "       "
									+ "Total Item :" + size.get(position));
					try {
						startActivity(smsVIntent);
					} catch (Exception ex) {
						Toast.makeText(getActivity(), "Your sms has failed...",
								Toast.LENGTH_LONG).show();

						ex.printStackTrace();

					}
				}
			});
			holder.yahoo.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent sharingIntent = new Intent(
							android.content.Intent.ACTION_SEND);
					sharingIntent.setType("text/plain");
					sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Sharing");
					sharingIntent.putExtra(Intent.EXTRA_TEXT,
							"Title:  " + Title.get(position) + ""
									+ "Total Item :" + size.get(position));
					startActivity(Intent.createChooser(sharingIntent, "Yahoo"));
				}
			});
			holder.google.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent sharingIntent = new Intent(
							android.content.Intent.ACTION_SEND);
					sharingIntent.setType("text/plain");
					sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Sharing");
					sharingIntent.putExtra(Intent.EXTRA_TEXT,
							"Title:  " + Title.get(position) + ""
									+ "Total Item :" + size.get(position));
					startActivity(Intent.createChooser(sharingIntent, "Goggle"));

				}
			});
			holder.clould.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent sharingIntent = new Intent(
							android.content.Intent.ACTION_SEND);
					sharingIntent.setType("text/plain");
					sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Sharing");
					sharingIntent.putExtra(Intent.EXTRA_TEXT,
							"Title:  " + Title.get(position) + ""
									+ "Total Item :" + size.get(position));
					startActivity(Intent.createChooser(sharingIntent,
							"Goggle Drive"));
				}
			});
			// Click end of Share Buttons
			return convertView;
		}

		private String currentdate() {

			try {

				SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
				String format = s.format(new Date());

				Log.e("format: ", "" + format);
				return format;
			} catch (Exception ex) {
				return null;
			}
		}

		public Bitmap RotateBitmap(Bitmap source, float angle) {
			Matrix matrix = new Matrix();
			matrix.postRotate(angle);
			return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
					source.getHeight(), matrix, true);
		}

		public DisplayImageOptions setupImageLoader() {
			return new DisplayImageOptions.Builder().cacheInMemory(true)
					.cacheOnDisk(true).considerExifParams(true)
					.displayer(new RoundedBitmapDisplayer(1)).build();

		}

		public class AsyncDeletePackageDataTask extends
				AsyncTask<String, Void, String> {
			List<NameValuePair> parameter = null;

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				ViewUtil.showProgressDialog(getActivity());
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {
					isInternetPresent = internetcheck.isConnectingToInternet();
					if (isInternetPresent) {
						if (result != null) {

							if (result.equals("error")) {
								ViewUtil.showAlertDialog(getActivity(),
										"ERROR", "SomeThing Wrong", false);
								ViewUtil.hideProgressDialog();
							} else {
								JSONObject jObj = new JSONObject(result);

								if (jObj.getBoolean("success")) {
									Log.e("Response delete package", result);
									title1.remove(getposition);
									date1.remove(getposition);
									size1.remove(getposition);
									packageid1.remove(getposition);
									profile1.remove(getposition);
									itemimage1.remove(getposition);
									itemimage2.remove(getposition);
									itemimage3.remove(getposition);
									adapter.notifyDataSetChanged();
									ViewUtil.hideProgressDialog();

								} else {
									Log.e("Response", result);

									String Server_message = jObj
											.getString("Account");
									ViewUtil.hideProgressDialog();
									ViewUtil.showAlertDialog(getActivity(),
											"SomeThing Wrong", Server_message,
											true);

								}
							}
						}
					} else {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"No Internet Connection", false);
						ViewUtil.hideProgressDialog();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			protected String doInBackground(String... params) {
				List<NameValuePair> parameter = new LinkedList<NameValuePair>();
				parameter.add(new BasicNameValuePair("PackageId", String
						.valueOf(params[0])));
				parameter.add(new BasicNameValuePair("UserId", String
						.valueOf(params[1])));
				Log.e("parameter delete package", "" + parameter);
				String result = NetworkConnector.deletePackage(parameter);
				return result;
			}

		}

		public class AsyncAddPickListTask extends
				AsyncTask<String, Void, String> {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				ViewUtil.showProgressDialog(getActivity());
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {
					isInternetPresent = internetcheck.isConnectingToInternet();
					if (isInternetPresent) {
						if (result != null) {

							if (result.equals("error")) {
								ViewUtil.showAlertDialog(getActivity(),
										"ERROR", "SomeThing Wrong", false);
								ViewUtil.hideProgressDialog();
							} else {
								JSONObject jObj = new JSONObject(result);

								if (jObj.getBoolean("success")) {
									Log.e("Response pick list add", result);
									size1.set(getposition, String
											.valueOf(Integer.parseInt(size1
													.get(getposition)) + 1));
									// title1.remove(getposition);
									// date1.remove(getposition);
									// size1.remove(getposition);
									// packageid1.remove(getposition);
									// profile1.remove(getposition);
									// itemimage1.remove(getposition);
									// itemimage2.remove(getposition);
									// itemimage3.remove(getposition);
									adapter.notifyDataSetChanged();
									ViewUtil.hideProgressDialog();

								}

								else {
									Log.e("Response", result);

									String Server_message = jObj
											.getString("message");
									ViewUtil.showAlertDialog(getActivity(),
											"Error", Server_message, true);
									ViewUtil.hideProgressDialog();

								}
							}
						}
					} else {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"No Internet Connection", false);
						ViewUtil.hideProgressDialog();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			protected String doInBackground(String... params) {

				List<NameValuePair> parameter = new LinkedList<NameValuePair>();
				parameter.add(new BasicNameValuePair("PicklistId", String
						.valueOf(params[0])));
				parameter.add(new BasicNameValuePair("UserId", String
						.valueOf(params[1])));
				parameter.add(new BasicNameValuePair("ItemId", String
						.valueOf(params[2])));
				parameter.add(new BasicNameValuePair("Type", String
						.valueOf(params[3])));

				Log.e("parameter pick list add", "" + parameter);
				String result = NetworkConnector.AddPicklist(parameter);
				return result;
			}

		}

		public class AsyncDeletePickListTask extends
				AsyncTask<String, Void, String> {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				ViewUtil.showProgressDialog(getActivity());
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {
					isInternetPresent = internetcheck.isConnectingToInternet();
					if (isInternetPresent) {
						if (result != null) {

							if (result.equals("error")) {
								ViewUtil.showAlertDialog(getActivity(),
										"ERROR", "SomeThing Wrong", false);
								ViewUtil.hideProgressDialog();
							} else {

								JSONObject jObj = new JSONObject(result);

								if (jObj.getBoolean("success")) {
									Log.e("Response delete picklist", result);

									title1.remove(getposition);
									date1.remove(getposition);
									size1.remove(getposition);
									packageid1.remove(getposition);
									profile1.remove(getposition);
									itemimage1.remove(getposition);
									itemimage2.remove(getposition);
									itemimage3.remove(getposition);
									adapter.notifyDataSetChanged();
									ViewUtil.hideProgressDialog();

								}

								else {
									Log.e("Response", result);

									String Server_message = jObj
											.getString("message");
									ViewUtil.showAlertDialog(getActivity(),
											"Error", Server_message, true);
									ViewUtil.hideProgressDialog();

								}
							}
						}
					} else {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"No Internet Connection", false);
						ViewUtil.hideProgressDialog();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			protected String doInBackground(String... params) {

				List<NameValuePair> parameter = new LinkedList<NameValuePair>();
				parameter.add(new BasicNameValuePair("PicklistId", String
						.valueOf(params[0])));
				parameter.add(new BasicNameValuePair("UserId", String
						.valueOf(params[1])));

				Log.e("parameter picklist", "" + parameter);
				String result = NetworkConnector.DeletePicklist(parameter);
				return result;
			}

		}
	}

	class ViewHolder {
		private LinearLayout layout_items, layout_dialog;
		private RelativeLayout relative_popUp, rel_rowItems;
		private ImageView imgOpen, imgInvite, imgdelete, imgadd, imgshare,
				imgshirt1, imgshirt2, imgshirt3;
		private TextView titlename, date, count;
		private Button mail, message, yahoo, google, clould;
	}

	public class AsyncGetPackageTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				ViewUtil.showProgressDialog(getActivity());
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result != null) {

						if (result.equals("error")) {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"SomeThing Wrong", false);
							ViewUtil.hideProgressDialog();
						} else {

							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("Response", result);

								JSONArray array = jObj.getJSONArray("message");
								Log.e("length", "" + array.length());
								for (int i = 0; i < array.length(); i++) {

									JSONObject object = array.getJSONObject(i);
									Log.e("object", "" + object);

									packageid
											.add(object.getString("PackageId"));
									Title.add(object.getString("Title"));
									size.add(object
											.getString("PackageDataCount"));
									profile.add(object
											.getString("ProfileImage"));
									Address.add(object.getString("Address"));
									SimpleDateFormat input = new SimpleDateFormat(
											"yyyy-MM-dd");
									SimpleDateFormat output = new SimpleDateFormat(
											"dd/MM/yyyy");

									Date oneWayTripDate = input.parse(object
											.getString("StartDate")); // parse

									Dates.add(output.format(oneWayTripDate));
									if (object.has("PackageDataItemImages")) {
										JSONArray prods = object
												.getJSONArray("PackageDataItemImages");
										Log.e("prods", "" + prods);
										if (prods != null) {
											for (int j = 0; j < prods.length(); j++) {
												JSONObject innerElem = prods
														.getJSONObject(j);

												if (j == 0) {

													itemimgeType.add("url");

													itemimges
															.add("http://beta.brstdev.com/yiiezefind"
																	+ innerElem
																			.getString("Image"));

												} else if (j == 1) {

													itemimgeType1.add("url");

													itemimges1
															.add("http://beta.brstdev.com/yiiezefind"
																	+ innerElem
																			.getString("Image"));

												} else if (j == 2) {

													itemimgeType2.add("url");

													itemimges2
															.add("http://beta.brstdev.com/yiiezefind"
																	+ innerElem
																			.getString("Image"));

												}

											}

										}
									} else {
										itemimgeType.add("url");
										itemimgeType1.add("url");
										itemimgeType2.add("url");
										itemimges
												.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
										itemimges1
												.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
										itemimges2
												.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
									}

								}

								adapter.notifyDataSetChanged();
								ViewUtil.hideProgressDialog();

							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");

								ViewUtil.hideProgressDialog();
								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);

							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;
			try {

				List<NameValuePair> parameter = new LinkedList<NameValuePair>();

				parameter.add(new BasicNameValuePair("UserId", String
						.valueOf(params[0])));
				parameter.add(new BasicNameValuePair("Typed", String
						.valueOf(params[1])));

				Log.e("parameter", "" + parameter);
				result = NetworkConnector.GetPackage(parameter);
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
			return result;
		}

	}

	public class AsyncGetPickListTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result != null) {

						if (result.equals("error")) {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"SomeThing Wrong", false);
							ViewUtil.hideProgressDialog();
						} else {

							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("Response pick list", result);

								JSONArray array = jObj.getJSONArray("message");
								Log.e("array", "" + array);
								for (int i = 0; i < array.length(); i++) {
									Log.e("Picklist" + "", "hee");
									JSONObject object = array.getJSONObject(i);
									Log.e("object", "" + object);

									packageid.add(object
											.getString("PicklistId"));
									Title.add(object.getString("Title"));
									size.add(object
											.getString("PickListItemsCount"));
									profile.add(object
											.getString("ProfileImage"));
									// Address.add(object.getString("Address"));
									Address.add(",.nkln");
									SimpleDateFormat input = new SimpleDateFormat(
											"yyyy-MM-dd");
									SimpleDateFormat output = new SimpleDateFormat(
											"dd/MM/yyyy");

									Date oneWayTripDate = input.parse(object
											.getString("StartDate")); // parse
									itemimgeType.add("url");
									itemimgeType1.add("url");
									itemimgeType2.add("url");
									Dates.add(output.format(oneWayTripDate));
									if (object.has("PicklistItemImages")) {
										JSONArray prods = object
												.getJSONArray("PicklistItemImages");
										Log.e("prods", "" + prods);
										if (prods != null) {
											for (int j = 0; j < prods.length(); j++) {
												JSONObject innerElem = prods
														.getJSONObject(j);

												if (j == 0) {
													itemimgeType.add("url");

													itemimges
															.add("http://beta.brstdev.com/yiiezefind"
																	+ innerElem
																			.getString("Image"));

												} else if (j == 1) {

													itemimgeType1.add("url");

													itemimges1
															.add("http://beta.brstdev.com/yiiezefind"
																	+ innerElem
																			.getString("Image"));

												} else if (j == 2) {

													itemimgeType2.add("url");

													itemimges2
															.add("http://beta.brstdev.com/yiiezefind"
																	+ innerElem
																			.getString("Image"));

												}

											}

										}
									} else {
										itemimgeType.add("url");
										itemimgeType1.add("url");
										itemimgeType2.add("url");
										itemimges
												.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
										itemimges1
												.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
										itemimges2
												.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
									}
								}

								adapter.notifyDataSetChanged();
								ViewUtil.hideProgressDialog();

							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");
								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);
								ViewUtil.hideProgressDialog();
							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));

			Log.e("parameter picklist", "" + parameter);
			String result = NetworkConnector.GetPicklist(parameter);
			return result;
		}

	}

}