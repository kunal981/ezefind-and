package com.appalmighty.ezefind.newentry.fgmt;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.DataBase.DatasourceHandler;
import com.appalmighty.ezefind.DataBase.NewEntryDataBase;
import com.appalmighty.ezefind.login.act.LoginActivity;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class OpenNewEntryFragment extends Fragment {
	private GridviewAdapter adapter;
	private GridView gridView;
	Button backbutton, btnhome, btnLogout;
	ImageButton imageButton_filter;
	ImageView btnDone, imgNewEntry, filter;
	RelativeLayout layout_filter;
	TextView addressheader;
	String titlekey, Typed;
	String UserId, packageid;
	SharedPreferences shared;
	CheckBox bag, box, singleitem, bin;
	TextView textViewTitle;

	ArrayList<String> Packageid;
	ArrayList<String> Packagedataid;
	ArrayList<String> dates;
	ArrayList<String> total;
	ArrayList<String> pic;
	ArrayList<String> pictype;
	ArrayList<String> type;
	ArrayList<String> location;
	ArrayList<String> value;
	ArrayList<String> filter1;
	String address;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	DatasourceHandler DATABASE;
	Cursor itemcount;
	public static final String TAG = OpenNewEntryFragment.class.getName();

	public static OpenNewEntryFragment newInstance() {
		OpenNewEntryFragment fragment = new OpenNewEntryFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_on_open, container,
				false);

		titlekey = getArguments().getString("Title_Open");
		gridView = (GridView) rootView.findViewById(R.id.id_gridview_open);
		backbutton = (Button) rootView.findViewById(R.id.back_button);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		btnLogout = (Button) rootView.findViewById(R.id.btn_logout);
		imageButton_filter = (ImageButton) rootView
				.findViewById(R.id.img_filter);
		textViewTitle = (TextView) rootView.findViewById(R.id.txt_title_header);
		layout_filter = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter);
		addressheader = (TextView) rootView.findViewById(R.id.textAddress);
		btnDone = (ImageView) rootView.findViewById(R.id.btnDone);
		imgNewEntry = (ImageView) rootView.findViewById(R.id.id_new_entry);
		filter = (ImageView) rootView.findViewById(R.id.fikl);
		bag = (CheckBox) rootView.findViewById(R.id.checkbox_bag);
		box = (CheckBox) rootView.findViewById(R.id.checkbox_box);
		singleitem = (CheckBox) rootView.findViewById(R.id.checkbox_singleitem);
		bin = (CheckBox) rootView.findViewById(R.id.checkbox_bin);
		textViewTitle.setText(titlekey);

		btnLogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				// set title
				alertDialogBuilder.setTitle("LOGOUT");

				// set dialog message
				alertDialogBuilder
						.setMessage("Are you sure for logout")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										SharedPreferences settings = getActivity()
												.getSharedPreferences(
														AppConstant.KEY_APP,
														Context.MODE_PRIVATE);
										settings.edit().clear().commit();
										getActivity().finish();
										Intent intent_home = new Intent(
												getActivity(),
												LoginActivity.class);
										startActivity(intent_home);

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		});
		bag.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (bag.isChecked()) {
					filter1.add("bag");
				} else {
					filter1.remove("bag");
				}

			}
		});
		box.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (box.isChecked()) {
					filter1.add("box");
				} else {
					filter1.remove("box");
				}

			}
		});
		bin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (bin.isChecked()) {
					filter1.add("bin");
				} else {
					filter1.remove("bin");
				}

			}
		});
		singleitem.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (singleitem.isChecked()) {
					filter1.add("single item");
				} else {
					filter1.remove("single item");
				}

			}
		});

		filter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Packageid = new ArrayList<String>();
				Packagedataid = new ArrayList<String>();
				dates = new ArrayList<String>();
				total = new ArrayList<String>();
				pic = new ArrayList<String>();
				pictype = new ArrayList<String>();
				type = new ArrayList<String>();
				location = new ArrayList<String>();
				value = new ArrayList<String>();

				if (isInternetPresent) {

					new AsyncFilterPackageTask().execute(UserId, packageid,
							filter1.toString().replace("]", "")
									.replace("[", ""));
				} else {

					for (String item : filter1) {
						Log.e("item", "" + item);
						Cursor c = DATABASE.FilterPackagedata(packageid, item,
								UserId);
						Log.e("test", "" + c.getCount());
						if (c.getCount() != 0) {

							while (c.moveToNext()) {

								Packageid.add(c.getString(c
										.getColumnIndex(NewEntryDataBase.KEY_PACKAGEIDdata)));
								itemcount = DATABASE.GetDataitemCount(
										UserId,
										c.getString(c
												.getColumnIndex(NewEntryDataBase.KEY_PACKAGEDATAIDdata)));

								total.add(String.valueOf(itemcount.getCount()));
								// pic.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");

								location.add("");
								value.add(c.getString(c
										.getColumnIndex(NewEntryDataBase.KEY_PRICE)));
								Packagedataid.add(c.getString(c
										.getColumnIndex(NewEntryDataBase.KEY_PACKAGEDATAIDdata)));
								Cursor cursor = DATABASE.getAllPackageItem(
										UserId,
										c.getString(c
												.getColumnIndex(NewEntryDataBase.KEY_PACKAGEIDdata)),
										c.getString(c
												.getColumnIndex(NewEntryDataBase.KEY_PACKAGEDATAIDdata)));
								Log.e("cursor", "" + cursor.getCount());
								if (cursor.getCount() != 0) {
									if (cursor.moveToFirst()) {
										pictype.add("64");
										pic.add(cursor.getString(cursor
												.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));

									} else {
										pictype.add("url");
										pic.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
									}
								} else {
									pictype.add("url");
									pic.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
								}
								type.add(String
										.valueOf(
												c.getString(c
														.getColumnIndex(NewEntryDataBase.KEY_TYPE)))
										.toUpperCase()
										+ " "
										+ ""
										+ c.getString(c
												.getColumnIndex(NewEntryDataBase.KEY_NUMBER)));

								dates.add(c.getString(c
										.getColumnIndex(NewEntryDataBase.KEY_CREATEDATE)));
							}

						} else {
							ViewUtil.showAlertDialog(getActivity(), "Error",
									"No Item found", true);
						}

					}

					// adapter = new GridviewAdapter(getActivity(), Packageid,
					// Packagedataid, dates, total, pic, type, location,
					// value, pictype);
					// gridView.setAdapter(adapter);
					adapter.notifyDataSetChanged();
					layout_filter.setVisibility(View.GONE);

				}

			}
		});
		backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		imageButton_filter.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				layout_filter.setVisibility(View.VISIBLE);
			}
		});

		btnDone.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				layout_filter.setVisibility(View.GONE);

			}
		});
		imgNewEntry.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				NewEntryMenuFragment fragment = new NewEntryMenuFragment();
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				FragmentTransaction ft = fm.beginTransaction();
				Bundle bundle = new Bundle();
				bundle.putString("Packedid", packageid);
				bundle.putString("Packedid", packageid);
				if (Typed.equals("Package")) {
					bundle.putString("Typed", "Package");
				} else {
					bundle.putString("Typed", "Inventory");
				}

				fragment.setArguments(bundle);
				ft.replace(R.id.container, fragment);
				ft.addToBackStack(null);
				ft.commit();
			}
		});
		btnhome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().finish();
				Intent intent = new Intent(getActivity(), MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});

		return rootView;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.e("onResume", "onResume");
		Packageid = new ArrayList<String>();
		Packagedataid = new ArrayList<String>();
		dates = new ArrayList<String>();
		total = new ArrayList<String>();
		pic = new ArrayList<String>();
		pictype = new ArrayList<String>();
		type = new ArrayList<String>();
		location = new ArrayList<String>();
		value = new ArrayList<String>();
		filter1 = new ArrayList<String>();
		Typed = getArguments().getString("Typed");
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = shared.getString(AppConstant.KEY_USER_ID, "");
		packageid = getArguments().getString("Packedid");
		if (getArguments().getString("Address").equals("")) {

		} else {
			addressheader.setText(getArguments().getString("Address"));
		}

		internetcheck = new ConnectionDetector(getActivity());
		DATABASE = new DatasourceHandler(getActivity());
		isInternetPresent = internetcheck.isConnectingToInternet();

		adapter = new GridviewAdapter(getActivity(), Packageid, Packagedataid,
				dates, total, pic, type, location, value, pictype);
		gridView.setAdapter(adapter);

		if (isInternetPresent) {
			Log.e("getall", "getall");
			new AsyncGetAlliteminPackageTask().execute(UserId, packageid);
		} else {

			Cursor c = DATABASE.getPackageItem(packageid, UserId);
			if (c.getCount() != 0) {

				while (c.moveToNext()) {

					Packageid
							.add(c.getString(c
									.getColumnIndex(NewEntryDataBase.KEY_PACKAGEIDdata)));

					itemcount = DATABASE
							.GetDataitemCount(
									UserId,
									c.getString(c
											.getColumnIndex(NewEntryDataBase.KEY_PACKAGEDATAIDdata)));

					total.add(String.valueOf(itemcount.getCount()));
					// pic.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");

					location.add("");
					value.add(c.getString(c
							.getColumnIndex(NewEntryDataBase.KEY_PRICE)));
					Packagedataid
							.add(c.getString(c
									.getColumnIndex(NewEntryDataBase.KEY_PACKAGEDATAIDdata)));
					Cursor cursor = DATABASE
							.getAllPackageItem(
									UserId,
									c.getString(c
											.getColumnIndex(NewEntryDataBase.KEY_PACKAGEIDdata)),
									c.getString(c
											.getColumnIndex(NewEntryDataBase.KEY_PACKAGEDATAIDdata)));
					Log.e("cursor", "" + cursor.getCount());
					if (cursor.getCount() != 0) {
						if (cursor.moveToFirst()) {
							pictype.add("64");
							pic.add(cursor.getString(cursor
									.getColumnIndex(NewEntryDataBase.KEY_ITEMIMAGE)));

						} else {
							pictype.add("url");
							pic.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
						}
					} else {
						pictype.add("url");
						pic.add("http://beta.brstdev.com/yiiezefind/uploads/items_pics/img_shirt.png");
					}
					type.add(String
							.valueOf(
									c.getString(c
											.getColumnIndex(NewEntryDataBase.KEY_TYPE)))
							.toUpperCase()
							+ " "
							+ ""
							+ c.getString(c
									.getColumnIndex(NewEntryDataBase.KEY_NUMBER)));

					dates.add(c.getString(c
							.getColumnIndex(NewEntryDataBase.KEY_CREATEDATE)));
				}

			} else {
				ViewUtil.showAlertDialog(getActivity(), "Error",
						"No Item found", true);
			}

			adapter.notifyDataSetChanged();
		}
	}

	class GridviewAdapter extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;
		ArrayList<String> Packageid1 = new ArrayList<String>();
		ArrayList<String> Packagedataid1 = new ArrayList<String>();
		ArrayList<String> dates1 = new ArrayList<String>();
		ArrayList<String> total1 = new ArrayList<String>();
		ArrayList<String> pic1 = new ArrayList<String>();
		ArrayList<String> pictype1 = new ArrayList<String>();
		ArrayList<String> type1 = new ArrayList<String>();
		ArrayList<String> location1 = new ArrayList<String>();
		ArrayList<String> value1 = new ArrayList<String>();
		DisplayImageOptions displayImageOptions;

		public GridviewAdapter(Activity activity, ArrayList<String> packageid,
				ArrayList<String> packagedataid, ArrayList<String> dates,
				ArrayList<String> total, ArrayList<String> pic,
				ArrayList<String> type, ArrayList<String> location,
				ArrayList<String> value, ArrayList<String> pictype) {
			this.activity = activity;
			Packageid1 = packageid;
			Packagedataid1 = packagedataid;
			dates1 = dates;
			total1 = total;
			pic1 = pic;
			pictype1 = pictype;
			type1 = type;
			location1 = location;
			value1 = value;
			displayImageOptions = setupImageLoader();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			Log.e("size", "" + Packageid1.size());
			return Packageid1.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return Packageid1.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			BitmapFactory.Options options;
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewHolder holder = null;
			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.fragment_on_open_griditems, null);
				holder = new ViewHolder();
				holder.imageView = (ImageView) convertView
						.findViewById(R.id.img_container);
				holder.date = (TextView) convertView
						.findViewById(R.id.textView1);
				holder.type = (TextView) convertView
						.findViewById(R.id.txt_jacket);
				holder.quantitynumber = (TextView) convertView
						.findViewById(R.id.txt_quantity_number);
				holder.add = (ImageButton) convertView
						.findViewById(R.id.btn_add);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();

			}
			try {
				holder.date.setText(dates1.get(position));
				holder.quantitynumber.setText(total1.get(position));
				holder.type.setText(type1.get(position));
				Log.e("type",
						""
								+ type1.get(position).substring(0,
										type1.get(position).indexOf(' ')));

				if (pictype1.get(position).equals("")) {

				} else {
					if (pictype1.get(position).equals("64")) {

						File image = new File(pic1.get(position));

						options = new BitmapFactory.Options();
						options.inSampleSize = 8;

						Bitmap bitmap = BitmapFactory.decodeFile(
								image.getAbsolutePath(), options);
						holder.imageView.setImageBitmap(bitmap);

					} else {
						ImageLoader.getInstance().displayImage(
								String.valueOf(pic1.get(position)),
								holder.imageView);

					}
				}
				holder.add.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (type1.get(position)
								.substring(0, type1.get(position).indexOf(' '))
								.equals("single")) {

						} else {
							AdditemsFragment openContainerFragment = new AdditemsFragment();
							FragmentManager fm = getFragmentManager();
							FragmentTransaction ft = fm.beginTransaction();
							ft.setCustomAnimations(R.anim.slide_in_right,
									R.anim.slide_out_left,
									R.anim.slide_in_left,
									R.anim.slide_out_right);

							ft.replace(R.id.container, openContainerFragment);
							ft.addToBackStack(null);
							ft.commit();

							Editor editor = shared.edit();

							editor.putString(AppConstant.KEY_Address,
									getArguments().getString("Address"));
							editor.putString(AppConstant.KEY_PackageId,
									Packageid1.get(position));
							editor.putString(
									AppConstant.KEY_PackageDataId,
									String.valueOf(Packagedataid1.get(position)));
							editor.putString(AppConstant.KEY_Date,
									String.valueOf(dates1.get(position)));
							editor.commit();
						}
					}
				});
				holder.imageView.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (type1.get(position)
								.substring(0, type1.get(position).indexOf(' '))
								.equals("single")) {

						} else {
							OpenContainerFragment openContainerFragment = new OpenContainerFragment();
							FragmentManager fm = getFragmentManager();
							FragmentTransaction ft = fm.beginTransaction();
							ft.setCustomAnimations(R.anim.slide_in_right,
									R.anim.slide_out_left,
									R.anim.slide_in_left,
									R.anim.slide_out_right);
							Bundle bundle = new Bundle();
							bundle.putString("Title_Open", titlekey);
							bundle.putString("Packageid",
									Packageid1.get(position));
							bundle.putString("packagedataid",
									Packagedataid1.get(position));
							bundle.putString("type", type1.get(position));
							bundle.putString("loction", location1.get(position));
							bundle.putString("value", value1.get(position));
							bundle.putString("address", addressheader.getText()
									.toString());
							openContainerFragment.setArguments(bundle);
							ft.replace(R.id.container, openContainerFragment);
							ft.addToBackStack(null);
							ft.commit();
						}
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
			return convertView;

		}

		public Bitmap ConvertToImage(String image) {
			try {
				byte[] imageAsBytes = Base64.decode(image.getBytes(),
						Base64.DEFAULT);

				Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
						imageAsBytes.length);

				return bitmap;
			} catch (Exception e) {
				return null;
			}
		}

		class ViewHolder {

			private ImageView imageView;
			TextView date, type, quantitynumber;
			private ImageButton add;
		}

		public DisplayImageOptions setupImageLoader() {
			return new DisplayImageOptions.Builder().cacheInMemory(true)
					.cacheOnDisk(true).considerExifParams(true)
					.displayer(new RoundedBitmapDisplayer(1)).build();

		}
	}

	public class AsyncGetAlliteminPackageTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				Log.e("response", "" + result);
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response", result);
							JSONObject object = jObj.getJSONObject("message");

							addressheader.setText(object.getString("Address"));

							JSONArray Array = object
									.getJSONArray("packageDatas");
							for (int j = 0; j < Array.length(); j++) {

								JSONObject objectfrom = Array.getJSONObject(j);
								String packat = objectfrom
										.getString("PackageId_fk");
								String dataid = objectfrom
										.getString("PackageDataId");
								String ItemsCount = objectfrom
										.getString("ItemsCount");
								String value_ = objectfrom.getString("value");

								String type_ = objectfrom.getString("Type");

								String no_ = objectfrom.getString("Number");

								String ItemImage = "http://beta.brstdev.com/yiiezefind"
										+ objectfrom.getString("ItemImage");
								SimpleDateFormat input = new SimpleDateFormat(
										"yyyy-MM-dd");
								SimpleDateFormat output = new SimpleDateFormat(
										"dd/MM/yyyy");

								Date oneWayTripDate = input.parse(object
										.getString("EntryDate"));

								if (objectfrom.has("Location")) {
									JSONObject objec = objectfrom
											.getJSONObject("Location");
									if (objec.getString("Value").equals("Gps")) {
										location.add(objec
												.getString("LocationName"));
									} else {
										location.add(objec.getString("Value"));
									}

								} else {
									location.add("null");
								}

								if (packat != null) {
									Packageid.add(packat);
								}
								if (value_ != null) {
									value.add(value_);
								}

								if (dataid != null) {
									Packagedataid.add(dataid);
								}

								if (ItemsCount != null) {
									total.add(ItemsCount);
								}

								if (ItemImage != null) {
									pic.add(ItemImage);
								}

								pictype.add("url");

								if (type_ != null && no_ != null) {
									type.add(type_ + "  " + no_);
								}

								dates.add(output.format(oneWayTripDate));

							}
							adapter.notifyDataSetChanged();
							ViewUtil.hideProgressDialog();
						}

						else {
							String Server_message = jObj.getString("message");

							ViewUtil.hideProgressDialog();
							ViewUtil.showAlertDialog(getActivity(), "Error",
									Server_message, true);
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"NO Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("PackageId", String
					.valueOf(params[1])));

			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.GetSinglePackage(parameter);
			return result;
		}

	}

	public class AsyncFilterPackageTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);
						Packageid.clear();
						Packagedataid.clear();
						dates.clear();
						total.clear();
						pic.clear();
						pictype.clear();
						type.clear();
						location.clear();
						value.clear();

						if (jObj.getBoolean("success")) {

							Log.e("Response on filter", result);
							JSONObject object = jObj.getJSONObject("message");

							addressheader.setText(object.getString("Address"));

							JSONArray Array = object
									.getJSONArray("packageDatas");

							for (int j = 0; j < Array.length(); j++) {

								JSONObject objectfrom = Array.getJSONObject(j);

								String packat = objectfrom
										.getString("PackageId_fk");
								String dataid = objectfrom
										.getString("PackageDataId");
								String ItemsCount = objectfrom
										.getString("ItemsCount");

								String ItemImage = "http://beta.brstdev.com/yiiezefind"
										+ objectfrom.getString("ItemImage");
								SimpleDateFormat input = new SimpleDateFormat(
										"yyyy-MM-dd");
								SimpleDateFormat output = new SimpleDateFormat(
										"dd/MM/yyyy");

								Date oneWayTripDate = input.parse(object
										.getString("EntryDate"));

								location.add("null");
								value.add(objectfrom.getString("value"));
								Packagedataid.add(dataid);
								total.add(ItemsCount);
								pic.add(ItemImage);
								type.add(objectfrom.getString("Type") + "  "
										+ objectfrom.getString("Number"));
								dates.add(output.format(oneWayTripDate));
								pictype.add("url");
								Packageid.add(packat);
							}

							layout_filter.setVisibility(View.GONE);

							adapter = new GridviewAdapter(getActivity(),
									Packageid, Packagedataid, dates, total,
									pic, type, location, value, pictype);
							gridView.setAdapter(adapter);

							layout_filter.setVisibility(View.INVISIBLE);
							ViewUtil.hideProgressDialog();
						}

						else {
							JSONObject object = jObj.getJSONObject("message");
							String Server_message = object
									.getString("packageDatas");
							layout_filter.setVisibility(View.GONE);
							ViewUtil.hideProgressDialog();
							ViewUtil.showAlertDialog(getActivity(), "Error",
									Server_message, true);

						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("PackageId", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("Type", String
					.valueOf(params[2])));

			Log.e("parameter filter", "" + parameter);
			String result = NetworkConnector.GetSinglePackage(parameter);
			return result;
		}

	}

}
