package com.appalmighty.ezefind.newentry.fgmt;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.login.act.LoginActivity;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;

public class InviteContactFragment extends Fragment {
	private ListAdapter1 adapter;
	private ListView listview;
	private Button btnInvite, btnLogout, btnhome;
	String phoneNumber;
	String name, Typed;

	ArrayList<String> contactname = new ArrayList<String>();
	ArrayList<String> contactnumber = new ArrayList<String>();
	ArrayList<String> allselected = new ArrayList<String>();
	String UserId;
	SharedPreferences shared;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_invite_page,
				container, false);
		listview = (ListView) rootView.findViewById(R.id.listView_contact);
		getNumber(getActivity().getContentResolver());
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = shared.getString(AppConstant.KEY_USER_ID, "");
		Log.e("", "" + getArguments().getString("Typed"));
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		btnLogout = (Button) rootView.findViewById(R.id.btn_logout);
		btnInvite = (Button) rootView.findViewById(R.id.btn_invite);
		Typed = getArguments().getString("Typed");
		btnInvite.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (allselected.size() == 0) {
					ViewUtil.showAlertDialog(getActivity(), "Error",
							"Please Select at Least One Contact", true);
				} else {
					
					Log.e("allselected", ""+allselected);
					new AsyncInviteTask().execute(UserId, getArguments()
							.getString("Packageid"), allselected.toString()
							.replace("]", "").replace("[", ""), Typed);
				}
			}
		});
		btnhome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				getActivity().finish();
				Intent intent = new Intent(getActivity(), MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);

			}
		});
		btnLogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				// set title
				alertDialogBuilder.setTitle("LOGOUT");

				// set dialog message
				alertDialogBuilder
						.setMessage("Are you sure for logout")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										SharedPreferences settings = getActivity()
												.getSharedPreferences(
														AppConstant.KEY_APP,
														Context.MODE_PRIVATE);
										settings.edit().clear().commit();
										getActivity().finish();
										Intent intent_home = new Intent(
												getActivity(),
												LoginActivity.class);
										startActivity(intent_home);

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			}
		});
		return rootView;
	}

	public void getNumber(ContentResolver cr) {
		Cursor phones = cr.query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
				null, null);
		while (phones.moveToNext()) {
			name = phones
					.getString(phones
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
			phoneNumber = phones
					.getString(phones
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

			contactname.add(name);
			contactnumber.add(phoneNumber.replace(")", "").replace("(", "")
					.replace("-", "").replace(" ", ""));

		}
		phones.close();
		Log.e("contact number", "" + contactnumber);

		adapter = new ListAdapter1(getActivity(), contactname, contactnumber);
		listview.setAdapter(adapter);

	}

	class ListAdapter1 extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;
		ArrayList<String> contactname1 = new ArrayList<String>();
		ArrayList<String> contactnumber1 = new ArrayList<String>();
		boolean my = false;

		public ListAdapter1(Activity activity, ArrayList<String> contactname,
				ArrayList<String> contactnumber) {
			this.contactname1.clear();
			this.contactnumber1.clear();
			this.activity = activity;
			this.contactname1 = contactname;
			this.contactnumber1 = contactnumber;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return contactname1.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return contactname1.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		class ViewHolder {

			private TextView titlename, titlenumber;
			private Button check;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			final ViewHolder holder = new ViewHolder();
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			try {
				if (convertView == null) {
					convertView = inflater.inflate(
							R.layout.fragment_invite_page_listitems, null);

					holder.titlename = (TextView) convertView
							.findViewById(R.id.txt_user_name);
					holder.titlenumber = (TextView) convertView
							.findViewById(R.id.txt_user_contactno);
					holder.check = (Button) convertView
							.findViewById(R.id.btn_check);

				}

				holder.check.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (my == false) {
							my = true;
							holder.check
									.setBackgroundResource(R.drawable.ic_radiocheck);
							allselected.add(contactnumber1.get(position));
							Log.e("checked", "" + allselected);

						} else if (my == true) {
							my = false;
							holder.check
									.setBackgroundResource(R.drawable.ic_radiouncheck);
							allselected.remove(contactnumber1.get(position));
							Log.e("unchecked", "" + allselected);

						}

					}
				});

				holder.titlename.setText(contactname1.get(position));

				holder.titlenumber.setText(contactnumber1.get(position));
			} catch (Exception e) {
				e.printStackTrace();
			}
			return convertView;
		}

	}

	public class AsyncInviteTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				JSONObject jObj = new JSONObject(result);

				if (jObj.getBoolean("success")) {
					Log.e("Response invite", result);

					if (Typed.equals("Package")) {
						getFragmentManager()
								.beginTransaction()
								.add(R.id.container,
										PickGoFragment.newInstance("pack_go"))
								.commit();
						getFragmentManager().popBackStack();
					} else {

						getFragmentManager()
								.beginTransaction()
								.add(R.id.container,
										PickGoFragment.newInstance("inventory"))
								.commit();
						getFragmentManager().popBackStack();
					}

					ViewUtil.hideProgressDialog();
				}

				else {
					Log.e("Response", result);

					String Server_message = jObj.getString("message");

					ViewUtil.hideProgressDialog();
					ViewUtil.showAlertDialog(getActivity(), "Error",
							Server_message, true);

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("PackageId", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("Contact", String
					.valueOf(params[2])));
			parameter.add(new BasicNameValuePair("Typed", String
					.valueOf(params[3])));

			Log.e("parameter invite", "" + parameter);
			String result = NetworkConnector.InviteUser(parameter);
			return result;
		}

	}

}
