package com.appalmighty.ezefind.newentry.fgmt;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.DataBase.DatasourceHandler;
import com.appalmighty.ezefind.login.act.LoginActivity;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.report.act.ViewReportMenuActivity;
import com.appalmighty.ezefind.setting.act.SettingActivity;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.appalmighty.ezefind.viewinventory.act.ViewInventoryActivity;

public class NewEntryMenuFragment extends Fragment implements OnClickListener {
	TextView btnBag, btnBox, btnSingleItem, btnBin;

	private ImageView buttonNewEntry, buttonReport, buttonViewInventory,
			buttonSetting;

	private Button buttonPacking, buttonInventory, btnhome, btnLogout;
	String UserId, packageid;
	SharedPreferences shared;
	NewEntryFormFragment newEntryFormFragment = null;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	DatasourceHandler DATABASE;
	int level;
	String time, Typed;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_newentry_menu_,
				container, false);
		buttonReport = (ImageView) rootView.findViewById(R.id.id_img_report);
		buttonViewInventory = (ImageView) rootView
				.findViewById(R.id.id_img_view_entry);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		buttonSetting = (ImageView) rootView.findViewById(R.id.id_img_setting);
		btnBag = (TextView) rootView.findViewById(R.id.button_bag);
		btnBox = (TextView) rootView.findViewById(R.id.button_box);
		btnLogout = (Button) rootView.findViewById(R.id.btn_logout);
		btnSingleItem = (TextView) rootView
				.findViewById(R.id.button_single_item);
		btnBin = (TextView) rootView.findViewById(R.id.button_bin);
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = shared.getString(AppConstant.KEY_USER_ID, "");

		btnBag.setOnClickListener(this);
		btnBox.setOnClickListener(this);
		btnSingleItem.setOnClickListener(this);
		btnBin.setOnClickListener(this);

		buttonReport.setOnClickListener(this);
		buttonViewInventory.setOnClickListener(this);
		buttonSetting.setOnClickListener(this);
		DATABASE = new DatasourceHandler(getActivity());
		internetcheck = new ConnectionDetector(getActivity());
		time = currenttime();

		Typed = getArguments().getString("Typed");
		if (getArguments().getString("Packageid") != null)
			packageid = getArguments().getString("Packageid");
		else {
			packageid = shared.getString(AppConstant.KEY_PackageId, "");
		}
		btnhome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().finish();
				Intent intent = new Intent(getActivity(), MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		btnLogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				// set title
				alertDialogBuilder.setTitle("LOGOUT");

				// set dialog message
				alertDialogBuilder
						.setMessage("Are you sure for logout")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										SharedPreferences settings = getActivity()
												.getSharedPreferences(
														AppConstant.KEY_APP,
														Context.MODE_PRIVATE);
										settings.edit().clear().commit();
										getActivity().finish();
										Intent intent_home = new Intent(
												getActivity(),
												LoginActivity.class);
										startActivity(intent_home);

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			}
		});
		return rootView;
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();

		switch (id) {
		case R.id.button_bag:

			isInternetPresent = internetcheck.isConnectingToInternet();

			if (isInternetPresent) {
				new AsyncPackageDataTask().execute(AppConstant.KEY_BAG, UserId,
						packageid, Typed);
				newEntryFormFragment = NewEntryFormFragment
						.newInstance(AppConstant.KEY_BAG);
			} else {
				Cursor cursor = DATABASE.getSinlgeEntry(packageid,
						AppConstant.KEY_BAG.toLowerCase());
				while (cursor.moveToNext()) {
					level = cursor.getInt(0);
				}

				Log.e("level", "" + level);
				String packagenumber = String.valueOf(level + 1);

				long packagedataid = DATABASE.InsertPackageData(packageid,
						AppConstant.KEY_BAG.toLowerCase(), packagenumber,
						UserId, time, Typed, "null", "null", "null", "null",
						"null", "null", "null", "null");

				Log.e("name inserted", "" + packagedataid);
				newEntryFormFragment = NewEntryFormFragment
						.newInstance(AppConstant.KEY_BAG);
				Editor editor = shared.edit();
				editor.putString(AppConstant.KEY_PackageId, packageid);
				editor.putString(AppConstant.KEY_PackageNumber, packagenumber);
				editor.putString(AppConstant.KEY_PackageDataId,
						String.valueOf(packagedataid));
				editor.commit();
				switchToFragment(newEntryFormFragment);

			}

			break;
		case R.id.button_box:
			// For Send Parameter Value of selected item,Title,Date,Time,UserId
			isInternetPresent = internetcheck.isConnectingToInternet();

			if (isInternetPresent) {
				new AsyncPackageDataTask().execute(AppConstant.KEY_BOX, UserId,
						packageid, Typed);
				newEntryFormFragment = NewEntryFormFragment
						.newInstance(AppConstant.KEY_BOX);
			} else {

				Cursor cursor = DATABASE.getSinlgeEntry(packageid,
						AppConstant.KEY_BOX.toLowerCase());
				while (cursor.moveToNext()) {
					level = cursor.getInt(0);
				}

				Log.e("level", "" + level);
				String packagenumber = String.valueOf(level + 1);

				long packagedataid = DATABASE.InsertPackageData(packageid,
						AppConstant.KEY_BOX.toLowerCase(), packagenumber,
						UserId, time, Typed, "null", "null", "null", "null",
						"null", "null", "null", "null");
				newEntryFormFragment = NewEntryFormFragment
						.newInstance(AppConstant.KEY_BOX);
				Editor editor = shared.edit();

				editor.putString(AppConstant.KEY_PackageNumber, packagenumber);
				editor.putString(AppConstant.KEY_PackageDataId,
						String.valueOf(packagedataid));
				editor.commit();
				switchToFragment(newEntryFormFragment);

			}

			// For Start New Fragment

			// switchToFragment(newEntryFormFragment);
			break;
		case R.id.button_single_item:
			// For Send Parameter Value of selected item,Title,Date,Time,UserId
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {
				new AsyncPackageDataTask().execute(AppConstant.KEY_SINGLE_ITEM,

				UserId, packageid, Typed);
				newEntryFormFragment = NewEntryFormFragment
						.newInstance(AppConstant.KEY_SINGLE_ITEM);
			} else {

				Cursor cursor = DATABASE.getSinlgeEntry(packageid,
						AppConstant.KEY_SINGLE_ITEM.toLowerCase());
				while (cursor.moveToNext()) {
					level = cursor.getInt(0);
				}

				Log.e("level", "" + level);
				String packagenumber = String.valueOf(level + 1);

				long packagedataid = DATABASE.InsertPackageData(packageid,
						AppConstant.KEY_SINGLE_ITEM.toLowerCase(),
						packagenumber, UserId, time, Typed, "null", "null",
						"null", "null", "null", "null", "null", "null");
				newEntryFormFragment = NewEntryFormFragment
						.newInstance(AppConstant.KEY_SINGLE_ITEM);
				Editor editor = shared.edit();

				editor.putString(AppConstant.KEY_PackageNumber, packagenumber);
				editor.putString(AppConstant.KEY_PackageDataId,
						String.valueOf(packagedataid));
				editor.commit();
				switchToFragment(newEntryFormFragment);
			}

			// For Start New Fragment

			// switchToFragment(newEntryFormFragment);
			break;
		case R.id.button_bin:
			// For Send Parameter Value of selected item,Title,Date,Time,UserId
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {
				new AsyncPackageDataTask().execute(AppConstant.KEY_BIN,

				UserId, packageid, Typed);
				newEntryFormFragment = NewEntryFormFragment
						.newInstance(AppConstant.KEY_BIN);
			} else {

				Cursor cursor = DATABASE.getSinlgeEntry(packageid,
						AppConstant.KEY_BIN.toLowerCase());
				while (cursor.moveToNext()) {
					level = cursor.getInt(0);
				}

				Log.e("level", "" + level);
				String packagenumber = String.valueOf(level + 1);

				long packagedataid = DATABASE.InsertPackageData(packageid,
						AppConstant.KEY_BIN.toLowerCase(), packagenumber,
						UserId, time, Typed, "null", "null", "null", "null",
						"null", "null", "null", "null");
				newEntryFormFragment = NewEntryFormFragment
						.newInstance(AppConstant.KEY_BIN);
				Editor editor = shared.edit();

				editor.putString(AppConstant.KEY_PackageNumber, packagenumber);
				editor.putString(AppConstant.KEY_PackageDataId,
						String.valueOf(packagedataid));
				editor.commit();
				switchToFragment(newEntryFormFragment);
			}

			break;

		case R.id.id_img_report:
			Intent intentReport = new Intent(getActivity(),
					ViewReportMenuActivity.class);
			startActivity(intentReport);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.id_img_view_entry:
			Intent i = new Intent(getActivity(), ViewInventoryActivity.class);
			startActivity(i);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.id_img_setting:
			Intent intentSetting = new Intent(getActivity(),
					SettingActivity.class);
			startActivity(intentSetting);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.id_button_home:
			getActivity().finish();
			Intent intent = new Intent(getActivity(), MainActivity.class);
			startActivity(intent);
			break;
		default:
			break;
		}

	}

	private String currenttime() {

		try {

			SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
			String format = s.format(new Date());

			Log.e("format: ", "" + format);
			return format;
		} catch (Exception ex) {
			return null;
		}
	}

	public class AsyncPackageDataTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response", result);

							JSONObject phone = jObj.getJSONObject("message");
							String number = phone.getString("Number");
							String packagedataid = phone
									.getString("PackageDataId");
							/*
							 * DATABASE.InsertPackageData(packageid,
							 * AppConstant.KEY_BAG.toLowerCase(), number,
							 * UserId, time,"Package");
							 */
							Editor editor = shared.edit();
							editor.putString(AppConstant.KEY_PackageId,
									packageid);
							editor.putString(AppConstant.KEY_PackageNumber,
									number);
							editor.putString(AppConstant.KEY_PackageDataId,
									packagedataid);
							editor.commit();

							switchToFragment(newEntryFormFragment);
							ViewUtil.hideProgressDialog();
						} else {
							Log.e("Response", result);

							String Server_message = jObj.getString("message");

							ViewUtil.hideProgressDialog();
							ViewUtil.showAlertDialog(getActivity(), "Error",
									Server_message, true);
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("Type", String
					.valueOf(params[0])));

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("PackageId", String
					.valueOf(params[2])));
			parameter.add(new BasicNameValuePair("Typed", String
					.valueOf(params[3])));
			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.PackageDetail1(parameter);
			return result;
		}

	}

	public void switchToFragment(Fragment newEntryFormFragment) {
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
				R.anim.slide_in_left, R.anim.slide_out_right);

		ft.replace(R.id.container, newEntryFormFragment);
		ft.addToBackStack(null);
		ft.commit();
	}

}
