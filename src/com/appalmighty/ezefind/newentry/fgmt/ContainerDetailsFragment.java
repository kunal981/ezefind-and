package com.appalmighty.ezefind.newentry.fgmt;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.DataBase.DatasourceHandler;
import com.appalmighty.ezefind.DataBase.NewEntryDataBase;
import com.appalmighty.ezefind.login.act.LoginActivity;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.net.NetworkConstant.Api;
import com.appalmighty.ezefind.net.NetworkConstant.Base;
import com.appalmighty.ezefind.newentry.act.PickGoActivity;
import com.appalmighty.ezefind.newentry.act.PostAPi;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.appalmighty.ezefind.util.Helper.ToastUi;
import com.appalmighty.ezefind.view.ChoosePopup;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class ContainerDetailsFragment extends Fragment implements
		OnClickListener {
	public static final int REQUEST_AUDIO_TO_TEXT_TITLE1 = 301;
	public static final int REQUEST_AUDIO_TO_TEXT_DES1 = 303;
	public static final int REQUEST_AUDIO_TO_TEXT_QUANTITY = 301;
	public static final int REQUEST_AUDIO_TO_TEXT_VALUE = 303;

	String titleKey;
	TextView textViewTitle, textaddress, atta;
	ImageButton imgPick;
	RelativeLayout rel_options, addToExitingList, newPickList;
	Button backbutton;
	Button btnClose, update, btnhome, btnLogout;
	ImageView main, attachment1, attachment2;
	EditText Title, Color, Description, Quantity, Attachment, Location, Value;
	GridView attchments;
	String UserId, packageid, packagedataid, itemid, image, quantity;
	SharedPreferences shared;
	public static final String KEY = "key";
	String Image;
	DisplayImageOptions displayImageOptions;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	DatasourceHandler DATABASE;
	Cursor itemcount;
	BitmapFactory.Options options;
	GridviewAdapter adapter;
	ArrayList<String> attachmentid;
	ArrayList<String> pic;
	ArrayList<String> pictype;
	ArrayList<String> picty;
	ArrayList<String> AllFile;
	ArrayList<String> Allfile1;
	int pos;
	String count;
	File file1, file2, file3, file4, file5;
	int attachmentcounty;
	public static final int REQUEST_AUDIO_TO_TEXT_TITLE = 301;
	public static final int REQUEST_AUDIO_TO_TEXT_DESC = 303;
	public static final int REQUEST_TAKE_PHOTO = 100;
	public static final int REQUEST_TAKE_PHOTO_ATTACHMENT = 101;
	public static final int REQUEST_TAKE_GALLERY = 200;
	public static final int RESULT_OK = -1;
	String outputFile = null;
	private File destination = null;
	String AUDIO_RECORDER_FOLDER = "eze-find";
	MediaRecorder myAudioRecorder = null;
	MediaPlayer myMediaPlayer = null;
	String typingvalue;
	String checkmedia = "0";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_container_details,
				container, false);
		System.gc();

		textViewTitle = (TextView) rootView.findViewById(R.id.txt_title_header);
		textaddress = (TextView) rootView.findViewById(R.id.textView1);

		imgPick = (ImageButton) rootView.findViewById(R.id.img_picklist);
		backbutton = (Button) rootView.findViewById(R.id.back_button);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		btnLogout = (Button) rootView.findViewById(R.id.btn_logout);
		rel_options = (RelativeLayout) rootView
				.findViewById(R.id.layout_selectoptions);
		attchments = (GridView) rootView.findViewById(R.id.attachmentview);
		update = (Button) rootView.findViewById(R.id.btn_edit);
		main = (ImageView) rootView.findViewById(R.id.image_view);
		atta = (TextView) rootView.findViewById(R.id.txt_attachments);
		Title = (EditText) rootView.findViewById(R.id.edt_items);
		Color = (EditText) rootView.findViewById(R.id.edt_color);
		Description = (EditText) rootView.findViewById(R.id.edt_desc);
		Quantity = (EditText) rootView.findViewById(R.id.edt_qty);
		Attachment = (EditText) rootView.findViewById(R.id.edt_attachment);
		Location = (EditText) rootView.findViewById(R.id.edt_location);
		Value = (EditText) rootView.findViewById(R.id.edt_value);
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		titleKey = shared.getString(AppConstant.KEY_TITLEOPEN, "");
		textViewTitle.setText(titleKey);
		attachmentid = new ArrayList<String>();
		pic = new ArrayList<String>();
		pictype = new ArrayList<String>();
		AllFile = new ArrayList<String>();
		Allfile1 = new ArrayList<String>();
		picty = new ArrayList<String>();
		UserId = shared.getString(AppConstant.KEY_USER_ID, "");
		DATABASE = new DatasourceHandler(getActivity());
		internetcheck = new ConnectionDetector(getActivity());
		adapter = new GridviewAdapter(getActivity(), attachmentid, pic,
				pictype, picty);
		attchments.setAdapter(adapter);
		itemid = getArguments().getString("itemid");
		atta.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Attachment.getText().toString().equals("5")) {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"You Already 5 Attachment", false);
				} else {
					openItemAttachMenu();
				}
			}
		});
		displayImageOptions = setupImageLoader();

		isInternetPresent = internetcheck.isConnectingToInternet();

		if (isInternetPresent) {

			new AsyncSelecteditemTask().execute(UserId, itemid);
		} else {

			itemcount = DATABASE.getSelectedItem(UserId, itemid);
			if (itemcount.getCount() != 0) {

				while (itemcount.moveToNext()) {

					image = itemcount.getString(8);

					File image = new File(itemcount.getString(8));
					Log.e("image", "" + image);
					options = new BitmapFactory.Options();
					options.inSampleSize = 8;

					Bitmap bitmap = BitmapFactory.decodeFile(
							image.getAbsolutePath(), options);
					main.setImageBitmap(bitmap);

					// main.setImageBitmap(ConvertToImage(itemcount.getString(8)));
					packageid = itemcount
							.getString(itemcount
									.getColumnIndex(NewEntryDataBase.KEY_PACKAGEIDdataitems));
					packagedataid = itemcount
							.getString(itemcount
									.getColumnIndex(NewEntryDataBase.KEY_PACKAGEDATAIDdataitems));

					Cursor location = DATABASE
							.GetPackagedatalocation(
									String.valueOf(itemcount
											.getColumnIndex(NewEntryDataBase.KEY_PACKAGEIDdataitems)),
									String.valueOf(itemcount
											.getColumnIndex(NewEntryDataBase.KEY_PACKAGEDATAIDdataitems)));

					Title.setText(itemcount.getString(4));
					Color.setText(null);
					Description.setText(itemcount.getString(5));
					Quantity.setText(itemcount.getString(6));

					Value.setText(itemcount.getString(11));
					if (location.getCount() != 0) {
						if (location.moveToFirst()) {
							Location.setText(location.getString(location
									.getColumnIndex(NewEntryDataBase.KEY_LOCATIONDATA)));

						}
					}
				}
				Cursor attachment = DATABASE.getAttachment(itemid);

				if (attachment.moveToFirst()) {
					count = attachment.getString(6);
					Attachment.setText(count);
					Log.e("attachment", "" + attachment.getString(6));
					attchments.setVisibility(View.VISIBLE);
					if (count.equals("1")) {
						attachmentid.add("1");
						pic.add(attachment.getString(1));
						pictype.add("jpg");
						attachmentcounty = pic.size();
						picty.add("64");
					} else if (count.equals("2")) {
						attachmentid.add("1");
						attachmentid.add("1");
						pic.add(attachment.getString(1));
						pic.add(attachment.getString(2));
						pictype.add("jpg");
						pictype.add("jpg");
						attachmentcounty = pic.size();
						attachmentcounty = pic.size();
						picty.add("64");
						picty.add("64");

					} else if (count.equals("3")) {
						attachmentid.add("1");
						attachmentid.add("1");
						attachmentid.add("1");
						pic.add(attachment.getString(1));
						pic.add(attachment.getString(2));
						pic.add(attachment.getString(3));
						pictype.add("jpg");
						pictype.add("jpg");
						pictype.add("jpg");
						attachmentcounty = pic.size();
						attachmentcounty = pic.size();
						attachmentcounty = pic.size();
						picty.add("64");
						picty.add("64");
						picty.add("64");
					} else if (count.equals("4")) {
						attachmentid.add("1");
						attachmentid.add("1");
						attachmentid.add("1");
						attachmentid.add("1");
						pic.add(attachment.getString(1));
						pic.add(attachment.getString(2));
						pic.add(attachment.getString(3));
						pic.add(attachment.getString(4));
						pictype.add("jpg");
						pictype.add("jpg");
						pictype.add("jpg");
						pictype.add("jpg");
						attachmentcounty = pic.size();
						attachmentcounty = pic.size();
						attachmentcounty = pic.size();
						attachmentcounty = pic.size();
						picty.add("64");
						picty.add("64");
						picty.add("64");
						picty.add("64");
					} else if (count.equals("5")) {
						attachmentid.add("1");
						attachmentid.add("2");
						attachmentid.add("1");
						attachmentid.add("2");
						attachmentid.add("1");

						pic.add(attachment.getString(1));
						pic.add(attachment.getString(2));
						pic.add(attachment.getString(3));
						pic.add(attachment.getString(4));
						pic.add(attachment.getString(5));

						pictype.add("jpg");
						pictype.add("jpg");
						pictype.add("jpg");
						pictype.add("jpg");
						pictype.add("jpg");

						attachmentcounty = pic.size();
						attachmentcounty = pic.size();
						attachmentcounty = pic.size();
						attachmentcounty = pic.size();
						attachmentcounty = pic.size();

						picty.add("64");
						picty.add("64");
						picty.add("64");
						picty.add("64");
						picty.add("64");
					}

				}

			}
			adapter.notifyDataSetChanged();
		}

		addListerToPickOption(rootView);
		update.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SQLiteDatabase database = getActivity()
						.openOrCreateDatabase("Eze-find.db",
								SQLiteDatabase.CREATE_IF_NECESSARY, null);
				if (isInternetPresent) {
					Allfile1.clear();
					for (int z = 0; z < AllFile.size(); z++) {
						File mFile = new File(AllFile.get(z));

						Allfile1.add(mFile.getAbsolutePath());
						Log.e("All: ", "" + Allfile1);

					}
					AllFile.clear();

					new UploadMediaFile().execute();

				} else {
					Log.e("count", "" + count);
					Allfile1.clear();
					for (int z = 0; z < AllFile.size(); z++) {
						File mFile = new File(AllFile.get(z));

						Allfile1.add(mFile.getAbsolutePath());
						Log.e("All: ", "" + Allfile1);

					}
					AllFile.clear();
					switch (count) {
					case "0":
						if (Allfile1.size() == 0) {

						} else if (Allfile1.size() == 1) {

							ContentValues cv = new ContentValues();

							cv.put(NewEntryDataBase.KEY_FILE, Allfile1.get(0));

							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);

						} else if (Allfile1.size() == 2) {
							ContentValues cv = new ContentValues();

							cv.put(NewEntryDataBase.KEY_FILE, Allfile1.get(0));
							cv.put(NewEntryDataBase.KEY_FILE1, Allfile1.get(1));
							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);
						} else if (Allfile1.size() == 3) {
							ContentValues cv = new ContentValues();
							cv.put(NewEntryDataBase.KEY_FILE, Allfile1.get(0));
							cv.put(NewEntryDataBase.KEY_FILE1, Allfile1.get(1));
							cv.put(NewEntryDataBase.KEY_FILE2, Allfile1.get(2));
							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);
						} else if (Allfile1.size() == 4) {
							ContentValues cv = new ContentValues();
							cv.put(NewEntryDataBase.KEY_FILE, Allfile1.get(0));
							cv.put(NewEntryDataBase.KEY_FILE1, Allfile1.get(1));
							cv.put(NewEntryDataBase.KEY_FILE2, Allfile1.get(2));
							cv.put(NewEntryDataBase.KEY_FILE3, Allfile1.get(3));
							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);
						} else if (Allfile1.size() == 5) {
							ContentValues cv = new ContentValues();
							cv.put(NewEntryDataBase.KEY_FILE, Allfile1.get(0));
							cv.put(NewEntryDataBase.KEY_FILE1, Allfile1.get(1));
							cv.put(NewEntryDataBase.KEY_FILE2, Allfile1.get(2));
							cv.put(NewEntryDataBase.KEY_FILE3, Allfile1.get(3));
							cv.put(NewEntryDataBase.KEY_FILE4, Allfile1.get(4));
							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);
						}

						break;
					case "1":
						if (Allfile1.size() == 0) {

						} else if (Allfile1.size() == 1) {

							ContentValues cv = new ContentValues();

							cv.put(NewEntryDataBase.KEY_FILE1, Allfile1.get(0));

							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);

						} else if (Allfile1.size() == 2) {
							ContentValues cv = new ContentValues();

							cv.put(NewEntryDataBase.KEY_FILE1, Allfile1.get(0));
							cv.put(NewEntryDataBase.KEY_FILE2, Allfile1.get(1));
							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);
						} else if (Allfile1.size() == 3) {
							ContentValues cv = new ContentValues();
							cv.put(NewEntryDataBase.KEY_FILE1, Allfile1.get(0));
							cv.put(NewEntryDataBase.KEY_FILE2, Allfile1.get(1));
							cv.put(NewEntryDataBase.KEY_FILE3, Allfile1.get(2));
							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);
						} else if (Allfile1.size() == 4) {
							ContentValues cv = new ContentValues();
							cv.put(NewEntryDataBase.KEY_FILE1, Allfile1.get(0));
							cv.put(NewEntryDataBase.KEY_FILE2, Allfile1.get(1));
							cv.put(NewEntryDataBase.KEY_FILE3, Allfile1.get(2));
							cv.put(NewEntryDataBase.KEY_FILE4, Allfile1.get(3));
							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);
						}
						break;
					case "2":
						if (Allfile1.size() == 0) {

						} else if (Allfile1.size() == 1) {

							ContentValues cv = new ContentValues();

							cv.put(NewEntryDataBase.KEY_FILE2, Allfile1.get(0));

							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);

						} else if (Allfile1.size() == 2) {
							ContentValues cv = new ContentValues();

							cv.put(NewEntryDataBase.KEY_FILE2, Allfile1.get(0));
							cv.put(NewEntryDataBase.KEY_FILE3, Allfile1.get(1));
							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);
						} else if (Allfile1.size() == 3) {
							ContentValues cv = new ContentValues();
							cv.put(NewEntryDataBase.KEY_FILE2, Allfile1.get(0));
							cv.put(NewEntryDataBase.KEY_FILE3, Allfile1.get(1));
							cv.put(NewEntryDataBase.KEY_FILE4, Allfile1.get(2));
							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);
						}

						break;
					case "3":
						Log.e("herein 3", "herein 3");
						if (Allfile1.size() == 0) {

						} else if (Allfile1.size() == 1) {
							Log.e("herein 3", "con");
							ContentValues cv = new ContentValues();

							cv.put(NewEntryDataBase.KEY_FILE3, Allfile1.get(0));

							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);
							Log.e("Updated row", "" + row);
						} else if (Allfile1.size() == 2) {
							ContentValues cv = new ContentValues();

							cv.put(NewEntryDataBase.KEY_FILE3, Allfile1.get(0));
							cv.put(NewEntryDataBase.KEY_FILE4, Allfile1.get(1));
							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);
						}
						break;
					case "4":
						if (Allfile1.size() == 0) {

						} else if (Allfile1.size() == 1) {

							ContentValues cv = new ContentValues();

							cv.put(NewEntryDataBase.KEY_FILE4, Allfile1.get(0));

							cv.put(NewEntryDataBase.KEY_FILECOUNT, Attachment
									.getText().toString());

							long row = database
									.update(NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
											cv, NewEntryDataBase.KEY_FILEITEMID
													+ "=" + itemid, null);

						}

						break;
					case "5":

						break;

					default:
						break;
					}

					long row = DATABASE.UpdatePackageDataitem(UserId,
							packageid, packagedataid, Title.getText()
									.toString(), Description.getText()
									.toString(), image, itemid, "64", Value
									.getText().toString(), Attachment.getText()
									.toString());

					if (row > 0) {
						Title.setText(Title.getText().toString());
						Color.setText(Color.getText().toString());
						Description.setText(Description.getText().toString());
						Quantity.setText(Quantity.getText().toString());
						Attachment.setText(Attachment.getText().toString());

						ViewUtil.showAlertDialog(getActivity(), "Success",
								"Your Data is Updated Successfully", true);
					}
				}
			}
		});

		imgPick.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rel_options.setVisibility(View.VISIBLE);
			}
		});
		backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		btnLogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// TODO Auto-generated method stub

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				// set title
				alertDialogBuilder.setTitle("LOGOUT");

				// set dialog message
				alertDialogBuilder
						.setMessage("Are you sure for logout")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										SharedPreferences settings = getActivity()
												.getSharedPreferences(
														AppConstant.KEY_APP,
														Context.MODE_PRIVATE);
										settings.edit().clear().commit();
										getActivity().finish();
										Intent intent_home = new Intent(
												getActivity(),
												LoginActivity.class);
										startActivity(intent_home);

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			}
		});
		btnhome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().finish();
				Intent intent = new Intent(getActivity(), MainActivity.class);
				startActivity(intent);
			}
		});
		Title.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				// TODO Auto-generated method stub
				// Log.e("focus", "focus:"+hasFocus);
				if (hasFocus) {
					typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE,
							"");
					// Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						Title.setInputType(InputType.TYPE_NULL);
						getActivity()
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_TITLE1);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}

			}
		});
		Description.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				// TODO Auto-generated method stub
				// Log.e("focus", "focus:"+hasFocus);
				if (hasFocus) {
					typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE,
							"");
					// Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						Description.setInputType(InputType.TYPE_NULL);
						getActivity()
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_DES1);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}

			}
		});
		Quantity.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				// TODO Auto-generated method stub
				// Log.e("focus", "focus:"+hasFocus);
				if (hasFocus) {
					typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE,
							"");
					// Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						Quantity.setInputType(InputType.TYPE_NULL);
						getActivity()
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_QUANTITY);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}

			}
		});
		Value.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				// TODO Auto-generated method stub
				// Log.e("focus", "focus:"+hasFocus);
				if (hasFocus) {
					typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE,
							"");
					// Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						Value.setInputType(InputType.TYPE_NULL);
						getActivity()
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(getActivity());
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(getActivity()) == true) {
							// if yes � running recognition
							startRecognition(getActivity(),
									REQUEST_AUDIO_TO_TEXT_VALUE);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									getActivity(),
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup.installGoogleVoiceSearch(getActivity());
						}
					}
				}

			}
		});
		Title.setOnClickListener(this);
		Description.setOnClickListener(this);
		Quantity.setOnClickListener(this);
		Value.setOnClickListener(this);
		return rootView;
	}

	protected void openItemAttachMenu() {
		// TODO Auto-generated method stub
		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_attachement, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

		Button optionphotoVideo = (Button) popupView
				.findViewById(R.id.btn_photo_video);
		final Button optionphotoAudio = (Button) popupView
				.findViewById(R.id.btn_audio);
		Button optionBarcode = (Button) popupView
				.findViewById(R.id.btn_barcode);
		ImageView btnClose = (ImageView) popupView.findViewById(R.id.btn_close);
		optionphotoVideo.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				openCameraOptionDailog();
				popupWindow.dismiss();

			}
		});
		optionphotoAudio.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				openAudioOptionDailog();
				popupWindow.dismiss();

			}
		});
		optionBarcode.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				ToastUi.print(getActivity(), "Coming soon");

			}
		});
		btnClose.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();

			}
		});
		popupWindow.showAtLocation(rel_options, Gravity.CENTER, 0, 0);
	}

	private void openAudioOptionDailog() {

		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_audio_recording, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		ImageView btnClose = (ImageView) popupView.findViewById(R.id.btn_close);
		ImageView btnSave = (ImageView) popupView
				.findViewById(R.id.id_btn_save);
		ImageView btnDel = (ImageView) popupView.findViewById(R.id.id_btn_del);
		final Chronometer focus = (Chronometer) popupView
				.findViewById(R.id.chronometer1);

		final TextView duration = (TextView) popupView
				.findViewById(R.id.duration);
		final ImageView btn_play_pause = (ImageView) popupView
				.findViewById(R.id.id_btn_record);
		final ImageView btn_play_record = (ImageView) popupView
				.findViewById(R.id.imageView1);

		final Handler seekHandler = new Handler();

		final SeekBar seekBar = (SeekBar) popupView.findViewById(R.id.seek_bar);

		btn_play_pause.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!btn_play_pause.isSelected()) {
					// to start audio recording

					initMediaRecorder();
					start(v);

				} else {
					// to stop audio recording
					stop(v);
				}

			}

			private void stop(View v) {
				myAudioRecorder.stop();
				myAudioRecorder.release();
				focus.stop();

				myAudioRecorder = null;
				btn_play_pause.setSelected(false);
				Toast.makeText(getActivity(), "Audio recorded successfully",
						Toast.LENGTH_LONG).show();
			}

			private void start(View v) {
				try {
					myAudioRecorder.prepare();
					myAudioRecorder.start();
					btn_play_pause.setSelected(true);
					Toast.makeText(getActivity(), "Recording started",
							Toast.LENGTH_LONG).show();
					focus.start();
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					btn_play_pause.setSelected(false);
					Toast.makeText(getActivity(), "Recording falied",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					btn_play_pause.setSelected(false);
					Toast.makeText(getActivity(), "Recording falied",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}

			}

			private void initMediaRecorder() {
				myAudioRecorder = new MediaRecorder();
				myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
				myAudioRecorder
						.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
				myAudioRecorder
						.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
				outputFile = getFilename();
				myAudioRecorder.setOutputFile(outputFile);
			}

			private String getFilename() {
				String filepath = Environment.getExternalStorageDirectory()
						.getPath();
				File file = new File(filepath, AUDIO_RECORDER_FOLDER);
				if (!file.exists()) {
					file.mkdirs();
				}
				String path = file.getAbsolutePath() + "/"
						+ System.currentTimeMillis() + ".3gp";

				Log.e("audioo", "" + path);
				AllFile.add(path);
				return path;
			}
		});

		btn_play_record.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!btn_play_record.isSelected()) {

					startPlaying();

				} else {
					stopPlayingRecord();

				}

			}

			private void stopPlayingRecord() {
				myMediaPlayer.stop();
				myMediaPlayer.release();
				myMediaPlayer = null;
				btn_play_record.setSelected(false);
			}

			private void startPlaying() {
				// TODO Auto-generated method stub

				if (outputFile != null && !outputFile.equals("")) {
					if (myMediaPlayer == null) {
						myMediaPlayer = new MediaPlayer();
					}

					try {
						myMediaPlayer.setDataSource(outputFile);// Write your
						// location

						myMediaPlayer.prepare();
						myMediaPlayer.start();

						seekBar.setMax(myMediaPlayer.getDuration());
						int mediaDuration = myMediaPlayer.getDuration();
						int mediaPosition = myMediaPlayer.getCurrentPosition();
						double timeRemaining = mediaDuration - mediaPosition;
						duration.setText(String.format(
								"%d min, %d sec",
								TimeUnit.MILLISECONDS
										.toMinutes((long) timeRemaining),
								TimeUnit.MILLISECONDS
										.toSeconds((long) timeRemaining)
										- TimeUnit.MINUTES
												.toSeconds(TimeUnit.MILLISECONDS
														.toMinutes((long) timeRemaining))));

						btn_play_record.setSelected(true);
						seekUpdation();
					} catch (Exception e) {
						e.printStackTrace();
						Toast.makeText(getActivity(), "File not exist",
								Toast.LENGTH_SHORT).show();
					}

				} else {
					Toast.makeText(getActivity(), "File not exist",
							Toast.LENGTH_SHORT).show();
				}

			}

			public void seekUpdation() {
				if (myMediaPlayer != null) {
					seekBar.setProgress(myMediaPlayer.getCurrentPosition());
					seekHandler.postDelayed(run, 1000);
				} else {
					seekBar.setProgress(0);
				}
			}

			Runnable run = new Runnable() {
				@Override
				public void run() {
					seekUpdation();
				}
			};

		});

		btnClose.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (myAudioRecorder != null) {
					myAudioRecorder.stop();
					myAudioRecorder.release();
					focus.stop();
					myAudioRecorder = null;
				}
				if (myMediaPlayer != null) {
					myMediaPlayer.stop();
					myMediaPlayer.release();
					myMediaPlayer = null;
				}
				popupWindow.dismiss();

			}
		});
		btnSave.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				Toast.makeText(getActivity(), "Successfully Save the file",
						Toast.LENGTH_SHORT).show();

			}
		});
		btnDel.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				deleteFile();

			}

			private void deleteFile() {
				if (outputFile != null && !outputFile.equals("")) {
					File file = new File(outputFile);
					if (file != null) {
						if (file.exists()) {
							file.delete();
							AllFile.remove(outputFile);

							Toast.makeText(getActivity(),
									"Audio Deleted successfully",
									Toast.LENGTH_SHORT).show();
						} else {
							Toast.makeText(getActivity(), "File not exit ",
									Toast.LENGTH_SHORT).show();
						}
					}

				} else {
					Toast.makeText(getActivity(), "File not exit ",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		popupWindow.showAtLocation(rel_options, Gravity.CENTER, 0, 0);
	}

	private void openCameraOptionDailog() {

		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_caputre_selection, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		Button camera = (Button) popupView
				.findViewById(R.id.id_button_take_camera);
		Button gallery = (Button) popupView.findViewById(R.id.id_button_save);
		camera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(intent, 101);
				popupWindow.dismiss();

			}
		});
		gallery.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				intent.setType("image/*");
				startActivityForResult(
						Intent.createChooser(intent, "Select File"), 200);
				popupWindow.dismiss();

			}
		});
		ImageView btnClose = (ImageView) popupView.findViewById(R.id.btn_close);
		btnClose.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();

				Attachment.setText("" + (attachmentcounty + AllFile.size()));

			}
		});
		popupWindow.showAtLocation(rel_options, Gravity.CENTER, 0, 0);

	}

	public Bitmap ConvertToImage(String image) {
		try {
			byte[] imageAsBytes = Base64.decode(image.getBytes(),
					Base64.DEFAULT);

			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);

			return bitmap;
		} catch (Exception e) {
			return null;
		}
	}

	private DisplayImageOptions setupImageLoader() {
		// TODO Auto-generated method stub
		return new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1)).build();
	}

	private void addListerToPickOption(View rootView) {
		btnClose = (Button) rootView.findViewById(R.id.btnclose);
		addToExitingList = (RelativeLayout) rootView
				.findViewById(R.id.linear_bag);
		newPickList = (RelativeLayout) rootView.findViewById(R.id.linear_bin);
		// addToExitingList = (ImageView) rootView
		// .findViewById(R.id.id_add_exiting_list);

		btnClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				rel_options.setVisibility(View.GONE);
			}
		});
		newPickList.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.e("itemid", "" + itemid);
				StartPackingFragment fragment = new StartPackingFragment();
				FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				ft.setCustomAnimations(R.anim.slide_in_right,
						R.anim.slide_out_left, R.anim.slide_in_left,
						R.anim.slide_out_right);
				Bundle bundle = new Bundle();

				bundle.putString("key", "pick_list");
				fragment.setArguments(bundle);
				// StartPackingFragment.newInstance("pick_list")
				ft.replace(R.id.container, fragment);
				ft.addToBackStack(null);
				ft.commit();

				Editor editor = shared.edit();

				editor.putString(AppConstant.KEY_ItemId, itemid);

				editor.commit();
			}
		});
		addToExitingList.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Editor editor = shared.edit();

				editor.putString(AppConstant.KEY_ItemId, itemid);

				editor.commit();
				Intent intent1 = new Intent(getActivity(), PickGoActivity.class);
				intent1.putExtra("Pack_go", "pick_list");
				startActivity(intent1);
				getActivity().overridePendingTransition(R.anim.fade_in,
						R.anim.fade_out);
				getActivity().finish();
			}
		});
	}

	protected void startRecognition(Activity mainActivity, int RequestCode) {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
		startActivityForResult(intent, RequestCode);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		/*
		 * if (requestCode == 100) {
		 */
		switch (requestCode) {

		case REQUEST_TAKE_PHOTO_ATTACHMENT:
			if (resultCode == RESULT_OK && null != data) {
				Bitmap thumbnail1 = (Bitmap) data.getExtras().get("data");
				ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();
				thumbnail1.compress(Bitmap.CompressFormat.JPEG, 100, bytes1);

				File destination1 = new File(
						Environment.getExternalStorageDirectory(),
						System.currentTimeMillis() + ".jpg");

				AllFile.add(destination1.toString());
				Attachment.setText("" + (attachmentcounty + AllFile.size()));

				FileOutputStream fos;
				try {
					destination1.createNewFile();
					fos = new FileOutputStream(destination1);
					fos.write(bytes1.toByteArray());

					fos.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			break;
		case REQUEST_TAKE_GALLERY:
			if (resultCode == RESULT_OK && null != data) {
				Uri selectedImageUri = data.getData();
				String[] projection = { MediaColumns.DATA };
				Cursor cursor = getActivity().managedQuery(selectedImageUri,
						projection, null, null, null);
				int column_index = cursor
						.getColumnIndexOrThrow(MediaColumns.DATA);
				cursor.moveToFirst();

				String selectedImagePath = cursor.getString(column_index);
				Log.e("selectedImagePath", "" + selectedImagePath);
				AllFile.add(selectedImagePath);
				Attachment.setText("" + (attachmentcounty + AllFile.size()));

			}
			break;

		}

	}

	public class AsyncSelecteditemTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response single item info", result);
							pic.clear();
							pictype.clear();
							attachmentid.clear();
							JSONArray array = jObj.getJSONArray("message");
							for (int i = 0; i < array.length(); i++) {
								JSONObject object = array.getJSONObject(i);

								Image = "http://beta.brstdev.com/yiiezefind"
										+ object.getString("Image");

								ImageLoader.getInstance().displayImage(
										String.valueOf(Image), main);
								Title.setText(object.getString("Title"));
								Color.setText(object.getString("Color"));
								Description.setText(object
										.getString("Description"));
								Quantity.setText(object.getString("Quantity"));
								Attachment.setText(object
										.getString("AttachmentCount"));
								textaddress
										.setText(object.getString("Address"));
								Value.setText("$ " + object.getString("value"));
								packageid = object.getString("PackageId");
								packagedataid = object
										.getString("PackageDataId");
								if (object.has("Location")) {
									JSONObject objec = object
											.getJSONObject("Location");
									if (objec.getString("Type").equals("gps")) {
										Location.setText(objec
												.getString("LocationName"));
									} else {
										Location.setText(objec
												.getString("Value"));
									}

								} else {
									Location.setText("null");
								}

								if (object.has("Attachments")) {
									attchments.setVisibility(View.VISIBLE);
									JSONArray Array = object
											.getJSONArray("Attachments");

									for (int j = 0; j < Array.length(); j++) {
										JSONObject objectfrom = Array
												.getJSONObject(j);

										attachmentid.add(objectfrom
												.getString("AttachmentId"));

										pic.add("http://beta.brstdev.com/yiiezefind"
												+ objectfrom
														.getString("FileName"));

										pictype.add(objectfrom
												.getString("FileType"));

										attachmentcounty = pic.size();

										// ImageLoader.getInstance().displayImage(
										// first, attachment1);
										picty.add("url");
									}

								} else {

								}
							}
							adapter.notifyDataSetChanged();
							ViewUtil.hideProgressDialog();
						}

						else {
							Log.e("Response", result);

							String Server_message = jObj.getString("message");

							ViewUtil.hideProgressDialog();
							ViewUtil.showAlertDialog(getActivity(), "Error",
									Server_message, true);

						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("ItemId", String
					.valueOf(params[1])));

			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.GetSingleItem(parameter);
			return result;
		}

	}

	public static ContainerDetailsFragment newInstance() {
		ContainerDetailsFragment fragment = new ContainerDetailsFragment();

		return fragment;
	}

	private class UploadMediaFile extends AsyncTask<String, Integer, String> {
		@Override
		protected void onPreExecute() {
			// setting progress bar to zero

			ViewUtil.showProgressDialog(getActivity());

			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {

			return uploadFile();

		}

		private String uploadFile() {
			String responseString = null;

			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(Base.PACKAGEAPI
					+ Api.UPDATEPACKAGEDATAITEM);
			// HttpPost httppost = new HttpPost(Base.PACKAGEAPI + Api.TEST);
			try {

				PostAPi entity = new PostAPi(getActivity());
				try {

					if (Allfile1.size() == 0) {

						Allfile1.clear();

						entity.addPart("PackageId", new StringBody(packageid));
						entity.addPart("PackageDataId", new StringBody(
								packagedataid));
						entity.addPart("ItemId",
								new StringBody(String.valueOf(itemid)));
						entity.addPart(
								"Quantity",
								new StringBody(String.valueOf(Quantity
										.getText().toString())));
						entity.addPart("Title", new StringBody(Title.getText()
								.toString()));
						entity.addPart("Description", new StringBody(
								Description.getText().toString()));

						entity.addPart("Value", new StringBody(Value.getText()
								.toString().replace("$", "").trim()));

						entity.addPart("UserId", new StringBody(UserId));

					} else if (Allfile1.size() == 1) {
						file1 = new File(Allfile1.get(0));

						Allfile1.clear();
						entity.addPart("PackageId", new StringBody(packageid));
						entity.addPart("PackageDataId", new StringBody(
								packagedataid));
						entity.addPart("ItemId",
								new StringBody(String.valueOf(itemid)));
						entity.addPart(
								"Quantity",
								new StringBody(String.valueOf(Quantity
										.getText().toString())));
						entity.addPart("Title", new StringBody(Title.getText()
								.toString()));
						entity.addPart("Description", new StringBody(
								Description.getText().toString()));
						entity.addPart("image1", new FileBody(file1));

						entity.addPart("Value", new StringBody(Value.getText()
								.toString().replace("$", "").trim()));

						entity.addPart("UserId", new StringBody(UserId));

					} else if (Allfile1.size() == 2) {

						file1 = new File(Allfile1.get(0));
						file2 = new File(Allfile1.get(1));

						Allfile1.clear();
						entity.addPart("PackageId", new StringBody(packageid));
						entity.addPart("PackageDataId", new StringBody(
								packagedataid));
						entity.addPart("ItemId",
								new StringBody(String.valueOf(itemid)));
						entity.addPart(
								"Quantity",
								new StringBody(String.valueOf(Quantity
										.getText().toString())));
						entity.addPart("Title", new StringBody(Title.getText()
								.toString()));
						entity.addPart("Description", new StringBody(
								Description.getText().toString()));

						entity.addPart("Value", new StringBody(Value.getText()
								.toString().replace("$", "").trim()));

						entity.addPart("UserId", new StringBody(UserId));
						entity.addPart("image1", new FileBody(file1));
						entity.addPart("image2", new FileBody(file2));
					} else if (Allfile1.size() == 3) {
						file1 = new File(Allfile1.get(0));
						file2 = new File(Allfile1.get(1));
						file3 = new File(Allfile1.get(2));

						Allfile1.clear();
						entity.addPart("PackageId", new StringBody(packageid));
						entity.addPart("PackageDataId", new StringBody(
								packagedataid));
						entity.addPart("ItemId",
								new StringBody(String.valueOf(itemid)));
						entity.addPart(
								"Quantity",
								new StringBody(String.valueOf(Quantity
										.getText().toString())));
						entity.addPart("Title", new StringBody(Title.getText()
								.toString()));
						entity.addPart("Description", new StringBody(
								Description.getText().toString()));

						entity.addPart("Value", new StringBody(Value.getText()
								.toString().replace("$", "").trim()));

						entity.addPart("UserId", new StringBody(UserId));
						entity.addPart("image1", new FileBody(file1));
						entity.addPart("image2", new FileBody(file2));
						entity.addPart("image3", new FileBody(file3));

					} else if (Allfile1.size() == 4) {
						file1 = new File(Allfile1.get(0));
						file2 = new File(Allfile1.get(1));
						file3 = new File(Allfile1.get(2));
						file4 = new File(Allfile1.get(3));
						entity.addPart("PackageId", new StringBody(packageid));
						entity.addPart("PackageDataId", new StringBody(
								packagedataid));
						entity.addPart("ItemId",
								new StringBody(String.valueOf(itemid)));
						entity.addPart(
								"Quantity",
								new StringBody(String.valueOf(Quantity
										.getText().toString())));
						entity.addPart("Title", new StringBody(Title.getText()
								.toString()));
						entity.addPart("Description", new StringBody(
								Description.getText().toString()));

						entity.addPart("Value", new StringBody(Value.getText()
								.toString().replace("$", "").trim()));

						entity.addPart("UserId", new StringBody(UserId));
						entity.addPart("image1", new FileBody(file1));
						entity.addPart("image2", new FileBody(file2));
						entity.addPart("image3", new FileBody(file3));
						entity.addPart("image4", new FileBody(file4));

						Allfile1.clear();

					} else if (Allfile1.size() == 5) {
						file1 = new File(Allfile1.get(0));
						file2 = new File(Allfile1.get(1));
						file3 = new File(Allfile1.get(2));
						file4 = new File(Allfile1.get(3));
						file5 = new File(Allfile1.get(4));
						entity.addPart("PackageId", new StringBody(packageid));
						entity.addPart("PackageDataId", new StringBody(
								packagedataid));
						entity.addPart("ItemId",
								new StringBody(String.valueOf(itemid)));
						entity.addPart(
								"Quantity",
								new StringBody(String.valueOf(Quantity
										.getText().toString())));
						entity.addPart("Title", new StringBody(Title.getText()
								.toString()));
						entity.addPart("Description", new StringBody(
								Description.getText().toString()));

						entity.addPart("Value", new StringBody(Value.getText()
								.toString().replace("$", "").trim()));

						entity.addPart("UserId", new StringBody(UserId));
						entity.addPart("image1", new FileBody(file1));
						entity.addPart("image2", new FileBody(file2));
						entity.addPart("image3", new FileBody(file3));
						entity.addPart("image4", new FileBody(file4));
						entity.addPart("image5", new FileBody(file5));
						Allfile1.clear();

					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				httppost.setEntity(entity);
				// file.delete();
				// Making server call
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity r_entity = response.getEntity();

				int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode == 200) {

					responseString = EntityUtils.toString(r_entity);
				} else {

					responseString = "Error occurred! Http Status Code: "
							+ statusCode;
				}

			} catch (ClientProtocolException e) {
				Log.e("Client", "" + e.toString());
				responseString = e.toString();
			} catch (IOException e) {
				Log.e("Io", "" + e.toString());
				responseString = e.toString();
			}

			return responseString;

		}

		@Override
		protected void onPostExecute(String result) {

			super.onPostExecute(result);
			Log.e("", "Response from server: " + result);

			JSONObject jObj;
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response", result);

							/*
							 * DATABASE.InsertPackage_Data_items(UserId,
							 * packageid, packagedataid, title, description,
							 * quantity .getText().toString(), Allfile.size(),
							 * image, "url");
							 */
							new AsyncSelecteditemTask().execute(UserId, itemid);
							ViewUtil.hideProgressDialog();

						} else {
							Toast.makeText(getActivity(), "Data Not Uploaded",
									Toast.LENGTH_LONG).show();
							ViewUtil.hideProgressDialog();
						}

					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	class GridviewAdapter extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;
		ArrayList<String> Packageid1 = new ArrayList<String>();

		ArrayList<String> pic1 = new ArrayList<String>();
		ArrayList<String> pictype1 = new ArrayList<String>();
		ArrayList<String> picty1 = new ArrayList<String>();

		DisplayImageOptions displayImageOptions;
		MediaPlayer mPlayer;

		public GridviewAdapter(Activity activity, ArrayList<String> packageid,
				ArrayList<String> pic, ArrayList<String> pictype,
				ArrayList<String> picty) {
			this.activity = activity;
			Packageid1 = packageid;
			pictype1 = pictype;
			pic1 = pic;
			picty1 = picty;

			displayImageOptions = setupImageLoader();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub

			return Packageid1.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return Packageid1.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			BitmapFactory.Options options;
			final ViewHolder holder;
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.fragmentgridsinleitem,
						null);
				holder = new ViewHolder();
				mPlayer = new MediaPlayer();
				holder.imageView = (ImageView) convertView
						.findViewById(R.id.attachmentimage);

				holder.add = (ImageView) convertView.findViewById(R.id.delete);
				holder.play = (ImageView) convertView.findViewById(R.id.play);
				holder.stop = (ImageView) convertView.findViewById(R.id.stop);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();

			}
			try {
				if (picty1.get(position).equals("64")) {

					File image = new File(pic1.get(position));

					options = new BitmapFactory.Options();
					options.inSampleSize = 8;

					Bitmap bitmap = BitmapFactory.decodeFile(
							image.getAbsolutePath(), options);
					holder.imageView.setImageBitmap(bitmap);

				} else {
					if (pictype1.get(position).equals("3gp"))
						holder.play.setVisibility(View.VISIBLE);
					else {
						holder.play.setVisibility(View.GONE);
						ImageLoader.getInstance().displayImage(
								pic1.get(position), holder.imageView);
					}
				}
				holder.add.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (isInternetPresent) {
							pos = position;
							new AsyncDeleteattachmentTask().execute(UserId,
									itemid, Packageid1.get(position));
						} else {
							// Long
							// check=DATABASE.Deleteattachment(itemid,""+pic1.get(position));
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"No Internet Connection", false);
						}

					}
				});

				holder.play.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Log.e("clickplay", "clickplay");
						try {
							holder.stop.setVisibility(View.VISIBLE);

							holder.play.setVisibility(View.GONE);
							Uri myUri1 = Uri.parse(pic.get(position));
							try {
								mPlayer.setDataSource(activity, myUri1);
								mPlayer.prepare();

							} catch (IllegalArgumentException e) {
								Toast.makeText(activity,
										"You might not set the URI correctly!",
										Toast.LENGTH_LONG).show();
							} catch (IllegalStateException e) {
								e.printStackTrace();
							} catch (SecurityException e) {
								Toast.makeText(activity,
										"You might not set the URI correctly!",
										Toast.LENGTH_LONG).show();
							} catch (IOException e) {
								e.printStackTrace();
							}

							try {

								mPlayer.start();

							} catch (IllegalArgumentException e) {
								Toast.makeText(activity,
										"You might not set the URI correctly!",
										Toast.LENGTH_LONG).show();
							} catch (IllegalStateException e) {
								e.printStackTrace();
							} catch (SecurityException e) {
								Toast.makeText(activity,
										"You might not set the URI correctly!",
										Toast.LENGTH_LONG).show();

							}
						} catch (Exception e) {
							Log.e("crash", "" + e.getMessage());
						}
					}
				});
				holder.stop.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try {
							Log.e("clickstop", "clickstop");
							holder.stop.setVisibility(View.GONE);
							holder.play.setVisibility(View.VISIBLE);
							mPlayer.stop();
							mPlayer.reset();
						} catch (Exception e) {
							Log.e("crash", "" + e.getMessage());
						}
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
			return convertView;

		}

		class ViewHolder {

			private ImageView imageView, add, play, stop;

		}

		public DisplayImageOptions setupImageLoader() {
			return new DisplayImageOptions.Builder().cacheInMemory(true)
					.cacheOnDisk(true).considerExifParams(true)
					.displayer(new RoundedBitmapDisplayer(1)).build();

		}

	}

	public class AsyncDeleteattachmentTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result != null) {

						if (result.equals("error")) {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"SomeThing Wrong", false);
							ViewUtil.hideProgressDialog();
						} else {

							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("Response", result);

								attachmentid.remove(pos);
								pic.remove(pos);
								Attachment.setText(""
										+ (Integer.parseInt(Attachment
												.getText().toString()) - 1));
								adapter.notifyDataSetChanged();

								ViewUtil.hideProgressDialog();
							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");

								ViewUtil.hideProgressDialog();
								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);

							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("ItemId", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("AttachmentId", String
					.valueOf(params[2])));

			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.Deleteattchment(parameter);
			return result;
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.edt_items:
			typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE, "");
			// Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				Title.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					Title.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_VALUE);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				Title.setInputType(InputType.TYPE_CLASS_TEXT);
			}

			break;
		case R.id.edt_desc:
			typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE, "");
			// Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				Description.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					Description.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_VALUE);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				Description.setInputType(InputType.TYPE_CLASS_TEXT);
			}

			break;
		case R.id.edt_qty:
			typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE, "");
			// Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				Quantity.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					Quantity.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_VALUE);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				Quantity.setInputType(InputType.TYPE_CLASS_TEXT);
			}

			break;
		case R.id.edt_value:
			typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE, "");
			// Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				Value.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(getActivity());
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(getActivity()) == true) {
					// if yes � running recognition
					Value.setInputType(InputType.TYPE_NULL);
					startRecognition(getActivity(), REQUEST_AUDIO_TO_TEXT_VALUE);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							getActivity(),
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(getActivity());
				}
			} else {
				Value.setInputType(InputType.TYPE_CLASS_TEXT);
			}

			break;

		default:
			break;
		}
	}
}