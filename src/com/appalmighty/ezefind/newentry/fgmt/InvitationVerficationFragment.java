package com.appalmighty.ezefind.newentry.fgmt;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;

public class InvitationVerficationFragment extends Fragment {
	private EditText code;
	private Button submit;
	SharedPreferences shared;
	String UserId;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(
				R.layout.fragment_invitation_verfication, container, false);
		code = (EditText) rootView.findViewById(R.id.edt_enter_code);
		submit = (Button) rootView.findViewById(R.id.btn_submit);
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = shared.getString(AppConstant.KEY_USER_ID, "");
		internetcheck = new ConnectionDetector(getActivity());
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (code.getText().toString().equals("")) {
					ViewUtil.showAlertDialog(getActivity(), "ALERT",
							"Please Enter Code", true);
				} else {
					if (isInternetPresent) {
						new AsyncVerificationTask().execute(UserId, code
								.getText().toString());

					} else {
						ViewUtil.showAlertDialog(getActivity(), "Error",
								"No Internet Connection", true);
					}
				}

			}

			// private boolean validateLogin() {
			// // TODO Auto-generated method stub
			// if (isNotValidEmail()) {
			// ViewUtil.showAlertDialog(getActivity(), "Email",
			// "Please enter your valid email.", true);
			// return false;
			// }
			// return true;
			// }
			//
			// private boolean isNotValidEmail() {
			// // TODO Auto-generated method stub
			// if (checkValidEmail(code.getText().toString())) {
			// return false;
			// }
			// return true;
			// }
			//
			// private boolean checkValidEmail(String string) {
			// // TODO Auto-generated method stub
			// boolean isValid = false;
			//
			// String PATTERN =
			// "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			// Pattern pattern = Pattern.compile(PATTERN);
			// Matcher matcher = pattern.matcher(string);
			// isValid = matcher.matches();
			//
			// return isValid;
			// }
		});
		return rootView;
	}

	public class AsyncVerificationTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				JSONObject jObj = new JSONObject(result);

				if (jObj.getBoolean("success")) {

					JSONObject phone = jObj.getJSONObject("msg");
					Log.e("phone", "" + phone);

					Editor editor = shared.edit();

					editor.putString(AppConstant.KEY_USER_ID,
							String.valueOf(phone.getInt("UserId")));

					editor.putString(
							AppConstant.KEY_USERIMAGE,
							"http://beta.brstdev.com/yiiezefind"
									+ phone.getString("ProfileImage"));
					editor.commit();
					startMainActivity();
					ViewUtil.hideProgressDialog();

				} else {
					Log.e("Response", result);

					String Server_message = jObj.getString("message");
					ViewUtil.hideProgressDialog();
					ViewUtil.showAlertDialog(getActivity(), "Some Thing Wrong",
							Server_message, true);

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		public void startMainActivity() {

			Intent intent = new Intent(getActivity(), MainActivity.class);
			startActivity(intent);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			getActivity().finish();
		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("Email", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("Password", String
					.valueOf(params[1])));
			String result = NetworkConnector.LoginVerification(parameter);
			return result;
		}

	}
}
