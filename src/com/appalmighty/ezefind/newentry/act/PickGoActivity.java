package com.appalmighty.ezefind.newentry.act;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.newentry.fgmt.PickGoFragment;
import com.appalmighty.ezefind.util.AppConstant;

public class PickGoActivity extends FragmentActivity {

	SharedPreferences sharedpreferences;
	String key;
	PickGoFragment pick;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pick_go);
		Intent i = getIntent();
		key = i.getStringExtra("Pack_go");
		pick = new PickGoFragment();
		sharedpreferences = getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		sharedpreferences.edit().putString(AppConstant.PICK_GO_TYPE, key);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, PickGoFragment.newInstance(key))
					.commit();
		}

	}

}
