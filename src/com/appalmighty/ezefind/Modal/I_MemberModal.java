/**
 * 
 */
package com.appalmighty.ezefind.Modal;

/**
 * @author ShalviSharma
 * 
 */
public class I_MemberModal {

	private String Name;
	private String ProfileImage;
	private String MemberId;
	private String Added;
	private String Viewed;
	private String Address;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getProfileImage() {
		return ProfileImage;
	}

	public void setProfileImage(String profileImage) {
		ProfileImage = profileImage;
	}

	public String getMemberId() {
		return MemberId;
	}

	public void setMemberId(String memberId) {
		MemberId = memberId;
	}

	public String getAdded() {
		return Added;
	}

	public void setAdded(String added) {
		Added = added;
	}

	public String getViewed() {
		return Viewed;
	}

	public void setViewed(String viewed) {
		Viewed = viewed;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}
}
