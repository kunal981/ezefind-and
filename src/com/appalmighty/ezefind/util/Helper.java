package com.appalmighty.ezefind.util;

import android.content.Context;
import android.widget.Toast;

public class Helper {

	public static class ToastUi {

		public static void print(Context context, String msg) {
			Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
		}
	}

}
