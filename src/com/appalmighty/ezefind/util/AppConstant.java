package com.appalmighty.ezefind.util;

public class AppConstant {

	public static final String KEY_SelectedType = "Type";
	public static final String KEY_BAG = "Bag";
	public static final String KEY_BOX = "Box";
	public static final String KEY_SINGLE_ITEM = "Single Item";
	public static final String KEY_BIN = "Bin";
	public static final String KEY_GCM = "gcm";
	public static final String GCM_ID = "reg_id";
	public static final String GCM_PROJECT_NUM = "850399714102";

	public static final String KEY_APP = "com.appalmighty.ezefind.APPLICATION";
	public static final String KEY_USER_NAME = "com.appalmighty.ezefind.USERNAME";
	public static final String KEY_USER_ID = "userid";
	public static final String KEY_DEFAULT_Filter = "filtertype";

	public static final String KEY_TITLEOPEN = "titleopen";
	public static final String KEY_USERIMAGE = "profile";
	public static final String KEY_USER_PASSWORD = "com.appalmighty.ezefind.PASSWORD";
	public static final String PICK_GO_TYPE = "com.appalmighty.ezefind.PICK_GO_TYPE";
	public static final String KEY_PackageId = "packageid";
	public static final String KEY_PackageDataId = "packagedataid";
	public static final String KEY_ItemId = "itemid";
	public static final String KEY_PackageNumber = "packagenumber";
	public static final String KEY_Date = "startdate";
	public static final String KEY_Time = "starttime";
	public static final String KEY_Address = "address";
	public static final String KEY_ENTRYTYPE = "entry";
	public static final String KEY_CATEGORY = "category";
	public static final String KEY_LOCATION = "location";

	

	public static final boolean DEBUG = true;
	public static final String DEBUG_TAG = "EzeFind";
	public static String BACKHANDLE = "0";
}
