package com.appalmighty.ezefind.login.act;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.login.fgmt.LoginFragment;

public class LoginActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container_login, LoginFragment.newInstance())
					.commit();
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

}
