package com.appalmighty.ezefind.login.fgmt;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.newentry.fgmt.InvitationVerficationFragment;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;

public class LoginFragment extends Fragment implements OnClickListener {

	public static final String TAG = LoginFragment.class.getName();

	private TextView textForgotButton;
	private TextView textRegisterButton;
	private Button buttonSubmit;
	private EditText username1;
	private String username;
	private EditText password1;
	private String password;

	SharedPreferences sharedpreferences;
	String PREF_NAME = "userid";
	String userId, firstname, profileimage;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	private CheckBox rememberMe;

	public LoginFragment() {
	}

	public static LoginFragment newInstance() {
		LoginFragment fragment = new LoginFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_login, container,
				false);

		sharedpreferences = getActivity().getSharedPreferences(
				AppConstant.KEY_APP, Context.MODE_PRIVATE);

		textForgotButton = (TextView) rootView
				.findViewById(R.id.id_forgot_password);
		username1 = (EditText) rootView.findViewById(R.id.id_username);
		rememberMe = (CheckBox) rootView.findViewById(R.id.check_remember_me);
		password1 = (EditText) rootView.findViewById(R.id.id_password);
		textForgotButton.setOnClickListener(this);
		textRegisterButton = (TextView) rootView.findViewById(R.id.id_register);
		textRegisterButton.setOnClickListener(this);
		buttonSubmit = (Button) rootView.findViewById(R.id.button_submit);
		internetcheck = new ConnectionDetector(getActivity());

		Log.e("sharedpreference",
				"" + sharedpreferences.getString(AppConstant.KEY_USER_ID, ""));
		buttonSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				username = username1.getText().toString();
				password = password1.getText().toString();

				isInternetPresent = internetcheck.isConnectingToInternet();
				if (validateLogin()) {
					if (isInternetPresent) {
						new AsyncLoginTask().execute(username, password);

					} else {
						ViewUtil.showAlertDialog(getActivity(), "Error",
								"No Internet Connection", true);
					}
				}

			}
		});
		return rootView;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		Fragment fragment = null;
		switch (id) {
		case R.id.id_forgot_password:
			fragment = PasswordFragment.newInstance();
			break;
		case R.id.id_register:
			fragment = RegisterFragment.newInstance();
			break;

		default:
			break;
		}

		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
				R.anim.slide_in_left, R.anim.slide_out_right);
		ft.replace(R.id.container_login, fragment);
		ft.addToBackStack(null);
		ft.commit();

	}

	protected boolean validateLogin() {
		if (isEmptyEmail()) {
			ViewUtil.showAlertDialog(getActivity(), "Email",
					"Please enter your email.", true);
			return false;
		}

		if (isNotValidEmail()) {
			ViewUtil.showAlertDialog(getActivity(), "Email",
					"Please enter your valid email.", true);
			return false;
		}
		if (password.trim().equals("") || password.length() < 6) {
			ViewUtil.showAlertDialog(getActivity(), "Password",
					"Please enter your password.", true);
			return false;
		}

		return true;
	}

	private boolean isEmptyEmail() {
		// TODO Auto-generated method stub
		if (username.trim().equals("")) {
			return true;
		}
		return false;
	}

	private boolean isNotValidEmail() {
		Log.e("username", "" + username);
		if (checkValidEmail(username)) {
			return false;
		}
		return true;
	}

	/*
	 * An Email validation function which checks the entered email is a valid
	 * email or not. returns true if valid , false if invalid
	 */
	protected boolean checkValidEmail(String email) {
		boolean isValid = false;
		// String PATTERN =
		// "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		String PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(PATTERN);
		Matcher matcher = pattern.matcher(email);
		isValid = matcher.matches();

		return isValid;
	}

	/*
	 * A user Login API is called.
	 */

	public class AsyncLoginTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);
						Log.e("login result--", ""+result);

						if (jObj.getBoolean("success") && jObj.has("invite")) {

							InvitationVerficationFragment fragment = new InvitationVerficationFragment();
							FragmentManager fm = getFragmentManager();
							fm.popBackStack(null,
									FragmentManager.POP_BACK_STACK_INCLUSIVE);
							FragmentTransaction ft = fm.beginTransaction();
							// Bundle bundle = new Bundle();
							// bundle.putString("Packedid", packageid);
							// bundle.putString("Packedid", packageid);
							// if (Typed.equals("Package")) {
							// bundle.putString("Typed", "Package");
							// } else {
							// bundle.putString("Typed", "Inventory");
							// }
							//
							// fragment.setArguments(bundle);
							ft.replace(R.id.container, fragment);
							ft.addToBackStack(null);
							ft.commit();

							// Log.e("Response", result);
							// JSONObject phone = jObj.getJSONObject("msg");
							// Log.e("phone", "" + phone);
							// firstname = phone.getString("FirstName");
							// profileimage =
							// "http://beta.brstdev.com/yiiezefind"
							// + phone.getString("ProfileImage");
							// userId = String.valueOf(phone.getInt("UserId"));
							// Log.e("first", "" + firstname);
							// Log.e("profileimage", "" + profileimage);
							// Log.e("userId", "" + userId);
							// ViewUtil.hideProgressDialog();
							// rememberUser();
							//
							// startMainActivity();

						} else if (jObj.getBoolean("success")) {
							JSONObject phone = jObj.getJSONObject("msg");
							Log.e("phone", "" + phone);
							firstname = phone.getString("FirstName");
							profileimage = "http://beta.brstdev.com/yiiezefind"
									+ phone.getString("ProfileImage");
							userId = String.valueOf(phone.getInt("UserId"));
							Log.e("first", "" + firstname);
							Log.e("profileimage", "" + profileimage);
							Log.e("userId", "" + userId);

							rememberUser();

							startMainActivity();
							ViewUtil.hideProgressDialog();

						} else {
							Log.e("Response", result);

							JSONObject phone = jObj.getJSONObject("message");
							String Server_message = phone.getString("Account");
							ViewUtil.hideProgressDialog();
							ViewUtil.showAlertDialog(getActivity(),
									"Login Failed", Server_message, true);

						}

					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void rememberUser() {

			if (rememberMe.isChecked()) {
				Editor editor = sharedpreferences.edit();
				editor.putString(AppConstant.KEY_USER_NAME, username);
				editor.putString(AppConstant.KEY_USER_PASSWORD, password);

				editor.commit();
			}

		}

		public void startMainActivity() {
			Editor editor = sharedpreferences.edit();

			editor.putString(AppConstant.KEY_USER_ID, userId);
			editor.putString(AppConstant.KEY_USER_NAME, firstname);
			editor.putString(AppConstant.KEY_USERIMAGE, profileimage);
			editor.putString(AppConstant.KEY_ENTRYTYPE, "0");
			editor.putString(AppConstant.KEY_DEFAULT_Filter, "package");
			editor.commit();
			Intent intent = new Intent(getActivity(), MainActivity.class);
			startActivity(intent);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			getActivity().finish();
		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("Email", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("Password", String
					.valueOf(params[1])));
			String result = NetworkConnector.loginAuthentication(parameter);
			return result;
		}

	}

}