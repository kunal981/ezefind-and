package com.appalmighty.ezefind.login.fgmt;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.login.act.LoginActivity;
import com.appalmighty.ezefind.newentry.act.PickGoActivity;
import com.appalmighty.ezefind.report.act.ViewReportMenuActivity;
import com.appalmighty.ezefind.setting.act.SettingActivity;
import com.appalmighty.ezefind.util.AppConstant;
import com.appalmighty.ezefind.viewinventory.act.ViewInventoryActivity;

public class HomeMenuFragment extends Fragment implements OnClickListener {

	public static final String TAG = HomeMenuFragment.class.getName();

	private ImageView buttonNewEntry, buttonReport, buttonViewInventory,
			buttonSetting;

	private Button buttonPacking, buttonInventory, btnLogout;
	private LinearLayout containerMenu;
	private RelativeLayout containerClick, menuCotainer;

	public HomeMenuFragment() {
	}

	public static HomeMenuFragment newInstance() {
		HomeMenuFragment fragment = new HomeMenuFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_login_menu,
				container, false);
		buttonNewEntry = (ImageView) rootView
				.findViewById(R.id.id_img_new_entry);
		buttonReport = (ImageView) rootView.findViewById(R.id.id_img_report);
		buttonViewInventory = (ImageView) rootView
				.findViewById(R.id.id_img_inventory);
		buttonSetting = (ImageView) rootView.findViewById(R.id.id_img_setting);
		containerMenu = (LinearLayout) rootView.findViewById(R.id.rel_menu_bar);
		buttonPacking = (Button) rootView.findViewById(R.id.id_buton_pack);
		btnLogout = (Button) rootView.findViewById(R.id.btn_logout);
		buttonInventory = (Button) rootView
				.findViewById(R.id.id_buton_inventory);
		containerClick = (RelativeLayout) rootView
				.findViewById(R.id.r_outer_click_container);
		menuCotainer = (RelativeLayout) rootView
				.findViewById(R.id.r_menu_container);
		buttonNewEntry.setOnClickListener(this);
		buttonReport.setOnClickListener(this);
		buttonViewInventory.setOnClickListener(this);
		buttonSetting.setOnClickListener(this);
		containerClick.setOnClickListener(this);
		buttonPacking.setOnClickListener(this);
		buttonInventory.setOnClickListener(this);
		btnLogout.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.id_img_new_entry:
			containerMenu.setVisibility(View.VISIBLE);
			break;
		case R.id.id_img_report:
			getActivity().finish();
			Intent intentReport = new Intent(getActivity(),
					ViewReportMenuActivity.class);
			startActivity(intentReport);

			getActivity().overridePendingTransition(R.anim.rotation,
					R.anim.fade_out);
			break;
		case R.id.id_img_inventory:
			getActivity().finish();
			Intent i = new Intent(getActivity(), ViewInventoryActivity.class);
			startActivity(i);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.id_img_setting:
			getActivity().finish();
			Intent intentSetting = new Intent(getActivity(),
					SettingActivity.class);
			startActivity(intentSetting);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.r_outer_click_container:
			if (containerMenu.isShown()) {
				containerMenu.setVisibility(View.INVISIBLE);
			}
			break;
		case R.id.id_buton_inventory:
			getActivity().finish();
			Intent intent1 = new Intent(getActivity(), PickGoActivity.class);
			intent1.putExtra("Pack_go", "inventory");
			startActivity(intent1);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			containerMenu.setVisibility(View.GONE);
			break;
		case R.id.id_buton_pack:
			getActivity().finish();
			Intent intent = new Intent(getActivity(), PickGoActivity.class);
			intent.putExtra("Pack_go", "pack_go");
			startActivity(intent);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			containerMenu.setVisibility(View.GONE);
			break;
		case R.id.btn_logout:
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					getActivity());
			// set title
			alertDialogBuilder.setTitle("LOGOUT");

			// set dialog message
			alertDialogBuilder
					.setMessage("Are you sure for logout")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									SharedPreferences settings = getActivity()
											.getSharedPreferences(
													AppConstant.KEY_APP,
													Context.MODE_PRIVATE);
									settings.edit().clear().commit();
									getActivity().finish();
									Intent intent_home = new Intent(
											getActivity(), LoginActivity.class);
									startActivity(intent_home);

								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									dialog.cancel();
								}
							});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

			break;
		default:
			break;
		}

	}

}