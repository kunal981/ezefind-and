package com.appalmighty.ezefind.viewinventory.act;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.viewinventory.fgmt.ViewInventoryFragment;

public class ViewInventoryActivity extends FragmentActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_inventory);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, ViewInventoryFragment.newInstance())
					.commit();
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

}
