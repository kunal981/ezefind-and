package com.appalmighty.ezefind.viewinventory.fgmt;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.appalmighty.ezefind.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class ViewLocationFragment extends Fragment {
	GoogleMap mMap;
	MapView mMapView;
	static final LatLng HAMBURG = new LatLng(53.558, 9.927);
	Marker marker;
	private Button backButton;
	private ImageButton imageButton;
	private RelativeLayout containerAddCategory;

	public static ViewLocationFragment newInstance() {
		ViewLocationFragment fragment = new ViewLocationFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_fullscan_location,
				container, false);

		MapsInitializer.initialize(getActivity());
		mMapView = (MapView) rootView.findViewById(R.id.map);
		mMapView.onCreate(savedInstanceState);
		backButton = (Button) rootView.findViewById(R.id.back_button);
		containerAddCategory = (RelativeLayout) rootView
				.findViewById(R.id.container_add_category);
		imageButton = (ImageButton) rootView.findViewById(R.id.img_filter);
		setUpMapAndUserCurrentLocation();
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		imageButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				containerAddCategory.setVisibility(View.VISIBLE);
			}
		});
		return rootView;
	}

	private void setUpMapAndUserCurrentLocation() {
		if (mMap == null) {
			LatLng clatLng;
			mMap = mMapView.getMap();
			mMap.getUiSettings().setMyLocationButtonEnabled(false);
			mMap.setMyLocationEnabled(true);

			// // Updates the location and zoom of the MapView
			Location loc = mMap.getMyLocation();
			if (loc != null) {
				//
				clatLng = new LatLng(loc.getLatitude(), loc.getLongitude());
			} else {
				clatLng = new LatLng(30.694209d, 76.860565d);
			}
			marker = mMap.addMarker(new MarkerOptions()
					.position(clatLng)
					.title("Green Shirt")
					.snippet("Bag No 1" + " " + "Lorem ipsum dolor sit amet")
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.ic_marker_)));

			mMap.moveCamera(CameraUpdateFactory.newLatLng(clatLng));
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
					clatLng, 15);
			mMap.animateCamera(cameraUpdate);
		}

		mMap.setInfoWindowAdapter(new InfoWindowAdapter() {

			@Override
			public View getInfoWindow(Marker arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public View getInfoContents(Marker arg0) {
				// TODO Auto-generated method stub
				View view = getActivity().getLayoutInflater().inflate(
						R.layout.activity_info_content, null);
				return view;
			}
		});
		mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

			@Override
			public void onInfoWindowClick(Marker arg0) {
				// TODO Auto-generated method stub
				ViewLocationOnClick fragment = new ViewLocationOnClick();
				FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.setCustomAnimations(R.anim.slide_in_right,
						R.anim.slide_out_left, R.anim.slide_in_left,
						R.anim.slide_out_right);
				ft.replace(R.id.container, fragment);
				ft.addToBackStack(null);
				ft.commit();
			}
		});

		// mMap.setOnMarkerClickListener(new OnMarkerClickListener() {
		//
		// @Override
		// public boolean onMarkerClick(Marker arg0) {
		// // TODO Auto-generated method stub
		// if (arg0.getSnippet() == null) {
		// mMap.moveCamera(CameraUpdateFactory.zoomIn());
		// return true;
		// }
		// // arg0.showInfoWindow();
		// // final DataClass data = myMapData.get(arg0);
		// final Dialog d = new Dialog(getActivity());
		// d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// // d.setTitle("Select");
		// d.getWindow().setBackgroundDrawable(
		// new ColorDrawable(Color.WHITE));
		// d.setContentView(R.layout.activity_info_content);
		// TextView txt = (TextView)d.findViewById(R.id.textView1);
		// txt.setText("Red Shirt");

		// ivPhoto = (ImageView) d
		// .findViewById(R.id.infocontent_iv_image);
		// AddImageOnWindow executeDownload = new AddImageOnWindow();
		// final LatLng l = arg0.getPosition();
		// executeDownload.execute(l);
		// TextView tvName = (TextView) d
		// .findViewById(R.id.infocontent_tv_name);
		// tvName.setText(data.getPlaceName());
		//
		// TextView tvType = (TextView) d
		// .findViewById(R.id.infocontent_tv_type);
		// tvType.setText("(" + data.getPlaceType() + ")");
		//
		// TextView tvDesc = (TextView) d
		// .findViewById(R.id.infocontent_tv_desc);
		// tvDesc.setText(data.getPlaceDesc());
		//
		// TextView tvAddr = (TextView) d
		// .findViewById(R.id.infocontent_tv_addr);
		// tvAddr.setText(Html.fromHtml(data.getPlaceAddr()));

		// d.show();
		// return true;
		// }
		// });
		//
		// }

	}

	@Override
	public void onResume() {
		super.onResume();
		mMapView.onResume();
	}

	@Override
	public void onPause() {
		mMapView.onPause();
		super.onPause();
		mMap = null;
	}

	@Override
	public void onDestroy() {
		mMapView.onDestroy();
		super.onDestroy();
	}

	//
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mMapView.onLowMemory();
	}

	//
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mMapView.onSaveInstanceState(outState);
	}

}
