package com.appalmighty.ezefind.viewinventory.fgmt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.newentry.fgmt.ContainerDetailsFragment;

public class ViewCategoryFragment extends Fragment implements OnClickListener {
	private Button btnBack, homeButton;
	private ImageButton imageButton;
	private RelativeLayout containerAddCategory;

	private GridViewAdapter adapter1;
	private GridView gridView;

	public static ViewCategoryFragment newInstance() {
		ViewCategoryFragment fragment = new ViewCategoryFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_view_category,
				container, false);
		btnBack = (Button) rootView.findViewById(R.id.back_button);
		imageButton = (ImageButton) rootView.findViewById(R.id.id_img_filter);
		containerAddCategory = (RelativeLayout) rootView
				.findViewById(R.id.container_add_category);
		gridView = (GridView) rootView.findViewById(R.id.id_gridview);
		adapter1 = new GridViewAdapter(getActivity());
		gridView.setAdapter(adapter1);
		homeButton = (Button) rootView.findViewById(R.id.id_button_home);
		homeButton.setOnClickListener(this);
		btnBack.setOnClickListener(this);
		imageButton.setOnClickListener(this);
		return rootView;
	}

	class GridViewAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;

		public GridViewAdapter(Activity activity) {
			super();
			this.activity = activity;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 9;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.fragment_scanview_listitems, null);

				ImageView imageView = (ImageView) convertView
						.findViewById(R.id.img_clothes);
				imageView.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ContainerDetailsFragment openContainerFragment = new ContainerDetailsFragment();
						FragmentManager fm = getFragmentManager();
						FragmentTransaction ft = fm.beginTransaction();
						ft.setCustomAnimations(R.anim.slide_in_right,
								R.anim.slide_out_left, R.anim.slide_in_left,
								R.anim.slide_out_right);
						Bundle bundle = new Bundle();
						bundle.putString("Title_Open", "View: Inventory");
						openContainerFragment.setArguments(bundle);
						ft.replace(R.id.container, openContainerFragment);
						ft.addToBackStack(null);
						ft.commit();

					}
				});

			}
			return convertView;
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back_button:
			getActivity().onBackPressed();
			break;

		case R.id.id_button_home:
			getActivity().finish();
			Intent intent = new Intent(getActivity(), MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);

			break;
		case R.id.id_img_filter:
			containerAddCategory.setVisibility(View.VISIBLE);
			break;
		}

	}

}
