package com.appalmighty.ezefind.viewinventory.fgmt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;

import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;

public class ConfirmReceiveFragment extends Fragment implements OnClickListener {

	private GridView gridView;
	private Button backButton;
	private Button homeButton;

	public static ConfirmReceiveFragment newInstance() {
		ConfirmReceiveFragment fragment = new ConfirmReceiveFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_confirm_recieve,
				container, false);
		backButton = (Button) rootView.findViewById(R.id.back_button);
		homeButton = (Button) rootView.findViewById(R.id.id_button_home);
		homeButton.setOnClickListener(this);
		backButton.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back_button:
			getActivity().onBackPressed();
			break;

		case R.id.id_button_home:
			getActivity().finish();
			Intent intent = new Intent(getActivity(), MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);

			break;
		}
	}
}