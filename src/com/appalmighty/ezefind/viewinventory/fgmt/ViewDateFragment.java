package com.appalmighty.ezefind.viewinventory.fgmt;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.newentry.fgmt.ContainerDetailsFragment;

public class ViewDateFragment extends Fragment {

	GridAdapter gridAdapter;
	GridView gridView;
	Button backButton, btnSetDate;
	EditText edt;
	ImageButton img_cal;
	private DatePickerDialog fromDatePickerDialog;
	private SimpleDateFormat dateFormatter;

	public static ViewDateFragment newInstance() {
		ViewDateFragment fragment = new ViewDateFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_full_menu_date,
				container, false);
		gridView = (GridView) rootView.findViewById(R.id.id_gridview_date);
		backButton = (Button) rootView.findViewById(R.id.back_button);
		img_cal = (ImageButton) rootView.findViewById(R.id.img_calendar);
		btnSetDate = (Button) rootView.findViewById(R.id.btn_datepicker);
		// edt = (EditText) rootView.findViewById(R.id.btn_datepicker);

		gridAdapter = new GridAdapter(getActivity());
		gridView.setAdapter(gridAdapter);
		dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

		setDateTimeField();

		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		img_cal.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fromDatePickerDialog.show();
			}
		});

		return rootView;
	}

	private void setDateTimeField() {

		Calendar newCalendar = Calendar.getInstance();
		fromDatePickerDialog = new DatePickerDialog(getActivity(),
				new OnDateSetListener() {

					public void onDateSet(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						Calendar newDate = Calendar.getInstance();
						newDate.set(year, monthOfYear, dayOfMonth);
						btnSetDate.setText(dateFormatter.format(newDate
								.getTime()));
					}

				}, newCalendar.get(Calendar.YEAR),
				newCalendar.get(Calendar.MONTH),
				newCalendar.get(Calendar.DAY_OF_MONTH));

	}

	class GridAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;

		public GridAdapter(Activity activity) {
			super();
			this.activity = activity;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 6;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.fragment_fullmenu_date_listitems, null);

				ImageView imageView = (ImageView) convertView
						.findViewById(R.id.img_clothes);
				imageView.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ContainerDetailsFragment openContainerFragment = new ContainerDetailsFragment();
						FragmentManager fm = getFragmentManager();
						FragmentTransaction ft = fm.beginTransaction();
						ft.setCustomAnimations(R.anim.slide_in_right,
								R.anim.slide_out_left, R.anim.slide_in_left,
								R.anim.slide_out_right);
						Bundle bundle = new Bundle();
						bundle.putString("Title_Open", "View: Inventory");
						openContainerFragment.setArguments(bundle);
						ft.replace(R.id.container, openContainerFragment);
						ft.addToBackStack(null);
						ft.commit();

					}
				});

			}
			return convertView;
		}

	}
}
