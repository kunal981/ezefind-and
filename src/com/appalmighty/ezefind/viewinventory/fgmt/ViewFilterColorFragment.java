package com.appalmighty.ezefind.viewinventory.fgmt;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.newentry.fgmt.ContainerDetailsFragment;

public class ViewFilterColorFragment extends Fragment {
	private GridViewAdapter adapter1;
	private GridView gridView;
	private Button backButton;
	ImageButton imgFilter;

	public static ViewFilterColorFragment newInstance() {
		ViewFilterColorFragment fragment = new ViewFilterColorFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_filter_color,
				container, false);
		gridView = (GridView) rootView.findViewById(R.id.id_gridview_memeber);
		backButton = (Button) rootView.findViewById(R.id.back_button);
		imgFilter = (ImageButton) rootView.findViewById(R.id.img_filter);
		adapter1 = new GridViewAdapter(getActivity());
		gridView.setAdapter(adapter1);
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				getActivity().onBackPressed();
			}
		});

		imgFilter.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				colorSelectionDialog(v);
			}
		});
		return rootView;
	}

	protected void colorSelectionDialog(View v) {

		LayoutInflater layoutInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_color_filter, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, true);

		Button btnClose = (Button) popupView.findViewById(R.id.btnclose);
		btnClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				popupWindow.dismiss();
			}
		});

		popupWindow.showAtLocation(gridView, Gravity.CENTER, 0, 0);
	}

	class GridViewAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;

		public GridViewAdapter(Activity activity) {
			super();
			this.activity = activity;
		}

		@Override
		public int getCount() {

			return 6;
		}

		@Override
		public Object getItem(int position) {

			return null;
		}

		@Override
		public long getItemId(int position) {

			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.fragment_fullmenu_memeber_griditems, null);

				ImageView imageView = (ImageView) convertView
						.findViewById(R.id.img_clothes);
				imageView.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ContainerDetailsFragment openContainerFragment = new ContainerDetailsFragment();
						FragmentManager fm = getFragmentManager();
						FragmentTransaction ft = fm.beginTransaction();
						ft.setCustomAnimations(R.anim.slide_in_right,
								R.anim.slide_out_left, R.anim.slide_in_left,
								R.anim.slide_out_right);
						Bundle bundle = new Bundle();
						bundle.putString("Title_Open", "View: Inventory");
						openContainerFragment.setArguments(bundle);
						ft.replace(R.id.container, openContainerFragment);
						ft.addToBackStack(null);
						ft.commit();

					}
				});

			}
			return convertView;
		}

	}

}
