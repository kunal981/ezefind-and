package com.appalmighty.ezefind.viewinventory.fgmt;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.newentry.fgmt.ContainerDetailsFragment;

public class ViewInventoryGoFragment extends Fragment {
	ListAdapter ladapter;
	Button btnBack;
	ListView listView;

	public static ViewInventoryGoFragment newInstance() {
		ViewInventoryGoFragment fragment = new ViewInventoryGoFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_view_inventory_go,
				container, false);
		listView = (ListView) rootView.findViewById(R.id.listview_go);
		btnBack = (Button) rootView.findViewById(R.id.back_button);
		ladapter = new ListAdapter(getActivity());
		listView.setAdapter(ladapter);
		btnBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				ContainerDetailsFragment openContainerFragment = new ContainerDetailsFragment();
				FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.setCustomAnimations(R.anim.slide_in_right,
						R.anim.slide_out_left, R.anim.slide_in_left,
						R.anim.slide_out_right);
				Bundle bundle = new Bundle();
				bundle.putString("Title_Open", "View: Inventory");
				openContainerFragment.setArguments(bundle);
				ft.replace(R.id.container, openContainerFragment);
				ft.addToBackStack(null);
				ft.commit();
			}
		});
		return rootView;
	}

	class ListAdapter extends BaseAdapter {

		private LayoutInflater inflater;
		private Activity activity;

		public ListAdapter(Activity activity) {

			this.activity = activity;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 6;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null)
				inflater = (LayoutInflater) getActivity().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.fragment_on_open_listitems, null);
			}
			return convertView;
		}

	}

}
