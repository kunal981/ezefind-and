package com.appalmighty.ezefind.viewinventory.fgmt;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.appalmighty.ezefind.R;

public class ViewLocationOnClick extends Fragment {
	private GridViewAdapter adapter1;
	private GridView gridView;
	private Button backButton;
	private ImageButton imageButton;
	private RelativeLayout containerAddCategory;

	public static ViewLocationOnClick newInstance() {
		ViewLocationOnClick fragment = new ViewLocationOnClick();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_location_onclick,
				container, false);
		gridView = (GridView) rootView.findViewById(R.id.id_gridview_location);
		backButton = (Button) rootView.findViewById(R.id.back_button);
		containerAddCategory = (RelativeLayout) rootView
				.findViewById(R.id.container_add_category);
		imageButton = (ImageButton) rootView.findViewById(R.id.img_filter);
		adapter1 = new GridViewAdapter(getActivity());
		gridView.setAdapter(adapter1);
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		imageButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				containerAddCategory.setVisibility(View.VISIBLE);
			}
		});
		return rootView;
	}

	class GridViewAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;

		public GridViewAdapter(Activity activity) {
			super();
			this.activity = activity;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 6;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.fragment_fullmenu_location_griditems, null);

			}
			return convertView;
		}

	}

}
