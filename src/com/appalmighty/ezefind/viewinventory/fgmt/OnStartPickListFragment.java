package com.appalmighty.ezefind.viewinventory.fgmt;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.DataBase.DatasourceHandler;
import com.appalmighty.ezefind.DataBase.NewEntryDataBase;
import com.appalmighty.ezefind.login.act.LoginActivity;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.newentry.fgmt.ContainerDetailsFragment;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class OnStartPickListFragment extends Fragment {
	private ListAdapter adapter;
	private ListView listview;
	Button backButton, btnLogout, btnhome;
	String UserId;
	SharedPreferences shared;
	ArrayList<String> Title;
	ArrayList<String> Picklistid;
	ArrayList<String> image;
	ArrayList<String> imagetype;
	ArrayList<String> Description;
	ArrayList<String> Quantity;
	ArrayList<String> Attacmentcount;
	ArrayList<String> Itemid;

	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	DatasourceHandler DATABASE;

	public static OnStartPickListFragment newInstance() {
		OnStartPickListFragment fragment = new OnStartPickListFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_picklist_open,
				container, false);
		listview = (ListView) rootView.findViewById(R.id.listView_pick);
		backButton = (Button) rootView.findViewById(R.id.back_button);
		btnLogout = (Button) rootView.findViewById(R.id.btn_logout);
		btnhome = (Button) rootView.findViewById(R.id.id_button_home);
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = shared.getString(AppConstant.KEY_USER_ID, "");
		Title = new ArrayList<String>();
		Picklistid = new ArrayList<String>();
		image = new ArrayList<String>();
		imagetype = new ArrayList<String>();
		Description = new ArrayList<String>();
		Quantity = new ArrayList<String>();
		Attacmentcount = new ArrayList<String>();
		Itemid = new ArrayList<String>();
		adapter = new ListAdapter(getActivity(), Title, Picklistid, image,
				imagetype, Description, Quantity, Attacmentcount, Itemid);
		listview.setAdapter(adapter);
		DATABASE = new DatasourceHandler(getActivity());
		internetcheck = new ConnectionDetector(getActivity());
		isInternetPresent = internetcheck.isConnectingToInternet();
		if (isInternetPresent) {
			new AsyncPicklistitemdeatailTask().execute(UserId, getArguments()
					.getString("Picklistid"));

		} else {
			Cursor count = DATABASE.GetPicklistitemcount(UserId, getArguments()
					.getString("Picklistid"));
			if (count.getCount() != 0) {

				while (count.moveToNext()) {
					Log.e("itemid",
							""
									+ count.getString(count
											.getColumnIndex(NewEntryDataBase.KEY_ADDEDitemid)));
					Cursor c = DATABASE
							.getAllItem(
									UserId,
									count.getString(count
											.getColumnIndex(NewEntryDataBase.KEY_ADDEDitemid)));

					if (c.getCount() != 0) {
						if (c.moveToFirst()) {
							Picklistid.add(getArguments().getString(
									"Picklistid"));

							image.add(c.getString(8));
							imagetype.add("64");
							Description
									.add(c.getString(c
											.getColumnIndex(NewEntryDataBase.KEY_ITEMDESCRIPTION)));
							Quantity.add(c.getString(6));
							Attacmentcount.add(c.getString(7));
							Itemid.add(c.getString(3));

							Title.add(c.getString(4));
						}
					}

				}

			}
			adapter.notifyDataSetChanged();
		}

		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		btnhome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().finish();
				Intent intent = new Intent(getActivity(), MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		btnLogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				// set title
				alertDialogBuilder.setTitle("LOGOUT");

				// set dialog message
				alertDialogBuilder
						.setMessage("Are you sure for logout")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										SharedPreferences settings = getActivity()
												.getSharedPreferences(
														AppConstant.KEY_APP,
														Context.MODE_PRIVATE);
										settings.edit().clear().commit();
										getActivity().finish();
										Intent intent_home = new Intent(
												getActivity(),
												LoginActivity.class);
										startActivity(intent_home);

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			}
		});
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				Log.e("bbib", "CLick");
				ContainerDetailsFragment openContainerFragment = new ContainerDetailsFragment();
				FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.setCustomAnimations(R.anim.slide_in_right,
						R.anim.slide_out_left, R.anim.slide_in_left,
						R.anim.slide_out_right);
				Bundle bundle = new Bundle();
				bundle.putString("itemid", Itemid.get(position));

				openContainerFragment.setArguments(bundle);
				ft.replace(R.id.container, openContainerFragment);
				ft.addToBackStack(null);
				ft.commit();

			}
		});
		return rootView;
	}

	class ListAdapter extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;
		ArrayList<String> title1 = new ArrayList<String>();
		ArrayList<String> picklistid1 = new ArrayList<String>();
		ArrayList<String> image1 = new ArrayList<String>();
		ArrayList<String> imagetype1 = new ArrayList<String>();
		ArrayList<String> description1 = new ArrayList<String>();
		ArrayList<String> quantity1 = new ArrayList<String>();
		ArrayList<String> attacmentcount1 = new ArrayList<String>();
		ArrayList<String> itemid1 = new ArrayList<String>();
		DisplayImageOptions displayImageOptions;

		public ListAdapter(Activity activity, ArrayList<String> title,
				ArrayList<String> picklistid, ArrayList<String> image,
				ArrayList<String> imagetype, ArrayList<String> description,
				ArrayList<String> quantity, ArrayList<String> attacmentcount,
				ArrayList<String> itemid) {
			this.activity = activity;
			title1 = title;
			picklistid1 = picklistid;
			image1 = image;
			imagetype1 = imagetype;
			description1 = description;
			quantity1 = quantity;
			attacmentcount1 = attacmentcount;
			itemid1 = itemid;
			displayImageOptions = setupImageLoader();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return itemid1.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return itemid1.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			BitmapFactory.Options options;
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.fragment_on_open_listitems, null);
				TextView titl = (TextView) convertView
						.findViewById(R.id.txt_shirts_name);

				ImageView itemimage = (ImageView) convertView
						.findViewById(R.id.img_shirts);
				TextView desc = (TextView) convertView
						.findViewById(R.id.txt_shirts_description);
				TextView quantity = (TextView) convertView
						.findViewById(R.id.ToatalQuantity);
				TextView attachment = (TextView) convertView
						.findViewById(R.id.Totalattachment);
				titl.setText(title1.get(position));
				desc.setText(description1.get(position));
				quantity.setText(quantity1.get(position));
				attachment.setText(attacmentcount1.get(position));
				if (imagetype1.get(position).equals("64")) {
					Iterator iterator = image1.iterator();
					while (iterator.hasNext()) {

						File image = new File(iterator.next().toString());
						Log.e("image", "" + image);
						options = new BitmapFactory.Options();
						options.inSampleSize = 8;

						Bitmap bitmap = BitmapFactory.decodeFile(
								image.getAbsolutePath(), options);
						itemimage.setImageBitmap(bitmap);

					}

				} else {
					ImageLoader.getInstance().displayImage(
							String.valueOf(image1.get(position)), itemimage);

				}
				itemimage.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Log.e("bbib", "CLick");
						ContainerDetailsFragment openContainerFragment = new ContainerDetailsFragment();
						FragmentManager fm = getFragmentManager();
						FragmentTransaction ft = fm.beginTransaction();
						ft.setCustomAnimations(R.anim.slide_in_right,
								R.anim.slide_out_left, R.anim.slide_in_left,
								R.anim.slide_out_right);
						Bundle bundle = new Bundle();
						bundle.putString("Title_Open", getArguments()
								.getString("Title_Open"));
						bundle.putString("itemid", itemid1.get(position));

						openContainerFragment.setArguments(bundle);
						ft.replace(R.id.container, openContainerFragment);
						ft.addToBackStack(null);
						ft.commit();
					}
				});
			}

			return convertView;

		}

		public Bitmap ConvertToImage(String image) {
			try {
				byte[] imageAsBytes = Base64.decode(image.getBytes(),
						Base64.DEFAULT);

				Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
						imageAsBytes.length);

				return bitmap;
			} catch (Exception e) {
				return null;
			}
		}

		public DisplayImageOptions setupImageLoader() {
			return new DisplayImageOptions.Builder().cacheInMemory(true)
					.cacheOnDisk(true).considerExifParams(true)
					.displayer(new RoundedBitmapDisplayer(1)).build();

		}

	}

	class AsyncPicklistitemdeatailTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				JSONObject jObj = new JSONObject(result);

				if (jObj.getBoolean("success")) {
					Log.e("Response", result);

					JSONObject phone = jObj.getJSONObject("message");
					Picklistid.add(phone.getString("PicklistId"));
					if (phone.getString("PicklistItemsCount").equals("0")) {
						ViewUtil.showAlertDialog(getActivity(), "Error",
								"No Data Found", true);
					} else {
						JSONArray Array = phone.getJSONArray("PicklistItems");

						for (int j = 0; j < Array.length(); j++) {
							JSONObject objectfrom = Array.getJSONObject(j);
							image.add("http://beta.brstdev.com/yiiezefind"
									+ objectfrom.getString("Image"));
							imagetype.add("url");
							Description
									.add(objectfrom.getString("Description"));
							Quantity.add(objectfrom.getString("Quantity"));
							Attacmentcount.add(objectfrom
									.getString("AttachmentCount"));
							Itemid.add(objectfrom.getString("ItemId"));

							Title.add(objectfrom.getString("Title"));
						}
					}
					adapter.notifyDataSetChanged();
					ViewUtil.hideProgressDialog();

				}

				else {
					Log.e("Response", result);

					String Server_message = jObj.getString("message");

					ViewUtil.hideProgressDialog();
					ViewUtil.showAlertDialog(getActivity(), "Error",
							Server_message, true);

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("PicklistId", String
					.valueOf(params[1])));

			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.GetPicklistDetail(parameter);
			return result;
		}

	}
}
