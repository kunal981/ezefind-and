package com.appalmighty.ezefind.viewinventory.fgmt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.newentry.act.PickGoActivity;
import com.appalmighty.ezefind.report.act.ViewReportMenuActivity;
import com.appalmighty.ezefind.setting.act.SettingActivity;

public class ViewInventoryFragment extends Fragment implements OnClickListener {
	private TextView txtScan, txtFullMenu, txtconfirm;
	Button btnGo;

	private ImageView buttonNewEntry, buttonReport, buttonSetting;
	private LinearLayout containerMenu;
	private RelativeLayout containerClick;

	private Button buttonPacking, buttonInventory;

	public static ViewInventoryFragment newInstance() {
		ViewInventoryFragment fragment = new ViewInventoryFragment();
		return fragment;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.fragment_view_inventory_menu_, container, false);

		buttonNewEntry = (ImageView) rootView
				.findViewById(R.id.id_img_new_entry);
		buttonReport = (ImageView) rootView.findViewById(R.id.id_img_report);
		buttonSetting = (ImageView) rootView.findViewById(R.id.id_img_setting);
		containerMenu = (LinearLayout) rootView.findViewById(R.id.rel_menu_bar);
		containerClick = (RelativeLayout) rootView
				.findViewById(R.id.r_outer_click_container);
		txtScan = (TextView) rootView.findViewById(R.id.text_scan);
		txtconfirm = (TextView) rootView
				.findViewById(R.id.text_confirm_recieve);
		txtFullMenu = (TextView) rootView.findViewById(R.id.text_full_menu);
		btnGo = (Button) rootView.findViewById(R.id.id_button_go);
		buttonPacking = (Button) rootView.findViewById(R.id.id_buton_pack);
		buttonInventory = (Button) rootView
				.findViewById(R.id.id_buton_inventory);
		buttonPacking.setOnClickListener(this);
		buttonInventory.setOnClickListener(this);

		txtScan.setOnClickListener(this);
		txtconfirm.setOnClickListener(this);
		txtFullMenu.setOnClickListener(this);
		btnGo.setOnClickListener(this);
		buttonNewEntry.setOnClickListener(this);
		buttonReport.setOnClickListener(this);
		buttonSetting.setOnClickListener(this);
		containerClick.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		Fragment fragment = null;
		switch (id) {
		case R.id.text_scan:
			fragment = ScanInventoryFragment.newInstance();
			switchFragment(fragment);
			break;
		case R.id.text_confirm_recieve:
			fragment = ConfirmReceiveFragment.newInstance();
			switchFragment(fragment);
			break;
		case R.id.text_full_menu:
			fragment = FullMenuFragment.newInstance();
			switchFragment(fragment);
			break;
		case R.id.id_button_go:
			fragment = ViewInventoryGoFragment.newInstance();
			switchFragment(fragment);
			break;
		case R.id.id_img_new_entry:
			containerMenu.setVisibility(View.VISIBLE);
			break;
		case R.id.id_img_report:
			Intent intentReport = new Intent(getActivity(),
					ViewReportMenuActivity.class);
			startActivity(intentReport);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.id_img_setting:
			Intent intentSetting = new Intent(getActivity(),
					SettingActivity.class);
			startActivity(intentSetting);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.r_outer_click_container:
			if (containerMenu.isShown()) {
				containerMenu.setVisibility(View.INVISIBLE);
			}
			break;
		case R.id.id_buton_inventory:
			Intent intent1 = new Intent(getActivity(), PickGoActivity.class);
			intent1.putExtra("Pack_go", "inventory");
			startActivity(intent1);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.id_buton_pack:
			Intent intent = new Intent(getActivity(), PickGoActivity.class);
			intent.putExtra("Pack_go", "pack_go");
			startActivity(intent);
			getActivity().overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		default:
			break;
		}

	}

	public void switchFragment(Fragment fragment) {
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
				R.anim.slide_in_left, R.anim.slide_out_right);
		ft.replace(R.id.container, fragment);
		ft.addToBackStack(null);
		ft.commit();
	}
}
