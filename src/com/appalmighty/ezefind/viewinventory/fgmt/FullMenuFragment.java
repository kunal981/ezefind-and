package com.appalmighty.ezefind.viewinventory.fgmt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.newentry.fgmt.PickGoFragment;

public class FullMenuFragment extends Fragment implements OnClickListener {
	private Button backButton, homeButton;
	private LinearLayout layoutCategory, layoutDate, layoutValue, layoutScan,
			layoutMemeber, layoutLocation, layoutPickList;
	public static final String KEY = "key";
	String pickey;
	private LinearLayout layoutColor;

	public static FullMenuFragment newInstance() {
		FullMenuFragment fragment = new FullMenuFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_full_menu,
				container, false);
		backButton = (Button) rootView.findViewById(R.id.back_button);
		homeButton = (Button) rootView.findViewById(R.id.id_button_home);
		layoutCategory = (LinearLayout) rootView
				.findViewById(R.id.linear_category);
		layoutDate = (LinearLayout) rootView.findViewById(R.id.linear_DATE);
		layoutValue = (LinearLayout) rootView.findViewById(R.id.linear_value);
		layoutScan = (LinearLayout) rootView
				.findViewById(R.id.linear_scanbarcode);
		layoutMemeber = (LinearLayout) rootView
				.findViewById(R.id.linear_member);
		layoutLocation = (LinearLayout) rootView
				.findViewById(R.id.linear_location);
		layoutColor = (LinearLayout) rootView.findViewById(R.id.linear_color);
		layoutPickList = (LinearLayout) rootView
				.findViewById(R.id.linear_pickList);

		layoutScan.setOnClickListener(this);
		layoutCategory.setOnClickListener(this);
		layoutDate.setOnClickListener(this);
		layoutValue.setOnClickListener(this);
		// layoutScan.setOnClickListener(this);

		layoutMemeber.setOnClickListener(this);
		layoutLocation.setOnClickListener(this);
		layoutColor.setOnClickListener(this);
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		homeButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().finish();
				Intent intent = new Intent(getActivity(), MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		layoutPickList.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pickey = "pick_list";
				PickGoFragment fragment = new PickGoFragment();

				FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.setCustomAnimations(R.anim.slide_in_right,
						R.anim.slide_out_left, R.anim.slide_in_left,
						R.anim.slide_out_right);
				ft.replace(R.id.container, fragment);
				Bundle bundle = new Bundle();
				bundle.putString(KEY, pickey);
				fragment.setArguments(bundle);
				ft.addToBackStack(null);
				ft.commit();
			}
		});
		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		Fragment fragment = null;
		switch (id) {
		case R.id.linear_category:
			fragment = ViewCategoryFragment.newInstance();
			break;
		case R.id.linear_DATE:
			fragment = ViewDateFragment.newInstance();
			break;
		case R.id.linear_value:
			fragment = ViewValueFragment.newInstance();
			break;
		case R.id.linear_member:
			fragment = ViewMemberFragment.newInstance();
			break;
		case R.id.linear_location:
			fragment = ViewLocationFragment.newInstance();
			break;
		case R.id.linear_color:
			fragment = ViewFilterColorFragment.newInstance();
			break;
		case R.id.linear_scanbarcode:
			fragment = ScanInventoryFragment.newInstance();
			break;
		default:
			break;
		}
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
				R.anim.slide_in_left, R.anim.slide_out_right);
		ft.replace(R.id.container, fragment);
		ft.addToBackStack(null);
		ft.commit();

	}
}
