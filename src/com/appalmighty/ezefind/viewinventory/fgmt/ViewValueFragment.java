package com.appalmighty.ezefind.viewinventory.fgmt;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.newentry.fgmt.ContainerDetailsFragment;
import com.appalmighty.ezefind.view.RangeSeekBar;
import com.appalmighty.ezefind.view.RangeSeekBar.OnRangeSeekBarChangeListener;

public class ViewValueFragment extends Fragment {

	GridAdapter1 gridAdapter1;
	GridView gridView;
	Button backButton;
	TextView textRange;

	public static ViewValueFragment newInstance() {
		ViewValueFragment fragment = new ViewValueFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_fullscan_value,
				container, false);
		gridView = (GridView) rootView.findViewById(R.id.id_gridview_value);
		backButton = (Button) rootView.findViewById(R.id.back_button);
		textRange = (TextView) rootView.findViewById(R.id.txt_label_range);
		gridAdapter1 = new GridAdapter1(getActivity());
		gridView.setAdapter(gridAdapter1);
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		// create RangeSeekBar as Integer range between 20 and 75
		RangeSeekBar<Integer> seekBar = new RangeSeekBar<Integer>(0, 5000,
				getActivity());
		seekBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() {
			@Override
			public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar,
					Integer minValue, Integer maxValue) {
				// handle changed range values
				Log.i("View", "User selected new range values: MIN=" + minValue
						+ ", MAX=" + maxValue);

				String label = String.valueOf("$" + minValue + " - $"
						+ maxValue);
				textRange.setText(label);

			}
		});

		// add RangeSeekBar to pre-defined layout
		ViewGroup layout = (ViewGroup) rootView
				.findViewById(R.id.layout_range_bar);
		layout.addView(seekBar);
		return rootView;
	}

	class GridAdapter1 extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;

		public GridAdapter1(Activity activity) {
			super();
			this.activity = activity;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 7;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.fragment_fullmenu_value_griditems, null);

				ImageView imageView = (ImageView) convertView
						.findViewById(R.id.img_clothes);
				imageView.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ContainerDetailsFragment openContainerFragment = new ContainerDetailsFragment();
						FragmentManager fm = getFragmentManager();
						FragmentTransaction ft = fm.beginTransaction();
						ft.setCustomAnimations(R.anim.slide_in_right,
								R.anim.slide_out_left, R.anim.slide_in_left,
								R.anim.slide_out_right);
						Bundle bundle = new Bundle();
						bundle.putString("Title_Open", "View: Inventory");
						openContainerFragment.setArguments(bundle);
						ft.replace(R.id.container, openContainerFragment);
						ft.addToBackStack(null);
						ft.commit();

					}
				});

			}
			return convertView;
		}

	}
}
