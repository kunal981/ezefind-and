package com.appalmighty.ezefind.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.AttributeSet;
import android.view.View;

public class CustomView extends View {

	Bitmap myBitmap;
	Paint paint;
	RectF r;
	private ShapeDrawable mDrawable;

	public CustomView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	public CustomView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CustomView(Context context) {
		super(context);
		init();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		// canvas.drawRoundRect(r, 0, 0, paint);
		canvas.drawBitmap(myBitmap, 0, 0, paint);
	}

	private void init() {
		// TODO Auto-generated method stub
		int x = 10;
		int y = 10;
		int width = 300;
		int height = 50;

		mDrawable = new ShapeDrawable(new OvalShape());
		mDrawable.getPaint().setColor(0xff74AC23);
		mDrawable.setBounds(x, y, x + width, y + height);

		// r = new RectF(1, 2, 3, 4);
		// paint = new Paint();
		// myBitmap = BitmapFactory.decodeResource(getResources(),
		// R.drawable.img_oval);
	}

}
