package com.appalmighty.ezefind.view;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.speech.RecognizerIntent;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.util.AppConstant;

public class ChoosePopup {

	public static void openTypeModePopWindow(final FragmentActivity context) {
		context.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.popup_layout_option_text_mode, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

		context.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		ImageView btnDismiss = (ImageView) popupView
				.findViewById(R.id.btn_close);

		btnDismiss.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				popupWindow.dismiss();
			}
		});
		Button audioMode = (Button) popupView.findViewById(R.id.btn_mode_audio);
		audioMode.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				SharedPreferences shared = context.getSharedPreferences(
						AppConstant.KEY_APP, Context.MODE_PRIVATE);

				Editor editor = shared.edit();
				editor.putString(AppConstant.KEY_ENTRYTYPE, "1");
				editor.commit();
				popupWindow.dismiss();
			}
		});
		Button textmode = (Button) popupView.findViewById(R.id.btn_mode_text);
		textmode.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				SharedPreferences shared = context.getSharedPreferences(
						AppConstant.KEY_APP, Context.MODE_PRIVATE);

				Editor editor = shared.edit();
				editor.putString(AppConstant.KEY_ENTRYTYPE, "0");
				editor.commit();
				popupWindow.dismiss();
			}
		});

		popupWindow.showAtLocation(popupView, Gravity.CENTER_HORIZONTAL, 0, 0);

	}

	public static boolean isSpeechRecognitionActivityPresented(
			Activity callerActivity) {
		try {
			// getting an instance of package manager
			PackageManager pm = callerActivity.getPackageManager();
			// a list of activities, which can process speech recognition Intent
			List activities = pm.queryIntentActivities(new Intent(
					RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);

			if (activities.size() != 0) { // if list not empty
				return true; // then we can recognize the speech
			}
		} catch (Exception e) {

		}

		return false; // we have no activities to recognize the speech
	}

	/**
	 * Asking the permission for installing Google Voice Search. If permission
	 * granted � sent user to Google Play
	 * 
	 * @param callerActivity
	 *            � Activity, that initialized installing
	 */
	public static void installGoogleVoiceSearch(final Activity ownerActivity) {

		// creating a dialog asking user if he want
		// to install the Voice Search
		Dialog dialog = new AlertDialog.Builder(ownerActivity)
				.setMessage(
						"For recognition it's necessary to install Google Voice Search") // dialog
																							// message
				.setTitle("Install Voice Search from Google Play?") // dialog
																	// header
				.setPositiveButton("Install",
						new DialogInterface.OnClickListener() { // confirm
							// button

							// Install Button click handler
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								try {
									// creating an Intent for opening
									// applications page in Google Play
									// Voice Search package name:
									// com.google.android.voicesearch

									Intent intent = new Intent(
											Intent.ACTION_VIEW,
											Uri.parse("market://details?id=com.google.android.voicesearch"));
									// setting flags to avoid going in
									// application history (Activity call stack)
									intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY
											| Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
									// sending an Intent
									ownerActivity.startActivity(intent);
								} catch (Exception ex) {
									// if something going wrong
									// doing nothing
								}
							}
						})

				.setNegativeButton("Cancel", null) // cancel button
				.create();

		dialog.show(); // showing dialog
	}

}
