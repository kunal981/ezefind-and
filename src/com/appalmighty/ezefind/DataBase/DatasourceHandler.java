package com.appalmighty.ezefind.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DatasourceHandler {

	private SQLiteDatabase database;

	private NewEntryDataBase dbHelper;

	String PackageColum[] = { NewEntryDataBase.KEY_PACKAGEID,
			NewEntryDataBase.KEY_USERID, NewEntryDataBase.KEY_TITLE,
			NewEntryDataBase.KEY_ADDRESS, NewEntryDataBase.KEY_STARTDATE,
			NewEntryDataBase.KEY_STARTTIME };

	String PackageDataColum[] = { NewEntryDataBase.KEY_USERIDData,
			NewEntryDataBase.KEY_PACKAGEIDdata,
			NewEntryDataBase.KEY_PACKAGEDATAIDdata, NewEntryDataBase.KEY_TYPE,
			NewEntryDataBase.KEY_COLOR, NewEntryDataBase.KEY_NUMBER,
			NewEntryDataBase.KEY_CAUTION, NewEntryDataBase.KEY_DESCRIPTION,
			NewEntryDataBase.KEY_CATEGORY, NewEntryDataBase.KEY_BARCODE,
			NewEntryDataBase.KEY_PRICE, NewEntryDataBase.KEY_CREATEDATE,
			NewEntryDataBase.KEY_OTHERBARCODE };

	String Package_Data_ItemsColum[] = {
			NewEntryDataBase.KEY_USERID_DATA_ITEMS,
			NewEntryDataBase.KEY_PACKAGEIDdataitems,
			NewEntryDataBase.KEY_PACKAGEDATAIDdataitems,
			NewEntryDataBase.KEY_ITEMID, NewEntryDataBase.KEY_ITEMTITLE,
			NewEntryDataBase.KEY_ITEMDESCRIPTION,
			NewEntryDataBase.KEY_ITEMQUANTITY, NewEntryDataBase.KEY_ITEMIMAGE,
			NewEntryDataBase.KEY_ITEMIMAGETYPE,
			NewEntryDataBase.KEY_ITEMBARCODE };

	String Package_Data_ItemsattacColum[] = { NewEntryDataBase.KEY_FILEITEMID,
			NewEntryDataBase.KEY_FILE, NewEntryDataBase.KEY_FILE1,
			NewEntryDataBase.KEY_FILE2, NewEntryDataBase.KEY_FILE3,
			NewEntryDataBase.KEY_FILE4 };

	String Package_Data_LocationColum[] = {
			NewEntryDataBase.KEY_USERIDLOCATION,
			NewEntryDataBase.KEY_PACKAGEIDLOCATION,
			NewEntryDataBase.KEY_PACKAGEDATAIDLOCATION,
			NewEntryDataBase.KEY_LOCATION_TYPE, NewEntryDataBase.KEY_LOCATION,
			NewEntryDataBase.KEY_LOCATIONDETAIL,
			NewEntryDataBase.KEY_LOCATIONLAT,
			NewEntryDataBase.KEY_LOCATIONLONG,
			NewEntryDataBase.KEY_LOCATIONDISTANCE };

	String PicklistColum[] = { NewEntryDataBase.KEY_USERIDPICKLIST,
			NewEntryDataBase.KEY_PICKLISTID,
			NewEntryDataBase.KEY_USERIDPICKLIST,
			NewEntryDataBase.KEY_PICKLISTTITLE,
			NewEntryDataBase.KEY_PICKLISTSTARTDATE,
			NewEntryDataBase.KEY_PICKLISTSTARTTIME,
			NewEntryDataBase.KEY_PICKLISTENDDATE,
			NewEntryDataBase.KEY_PICKLISTENDTIME,
			NewEntryDataBase.KEY_PICKLISTTYPE };

	// String PicklistItemColum[] = { NewEntryDataBase.KEY_PICKLISTITEMUSERID,
	// NewEntryDataBase.KEY_PICKLISTITEMID,
	// NewEntryDataBase.KEY_PICKLISTITEMTITLE,
	// NewEntryDataBase.KEY_PICKLISTITEMSTARTDATE,
	// NewEntryDataBase.KEY_PICKLISTITEMSTARTTIME,
	// NewEntryDataBase.KEY_PICKLISTITEMENDDATE,
	// NewEntryDataBase.KEY_PICKLISTITEMENDTIME,
	// NewEntryDataBase.KEY_PICKLISTITEMTYPE, };

	public DatasourceHandler(Context context) {
		try {

			dbHelper = new NewEntryDataBase(context);
			database = dbHelper.getWritableDatabase();
			database = dbHelper.getReadableDatabase();

		} catch (Exception e) {
			// TODO: handle exception
			Log.e("eeeeeeeeeeeeeeeee", e.getMessage());
		}

	}

	public void close() {
		dbHelper.close();
	}

	public long InsertPackage(String TITLE, String DATE, String TIME,
			String ADDRESS, String USERID, String Typed) {

		ContentValues cv = new ContentValues();
		cv.put(NewEntryDataBase.KEY_TITLE, TITLE);
		cv.put(NewEntryDataBase.KEY_STARTDATE, DATE);
		cv.put(NewEntryDataBase.KEY_STARTTIME, TIME);
		cv.put(NewEntryDataBase.KEY_ADDRESS, ADDRESS);
		cv.put(NewEntryDataBase.KEY_USERID, USERID);
		cv.put(NewEntryDataBase.KEY_TYPED, Typed);
		long row = database.insert(NewEntryDataBase.TABLE_PACKAGE, null, cv);

		return row;
	}

	public long InsertPackageData(String PACKAGEID, String TYPE, String NUMBER,
			String USERID, String time, String string, String COLOR,
			String CAUTION, String DESCRIPTION, String CATEGORY,
			String BARCODE, String PRICE, String OTHERBARCODE,
			String LOCATIONDATA) {

		ContentValues cv = new ContentValues();
		cv.put(NewEntryDataBase.KEY_PACKAGEIDdata, PACKAGEID);
		cv.put(NewEntryDataBase.KEY_TYPE, TYPE);
		cv.put(NewEntryDataBase.KEY_NUMBER, NUMBER);
		cv.put(NewEntryDataBase.KEY_USERIDData, USERID);
		cv.put(NewEntryDataBase.KEY_CREATEDATE, time);
		cv.put(NewEntryDataBase.KEY_DATATYPED, string);
		cv.put(NewEntryDataBase.KEY_COLOR, COLOR);
		cv.put(NewEntryDataBase.KEY_CAUTION, CAUTION);
		cv.put(NewEntryDataBase.KEY_DESCRIPTION, DESCRIPTION);
		cv.put(NewEntryDataBase.KEY_CATEGORY, CATEGORY);
		cv.put(NewEntryDataBase.KEY_BARCODE, BARCODE);
		cv.put(NewEntryDataBase.KEY_PRICE, PRICE);
		cv.put(NewEntryDataBase.KEY_OTHERBARCODE, OTHERBARCODE);
		cv.put(NewEntryDataBase.KEY_LOCATIONDATA, LOCATIONDATA);
		Log.e("jbb", "" + cv);
		long row = database
				.insert(NewEntryDataBase.TABLE_PACKAGEDATA, null, cv);

		return row;
	}

	public Cursor getSinlgeEntry(String email, String keyBag) {

		// TODO Auto-generated method stub
		String query = "Select max(" + NewEntryDataBase.KEY_NUMBER + ") from "
				+ NewEntryDataBase.TABLE_PACKAGEDATA + " Where "
				+ NewEntryDataBase.KEY_PACKAGEIDdata + " =? and Type =?";
		Log.e("query", "" + query);
		Cursor cursor = database
				.rawQuery(query, new String[] { email, keyBag });

		return cursor;
	}

	public long UpdatePackageData(String USERID, String PACKAGEID, String TYPE,
			String PACKAGEDATAID, String COLOR, String CAUTION,
			String DESCRIPTION, String CATEGORY, String BARCODE, String PRICE,
			String OTHERBARCODE, String LOCATIONDATA, String Type,
			String profile) {

		ContentValues cv = new ContentValues();
		cv.put(NewEntryDataBase.KEY_COLOR, COLOR);
		cv.put(NewEntryDataBase.KEY_CAUTION, CAUTION);
		cv.put(NewEntryDataBase.KEY_DESCRIPTION, DESCRIPTION);
		cv.put(NewEntryDataBase.KEY_CATEGORY, CATEGORY);
		cv.put(NewEntryDataBase.KEY_BARCODE, BARCODE);
		cv.put(NewEntryDataBase.KEY_PRICE, PRICE);
		cv.put(NewEntryDataBase.KEY_OTHERBARCODE, OTHERBARCODE);
		cv.put(NewEntryDataBase.KEY_LOCATIONDATA, LOCATIONDATA);
		cv.put(NewEntryDataBase.KEY_MEMBERPROFILE, profile);

		long row = database.update(NewEntryDataBase.TABLE_PACKAGEDATA, cv,
				NewEntryDataBase.KEY_USERIDData + "=" + USERID + " and "
						+ NewEntryDataBase.KEY_PACKAGEIDdata + "=" + PACKAGEID
						+ " and " + NewEntryDataBase.KEY_PACKAGEDATAIDdata
						+ "=" + PACKAGEDATAID, null);

		return row;
	}

	public long InsertPackage_Data_items(String USERID, String PACKAGEID,
			String PACKAGEDATAID, String TITLE, String DESCRIPTION,
			String QUANTITY, int i, String IMAGE, String IMAGETYPE,
			String ITEMVALUE) {

		ContentValues cv = new ContentValues();
		cv.put(NewEntryDataBase.KEY_USERID_DATA_ITEMS, USERID);
		cv.put(NewEntryDataBase.KEY_PACKAGEIDdataitems, PACKAGEID);
		cv.put(NewEntryDataBase.KEY_PACKAGEDATAIDdataitems, PACKAGEDATAID);
		cv.put(NewEntryDataBase.KEY_ITEMTITLE, TITLE);
		cv.put(NewEntryDataBase.KEY_ITEMDESCRIPTION, DESCRIPTION);
		cv.put(NewEntryDataBase.KEY_ITEMQUANTITY, QUANTITY);
		cv.put(NewEntryDataBase.KEY_ITEMATTACHMENT, i);
		cv.put(NewEntryDataBase.KEY_ITEMIMAGE, IMAGE);
		cv.put(NewEntryDataBase.KEY_ITEMIMAGETYPE, IMAGETYPE);
		cv.put(NewEntryDataBase.KEY_ITEMVALUE, ITEMVALUE);

		long row = database.insert(NewEntryDataBase.TABLE_PACKAGEDATAITEMS,
				null, cv);

		return row;
	}

	public Cursor FilterPackagedata(String packagedataid, String string,
			String userId) {

		// TODO Auto-generated method stub
		//

		Cursor cursor = null;
		String query = "Select * from " + NewEntryDataBase.TABLE_PACKAGEDATA
				+ " Where " + NewEntryDataBase.KEY_PACKAGEIDdata + " =? and "
				+ NewEntryDataBase.KEY_TYPE + "=? and "
				+ NewEntryDataBase.KEY_USERIDData + "=?";
		Log.e("query", "" + query);

		cursor = database.rawQuery(query, new String[] { packagedataid, string,
				userId });
		Log.e("count", "" + cursor.getCount());

		return cursor;
	}

	public Cursor getItem(String packagedataid, String packagedataid2,
			String userId) {

		// TODO Auto-generated method stub
		String query = "Select * from "
				+ NewEntryDataBase.TABLE_PACKAGEDATAITEMS + " Where "
				+ NewEntryDataBase.KEY_PACKAGEIDdataitems + " =? and "
				+ NewEntryDataBase.KEY_PACKAGEDATAIDdataitems + "=? and "
				+ NewEntryDataBase.KEY_USERID_DATA_ITEMS + "=?";
		Log.e("query", "" + query);
		Cursor cursor = database.rawQuery(query, new String[] { packagedataid,
				packagedataid2, userId });

		return cursor;
	}

	public long UpdatePackageDataitem(String USERID, String PACKAGEID,
			String PACKAGEDATAID, String TITLE, String DESCRIPTION,
			String IMAGE, String ITEMID, String IMAGETYPE, String VALUE,
			String ATTACHMENT) {

		ContentValues cv = new ContentValues();
		cv.put(NewEntryDataBase.KEY_ITEMTITLE, TITLE);
		cv.put(NewEntryDataBase.KEY_ITEMDESCRIPTION, DESCRIPTION);
		cv.put(NewEntryDataBase.KEY_ITEMIMAGE, IMAGE);
		cv.put(NewEntryDataBase.KEY_ITEMIMAGETYPE, IMAGETYPE);
		cv.put(NewEntryDataBase.KEY_ITEMVALUE, VALUE);
		cv.put(NewEntryDataBase.KEY_ITEMATTACHMENT, ATTACHMENT);
		long row = database.update(NewEntryDataBase.TABLE_PACKAGEDATAITEMS, cv,
				NewEntryDataBase.KEY_USERID_DATA_ITEMS + "=" + USERID + " and "
						+ NewEntryDataBase.KEY_PACKAGEIDdataitems + "="
						+ PACKAGEID + " and "
						+ NewEntryDataBase.KEY_PACKAGEDATAIDdataitems + "="
						+ PACKAGEDATAID + " and " + NewEntryDataBase.KEY_ITEMID
						+ "=" + ITEMID, null);

		return row;
	}

	public long DeletePackageDataitem(String USERID, String PACKAGEID,
			String PACKAGEDATAID, String ITEMID) {

		long row = database.delete(NewEntryDataBase.TABLE_PACKAGEDATAITEMS,
				NewEntryDataBase.KEY_USERID_DATA_ITEMS + "=" + USERID + " and "
						+ NewEntryDataBase.KEY_PACKAGEIDdataitems + "="
						+ PACKAGEID + " and "
						+ NewEntryDataBase.KEY_PACKAGEDATAIDdataitems + "="
						+ PACKAGEDATAID + " and " + NewEntryDataBase.KEY_ITEMID
						+ "=" + ITEMID, null);

		return row;
	}

	public long InsertItemAttachment(String ITEMID, String FILE, String FILE1,
			String FILE2, String FILE3, String FILE4, String COUNT) {

		ContentValues cv = new ContentValues();
		cv.put(NewEntryDataBase.KEY_FILEITEMID, ITEMID);
		cv.put(NewEntryDataBase.KEY_FILE, FILE);
		cv.put(NewEntryDataBase.KEY_FILE1, FILE1);
		cv.put(NewEntryDataBase.KEY_FILE2, FILE2);
		cv.put(NewEntryDataBase.KEY_FILE3, FILE3);
		cv.put(NewEntryDataBase.KEY_FILE4, FILE4);
		cv.put(NewEntryDataBase.KEY_FILECOUNT, COUNT);
		long row = database.insert(
				NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT, null, cv);

		return row;
	}

	public Cursor getPackageItem(String packagedataid, String userId) {

		// TODO Auto-generated method stub
		String query = "Select * from " + NewEntryDataBase.TABLE_PACKAGEDATA
				+ " Where " + NewEntryDataBase.KEY_PACKAGEIDdataitems
				+ " =? and " + NewEntryDataBase.KEY_USERID_DATA_ITEMS + "=?";
		Log.e("query", "" + query);
		Cursor cursor = database.rawQuery(query, new String[] { packagedataid,
				userId });

		return cursor;
	}

	public boolean InsertPackage_Data_Location(String TITLE, String DATE,
			String TIME, String ADDRESS, String USERID) {
		boolean flag = false;
		try {
			ContentValues cv = new ContentValues();
			cv.put(NewEntryDataBase.KEY_TITLE, TITLE);
			cv.put(NewEntryDataBase.KEY_STARTDATE, DATE);
			cv.put(NewEntryDataBase.KEY_STARTTIME, TIME);
			cv.put(NewEntryDataBase.KEY_ADDRESS, ADDRESS);
			cv.put(NewEntryDataBase.KEY_USERID, USERID);
			long row = database.insert(
					NewEntryDataBase.TABLE_PACKAGEDATALOCATION, null, cv);

			if (row > 0) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return flag;
	}

	public Cursor viewData(String userId, String string) {
		// TODO Auto-generated method stub

		String query = "Select * from " + NewEntryDataBase.TABLE_PACKAGE
				+ " Where " + NewEntryDataBase.KEY_USERID + " =? and "
				+ NewEntryDataBase.KEY_TYPED + "=?";
		;
		Log.e("query", "" + query);
		Cursor cursor = database.rawQuery(query,
				new String[] { userId, string });
		Log.e("viewData", "" + cursor.getCount());
		return cursor;
	}

	public Cursor GetDataCount(String userId, String PACKAGEID) {
		// TODO Auto-generated method stub

		String query = "Select * from " + NewEntryDataBase.TABLE_PACKAGEDATA
				+ " Where " + NewEntryDataBase.KEY_USERIDData + " =? and "
				+ NewEntryDataBase.KEY_PACKAGEIDdata + "=?";
		Log.e("query", "" + query);
		Cursor cursor = database.rawQuery(query, new String[] { userId,
				PACKAGEID });
		Log.e("GetDataCount", "" + cursor.getCount());
		return cursor;
	}

	public Cursor GetDataitemCount(String userId, String PACKAGEDataID) {
		// TODO Auto-generated method stub

		String query = "Select * from "
				+ NewEntryDataBase.TABLE_PACKAGEDATAITEMS + " Where "
				+ NewEntryDataBase.KEY_USERID_DATA_ITEMS + " =? and "
				+ NewEntryDataBase.KEY_PACKAGEDATAIDdataitems + "=?";

		Cursor cursor = database.rawQuery(query, new String[] { userId,
				PACKAGEDataID });

		return cursor;
	}

	public Cursor getAllPackageItem(String userId, String packageid,
			String packagedata) {
		// TODO Auto-generated method stub
		String query = "Select * from "
				+ NewEntryDataBase.TABLE_PACKAGEDATAITEMS + " Where "
				+ NewEntryDataBase.KEY_PACKAGEIDdataitems + " =? and "
				+ NewEntryDataBase.KEY_PACKAGEDATAIDdataitems + "=? and "
				+ NewEntryDataBase.KEY_USERID_DATA_ITEMS + "=?";

		Cursor cursor = database.rawQuery(query, new String[] { packageid,
				packagedata, userId });
		Log.e("GetAllPackage", "" + cursor.getCount());
		return cursor;
	}

	public Cursor getSelectedItem(String userId, String itemid) {
		// TODO Auto-generated method stub

		String query = "Select * from "
				+ NewEntryDataBase.TABLE_PACKAGEDATAITEMS + " Where "
				+ NewEntryDataBase.KEY_USERID_DATA_ITEMS + "=? and "
				+ NewEntryDataBase.KEY_ITEMID + "=?";
		Log.e("query", "" + query);
		Cursor cursor = database.rawQuery(query,
				new String[] { userId, itemid });

		return cursor;
	}

	public long InsertLocation(String packageid, String packagedataid,
			String value, String type, String userId, String locname,
			String locdeatail, String localat, String localong, String distance) {
		// TODO Auto-generated method stub

		ContentValues cv = new ContentValues();
		cv.put(NewEntryDataBase.KEY_USERID_DATA_ITEMS, userId);
		cv.put(NewEntryDataBase.KEY_PACKAGEIDLOCATION, packageid);
		cv.put(NewEntryDataBase.KEY_PACKAGEDATAIDLOCATION, packagedataid);
		cv.put(NewEntryDataBase.KEY_LOCATIONVALUE, value);
		cv.put(NewEntryDataBase.KEY_LOCATION_TYPE, type);
		cv.put(NewEntryDataBase.KEY_LOCATION, locname);
		cv.put(NewEntryDataBase.KEY_LOCATIONDETAIL, locdeatail);
		cv.put(NewEntryDataBase.KEY_LOCATIONLAT, localat);
		cv.put(NewEntryDataBase.KEY_LOCATIONLONG, localong);
		cv.put(NewEntryDataBase.KEY_LOCATIONDISTANCE, distance);

		long row = database.insert(NewEntryDataBase.TABLE_PACKAGEDATALOCATION,
				null, cv);

		return row;
	}

	public long DeletePackageData(String userId, String packageid,
			String packagedataid) {
		// TODO Auto-generated method stub
		long row = database.delete(NewEntryDataBase.TABLE_PACKAGEDATA,
				NewEntryDataBase.KEY_USERIDData + "=" + userId + " and "
						+ NewEntryDataBase.KEY_PACKAGEIDdata + "=" + packageid
						+ " and " + NewEntryDataBase.KEY_PACKAGEDATAIDdata
						+ "=" + packagedataid, null);
		return row;
	}

	public long DeletePackage(String string, String userId) {
		// TODO Auto-generated method stub
		long row = database.delete(NewEntryDataBase.TABLE_PACKAGE,
				NewEntryDataBase.KEY_USERID + "=" + userId + " and "
						+ NewEntryDataBase.KEY_PACKAGEID + "=" + string, null);
		return row;
	}

	public long DeletePackagedata(String string, String userId) {
		// TODO Auto-generated method stub
		long row = database.delete(NewEntryDataBase.TABLE_PACKAGEDATA,
				NewEntryDataBase.KEY_USERIDData + "=" + userId + " and "
						+ NewEntryDataBase.KEY_PACKAGEIDdata + "=" + string,
				null);
		return row;
	}

	public long DeletePackagedataitem(String string, String userId) {
		// TODO Auto-generated method stub
		long row = database.delete(NewEntryDataBase.TABLE_PACKAGEDATAITEMS,
				NewEntryDataBase.KEY_USERID_DATA_ITEMS + "=" + userId + " and "
						+ NewEntryDataBase.KEY_PACKAGEIDdataitems + "="
						+ string, null);
		return row;
	}

	public long InsertPicklist(String TITLE, String DATE, String TIME,
			String USERID, String Type) {
		// TODO Auto-generated method stub

		Log.e("query", "" + NewEntryDataBase.QUERY_PICKLIST);
		ContentValues cv = new ContentValues();
		cv.put(NewEntryDataBase.KEY_PICKLISTTITLE, TITLE);
		cv.put(NewEntryDataBase.KEY_PICKLISTSTARTDATE, DATE);
		cv.put(NewEntryDataBase.KEY_PICKLISTSTARTTIME, TIME);

		cv.put(NewEntryDataBase.KEY_USERIDPICKLIST, USERID);
		cv.put(NewEntryDataBase.KEY_PICKLISTTYPE, Type);
		long row = database.insert(NewEntryDataBase.TABLE_PICKLIST, null, cv);
		Log.e("row", "" + row);
		return row;
	}

	public Cursor viewPicklistData(String userId) {
		// TODO Auto-generated method stub
		String query = "Select * from " + NewEntryDataBase.TABLE_PICKLIST
				+ " Where " + NewEntryDataBase.KEY_USERIDPICKLIST + " =?";
		;
		Log.e("query", "" + query);
		Cursor cursor = database.rawQuery(query, new String[] { userId });
		Log.e("viewPicklistData", "" + cursor.getCount());
		return cursor;
	}

	public long InsertPicklistitem(String picklistid, String userId,
			String itemid, String type, String currentdate) {
		// TODO Auto-generated method stub
		Log.e("query", "" + NewEntryDataBase.QUERY_PICKLISTITEM);

		ContentValues cv = new ContentValues();
		cv.put(NewEntryDataBase.KEY_PICKLISTIDitem, picklistid);
		cv.put(NewEntryDataBase.KEY_PICKLISTITEMUSERID, userId);
		cv.put(NewEntryDataBase.KEY_PICKLISTITEMSTARTDATE, currentdate);

		cv.put(NewEntryDataBase.KEY_ADDEDitemid, itemid);
		cv.put(NewEntryDataBase.KEY_PICKLISTITEMTYPE, type);
		long row = database.insert(NewEntryDataBase.TABLE_PICKLISTITEM, null,
				cv);
		Log.e("row", "" + row);
		return row;
	}

	public long DeletePicklist(String picklistid, String userId) {
		// TODO Auto-generated method stub
		long row = database.delete(NewEntryDataBase.TABLE_PICKLIST,
				NewEntryDataBase.KEY_USERIDPICKLIST + "=" + userId + " and "
						+ NewEntryDataBase.KEY_PICKLISTID + "=" + picklistid,
				null);
		Log.e("row", "" + row);
		return row;
	}

	public long DeletePicklistitem(String picklistid, String userId) {
		// TODO Auto-generated method stub
		long row = database.delete(NewEntryDataBase.TABLE_PICKLISTITEM,
				NewEntryDataBase.KEY_PICKLISTITEMUSERID + "=" + userId
						+ " and " + NewEntryDataBase.KEY_ADDEDitemid + "="
						+ picklistid, null);
		Log.e("DeletePicklistitem", "" + row);
		return row;
	}

	public Cursor checkingduplicate(String Picklistid, String userId,
			String itemid, String type) {
		// TODO Auto-generated method stub

		String query = "Select * from " + NewEntryDataBase.TABLE_PICKLISTITEM
				+ " Where " + NewEntryDataBase.KEY_PICKLISTITEMUSERID
				+ " =? and " + NewEntryDataBase.KEY_PICKLISTIDitem + "=? and "
				+ NewEntryDataBase.KEY_ADDEDitemid + "=? and "
				+ NewEntryDataBase.KEY_PICKLISTITEMTYPE + "=?";
		Log.e("query", "" + query);
		Cursor cursor = database.rawQuery(query, new String[] { userId,
				Picklistid, itemid, type });

		return cursor;
	}

	public Cursor GetPicklistitemcount(String userId, String picklistid) {
		// TODO Auto-generated method stub
		String query = "Select * from " + NewEntryDataBase.TABLE_PICKLISTITEM
				+ " Where " + NewEntryDataBase.KEY_PICKLISTITEMUSERID
				+ " =? and " + NewEntryDataBase.KEY_PICKLISTIDitem + "=?";
		Log.e("query", "" + query);
		Cursor cursor = database.rawQuery(query, new String[] { userId,
				picklistid });
		Log.e("GetPicklistitemcount", "" + cursor.getCount());
		return cursor;
	}

	public Cursor getAllItem(String userId, String itemid) {
		// TODO Auto-generated method stub
		String query = "Select * from "
				+ NewEntryDataBase.TABLE_PACKAGEDATAITEMS + " Where "
				+ NewEntryDataBase.KEY_ITEMID + " =? and "
				+ NewEntryDataBase.KEY_USERID_DATA_ITEMS + "=?";

		Cursor cursor = database.rawQuery(query,
				new String[] { itemid, userId });
		Log.e("GetAllitem", "" + cursor.getCount());
		return cursor;
	}

	public Cursor GetPackagedatalocation(String packageid, String packagedataid) {
		// TODO Auto-generated method stub
		String query = "Select * from " + NewEntryDataBase.TABLE_PACKAGEDATA
				+ " Where " + NewEntryDataBase.KEY_PACKAGEIDdata + " =? and "
				+ NewEntryDataBase.KEY_PACKAGEDATAIDdata + "=?";

		Cursor cursor = database.rawQuery(query, new String[] { packageid,
				packagedataid });
		Log.e("getPackagedata", "" + cursor.getCount());
		return cursor;
	}

	public Cursor GetTypeCount(String userId, String keyBag) {
		// TODO Auto-generated method stub

		String query = "Select * from " + NewEntryDataBase.TABLE_PACKAGEDATA
				+ " Where " + NewEntryDataBase.KEY_USERIDData + " =? and "
				+ NewEntryDataBase.KEY_TYPE + "=?";
		Log.e("query", "" + query);
		Cursor cursor = database.rawQuery(query,
				new String[] { userId, keyBag });
		Log.e("getcount", "" + cursor.getCount());
		return cursor;
	}

	public Cursor Getitemcount(String userId) {
		// TODO Auto-generated method stub
		String query = "Select * from "
				+ NewEntryDataBase.TABLE_PACKAGEDATAITEMS + " Where "
				+ NewEntryDataBase.KEY_USERID_DATA_ITEMS + " =?";
		Log.e("query", "" + query);
		Cursor cursor = database.rawQuery(query, new String[] { userId });
		Log.e("getitemcount", "" + cursor.getCount());
		return cursor;
	}

	public Cursor getAttachment(String itemid) {
		// TODO Auto-generated method stub
		String query = "Select * from "
				+ NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT + " Where "
				+ NewEntryDataBase.KEY_FILEITEMID + " =?";
		Log.e("query", "" + query);
		Cursor cursor = database.rawQuery(query, new String[] { itemid });
		Log.e("getitemcount", "" + cursor.getCount());
		return cursor;
	}

	public Long Deleteattachment(String itemid, String string) {
		// TODO Auto-generated method stub
		long row = database.delete(
				NewEntryDataBase.TABLE_PACKAGEITEMATTACHEMENT,
				NewEntryDataBase.KEY_FILEITEMID + "=" + itemid + " and "
						+ NewEntryDataBase.KEY_FILE + "= " + string + " OR "
						+ NewEntryDataBase.KEY_FILE1 + "= " + string + " OR "
						+ NewEntryDataBase.KEY_FILE2 + "= " + string + " OR "
						+ NewEntryDataBase.KEY_FILE3 + "= " + string + " OR "
						+ NewEntryDataBase.KEY_FILE4 + "= " + string, null);
		Log.e("Deleteattachment", "" + row);

		return row;

	}

	public Cursor getItemPic(String userId, String string) {
		// TODO Auto-generated method stub
		String query = "Select * from "
				+ NewEntryDataBase.TABLE_PACKAGEDATAITEMS + " Where "

				+ NewEntryDataBase.KEY_ITEMID + "=? and "
				+ NewEntryDataBase.KEY_USERID_DATA_ITEMS + "=?";

		Cursor cursor = database.rawQuery(query,
				new String[] { string, userId });
		Log.e("GetPic", "" + cursor.getCount());
		return cursor;
	}
}
