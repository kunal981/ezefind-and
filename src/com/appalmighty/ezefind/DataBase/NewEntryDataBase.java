package com.appalmighty.ezefind.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class NewEntryDataBase extends SQLiteOpenHelper {

	public static final int DATABASE_VERSION = 2;
	// Database Name
	public static final String DATABASE_NAME = "Eze-find.db";

	// Package table name
	public static final String TABLE_PACKAGE = "package";
	// Package data table name
	public static final String TABLE_PACKAGEDATA = "package_data";
	// Package data items table name
	public static final String TABLE_PACKAGEDATAITEMS = "package_data_items";
	// PackageITEMATTACHHEMENT table name
	public static final String TABLE_PACKAGEITEMATTACHEMENT = "attachment";
	// PackageDataLocation table name
	public static final String TABLE_PACKAGEDATALOCATION = "package_data_location";
	// Picklist table name
	public static final String TABLE_PICKLIST = "picklists";
	// Picklistitem table name
	public static final String TABLE_PICKLISTITEM = "picklist_items";

	// Parameter for package table
	public static final String KEY_PACKAGEID = "PackageId";
	public static final String KEY_USERID = "UserId";
	public static final String KEY_TITLE = "Title";
	public static final String KEY_ADDRESS = "Address";
	public static final String KEY_STARTDATE = "StartDate";
	public static final String KEY_STARTTIME = "StartTime";
	public static final String KEY_ENDDATE = "EndDate";
	public static final String KEY_ENDTIME = "EndTime";
	public static final String KEY_TYPED = "Typedpackage";

	// Parameter for packageData table

	public static final String KEY_USERIDData = "UserId_fk";
	public static final String KEY_PACKAGEIDdata = "PackageId";
	public static final String KEY_PACKAGEDATAIDdata = "_PackageDataId";
	public static final String KEY_TYPE = "Type";
	public static final String KEY_MEMBERPROFILE = "Member";
	public static final String KEY_COLOR = "Color";
	public static final String KEY_NUMBER = "Number";
	public static final String KEY_CAUTION = "Caution";
	public static final String KEY_DESCRIPTION = "Description";
	public static final String KEY_CATEGORY = "Category";
	public static final String KEY_LOCATIONDATA = "Location";
	public static final String KEY_BARCODE = "BarCode";
	public static final String KEY_PRICE = "value";
	public static final String KEY_OTHERBARCODE = "OtherBarCode";
	public static final String KEY_CREATEDATE = "_createDate";
	public static final String KEY_DATATYPED = "Typedpackagedata";

	// Parameter for packageDataitems table
	public static final String KEY_USERID_DATA_ITEMS = "UserId_fk";
	public static final String KEY_PACKAGEIDdataitems = "PackageId";
	public static final String KEY_PACKAGEDATAIDdataitems = "PackageDataId";
	public static final String KEY_ITEMID = "ItemId";
	public static final String KEY_ITEMTITLE = "Title";
	public static final String KEY_ITEMDESCRIPTION = "Description";
	public static final String KEY_ITEMQUANTITY = "Quantity";
	public static final String KEY_ITEMATTACHMENT = "ATTCHMENT";
	public static final String KEY_ITEMIMAGE = "Image";
	public static final String KEY_ITEMIMAGETYPE = "ImageType";
	public static final String KEY_ITEMVALUE = "ItemValue";
	public static final String KEY_ITEMBARCODE = "Barcode";

	// Parameter for packageitemattchment table
	public static final String KEY_FILEITEMID = "itemid";
	public static final String KEY_FILE = "file";
	public static final String KEY_FILE1 = "file1";
	public static final String KEY_FILE2 = "file2";
	public static final String KEY_FILE3 = "file3";
	public static final String KEY_FILE4 = "file4";
	public static final String KEY_FILECOUNT = "filecount";

	// Parameter for packageDataLocation table
	public static final String KEY_USERIDLOCATION = "UserId_fk";
	public static final String KEY_PACKAGEIDLOCATION = "PackageId";
	public static final String KEY_PACKAGEDATAIDLOCATION = "PackageDataId";
	public static final String KEY_LOCATION_TYPE = "_type";
	public static final String KEY_LOCATIONVALUE = "_value";
	public static final String KEY_LOCATION = "LocationName";
	public static final String KEY_LOCATIONDETAIL = "Details";
	public static final String KEY_LOCATIONLAT = "Lat";
	public static final String KEY_LOCATIONLONG = "Long";
	public static final String KEY_LOCATIONDISTANCE = "DistanceFromCurrentLocation";

	// Parameter for Picklist table
	public static final String KEY_PICKLISTID = "PicklistId";
	public static final String KEY_USERIDPICKLIST = "UserId_fk";
	public static final String KEY_PICKLISTTITLE = "Title";
	public static final String KEY_PICKLISTSTARTDATE = "StartDate";
	public static final String KEY_PICKLISTSTARTTIME = "StartTime";
	public static final String KEY_PICKLISTENDDATE = "EndDate";
	public static final String KEY_PICKLISTENDTIME = "EndTime";
	public static final String KEY_PICKLISTTYPE = "Type";

	// Parameter for Picklist ITEM table
	public static final String KEY_PICKLISTITEMUSERID = "UserId_fk";
	public static final String KEY_PICKLISTIDitem = "PicklistId";
	public static final String KEY_PICKLISTITEMID = "PicklistitemId";
	public static final String KEY_ADDEDitemid = "itemid";
	public static final String KEY_PICKLISTITEMSTARTDATE = "StartDate";
	public static final String KEY_PICKLISTITEMTYPE = "Type";

	// public static final String KEY_PLATFORM = "_plateform";

	public static String QUERY_PACKAGE = "create table " + TABLE_PACKAGE
			+ " ( " + KEY_PACKAGEID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ KEY_TITLE + " TEXT," + KEY_STARTDATE + " TEXT," + KEY_STARTTIME
			+ " TEXT," + KEY_ADDRESS + " TEXT," + KEY_USERID + " TEXT, "
			+ KEY_TYPED + " TEXT)";

	// TABLE CCREATE QUERY FOR PACKAGEDATA

	public static String QUERY_PACKAGEDATA = "create table "
			+ TABLE_PACKAGEDATA + " ( " + KEY_USERIDData + " TEXT, "
			+ KEY_PACKAGEIDdata + " TEXT, " + KEY_PACKAGEDATAIDdata
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_TYPE + " TEXT , "
			+ KEY_COLOR + " TEXT, " + KEY_NUMBER + " TEXT, " + KEY_CAUTION
			+ " TEXT, " + KEY_DESCRIPTION + " TEXT , " + KEY_CATEGORY
			+ " TEXT, " + KEY_LOCATIONDATA + " TEXT, " + KEY_BARCODE
			+ " TEXT, " + KEY_PRICE + " TEXT, " + KEY_OTHERBARCODE + " TEXT, "
			+ KEY_CREATEDATE + " TEXT, " + KEY_MEMBERPROFILE + " TEXT, "
			+ KEY_DATATYPED + " TEXT)";

	// TABLE CCREATE QUERY FOR PACKAGE_DATA_ITEMS

	public static String QUERY_PACKAGEDATAITEMS = "create table "
			+ TABLE_PACKAGEDATAITEMS + " ( " + KEY_USERID_DATA_ITEMS
			+ " TEXT, " + KEY_PACKAGEIDdataitems + " TEXT, "
			+ KEY_PACKAGEDATAIDdataitems + " TEXT , " + KEY_ITEMID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT , " + KEY_ITEMTITLE
			+ " TEXT," + KEY_ITEMDESCRIPTION + " TEXT," + KEY_ITEMQUANTITY
			+ " TEXT, " + KEY_ITEMATTACHMENT + " TEXT, " + KEY_ITEMIMAGE
			+ " TEXT, " + KEY_ITEMIMAGETYPE + " TEXT, " + KEY_ITEMBARCODE
			+ " TEXT, " + KEY_ITEMVALUE + " TEXT)";

	// TABLE CCREATE QUERY FOR PACKAGE_DATA_ITEMS_ATTACHMENT

	public static String QUERY_PACKAGEDATAITEMATTEACHMENT = "create table "
			+ TABLE_PACKAGEITEMATTACHEMENT + " ( " + KEY_FILEITEMID + " TEXT,"
			+ KEY_FILE + " TEXT," + KEY_FILE1 + " TEXT," + KEY_FILE2 + " TEXT,"
			+ KEY_FILE3 + " TEXT," + KEY_FILE4 + " TEXT, " + KEY_FILECOUNT
			+ " TEXT)";

	// TABLE CCREATE QUERY FOR PACKAGEDATALOCATION
	public static String QUERY_PACKAGEDATALOCATION = "create table "
			+ TABLE_PACKAGEDATALOCATION + " ( " + KEY_USERIDLOCATION
			+ " TEXT, " + KEY_PACKAGEIDLOCATION + " TEXT , "
			+ KEY_PACKAGEDATAIDLOCATION + " TEXT , " + KEY_LOCATION_TYPE
			+ " TEXT," + KEY_LOCATIONVALUE + " TEXT, " + KEY_LOCATION
			+ " TEXT, " + KEY_LOCATIONDETAIL + " TEXT , " + KEY_LOCATIONLAT
			+ " TEXT , " + KEY_LOCATIONLONG + " TEXT," + KEY_LOCATIONDISTANCE
			+ " TEXT)";

	// TABLE CCREATE QUERY FOR PICKLIST

	public static String QUERY_PICKLIST = "create table " + TABLE_PICKLIST
			+ " ( " + KEY_PICKLISTIDitem + "TEXT, " + KEY_PICKLISTID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_USERIDPICKLIST
			+ " TEXT, " + KEY_PICKLISTTITLE + " TEXT, " + KEY_PICKLISTSTARTDATE
			+ " TEXT," + KEY_PICKLISTSTARTTIME + " TEXT, "
			+ KEY_PICKLISTENDDATE + " TEXT, " + KEY_PICKLISTENDTIME + " TEXT, "
			+ KEY_PICKLISTTYPE + " TEXT)";

	// TABLE CCREATE QUERY FOR PICKLIST ITEM

	public static String QUERY_PICKLISTITEM = "create table "
			+ TABLE_PICKLISTITEM + " ( " + KEY_PICKLISTITEMUSERID + " TEXT, "
			+ KEY_PICKLISTITEMID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
			+ KEY_PICKLISTIDitem + " TEXT , " + KEY_ADDEDitemid + " TEXT , "
			+ KEY_PICKLISTITEMSTARTDATE + " TEXT," + KEY_PICKLISTITEMTYPE
			+ " TEXT )";

	public NewEntryDataBase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
		Log.e("newentrydatabase", "newentrydatabase");
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		try {
			Log.e("OnCreatedatabase", "OnCreatedatabase");

			db.execSQL(QUERY_PACKAGE);
			db.execSQL(QUERY_PACKAGEDATA);
			db.execSQL(QUERY_PACKAGEDATAITEMS);
			db.execSQL(QUERY_PACKAGEDATAITEMATTEACHMENT);
			db.execSQL(QUERY_PACKAGEDATALOCATION);
			db.execSQL(QUERY_PICKLIST);
			db.execSQL(QUERY_PICKLISTITEM);
			Log.e("lst OnCreatedatabase", "OnCreatedatabase");

		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Table Exception", e.getMessage());
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PACKAGE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PACKAGEDATA);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PACKAGEDATAITEMS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PACKAGEITEMATTACHEMENT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PACKAGEDATALOCATION);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PICKLIST);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PICKLISTITEM);
	}

}
