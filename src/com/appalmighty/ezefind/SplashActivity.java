package com.appalmighty.ezefind;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.appalmighty.ezefind.login.act.LoginActivity;
import com.appalmighty.ezefind.util.AppConstant;

public class SplashActivity extends Activity {

	private int SPLASH_TIME_OUT = 1200;

	SharedPreferences sharedpreferences;
	Intent intent_home;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_splash);
		// setContentView(R.layout.fragment_view_report_menu);

		sharedpreferences = getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		Log.e("sharedpreference",
				"" + sharedpreferences.getString(AppConstant.KEY_USER_ID, ""));

		if (sharedpreferences != null
				&& sharedpreferences.contains(AppConstant.KEY_USER_NAME)) {
			intent_home = new Intent(SplashActivity.this, MainActivity.class);

		} else {
			intent_home = new Intent(SplashActivity.this, LoginActivity.class);

		}

		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {
				// This method will be executed once the timer is over
				// Start your app main activity
				startActivity(intent_home);
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
				finish();
			}
		}, SPLASH_TIME_OUT);

	}

}
