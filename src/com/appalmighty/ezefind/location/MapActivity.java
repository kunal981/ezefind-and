package com.appalmighty.ezefind.location;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.appalmighty.ezefind.util.Helper;
import com.appalmighty.ezefind.view.ChoosePopup;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentActivity implements LocationListener,
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener, OnClickListener,
		android.location.LocationListener {

	private GoogleMap map;
	private static String TAG = "Map";

	// A request to connect to Location Services
	private LocationRequest mLocationRequest;
	public static String name;
	// Stores the current instantiation of the location client in this object
	private LocationClient mLocationClient;
	private LatLng latLng;
	private EditText locationname, locationdetail, distance, lat, longitu;
	private RelativeLayout relContainer, relShare;
	private ImageView imageBtnCancel, imagebtnShare, imagebtnsave, imagemap;
	String UserId, mlocationname, mlocationdetail, mdistance, mlat, mlong,
			packageid, packagedataid;
	TextView date, item;
	Boolean isInternetPresent = false;
	Button back;
	ConnectionDetector internetcheck;
	SharedPreferences shared;
	String typingvalue;
	public static final int REQUEST_AUDIO_TO_TEXT_LOCATION_DETIAL = 301;
	public static final int REQUEST_AUDIO_TO_TEXT_LOCATION_NAME = 302;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);

		map = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map)).getMap();
		date = (TextView) findViewById(R.id.txt_date);
		item = (TextView) findViewById(R.id.id_text_item_number);
		back = (Button) findViewById(R.id.back_back);
		Intent intent = getIntent();
		initWaypointContainer();
		shared = this.getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = shared.getString(AppConstant.KEY_USER_ID, "");
		packageid = shared.getString(AppConstant.KEY_PackageId, "");
		packagedataid = shared.getString(AppConstant.KEY_PackageDataId, "");
		item.setText(intent.getStringExtra("item"));
		date.setText(shared.getString(AppConstant.KEY_Date, ""));
		internetcheck = new ConnectionDetector(MapActivity.this);

		locationInitilizer();

		map.setMyLocationEnabled(true);
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		String bestProvider = locationManager.getBestProvider(criteria, true);
		Location location = locationManager.getLastKnownLocation(bestProvider);
		locationManager.requestLocationUpdates(bestProvider, 2000, 0, this);
		if (location != null) {
			// onLocationChanged(location);
			double latitude = location.getLatitude();

			double longitude = location.getLongitude();

			LatLng latLng = new LatLng(latitude, longitude);

			latLng = new LatLng(latitude, longitude);

			map.addMarker(new MarkerOptions().position(latLng).title("Start"));
		}

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}

	private void initWaypointContainer() {
		relContainer = (RelativeLayout) findViewById(R.id.container_waypoint);
		relShare = (RelativeLayout) findViewById(R.id.layout_share);
		imageBtnCancel = (ImageView) findViewById(R.id.img_btn_cancel);
		imagebtnShare = (ImageView) findViewById(R.id.img_btn_share);
		imagemap = (ImageView) findViewById(R.id.img_btn_pin);
		locationname = (EditText) findViewById(R.id.et_loc_name);
		locationdetail = (EditText) findViewById(R.id.et_loc_detail);
		distance = (EditText) findViewById(R.id.et_loc_dst);
		lat = (EditText) findViewById(R.id.et_loc_lat);
		longitu = (EditText) findViewById(R.id.et_loc_lng);
		imagebtnsave = (ImageView) findViewById(R.id.img_btn_save);
		imagebtnShare.setOnClickListener(this);
		imageBtnCancel.setOnClickListener(this);
		imagemap.setOnClickListener(this);
		imagebtnsave.setOnClickListener(this);
		locationname.setOnClickListener(this);
		locationdetail.setOnClickListener(this);
		locationname.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE,
							"");
					// Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						locationname.setInputType(InputType.TYPE_NULL);
						MapActivity.this
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(MapActivity.this);
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(MapActivity.this) == true) {
							// if yes � running recognition
							startRecognition(MapActivity.this,
									REQUEST_AUDIO_TO_TEXT_LOCATION_NAME);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									MapActivity.this,
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup
									.installGoogleVoiceSearch(MapActivity.this);
						}
					}
				}

			}
		});
		locationdetail.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE,
							"");
					// Log.e("typingvalue", "" + typingvalue);
					if (typingvalue.equals("2")) {
						locationdetail.setInputType(InputType.TYPE_NULL);
						MapActivity.this
								.getWindow()
								.setSoftInputMode(
										WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
						ChoosePopup.openTypeModePopWindow(MapActivity.this);
					} else if (typingvalue.equals("1")) {
						if (ChoosePopup
								.isSpeechRecognitionActivityPresented(MapActivity.this) == true) {
							// if yes � running recognition
							startRecognition(MapActivity.this,
									REQUEST_AUDIO_TO_TEXT_LOCATION_NAME);
						} else {
							// if no, then showing notification to install Voice
							// Search
							Toast.makeText(
									MapActivity.this,
									"In order to activate speech recognition you must install Google Voice Search",
									Toast.LENGTH_LONG).show();
							// start installing process
							ChoosePopup
									.installGoogleVoiceSearch(MapActivity.this);
						}
					}
				}

			}
		});
	}

	private void locationInitilizer() {

		if (servicesConnected()) {
			LocationManager locationManager = (LocationManager) this
					.getSystemService(Context.LOCATION_SERVICE);
			if (locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
					|| locationManager
							.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				mLocationRequest = LocationRequest.create();
				mLocationRequest
						.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
				mLocationRequest.setInterval(1000);
				mLocationRequest.setFastestInterval(500);
				mLocationClient = new LocationClient(this, this, this);
				mLocationClient.connect();

			} else {
				showSettingsAlert();
			}

		}

	}

	protected void startRecognition(Activity mainActivity, int RequestCode) {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
		startActivityForResult(intent, RequestCode);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case REQUEST_AUDIO_TO_TEXT_LOCATION_NAME:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				locationname.setText(text.get(0));
			}
			break;
		case REQUEST_AUDIO_TO_TEXT_LOCATION_DETIAL:
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				locationdetail.setText(text.get(0));
			}
			break;
		}
	}

	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

		// Setting Dialog Title
		alertDialog.setTitle("GPS is settings");

		// Setting Dialog Message
		alertDialog
				.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(intent);
					}
				});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	/**
	 * Verify that Google Play services is available before making a request.
	 * 
	 * @return true if Google Play services is available, otherwise false
	 */
	private boolean servicesConnected() {

		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status

			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {
			// Display an error dialog
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					this, 0);
			if (dialog != null) {
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(dialog);
				errorFragment.show(getSupportFragmentManager(), TAG);
			}
			return false;
		}
	}

	/**
	 * Show a dialog returned by Google Play services for the connection error
	 * code
	 * 
	 * @param errorCode
	 *            An error code returned from onConnectionFailed
	 */
	private void showErrorDialog(int errorCode) {

		// Get the error dialog from Google Play services
		Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode,
				this, 9000);

		// If Google Play services can provide an error dialog
		if (errorDialog != null) {

			// Create a new DialogFragment in which to show the error dialog
			ErrorDialogFragment errorFragment = new ErrorDialogFragment();

			// Set the dialog in the DialogFragment
			errorFragment.setDialog(errorDialog);

			// Show the error dialog in the DialogFragment
			errorFragment.show(getSupportFragmentManager(), TAG);
		}
	}

	/**
	 * Define a DialogFragment to display the error dialog generated in
	 * showErrorDialog.
	 */
	public static class ErrorDialogFragment extends DialogFragment {

		// Global field to contain the error dialog
		private Dialog mDialog;

		/**
		 * Default constructor. Sets the dialog field to null
		 */
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}

		/**
		 * Set the dialog to display
		 * 
		 * @param dialog
		 *            An error dialog
		 */
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}

		/*
		 * This method must return a Dialog to the DialogFragment.
		 */
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return mDialog;
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		/*
		 * Google Play services can resolve some errors it detects. If the error
		 * has a resolution, try sending an Intent to start a Google Play
		 * services activity that can resolve error.
		 */

		if (connectionResult.hasResolution()) {
			try {

				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(this, 9000);

				/*
				 * Thrown if Google Play services canceled the original
				 * PendingIntent
				 */

			} catch (IntentSender.SendIntentException e) {

				// Log the error
				e.printStackTrace();
			}
		} else {

			// If no resolution is available, display a dialog to the user with
			// the error.
			showErrorDialog(connectionResult.getErrorCode());
		}

	}

	@Override
	public void onConnected(Bundle arg0) {
		latLng = getCurrentPosition();
		if (latLng != null) {
			// Move the camera instantly to hamburg with a zoom of 15.
			map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

			// Zoom in, animating the camera.
			map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
			relContainer.setVisibility(View.VISIBLE);

		}
	}

	@Override
	public void onDisconnected() {

	}

	@Override
	public void onLocationChanged(Location arg0) {
		double latitude = arg0.getLatitude();
		double longitude = arg0.getLongitude();
		LatLng latLng = new LatLng(latitude, longitude);
		map.addMarker(new MarkerOptions().position(latLng));
		map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
		map.animateCamera(CameraUpdateFactory.zoomTo(15));
	}

	private LatLng getCurrentPosition() {
		LatLng latLng = null;
		Location loc = getLocation();
		if (loc != null) {
			latLng = new LatLng(loc.getLatitude(), loc.getLongitude()); // vancouver
																		// location
		}
		Log.e(TAG, "Latitude:" + latLng.latitude + " longitude:"
				+ latLng.longitude);

		lat.setText(Double.toString(latLng.latitude));
		longitu.setText(Double.toString(latLng.longitude));
		isInternetPresent = internetcheck.isConnectingToInternet();
		map.addMarker(new MarkerOptions().position(latLng));

		// map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
		// map.animateCamera(CameraUpdateFactory.zoomTo(15));

		if (isInternetPresent) {
			new AsyncdistanceTask().execute(packageid, UserId,
					Double.toString(latLng.latitude),
					Double.toString(latLng.longitude));

			Helper.ToastUi.print(this, "Latitude:" + latLng.latitude
					+ " longitude:" + latLng.longitude);

		} else {

		}
		// latLng = new LatLng(49.2500, -123.1000);
		// LatLng latLng = new LatLng(latitude, longitude);

		return latLng;
	}

	public class AsyncdistanceTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(MapActivity.this);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				JSONObject jObj = new JSONObject(result);

				if (jObj.getBoolean("success")) {
					Log.e("Response", result);
					String Server_message = jObj.getString("distance");
					distance.setText(Server_message);
					ViewUtil.hideProgressDialog();

				} else {
					Log.e("Response", result);

					String Server_message = jObj.getString("message");

					ViewUtil.hideProgressDialog();

					ViewUtil.showAlertDialog(MapActivity.this,
							"SomeThing Wrong", Server_message, true);

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("PackageId", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("Lat", String
					.valueOf(params[2])));

			parameter.add(new BasicNameValuePair("Long", String
					.valueOf(params[3])));
			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.Distance(parameter);
			return result;
		}

	}

	public Location getLocation() {
		Location currentLocation = null;
		// If Google Play Services is available
		// Get the current location
		if (mLocationClient.isConnected()) {
			currentLocation = mLocationClient.getLastLocation();
		} else {
			Helper.ToastUi.print(this, "Enable To connect");
		}

		// Display the current location in the UI
		// mLatLng.setText(LocationUtils.getLatLng(this, currentLocation));

		return currentLocation;
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.et_loc_name:
			typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE, "");
			// Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				locationname.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(MapActivity.this);
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(MapActivity.this) == true) {
					// if yes � running recognition
					locationname.setInputType(InputType.TYPE_NULL);
					startRecognition(MapActivity.this,
							REQUEST_AUDIO_TO_TEXT_LOCATION_NAME);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							MapActivity.this,
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(MapActivity.this);
				}
			} else {
				locationname.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;
		case R.id.et_loc_detail:
			typingvalue = shared.getString(AppConstant.KEY_ENTRYTYPE, "");
			// Log.e("typingvalue", "" + typingvalue);
			if (typingvalue.equals("2")) {
				locationdetail.setInputType(InputType.TYPE_NULL);
				ChoosePopup.openTypeModePopWindow(MapActivity.this);
			} else if (typingvalue.equals("1")) {
				if (ChoosePopup
						.isSpeechRecognitionActivityPresented(MapActivity.this) == true) {
					// if yes � running recognition
					locationdetail.setInputType(InputType.TYPE_NULL);
					startRecognition(MapActivity.this,
							REQUEST_AUDIO_TO_TEXT_LOCATION_DETIAL);
				} else {
					// if no, then showing notification to install Voice
					// Search
					Toast.makeText(
							MapActivity.this,
							"In order to activate speech recognition you must install Google Voice Search",
							Toast.LENGTH_LONG).show();
					// start installing process
					ChoosePopup.installGoogleVoiceSearch(MapActivity.this);
				}
			} else {
				locationdetail.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;
		case R.id.img_btn_pin:
			relContainer.setVisibility(View.GONE);
			// Intent intent = new Intent(MapActivity.this, MapView.class);
			// startActivity(intent);
			break;
		case R.id.img_btn_cancel:
			// relContainer.setVisibility(View.GONE);
			finish();
			break;
		case R.id.img_btn_share:
			Intent sharingIntent = new Intent(
					android.content.Intent.ACTION_SEND);
			sharingIntent.setType("text/plain");
			sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Sharing");
			sharingIntent
					.putExtra(Intent.EXTRA_TEXT, "LocationName :  "
							+ mlocationname + "" + "LocationDetail :"
							+ mlocationdetail);
			startActivity(Intent.createChooser(sharingIntent, "SHARE"));
			break;
		case R.id.img_btn_save:
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					MapActivity.this);
			alertDialogBuilder.setTitle("ALERT");

			alertDialogBuilder
					.setMessage("Are you sure for save")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									mlocationname = locationname.getText()
											.toString();
									AppConstant.BACKHANDLE = "1";
									name = locationname.getText().toString();
									mlocationdetail = locationdetail.getText()
											.toString();
									mdistance = distance.getText().toString();

									new AsynccurrentLocationTask().execute(
											packageid,
											Double.toString(latLng.latitude),
											Double.toString(latLng.longitude),
											UserId, packagedataid, "gps",
											mlocationname, mlocationdetail,
											mdistance);

								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									dialog.cancel();
								}
							});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

			break;
		default:
			break;
		}

	}

	public class AsynccurrentLocationTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(MapActivity.this);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				JSONObject jObj = new JSONObject(result);

				if (jObj.getBoolean("success")) {
					Log.e("Response", result);
					ViewUtil.hideProgressDialog();
					finish();
				} else {
					Log.e("Response", result);

					String Server_message = jObj.getString("distance");

					ViewUtil.hideProgressDialog();
					/*
					 * ViewUtil.showAlertDialog(this, "Login Failed",
					 * Server_message, true);
					 */

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("PackageId", String
					.valueOf(params[0])));

			parameter.add(new BasicNameValuePair("Lat", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("Long", String
					.valueOf(params[2])));

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[3])));
			parameter.add(new BasicNameValuePair("PackageDataId", String
					.valueOf(params[4])));
			parameter.add(new BasicNameValuePair("Type", String
					.valueOf(params[5])));
			parameter.add(new BasicNameValuePair("LocationName", String
					.valueOf(params[6])));
			parameter.add(new BasicNameValuePair("Details", String
					.valueOf(params[7])));
			parameter.add(new BasicNameValuePair("DistanceFromCurrentLocation",
					String.valueOf(params[8])));
			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.PackageLocation(parameter);
			return result;
		}

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}
}
