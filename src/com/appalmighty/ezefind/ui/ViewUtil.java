package com.appalmighty.ezefind.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.appalmighty.ezefind.R;

public class ViewUtil {
	Context context;

	public static void Toast(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	static ProgressDialog pDialog;

	public static void showProgressDialog(Context context) {
		if (pDialog == null) {
			pDialog = new ProgressDialog(context, R.style.progress_dialog);
			pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
			pDialog.setCancelable(false);
			pDialog.show();
		}
	}

	public static void hideProgressDialog() {
		if (pDialog != null && pDialog.isShowing()) {
			pDialog.dismiss();
			pDialog = null;
		}
	}

	/**
	 * Function to display simple Alert Dialog
	 * 
	 * @param context
	 *            - application context
	 * @param title
	 *            - alert dialog title
	 * @param message
	 *            - alert message
	 * @param status
	 *            - success/failure (used to set icon)
	 * */
	public static void showAlertDialog(FragmentActivity activity, String title,
			String message, boolean error) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				activity);

		// set title
		alertDialogBuilder.setTitle(title);
		if (error) {
			alertDialogBuilder.setIcon(R.drawable.fail);
		}

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						dialog.cancel();

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

}
