package com.appalmighty.ezefind;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.appalmighty.ezefind.util.AppConstant;
import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {
	SharedPreferences mSharedPreferences;
	String mString;
	Editor editor;

	public GCMIntentService() {
		super(AppConstant.GCM_PROJECT_NUM);
	}

	/**
	 * Method called on device registered
	 **/

	protected void onRegistered(Context context, String registrationId) {
		// TODO Auto-generated method stub

		mSharedPreferences = this.getSharedPreferences(AppConstant.KEY_GCM,
				Context.MODE_PRIVATE);

		Log.e("registrationId", "" + registrationId);
		editor = mSharedPreferences.edit();
		editor.putString(AppConstant.GCM_ID, registrationId);
		editor.commit();

	}

	/**
	 * Method called on device un registred
	 * */

	protected void onUnregistered(Context context, String registrationId) {
		// TODO Auto-generated method stub

	}

	/**
	 * Method called on Receiving a new message
	 * */
	protected void onMessage(Context context, Intent intent) {
		// TODO Auto-generated method stub

		mString = intent.getExtras().getString("message");
		Log.e("mString: ", "" + mString);
		generateNotification_chat_message(context, mString);
	}

	/**
	 * Method called on Error
	 * */

	protected void onError(Context context, String errorId) {
		// TODO Auto-generated method stub
		Log.e(TAG, "Received error: " + errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message
	 * Chat side.
	 */

	// Chat notification coming from Chat screen
	private static void generateNotification_chat_message(Context context,
			String message) {
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		String title = context.getString(R.string.app_name);

		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);
		Intent notificationIntent = new Intent(context, SplashActivity.class);

		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		// Play default notification sound
		notification.defaults |= Notification.DEFAULT_SOUND;

		// notification.sound = Uri.parse("android.resource://" +
		// context.getPackageName() + "your_sound_file_name.mp3");

		// Vibrate if vibrate is enabled
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notificationManager.notify((int) System.currentTimeMillis(),
				notification);

	}

}
