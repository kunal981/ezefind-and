package com.appalmighty.ezefind.report.fgmt;

import java.text.DecimalFormat;

import com.github.mikephil.charting.utils.ValueFormatter;

public class MyValueFormatter implements ValueFormatter {

	private DecimalFormat mFormat;

	public MyValueFormatter() {
		mFormat = new DecimalFormat("###,###,###");
	}

	@Override
	public String getFormattedValue(float value) {
		return mFormat.format(value);
	}

}
