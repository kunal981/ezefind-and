package com.appalmighty.ezefind.report.fgmt;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.filter.Approximator;
import com.github.mikephil.charting.data.filter.Approximator.ApproximatorType;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ValueFormatter;

public class ReportDateFragment extends Fragment implements OnClickListener,
		OnChartValueSelectedListener {
	static RelativeLayout filterdialog;
	ImageView filtering, cancel;
	CheckBox chkpackage, chkinventory, chkboth;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	String filtertype = "package", filterby = "day", startdate, Enddate = "",
			Selctedmonth;
	Editor editor;
	SharedPreferences shared;
	String outputDateStr, outputDateyear;
	public GregorianCalendar cal_month, cal_month_copy;
	private CalendarAdapter cal_adapter;
	private TextView tv_month, tv_total, view;
	Button btnday, btnmonth, btnyear;
	protected BarChart mChart;
	ArrayList<String> allkeys;
	private ArrayList<String> count;
	ArrayList<String> allvalues;
	TableRow week;
	GridView gridview;
	LinearLayout main;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_view_report_date,
				container, false);
		mChart = (BarChart) rootView.findViewById(R.id.chart1);
		mChart.setOnChartValueSelectedListener(this);

		week = (TableRow) rootView.findViewById(R.id.tableRow1);
		view = (TextView) rootView.findViewById(R.id.textView2);
		main = (LinearLayout) rootView.findViewById(R.id.inner_column_1);
		filtering = (ImageView) rootView.findViewById(R.id.id_report_filtering);
		chkpackage = (CheckBox) rootView.findViewById(R.id.checkbox_package1);
		chkinventory = (CheckBox) rootView
				.findViewById(R.id.checkbox_inventory1);
		chkboth = (CheckBox) rootView.findViewById(R.id.checkbox_both1);
		cancel = (ImageView) rootView.findViewById(R.id.btnDone);
		filterdialog = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter_all);
		cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
		cal_month_copy = (GregorianCalendar) cal_month.clone();
		cal_adapter = new CalendarAdapter(getActivity(), cal_month,
				CalendarCollection.date_collection_arr);

		tv_month = (TextView) rootView.findViewById(R.id.tv_month);
		tv_total = (TextView) rootView.findViewById(R.id.txt_week_val);
		tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy",
				cal_month));
		btnday = (Button) rootView.findViewById(R.id.day);
		btnmonth = (Button) rootView.findViewById(R.id.month);
		btnyear = (Button) rootView.findViewById(R.id.year);

		btnday.setBackgroundColor(Color.rgb(255, 140, 0));
		ImageButton previous = (ImageButton) rootView
				.findViewById(R.id.ib_prev);
		allkeys = new ArrayList<String>();
		allvalues = new ArrayList<String>();
		count = new ArrayList<String>();
		internetcheck = new ConnectionDetector(getActivity());
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date d = new Date();
		startdate = sdf.format(d);

		isInternetPresent = internetcheck.isConnectingToInternet();
		if (isInternetPresent) {
			new AsyncGetDataTask().execute(filtertype, filterby, startdate,
					Enddate);
		} else {
			ViewUtil.showAlertDialog(getActivity(), "ERROR",
					"No Internet Connection", false);
		}
		filtertype = shared.getString(AppConstant.KEY_DEFAULT_Filter, "");
		if (filtertype.equals("package")) {
			chkpackage.setChecked(true);
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
		} else if (filtertype.equals("inventory")) {
			chkpackage.setChecked(false);
			chkinventory.setChecked(true);
			chkboth.setChecked(false);
		} else {
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			chkboth.setChecked(true);
		}

		chkpackage.setOnClickListener(this);
		chkinventory.setOnClickListener(this);
		chkboth.setOnClickListener(this);

		filtering.setOnClickListener(this);
		cancel.setOnClickListener(this);
		btnday.setOnClickListener(this);
		btnmonth.setOnClickListener(this);
		btnyear.setOnClickListener(this);

		previous.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setPreviousMonth();
				refreshCalendar();
				count.clear();

			}
		});

		ImageButton next = (ImageButton) rootView.findViewById(R.id.Ib_next);
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setNextMonth();
				refreshCalendar();
				count.clear();
			}
		});

		gridview = (GridView) rootView.findViewById(R.id.gv_calendar);
		gridview.setAdapter(cal_adapter);

		return rootView;
	}

	public static Fragment newInstance() {
		ReportDateFragment fragment = new ReportDateFragment();
		return fragment;
	}

	public static void showdialog() {
		filterdialog.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.id_report_filtering:
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {
				if (filterby.equals("day")) {
					new AsyncGetDataTask().execute(filtertype, filterby,
							startdate, Enddate);
				} else if (filterby.equals("month")) {
					new AsyncGetDataTask().execute(filtertype, filterby,
							outputDateStr);
				} else if (filterby.equals("year")) {
					new AsyncGetDataTask().execute(filtertype, filterby,
							outputDateyear);
				}

			} else {
				ViewUtil.showAlertDialog(getActivity(), "ERROR",
						"No Internet Connection", false);
			}
			break;
		case R.id.checkbox_package1:
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "package";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.checkbox_inventory1:

			chkpackage.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "inventory";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();

			break;
		case R.id.checkbox_both1:
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			filtertype = "both";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.btnDone:
			filterdialog.setVisibility(View.GONE);
			break;
		case R.id.day:
			btnday.setBackgroundColor(Color.rgb(255, 140, 0));
			btnmonth.setBackgroundColor(Color.LTGRAY);
			btnyear.setBackgroundColor(Color.LTGRAY);
			filterby = "day";
			view.setVisibility(View.VISIBLE);
			week.setVisibility(View.VISIBLE);
			gridview.setVisibility(View.VISIBLE);
			main.setBackgroundColor(Color.BLACK);
			refreshCalendar();
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {

				new AsyncGetDataTask().execute(filtertype, filterby, startdate,
						Enddate);
			} else {
				ViewUtil.showAlertDialog(getActivity(), "ERROR",
						"No Internet Connection", false);
			}
			break;
		case R.id.month:
			btnmonth.setBackgroundColor(Color.rgb(255, 140, 0));
			btnday.setBackgroundColor(Color.LTGRAY);
			btnyear.setBackgroundColor(Color.LTGRAY);
			filterby = "month";
			view.setVisibility(View.INVISIBLE);
			week.setVisibility(View.INVISIBLE);
			gridview.setVisibility(View.INVISIBLE);
			main.setBackgroundColor(Color.WHITE);
			refreshCalendar();
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {
				new AsyncGetDataTask().execute(filtertype, filterby,
						outputDateStr);
			} else {
				ViewUtil.showAlertDialog(getActivity(), "ERROR",
						"No Internet Connection", false);
			}

			break;
		case R.id.year:
			btnyear.setBackgroundColor(Color.rgb(255, 140, 0));
			btnmonth.setBackgroundColor(Color.LTGRAY);
			btnday.setBackgroundColor(Color.LTGRAY);
			view.setVisibility(View.INVISIBLE);
			week.setVisibility(View.INVISIBLE);
			gridview.setVisibility(View.INVISIBLE);
			main.setBackgroundColor(Color.WHITE);
			filterby = "year";
			refreshCalendar();
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {
				new AsyncGetDataTask().execute(filtertype, filterby,
						outputDateyear);
			} else {
				ViewUtil.showAlertDialog(getActivity(), "ERROR",
						"No Internet Connection", false);
			}

			break;

		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

		inflater.inflate(R.menu.bar, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.actionToggleValues: {

			for (DataSet<?> set : mChart.getData().getDataSets())
				set.setDrawValues(!set.isDrawValuesEnabled());

			mChart.invalidate();
			break;
		}
		case R.id.actionToggleHighlight: {

			if (mChart.isHighlightEnabled())
				mChart.setHighlightEnabled(false);
			else
				mChart.setHighlightEnabled(true);
			mChart.invalidate();
			break;
		}
		case R.id.actionTogglePinch: {

			if (mChart.isPinchZoomEnabled())
				mChart.setPinchZoom(false);
			else
				mChart.setPinchZoom(true);

			mChart.invalidate();
			break;
		}
		case R.id.actionToggleAutoScaleMinMax: {

			mChart.setAutoScaleMinMaxEnabled(!mChart.isAutoScaleMinMaxEnabled());
			mChart.notifyDataSetChanged();
			break;
		}
		case R.id.actionToggleHighlightArrow: {

			if (mChart.isDrawHighlightArrowEnabled())
				mChart.setDrawHighlightArrow(false);
			else
				mChart.setDrawHighlightArrow(true);
			mChart.invalidate();
			break;
		}
		case R.id.actionToggleStartzero: {

			mChart.getAxisLeft().setStartAtZero(
					!mChart.getAxisLeft().isStartAtZeroEnabled());
			mChart.getAxisRight().setStartAtZero(
					!mChart.getAxisRight().isStartAtZeroEnabled());
			mChart.notifyDataSetChanged();
			mChart.invalidate();
			break;
		}
		case R.id.animateX: {

			mChart.animateX(3000);
			break;
		}
		case R.id.animateY: {

			mChart.animateY(3000);
			break;
		}
		case R.id.animateXY: {

			mChart.animateXY(3000, 3000);
			break;
		}
		case R.id.actionToggleFilter: {

			Approximator a = new Approximator(ApproximatorType.DOUGLAS_PEUCKER,
					25);

			if (!mChart.isFilteringEnabled()) {
				mChart.enableFiltering(a);
			} else {
				mChart.disableFiltering();
			}
			mChart.invalidate();
			break;
		}
		case R.id.actionSave: {

			if (mChart.saveToGallery("title" + System.currentTimeMillis(), 50)) {
				Toast.makeText(getActivity(), "Saving SUCCESSFUL!",
						Toast.LENGTH_SHORT).show();
			} else
				Toast.makeText(getActivity(), "Saving FAILED!",
						Toast.LENGTH_SHORT).show();
			break;
		}
		}
		return true;
	}

	public class AsyncGetDataTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				ViewUtil.showProgressDialog(getActivity());
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				allkeys.clear();
				allvalues.clear();
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						if (result != null) {
							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("Response date", result);
								responsDate(result);

								filterdialog.setVisibility(View.GONE);
								ViewUtil.hideProgressDialog();
							}

							else {
								Log.e("Response", result);

								// String Server_message =
								// jObj.getString("message");
								filterdialog.setVisibility(View.GONE);
								ViewUtil.hideProgressDialog();
								// ViewUtil.showAlertDialog(getActivity(),
								// "Error",
								// Server_message, true);

							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("main_filter", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("Filter_by", String
					.valueOf(params[1])));
			if (filterby.equals("day")) {
				parameter.add(new BasicNameValuePair("StartDate", String
						.valueOf(params[2])));
				parameter.add(new BasicNameValuePair("EndDate", String
						.valueOf(params[3])));
			} else if (filterby.equals("month")) {
				parameter.add(new BasicNameValuePair("SelectedMonth", String
						.valueOf(params[2])));
			} else if (filterby.equals("year")) {
				parameter.add(new BasicNameValuePair("SelectedYear", String
						.valueOf(params[2])));
			}
			Log.e("parameter date", "" + parameter);
			result = NetworkConnector.GetDateData(parameter);

			return result;
		}

	}

	public void responsDate(String result) {

		try {

			JSONObject jObj = new JSONObject(result);
			JSONObject phone = jObj.getJSONObject("Data");
			tv_total.setText(jObj.getString("Total_Value"));
			Iterator<String> iter = phone.keys();
			while (iter.hasNext()) {
				String key = iter.next();

				allkeys.add(key);
				Object value = phone.get(key);

				allvalues.add(value.toString());
			}
			mChart.removeAllViews();
			mChart.setDrawBarShadow(false);
			mChart.setDrawValueAboveBar(true);

			mChart.setMaxVisibleValueCount(100);

			mChart.setPinchZoom(false);

			mChart.setDescription("");

			mChart.setDrawGridBackground(false);
			// mChart.setDrawBorders(false);

			XAxis xAxis = mChart.getXAxis();
			xAxis.setPosition(XAxisPosition.BOTTOM);
			xAxis.setValues(allkeys);
			xAxis.setDrawGridLines(false);
			xAxis.setDrawLabels(true);

			xAxis.setSpaceBetweenLabels(0);

			ValueFormatter custom = new MyValueFormatter();

			YAxis leftAxis = mChart.getAxisLeft();
			// leftAxis.setTypeface(mTf);
			leftAxis.setLabelCount(5, true);
			leftAxis.setValueFormatter(custom);
			leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
			leftAxis.setSpaceTop(15f);

			YAxis rightAxis = mChart.getAxisRight();
			rightAxis.setDrawGridLines(false);
			// // rightAxis.setTypeface(mTf);
			rightAxis.setLabelCount(0, false);
			rightAxis.setDrawLabels(false);
			// // rightAxis.setValueFormatter(custom);
			// rightAxis.setSpaceTop(0);

			Legend l = mChart.getLegend();
			l.setPosition(LegendPosition.BELOW_CHART_LEFT);

			l.setForm(LegendForm.LINE);
			l.setFormSize(0);
			l.setTextSize(0);
			l.setXEntrySpace(0f);
			setData(allvalues.size(), 100);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void setNextMonth() {
		if (cal_month.get(GregorianCalendar.MONTH) == cal_month
				.getActualMaximum(GregorianCalendar.MONTH)) {
			cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1),
					cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
		} else {
			cal_month.set(GregorianCalendar.MONTH,
					cal_month.get(GregorianCalendar.MONTH) + 1);
		}

	}

	protected void setPreviousMonth() {
		if (cal_month.get(GregorianCalendar.MONTH) == cal_month
				.getActualMinimum(GregorianCalendar.MONTH)) {
			cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1),
					cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
		} else {
			cal_month.set(GregorianCalendar.MONTH,
					cal_month.get(GregorianCalendar.MONTH) - 1);
		}

	}

	public void refreshCalendar() {
		cal_adapter.refreshDays();
		cal_adapter.notifyDataSetChanged();
		tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy",
				cal_month));
		DateFormat inputFormat1 = new SimpleDateFormat("MMMM yyyy");
		DateFormat outputFormat1 = new SimpleDateFormat("yyyy");

		Date date1;
		try {
			date1 = inputFormat1.parse(tv_month.getText().toString());
			outputDateyear = outputFormat1.format(date1);
			Log.e("enhfbb", "" + outputDateyear);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DateFormat inputFormat = new SimpleDateFormat("MMMM yyyy");
		DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");

		Date date;
		try {
			date = inputFormat.parse(tv_month.getText().toString());
			outputDateStr = outputFormat.format(date);
			Log.e("enhfbb", "" + outputDateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// isInternetPresent = internetcheck.isConnectingToInternet();
		// if (isInternetPresent) {
		// if(filterby.equals("year")){
		// new AsyncGetDataTask().execute(filtertype, filterby,
		// outputDateyear);
		// }else{
		// new AsyncGetDataTask().execute(filtertype, filterby,
		// outputDateStr);
		// }
		// } else {
		// ViewUtil.showAlertDialog(getActivity(), "ERROR",
		// "No Internet Connection", false);
		// }

	}

	public class CalendarAdapter extends BaseAdapter {
		private Activity activity;
		private java.util.Calendar month;
		public GregorianCalendar pmonth;
		/**
		 * calendar instance for previous month for getting complete view
		 */
		public GregorianCalendar pmonthmaxset;
		private GregorianCalendar selectedDate;
		int firstDay;
		int maxWeeknumber;
		int maxP;
		int calMaxP;
		int lastWeekDay;
		int leftDays;
		int mnthlength;
		String itemvalue, curentDateString;
		DateFormat df;

		private ArrayList<String> items;
		public List<String> day_string = new ArrayList<String>();
		private View previousView;
		String strdate, enddate;
		public ArrayList<CalendarCollection> date_collection_arr;
		int i = 0;

		public CalendarAdapter(FragmentActivity fragmentActivity,
				GregorianCalendar monthCalendar,
				ArrayList<CalendarCollection> date_collection_arr) {
			this.date_collection_arr = date_collection_arr;

			Locale.setDefault(Locale.US);
			month = monthCalendar;
			selectedDate = (GregorianCalendar) monthCalendar.clone();
			activity = fragmentActivity;
			month.set(GregorianCalendar.DAY_OF_MONTH, 1);

			this.items = new ArrayList<String>();

			df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
			curentDateString = df.format(selectedDate.getTime());
			refreshDays();

		}

		public void setItems(ArrayList<String> items) {
			for (int i = 0; i != items.size(); i++) {
				if (items.get(i).length() == 1) {
					items.set(i, "0" + items.get(i));
				}
			}
			this.items = items;
		}

		public int getCount() {
			return day_string.size();
		}

		public Object getItem(int position) {
			return day_string.get(position);
		}

		public long getItemId(int position) {
			return 0;
		}

		// create a new view for each item referenced by the Adapter
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View v = convertView;
			final TextView dayView;
			final LinearLayout main;
			if (convertView == null) { // if it's not recycled, initialize some
										// attributes
				LayoutInflater vi = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.cal_item, null);

			}

			dayView = (TextView) v.findViewById(R.id.date);
			main = (LinearLayout) v.findViewById(R.id.calenderlayout);
			main.setBackgroundColor(Color.parseColor("#343434"));

			String[] separatedTime = day_string.get(position).split("-");

			String gridvalue = separatedTime[2].replaceFirst("^0*", "");
			if ((Integer.parseInt(gridvalue) > 1) && (position < firstDay)) {
				dayView.setTextColor(Color.GRAY);
				dayView.setClickable(false);
				dayView.setFocusable(false);
			} else if ((Integer.parseInt(gridvalue) < 7) && (position > 28)) {
				dayView.setTextColor(Color.GRAY);
				dayView.setClickable(false);
				dayView.setFocusable(false);
			} else {
				// setting curent month's days in blue color.
				dayView.setTextColor(Color.WHITE);
			}

			if (day_string.get(position).equals(curentDateString)) {

				// v.setBackgroundColor(Color.CYAN);
			} else {
				Log.e("", "");
				v.setBackgroundColor(Color.parseColor("#343434"));
			}

			dayView.setText(gridvalue);
			dayView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if (count.size() < 2) {
						count.add(day_string.get(position));

						if (count.size() == 1) {

							startdate = day_string.get(position);

							main.setBackgroundColor(Color.CYAN);
							isInternetPresent = internetcheck
									.isConnectingToInternet();
							if (isInternetPresent) {

								new AsyncGetDataTask().execute(filtertype,
										filterby, startdate, Enddate);

							} else {
								ViewUtil.showAlertDialog(getActivity(),
										"ERROR", "No Internet Connection",
										false);
							}
						} else if (count.size() == 2) {
							if (startdate.equals(day_string.get(position))) {

							} else if (Integer.parseInt(startdate.replace("-",
									"")) > Integer.parseInt(day_string.get(
									position).replace("-", ""))) {
								Enddate = startdate;
								startdate = day_string.get(position);
							} else {
								Enddate = day_string.get(position);
							}
							main.setBackgroundColor(Color.CYAN);
							isInternetPresent = internetcheck
									.isConnectingToInternet();
							if (isInternetPresent) {

								new AsyncGetDataTask().execute(filtertype,
										filterby, startdate, Enddate);
								startdate = "";
								Enddate = "";
							} else {
								ViewUtil.showAlertDialog(getActivity(),
										"ERROR", "No Internet Connection",
										false);
							}
						}

					} else {

						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"For Select New Date Please Change Month ",
								false);

					}
				}

			});
			// create date string for comparison

			// updateUI method related to a Runnable

			String date = day_string.get(position);

			if (date.length() == 1) {
				date = "0" + date;
			}
			String monthStr = "" + (month.get(GregorianCalendar.MONTH) + 1);
			if (monthStr.length() == 1) {
				monthStr = "0" + monthStr;
			}

			// show icon if date is not empty and it exists in the items array
			/*
			 * ImageView iw = (ImageView) v.findViewById(R.id.date_icon); if
			 * (date.length() > 0 && items != null && items.contains(date)) {
			 * iw.setVisibility(View.VISIBLE); } else {
			 * iw.setVisibility(View.GONE); }
			 */

			// setEventView(v, position, dayView);

			return v;
		}

		public View setSelected(View view, int pos) {
			if (previousView != null) {
				previousView.setBackgroundColor(Color.parseColor("#343434"));
			}

			view.setBackgroundColor(Color.CYAN);

			int len = day_string.size();
			if (len > pos) {
				if (day_string.get(pos).equals(curentDateString)) {

				} else {
					previousView = view;

				}

			}

			return view;
		}

		public void refreshDays() {
			// clear items
			items.clear();
			day_string.clear();
			Locale.setDefault(Locale.US);
			pmonth = (GregorianCalendar) month.clone();
			// month start day. ie; sun, mon, etc
			firstDay = month.get(GregorianCalendar.DAY_OF_WEEK);
			// finding number of weeks in current month.
			maxWeeknumber = month
					.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH);
			// allocating maximum row number for the gridview.
			mnthlength = maxWeeknumber * 7;
			maxP = getMaxP(); // previous month maximum day 31,30....
			calMaxP = maxP - (firstDay - 1);// calendar offday starting 24,25
											// ...
			/**
			 * Calendar instance for getting a complete gridview including the
			 * three month's (previous,current,next) dates.
			 */
			pmonthmaxset = (GregorianCalendar) pmonth.clone();
			/**
			 * setting the start date as previous month's required date.
			 */
			pmonthmaxset.set(GregorianCalendar.DAY_OF_MONTH, calMaxP + 1);

			/**
			 * filling calendar gridview.
			 */
			for (int n = 0; n < mnthlength; n++) {

				itemvalue = df.format(pmonthmaxset.getTime());
				pmonthmaxset.add(GregorianCalendar.DATE, 1);
				day_string.add(itemvalue);

			}
		}

		private int getMaxP() {
			int maxP;
			if (month.get(GregorianCalendar.MONTH) == month
					.getActualMinimum(GregorianCalendar.MONTH)) {
				pmonth.set((month.get(GregorianCalendar.YEAR) - 1),
						month.getActualMaximum(GregorianCalendar.MONTH), 1);
			} else {
				pmonth.set(GregorianCalendar.MONTH,
						month.get(GregorianCalendar.MONTH) - 1);
			}
			maxP = pmonth.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

			return maxP;
		}

		public void setEventView(View v, int pos, TextView txt) {

			int len = CalendarCollection.date_collection_arr.size();
			for (int i = 0; i < len; i++) {
				CalendarCollection cal_obj = CalendarCollection.date_collection_arr
						.get(i);
				String date = cal_obj.date;
				int len1 = day_string.size();
				if (len1 > pos) {

					if (day_string.get(pos).equals(date)) {
						v.setBackgroundColor(Color.parseColor("#343434"));
						v.setBackgroundResource(R.drawable.rounded_calender_item);

						txt.setTextColor(Color.WHITE);
					}
				}
			}

		}

		public void getPositionList(String date, final Activity act) {

			int len = CalendarCollection.date_collection_arr.size();
			for (int i = 0; i < len; i++) {
				CalendarCollection cal_collection = CalendarCollection.date_collection_arr
						.get(i);
				String event_date = cal_collection.date;

				String event_message = cal_collection.event_message;

				if (date.equals(event_date)) {

					Toast.makeText(activity,
							"You have event on this date: " + event_date,
							Toast.LENGTH_LONG).show();
					new AlertDialog.Builder(activity)
							.setIcon(android.R.drawable.ic_dialog_alert)
							.setTitle("Date: " + event_date)
							.setMessage("Event: " + event_message)
							.setPositiveButton(
									"OK",
									new android.content.DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int which) {
											act.finish();
										}
									}).show();
					break;
				} else {

				}
			}

		}

	}

	private void setData(int count, float range) {

		ArrayList<String> xVals = new ArrayList<String>();
		for (int i = 0; i < count; i++) {
			xVals.add(allkeys.get(i));
		}

		ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

		for (int i = 0; i < count; i++) {

			yVals1.add(new BarEntry(Integer.parseInt(allvalues.get(i)), i));
		}

		BarDataSet set1 = new BarDataSet(yVals1, "");
		set1.setBarSpacePercent(35f);

		ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
		dataSets.add(set1);

		BarData data = new BarData(xVals, dataSets);
		// data.setValueFormatter(new MyValueFormatter());
		data.setValueTextSize(10f);
		// data.setValueTypeface(mTf);
		mChart.setHorizontalScrollBarEnabled(true);
		mChart.setData(data);
	}

	@Override
	public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

		if (e == null)
			return;

		RectF bounds = mChart.getBarBounds((BarEntry) e);
		PointF position = mChart.getPosition(e, AxisDependency.LEFT);

	}

	public void onNothingSelected() {
	};
}
