package com.appalmighty.ezefind.report.fgmt;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

public class ReportItemFragment extends Fragment implements OnClickListener {
	TextView bagcount, boxcount, singleitemcount, bincount, date, totalofall,
			selectedvalue;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	// DatasourceHandler DATABASE;
	String UserId;
	SharedPreferences shared;
	Button bagway, boxway, binway, singleitemway;
	static RelativeLayout filterdialog, filteritemdialog, baglayout, boxlayout,
			singleitemlayout, binlayout;
	ImageView filtering, filteringitem, cancel, cancelitemdialog, bagfilter,
			boxfilter, singleitemfilter, binfilter;
	CheckBox chkpackage, chkinventory, chkboth, chkyear, chkmonth, chkweek;
	String filtertype = "both";
	String filteritem, filteritemtype, checkpoint;
	GraphView graph;
	BarGraphSeries<DataPoint> series;
	StaticLabelsFormatter staticLabelsFormatter;
	ArrayList<String> year;
	ArrayList<String> yearvalue;
	Editor editor;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_view_report_item,
				container, false);
		// DATABASE = new DatasourceHandler(getActivity());
		graph = (GraphView) rootView.findViewById(R.id.graph);
		totalofall = (TextView) rootView.findViewById(R.id.txt_week_val);
		selectedvalue = (TextView) rootView.findViewById(R.id.txt_day_val);
		date = (TextView) rootView.findViewById(R.id.txt_label_date);
		bagcount = (TextView) rootView.findViewById(R.id.txt_count_item_bag);
		boxcount = (TextView) rootView.findViewById(R.id.txt_count_item_box);
		singleitemcount = (TextView) rootView
				.findViewById(R.id.txt_count_item_single_item);
		bincount = (TextView) rootView.findViewById(R.id.txt_count_item_bin);

		bagfilter = (ImageView) rootView.findViewById(R.id.img_);
		boxfilter = (ImageView) rootView.findViewById(R.id.img_2);
		singleitemfilter = (ImageView) rootView.findViewById(R.id.img_4);
		binfilter = (ImageView) rootView.findViewById(R.id.img_3);
		filterdialog = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter_all);
		filtering = (ImageView) rootView.findViewById(R.id.id_report_filtering);
		cancel = (ImageView) rootView.findViewById(R.id.btnDone);
		chkpackage = (CheckBox) rootView.findViewById(R.id.checkbox_package1);
		chkinventory = (CheckBox) rootView
				.findViewById(R.id.checkbox_inventory1);
		chkboth = (CheckBox) rootView.findViewById(R.id.checkbox_both1);
		chkyear = (CheckBox) rootView.findViewById(R.id.checkbox_year);
		chkmonth = (CheckBox) rootView.findViewById(R.id.checkbox_month);
		chkweek = (CheckBox) rootView.findViewById(R.id.checkbox_week);
		cancelitemdialog = (ImageView) rootView.findViewById(R.id.btnDo);
		filteritemdialog = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter_item);
		baglayout = (RelativeLayout) rootView.findViewById(R.id.item_row_1);
		boxlayout = (RelativeLayout) rootView.findViewById(R.id.item_row_2);
		singleitemlayout = (RelativeLayout) rootView
				.findViewById(R.id.item_row_4);
		binlayout = (RelativeLayout) rootView.findViewById(R.id.item_row_3);
		bagway = (Button) rootView.findViewById(R.id.button_week);
		boxway = (Button) rootView.findViewById(R.id.button_week_2);
		binway = (Button) rootView.findViewById(R.id.button_week_3);
		singleitemway = (Button) rootView.findViewById(R.id.button_week_4);
		filteringitem = (ImageView) rootView
				.findViewById(R.id.id_item_filtering);
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = shared.getString(AppConstant.KEY_USER_ID, "");
		filtertype = shared.getString(AppConstant.KEY_DEFAULT_Filter, "");
		if (filtertype.equals("package")) {
			chkpackage.setChecked(true);
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
		} else if (filtertype.equals("inventory")) {
			chkpackage.setChecked(false);
			chkinventory.setChecked(true);
			chkboth.setChecked(false);
		} else {
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			chkboth.setChecked(true);
		}
		internetcheck = new ConnectionDetector(getActivity());
		isInternetPresent = internetcheck.isConnectingToInternet();
		if (isInternetPresent) {
			new AsyncGetItemDataTask().execute(filtertype);
		} else {
			ViewUtil.showAlertDialog(getActivity(), "ERROR",
					"No Internet Connection", false);
		}

		SimpleDateFormat sdf = new SimpleDateFormat("EEE dd/MM/yyyy");
		Date d = new Date();
		String dayOfTheWeek = sdf.format(d);
		date.setText(dayOfTheWeek);

		// series.setValuesOnTopSize(50);
		chkpackage.setOnClickListener(this);
		chkinventory.setOnClickListener(this);
		chkboth.setOnClickListener(this);
		chkyear.setOnClickListener(this);
		chkmonth.setOnClickListener(this);
		chkweek.setOnClickListener(this);
		filtering.setOnClickListener(this);
		cancel.setOnClickListener(this);
		cancelitemdialog.setOnClickListener(this);
		filteringitem.setOnClickListener(this);
		bagfilter.setOnClickListener(this);
		boxfilter.setOnClickListener(this);
		singleitemfilter.setOnClickListener(this);
		binfilter.setOnClickListener(this);
		baglayout.setOnClickListener(this);
		boxlayout.setOnClickListener(this);
		singleitemlayout.setOnClickListener(this);
		binlayout.setOnClickListener(this);
		return rootView;
	}

	public static Fragment newInstance() {
		ReportItemFragment fragment = new ReportItemFragment();
		return fragment;
	}

	public static void showdialog() {
		filterdialog.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.id_report_filtering:
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {

				new AsyncGetItemDataTask().execute(filtertype);

			} else {
				ViewUtil.showAlertDialog(getActivity(), "ERROR",
						"No Internet Connection", false);
			}
			break;
		case R.id.id_item_filtering:
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {
				if (filteritem.equals("week")) {
					Calendar calender = Calendar.getInstance();
					int week = calender.get(Calendar.WEEK_OF_YEAR);
					new AsyncGetItemfilterDataTask().execute(filteritem,
							filteritemtype, String.valueOf(week));
				} else {
					new AsyncGetItemfilterDataTask().execute(filteritem,
							filteritemtype);
				}

			}
			break;
		case R.id.checkbox_package1:
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "package";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.checkbox_inventory1:

			chkpackage.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "inventory";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();

			break;
		case R.id.checkbox_both1:
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			filtertype = "both";
			editor = shared.edit();
			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.checkbox_year:
			chkmonth.setChecked(false);
			chkweek.setChecked(false);
			filteritem = "year";
			break;
		case R.id.checkbox_month:

			chkyear.setChecked(false);
			chkweek.setChecked(false);
			filteritem = "month";

			break;
		case R.id.checkbox_week:
			chkyear.setChecked(false);
			chkmonth.setChecked(false);
			filteritem = "week";

			break;
		case R.id.btnDone:
			filterdialog.setVisibility(View.GONE);
			break;
		case R.id.btnDo:
			filteritemdialog.setVisibility(View.GONE);
			break;
		case R.id.img_:
			filteritemdialog.setVisibility(View.VISIBLE);
			filteritemtype = "bag";
			break;
		case R.id.img_4:
			filteritemdialog.setVisibility(View.VISIBLE);
			filteritemtype = "single item";
			break;
		case R.id.img_2:
			filteritemdialog.setVisibility(View.VISIBLE);
			filteritemtype = "box";
			break;
		case R.id.img_3:
			filteritemdialog.setVisibility(View.VISIBLE);
			filteritemtype = "bin";
			break;
		case R.id.item_row_1:
			baglayout.setBackgroundColor(Color.LTGRAY);
			boxlayout.setBackgroundColor(Color.WHITE);
			binlayout.setBackgroundColor(Color.WHITE);
			singleitemlayout.setBackgroundColor(Color.WHITE);
			selectedvalue.setText(bagcount.getText().toString());
			break;
		case R.id.item_row_2:
			baglayout.setBackgroundColor(Color.WHITE);
			boxlayout.setBackgroundColor(Color.LTGRAY);
			binlayout.setBackgroundColor(Color.WHITE);
			singleitemlayout.setBackgroundColor(Color.WHITE);
			selectedvalue.setText(boxcount.getText().toString());
			break;
		case R.id.item_row_3:
			baglayout.setBackgroundColor(Color.WHITE);
			boxlayout.setBackgroundColor(Color.WHITE);
			binlayout.setBackgroundColor(Color.LTGRAY);
			singleitemlayout.setBackgroundColor(Color.WHITE);
			selectedvalue.setText(bincount.getText().toString());
			break;
		case R.id.item_row_4:
			baglayout.setBackgroundColor(Color.WHITE);
			boxlayout.setBackgroundColor(Color.WHITE);
			binlayout.setBackgroundColor(Color.WHITE);
			singleitemlayout.setBackgroundColor(Color.LTGRAY);
			selectedvalue.setText(singleitemcount.getText().toString());
			break;
		default:
			break;
		}
	}

	public class AsyncGetItemDataTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				ViewUtil.showProgressDialog(getActivity());
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						if (result != null) {
							if (result.equals("error")) {
								ViewUtil.showAlertDialog(getActivity(),
										"ERROR", "SomeThing Wrong", false);
								ViewUtil.hideProgressDialog();
							} else {

								JSONObject jObj = new JSONObject(result);

								if (jObj.getBoolean("success")) {
									Log.e("Response items", result);
									JSONObject phone = jObj
											.getJSONObject("message");
									bagcount.setText(phone.getString("bag"));
									boxcount.setText(phone.getString("box"));
									singleitemcount.setText(phone
											.getString("single item"));
									bincount.setText(phone.getString("bin"));
									totalofall.setText(String.valueOf(Integer
											.parseInt(phone.getString("bag"))
											+ Integer.parseInt(phone
													.getString("box"))
											+ Integer.parseInt(phone
													.getString("single item"))
											+ Integer.parseInt(phone
													.getString("bin"))));
									filterdialog.setVisibility(View.GONE);
									ViewUtil.hideProgressDialog();

									graph.removeAllSeries();
									series = new BarGraphSeries<DataPoint>(
											new DataPoint[] {

													new DataPoint(
															2,
															Integer.parseInt(bagcount
																	.getText()
																	.toString())),
													new DataPoint(
															10,
															Integer.parseInt(boxcount
																	.getText()
																	.toString())),
													new DataPoint(
															20,
															Integer.parseInt(singleitemcount
																	.getText()
																	.toString())),
													new DataPoint(
															30,
															Integer.parseInt(bincount
																	.getText()
																	.toString())), });
									graph.addSeries(series);

									staticLabelsFormatter = new StaticLabelsFormatter(
											graph);

									staticLabelsFormatter
											.setHorizontalLabels(new String[] {
													"Bag", "Box",
													"Item", "Bin", " ",
													" " });

									graph(staticLabelsFormatter);
								}

								else {
									Log.e("Response", result);

									String Server_message = jObj
											.getString("message");
									filterdialog.setVisibility(View.GONE);
									ViewUtil.hideProgressDialog();
									ViewUtil.showAlertDialog(getActivity(),
											"Error", Server_message, true);

								}
							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;
			try {

				List<NameValuePair> parameter = new LinkedList<NameValuePair>();

				parameter.add(new BasicNameValuePair("main_filter", String
						.valueOf(params[0])));

				Log.e("parameter items", "" + parameter);
				result = NetworkConnector.GetItemData(parameter);
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
			return result;
		}

	}

	public class AsyncGetItemfilterDataTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				ViewUtil.showProgressDialog(getActivity());
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result != null) {
						if (result.equals("error")) {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"SomeThing Wrong", false);
							ViewUtil.hideProgressDialog();
						} else {

							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("Response", result);
								JSONObject phone = jObj.getJSONObject("Data");
								if (filteritemtype.equals("bag")
										&& filteritem.equals("month")) {
									bagway.setText("M");
									checkpoint = "0";
									responsedilter(result);
									bagcount.setText(jObj
											.getString("TotalData"));

								} else if (filteritemtype.equals("bag")
										&& filteritem.equals("week")) {
									bagway.setText("W");
									checkpoint = "1";
									responsedilter(result);
									bagcount.setText(jObj
											.getString("TotalData"));
								} else if (filteritemtype.equals("bag")
										&& filteritem.equals("year")) {
									bagway.setText("Y");
									checkpoint = "2";
									responsedilter(result);
									bagcount.setText(jObj
											.getString("TotalData"));
								}

								else if (filteritemtype.equals("box")
										&& filteritem.equals("month")) {
									boxway.setText("M");
									checkpoint = "0";
									responsedilter(result);
									boxcount.setText(jObj
											.getString("TotalData"));
								} else if (filteritemtype.equals("box")
										&& filteritem.equals("week")) {
									boxway.setText("W");
									checkpoint = "1";
									responsedilter(result);
									boxcount.setText(jObj
											.getString("TotalData"));

								} else if (filteritemtype.equals("box")
										&& filteritem.equals("year")) {
									boxway.setText("Y");
									checkpoint = "2";
									responsedilter(result);
									boxcount.setText(jObj
											.getString("TotalData"));
								} else if (filteritemtype.equals("single item")
										&& filteritem.equals("month")) {
									singleitemway.setText("M");
									checkpoint = "0";
									singleitemcount.setText(jObj
											.getString("TotalData"));
								} else if (filteritemtype.equals("single item")
										&& filteritem.equals("week")) {
									singleitemway.setText("W");
									checkpoint = "1";
									responsedilter(result);
									singleitemcount.setText(jObj
											.getString("TotalData"));
								} else if (filteritemtype.equals("single item")
										&& filteritem.equals("year")) {
									singleitemway.setText("Y");
									checkpoint = "2";
									responsedilter(result);
									singleitemcount.setText(jObj
											.getString("TotalData"));
								} else if (filteritemtype.equals("bin")
										&& filteritem.equals("month")) {
									binway.setText("M");
									checkpoint = "0";
									responsedilter(result);
									bincount.setText(jObj
											.getString("TotalData"));
								} else if (filteritemtype.equals("bin")
										&& filteritem.equals("week")) {
									binway.setText("W");
									checkpoint = "1";
									responsedilter(result);
									bincount.setText(jObj
											.getString("TotalData"));
								} else if (filteritemtype.equals("bin")
										&& filteritem.equals("year")) {
									binway.setText("Y");
									checkpoint = "2";
									responsedilter(result);
									bincount.setText(jObj
											.getString("TotalData"));
								}

								filteritemdialog.setVisibility(View.GONE);
								ViewUtil.hideProgressDialog();

							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");
								filteritemdialog.setVisibility(View.GONE);
								ViewUtil.hideProgressDialog();
								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);

							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;
			try {

				List<NameValuePair> parameter = new LinkedList<NameValuePair>();

				parameter.add(new BasicNameValuePair("Filter_by", String
						.valueOf(params[0])));
				parameter.add(new BasicNameValuePair("Type", String
						.valueOf(params[1])));
				if (filteritem.equals("week")) {
					parameter.add(new BasicNameValuePair("weekNo", String
							.valueOf(params[2])));
				}

				Log.e("parameter", "" + parameter);
				result = NetworkConnector.GetItemFilterData(parameter);
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
			return result;
		}

	}

	public void responsedilter(String result) {
		switch (checkpoint) {
		case "0":

			try {
				totalofall
						.setText(String.valueOf(Integer.parseInt(bagcount
								.getText().toString())
								+ Integer.parseInt(boxcount.getText()
										.toString())
								+ Integer.parseInt(bincount.getText()
										.toString())
								+ Integer.parseInt(singleitemcount.getText()
										.toString())));
				JSONObject jObj = new JSONObject(result);
				JSONObject phone = jObj.getJSONObject("Data");

				graph.removeAllSeries();
				series = new BarGraphSeries<DataPoint>(new DataPoint[] {

						new DataPoint(2,
								Integer.parseInt(phone.getString("01"))),
						new DataPoint(10, Integer.parseInt(phone
								.getString("02"))),
						new DataPoint(20, Integer.parseInt(phone
								.getString("03"))),
						new DataPoint(30, Integer.parseInt(phone
								.getString("04"))),
						new DataPoint(40, Integer.parseInt(phone
								.getString("05"))),
						new DataPoint(50, Integer.parseInt(phone
								.getString("06"))),
						new DataPoint(60, Integer.parseInt(phone
								.getString("07"))),
						new DataPoint(70, Integer.parseInt(phone
								.getString("08"))),
						new DataPoint(80, Integer.parseInt(phone
								.getString("09"))),
						new DataPoint(90, Integer.parseInt(phone
								.getString("10"))),
						new DataPoint(100, Integer.parseInt(phone
								.getString("11"))),
						new DataPoint(110, Integer.parseInt(phone
								.getString("12"))), });
				graph.addSeries(series);

				staticLabelsFormatter = new StaticLabelsFormatter(graph);

				staticLabelsFormatter.setHorizontalLabels(new String[] { "Jan",
						" Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
						"Sep", "Oct", "Nov", "Dec", "" });

				graph(staticLabelsFormatter);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		case "1":
			try {
				totalofall
						.setText(String.valueOf(Integer.parseInt(bagcount
								.getText().toString())
								+ Integer.parseInt(boxcount.getText()
										.toString())
								+ Integer.parseInt(bincount.getText()
										.toString())
								+ Integer.parseInt(singleitemcount.getText()
										.toString())));
				JSONObject jObj = new JSONObject(result);
				JSONObject phone = jObj.getJSONObject("Data");

				graph.removeAllSeries();
				series = new BarGraphSeries<DataPoint>(new DataPoint[] {

						new DataPoint(2, Integer.parseInt(phone
								.getString("Mon"))),
						new DataPoint(10, Integer.parseInt(phone
								.getString("Tue"))),
						new DataPoint(20, Integer.parseInt(phone
								.getString("Wed"))),
						new DataPoint(30, Integer.parseInt(phone
								.getString("Thu"))),
						new DataPoint(40, Integer.parseInt(phone
								.getString("Fri"))),
						new DataPoint(50, Integer.parseInt(phone
								.getString("Sat"))),
						new DataPoint(60, Integer.parseInt(phone
								.getString("Sun"))), });
				graph.addSeries(series);

				staticLabelsFormatter = new StaticLabelsFormatter(graph);

				staticLabelsFormatter.setHorizontalLabels(new String[] { "Mon",
						" Tue", "Wed", "Thu", "Fri", "Sat", "Sun", "" });

				graph(staticLabelsFormatter);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case "2":
			try {
				graph.removeAllSeries();
				year = new ArrayList<String>();
				yearvalue = new ArrayList<String>();
				totalofall
						.setText(String.valueOf(Integer.parseInt(bagcount
								.getText().toString())
								+ Integer.parseInt(boxcount.getText()
										.toString())
								+ Integer.parseInt(bincount.getText()
										.toString())
								+ Integer.parseInt(singleitemcount.getText()
										.toString())));
				JSONObject jObj = new JSONObject(result);
				JSONObject phone = jObj.getJSONObject("Data");
				Iterator<String> iter = phone.keys();
				while (iter.hasNext()) {
					String key = iter.next();
					try {
						Log.e("key", "" + key);
						year.add(key);
						Object value = phone.get(key);
						Log.e("value", "" + value.toString());
						yearvalue.add(value.toString());
					} catch (JSONException e) {
						// Something went wrong!
					}
				}
				if (year.size() == 1) {
					series = new BarGraphSeries<DataPoint>(new DataPoint[] {

					new DataPoint(2, Integer.parseInt(yearvalue.get(0))),

					});
					graph.addSeries(series);

					staticLabelsFormatter = new StaticLabelsFormatter(graph);

					staticLabelsFormatter.setHorizontalLabels(new String[] {
							year.get(0), "" });
				} else if (year.size() == 2) {
					series = new BarGraphSeries<DataPoint>(
							new DataPoint[] {

									new DataPoint(2, Integer.parseInt(yearvalue
											.get(0))),
									new DataPoint(10, Integer
											.parseInt(yearvalue.get(1))), });
					graph.addSeries(series);

					staticLabelsFormatter = new StaticLabelsFormatter(graph);

					staticLabelsFormatter.setHorizontalLabels(new String[] {
							year.get(0), year.get(1), "" });
				} else if (year.size() == 3) {
					series = new BarGraphSeries<DataPoint>(
							new DataPoint[] {

									new DataPoint(2, Integer.parseInt(yearvalue
											.get(0))),
									new DataPoint(10, Integer
											.parseInt(yearvalue.get(1))),
									new DataPoint(20, Integer
											.parseInt(yearvalue.get(2))), });
					graph.addSeries(series);

					staticLabelsFormatter = new StaticLabelsFormatter(graph);

					staticLabelsFormatter.setHorizontalLabels(new String[] {
							year.get(0), year.get(1), year.get(2), "" });
				} else if (year.size() == 4) {
					series = new BarGraphSeries<DataPoint>(
							new DataPoint[] {

									new DataPoint(2, Integer.parseInt(yearvalue
											.get(0))),
									new DataPoint(10, Integer
											.parseInt(yearvalue.get(1))),
									new DataPoint(20, Integer
											.parseInt(yearvalue.get(2))),
									new DataPoint(30, Integer
											.parseInt(yearvalue.get(3))), });
					graph.addSeries(series);

					staticLabelsFormatter = new StaticLabelsFormatter(graph);

					staticLabelsFormatter.setHorizontalLabels(new String[] {
							year.get(0), year.get(1), year.get(2), year.get(3),
							"" });
				} else if (year.size() == 5) {
					series = new BarGraphSeries<DataPoint>(
							new DataPoint[] {

									new DataPoint(2, Integer.parseInt(yearvalue
											.get(0))),
									new DataPoint(10, Integer
											.parseInt(yearvalue.get(1))),
									new DataPoint(20, Integer
											.parseInt(yearvalue.get(2))),
									new DataPoint(30, Integer
											.parseInt(yearvalue.get(3))),
									new DataPoint(40, Integer
											.parseInt(yearvalue.get(4))), });
					graph.addSeries(series);

					staticLabelsFormatter = new StaticLabelsFormatter(graph);

					staticLabelsFormatter.setHorizontalLabels(new String[] {
							year.get(0), year.get(1), year.get(2), year.get(3),
							year.get(4), "" });
				} else if (year.size() == 6) {
					series = new BarGraphSeries<DataPoint>(
							new DataPoint[] {

									new DataPoint(2, Integer.parseInt(yearvalue
											.get(0))),
									new DataPoint(10, Integer
											.parseInt(yearvalue.get(1))),
									new DataPoint(20, Integer
											.parseInt(yearvalue.get(2))),
									new DataPoint(30, Integer
											.parseInt(yearvalue.get(3))),
									new DataPoint(40, Integer
											.parseInt(yearvalue.get(4))),
									new DataPoint(50, Integer
											.parseInt(yearvalue.get(5))), });
					graph.addSeries(series);

					staticLabelsFormatter = new StaticLabelsFormatter(graph);

					staticLabelsFormatter.setHorizontalLabels(new String[] {
							year.get(0), year.get(1), year.get(2), year.get(3),
							year.get(4), year.get(5), "" });
				} else if (year.size() == 7) {
					series = new BarGraphSeries<DataPoint>(
							new DataPoint[] {

									new DataPoint(2, Integer.parseInt(yearvalue
											.get(0))),
									new DataPoint(10, Integer
											.parseInt(yearvalue.get(1))),
									new DataPoint(20, Integer
											.parseInt(yearvalue.get(2))),
									new DataPoint(30, Integer
											.parseInt(yearvalue.get(3))),
									new DataPoint(40, Integer
											.parseInt(yearvalue.get(4))),
									new DataPoint(50, Integer
											.parseInt(yearvalue.get(5))),
									new DataPoint(60, Integer
											.parseInt(yearvalue.get(6))),

							});
					graph.addSeries(series);

					staticLabelsFormatter = new StaticLabelsFormatter(graph);

					staticLabelsFormatter.setHorizontalLabels(new String[] {
							year.get(0), year.get(1), year.get(2), year.get(3),
							year.get(4), year.get(5), year.get(6), "" });
				} else if (year.size() == 8) {
					series = new BarGraphSeries<DataPoint>(
							new DataPoint[] {

									new DataPoint(2, Integer.parseInt(yearvalue
											.get(0))),
									new DataPoint(10, Integer
											.parseInt(yearvalue.get(1))),
									new DataPoint(20, Integer
											.parseInt(yearvalue.get(2))),
									new DataPoint(30, Integer
											.parseInt(yearvalue.get(3))),
									new DataPoint(40, Integer
											.parseInt(yearvalue.get(4))),
									new DataPoint(50, Integer
											.parseInt(yearvalue.get(5))),
									new DataPoint(60, Integer
											.parseInt(yearvalue.get(6))),
									new DataPoint(70, Integer
											.parseInt(yearvalue.get(7))), });
					graph.addSeries(series);

					staticLabelsFormatter = new StaticLabelsFormatter(graph);

					staticLabelsFormatter.setHorizontalLabels(new String[] {
							year.get(0), year.get(1), year.get(2), year.get(3),
							year.get(4), year.get(5), year.get(6), year.get(7),
							"" });
				} else if (year.size() == 9) {
					series = new BarGraphSeries<DataPoint>(
							new DataPoint[] {

									new DataPoint(2, Integer.parseInt(yearvalue
											.get(0))),
									new DataPoint(10, Integer
											.parseInt(yearvalue.get(1))),
									new DataPoint(20, Integer
											.parseInt(yearvalue.get(2))),
									new DataPoint(30, Integer
											.parseInt(yearvalue.get(3))),
									new DataPoint(40, Integer
											.parseInt(yearvalue.get(4))),
									new DataPoint(50, Integer
											.parseInt(yearvalue.get(5))),
									new DataPoint(60, Integer
											.parseInt(yearvalue.get(6))),
									new DataPoint(70, Integer
											.parseInt(yearvalue.get(7))),
									new DataPoint(80, Integer
											.parseInt(yearvalue.get(8))), });
					graph.addSeries(series);

					staticLabelsFormatter = new StaticLabelsFormatter(graph);

					staticLabelsFormatter.setHorizontalLabels(new String[] {
							year.get(0), year.get(1), year.get(2), year.get(3),
							year.get(4), year.get(5), year.get(6), year.get(7),
							year.get(8), "" });
				} else if (year.size() == 10) {
					series = new BarGraphSeries<DataPoint>(
							new DataPoint[] {

									new DataPoint(2, Integer.parseInt(yearvalue
											.get(0))),
									new DataPoint(10, Integer
											.parseInt(yearvalue.get(1))),
									new DataPoint(20, Integer
											.parseInt(yearvalue.get(2))),
									new DataPoint(30, Integer
											.parseInt(yearvalue.get(3))),
									new DataPoint(40, Integer
											.parseInt(yearvalue.get(4))),
									new DataPoint(50, Integer
											.parseInt(yearvalue.get(5))),
									new DataPoint(60, Integer
											.parseInt(yearvalue.get(6))),
									new DataPoint(70, Integer
											.parseInt(yearvalue.get(7))),
									new DataPoint(80, Integer
											.parseInt(yearvalue.get(8))),
									new DataPoint(90, Integer
											.parseInt(yearvalue.get(9))), });
					graph.addSeries(series);

					staticLabelsFormatter = new StaticLabelsFormatter(graph);

					staticLabelsFormatter.setHorizontalLabels(new String[] {
							year.get(0), year.get(1), year.get(2), year.get(3),
							year.get(4), year.get(5), year.get(6), year.get(7),
							year.get(8), year.get(9), "" });
				}

				graph(staticLabelsFormatter);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;

		default:
			break;
		}
	}

	public void graph(StaticLabelsFormatter staticLabelsFormatter2) {
		graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter2);
		graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.BLACK);

		graph.getGridLabelRenderer().setVerticalLabelsColor(Color.BLACK);
		// graph.getGridLabelRenderer().setVerticalAxisTitle("            ");
		graph.getGridLabelRenderer().setHorizontalAxisTitleTextSize(10);
		// graph.getViewport().setXAxisBoundsManual(true);
		// graph.getViewport().setMinX(0);
		// graph.getViewport().setMaxX(1000);
		//
		// graph.getViewport().setScrollable(true);

		// graph.setTitle("");
		graph.getViewport().setYAxisBoundsManual(true);
		graph.getViewport().setMinY(0);
		graph.getViewport().setMaxY(100);

		// styling
		series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
			@Override
			public int get(DataPoint data) {
				return Color.rgb((int) data.getX() * 255 / 4,
						(int) Math.abs(data.getY() * 255 / 4), 100);
			}
		});
		series.setOnDataPointTapListener(new OnDataPointTapListener() {

			@Override
			public void onTap(Series arg0, DataPointInterface arg1) {
				// TODO Auto-generated method stub

				Log.e("on tabY", "" + arg1.getY());

			}
		});
		series.setSpacing(30);

		// draw values on top
		series.setDrawValuesOnTop(true);
		series.setValuesOnTopColor(Color.RED);

	}
}
