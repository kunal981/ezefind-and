package com.appalmighty.ezefind.report.fgmt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.filter.Approximator;
import com.github.mikephil.charting.data.filter.Approximator.ApproximatorType;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ValueFormatter;
import com.jjoe64.graphview.GraphView;

public class ReportLocationFragment extends Fragment implements
		OnClickListener, OnChartValueSelectedListener {
	GridView alllocation;
	LocationAdapter adapter;
	CheckBox chkpackage, chkinventory, chkboth;
	ImageView filtering, cancel;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	SharedPreferences shared;
	static RelativeLayout filterdialog;

	String filtertype = "both";
	String user_id;
	Editor editor;
	ArrayList<String> locationname;
	ArrayList<String> locationcount;
	protected BarChart mChart;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(
				R.layout.fragment_view_report_location, container, false);
		alllocation = (GridView) rootView.findViewById(R.id.locationgrid);
		mChart = (BarChart) rootView.findViewById(R.id.chart1);
		mChart.setOnChartValueSelectedListener(this);
		filterdialog = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter_all);
		filtering = (ImageView) rootView.findViewById(R.id.id_report_filtering);
		cancel = (ImageView) rootView.findViewById(R.id.btnDone);
		chkpackage = (CheckBox) rootView.findViewById(R.id.checkbox_package1);
		chkinventory = (CheckBox) rootView
				.findViewById(R.id.checkbox_inventory1);
		chkboth = (CheckBox) rootView.findViewById(R.id.checkbox_both1);
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		locationname = new ArrayList<String>();
		locationcount = new ArrayList<String>();

		filtertype = shared.getString(AppConstant.KEY_DEFAULT_Filter, "");
		user_id = shared.getString(AppConstant.KEY_USER_ID, "");
		Log.e("user_id", "" + shared.getString(AppConstant.KEY_USER_ID, ""));
		// BarGraphSeries<DataPoint> series = new BarGraphSeries<DataPoint>(
		// new DataPoint[] {
		//
		// new DataPoint(5, 20), new DataPoint(15, 45),
		// new DataPoint(25, 55), new DataPoint(35, 75),
		// new DataPoint(45, 90), });
		// graph.addSeries(series);
		//
		// graph.setTitle("graph");
		//
		// StaticLabelsFormatter staticLabelsFormatter = new
		// StaticLabelsFormatter(
		// graph);
		// staticLabelsFormatter.setHorizontalLabels(new String[] { "Mon",
		// "Tue",
		// "Wed", "Thu", "Fri", "Sat" });
		// graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
		//
		// graph.getViewport().setYAxisBoundsManual(true);
		// graph.getViewport().setMinY(0);
		// graph.getViewport().setMaxY(100);
		//
		// // styling
		// series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
		// @Override
		// public int get(DataPoint data) {
		// return Color.rgb((int) data.getX() * 255 / 4,
		// (int) Math.abs(data.getY() * 255 / 6), 100);
		// }
		// });
		//
		// series.setSpacing(40);
		//
		// // draw values on top
		// series.setDrawValuesOnTop(true);
		// series.setValuesOnTopColor(Color.RED);
		// series.setValuesOnTopSize(50);
		if (filtertype.equals("package")) {
			chkpackage.setChecked(true);
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
		} else if (filtertype.equals("inventory")) {
			chkpackage.setChecked(false);
			chkinventory.setChecked(true);
			chkboth.setChecked(false);
		} else {
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			chkboth.setChecked(true);
		}
		internetcheck = new ConnectionDetector(getActivity());
		isInternetPresent = internetcheck.isConnectingToInternet();
		if (isInternetPresent) {
			new AsyncGetLocationDataTask().execute(user_id);
		} else {
			ViewUtil.showAlertDialog(getActivity(), "ERROR",
					"No Internet Connection", false);
		}
		chkpackage.setOnClickListener(this);
		chkinventory.setOnClickListener(this);
		chkboth.setOnClickListener(this);
		filtering.setOnClickListener(this);
		cancel.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

		inflater.inflate(R.menu.bar, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.actionToggleValues: {
			for (DataSet<?> set : mChart.getData().getDataSets())
				set.setDrawValues(!set.isDrawValuesEnabled());

			mChart.invalidate();
			break;
		}
		case R.id.actionToggleHighlight: {
			if (mChart.isHighlightEnabled())
				mChart.setHighlightEnabled(false);
			else
				mChart.setHighlightEnabled(true);
			mChart.invalidate();
			break;
		}
		case R.id.actionTogglePinch: {
			if (mChart.isPinchZoomEnabled())
				mChart.setPinchZoom(false);
			else
				mChart.setPinchZoom(true);

			mChart.invalidate();
			break;
		}
		case R.id.actionToggleAutoScaleMinMax: {
			mChart.setAutoScaleMinMaxEnabled(!mChart.isAutoScaleMinMaxEnabled());
			mChart.notifyDataSetChanged();
			break;
		}
		case R.id.actionToggleHighlightArrow: {
			if (mChart.isDrawHighlightArrowEnabled())
				mChart.setDrawHighlightArrow(false);
			else
				mChart.setDrawHighlightArrow(true);
			mChart.invalidate();
			break;
		}
		case R.id.actionToggleStartzero: {
			mChart.getAxisLeft().setStartAtZero(
					!mChart.getAxisLeft().isStartAtZeroEnabled());

			mChart.notifyDataSetChanged();
			mChart.invalidate();
			break;
		}
		case R.id.animateX: {
			mChart.animateX(3000);
			break;
		}
		case R.id.animateY: {
			mChart.animateY(3000);
			break;
		}
		case R.id.animateXY: {

			mChart.animateXY(3000, 3000);
			break;
		}
		case R.id.actionToggleFilter: {

			Approximator a = new Approximator(ApproximatorType.DOUGLAS_PEUCKER,
					25);

			if (!mChart.isFilteringEnabled()) {
				mChart.enableFiltering(a);
			} else {
				mChart.disableFiltering();
			}
			mChart.invalidate();
			break;
		}
		case R.id.actionSave: {
			if (mChart.saveToGallery("title" + System.currentTimeMillis(), 50)) {
				Toast.makeText(getActivity(), "Saving SUCCESSFUL!",
						Toast.LENGTH_SHORT).show();
			} else
				Toast.makeText(getActivity(), "Saving FAILED!",
						Toast.LENGTH_SHORT).show();
			break;
		}
		}
		return true;
	}

	public static Fragment newInstance() {
		ReportLocationFragment fragment = new ReportLocationFragment();
		return fragment;
	}

	public static void showdialog() {
		filterdialog.setVisibility(View.VISIBLE);
	}

	class LocationAdapter extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;
		ArrayList<String> locationname1 = new ArrayList<String>();
		ArrayList<String> locationcount1 = new ArrayList<String>();

		public LocationAdapter(FragmentActivity activity,
				ArrayList<String> locationname, ArrayList<String> locationcount) {
			// TODO Auto-generated constructor stub
			this.activity = activity;
			locationname1 = locationname;
			locationcount1 = locationcount;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return locationname1.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return locationname1.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();

				convertView = inflater.inflate(R.layout.fragment_locationview,
						null);
				holder.txtlocname = (TextView) convertView
						.findViewById(R.id.label_items);
				holder.txtloccount = (TextView) convertView
						.findViewById(R.id.txt_count_item);
				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();

			}
			try {
				holder.txtlocname.setId(position);
				holder.txtloccount.setId(position);
				holder.txtlocname.setText(locationname1.get(position));
				holder.txtloccount.setText(locationcount1.get(position));
			} catch (Exception e) {
				Log.e("CRASH", "" + e.getMessage());
			}
			return convertView;
		}

	}

	class ViewHolder {
		TextView txtlocname, txtloccount;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.id_report_filtering:
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {

				new AsyncGetLocationDataTask().execute(filtertype);

			} else {
				ViewUtil.showAlertDialog(getActivity(), "ERROR",
						"No Internet Connection", false);
			}
			break;

		case R.id.checkbox_package1:
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "package";
			editor = shared.edit();
			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.checkbox_inventory1:

			chkpackage.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "inventory";
			editor = shared.edit();
			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();

			break;
		case R.id.checkbox_both1:
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			filtertype = "both";
			editor = shared.edit();
			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.btnDone:
			filterdialog.setVisibility(View.GONE);
			break;
		}
	}

	public class AsyncGetLocationDataTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				ViewUtil.showProgressDialog(getActivity());
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result != null) {
						if (result.equals("error")) {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"SomeThing Wrong", false);
							ViewUtil.hideProgressDialog();
						} else {

							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("Response location", result);
								JSONObject phone = jObj.getJSONObject("Data");

								Iterator<String> iter = phone.keys();
								while (iter.hasNext()) {
									String key = iter.next();
									try {
										Log.e("key", "" + key);
										locationname.add(key);
										Object value = phone.get(key);
										Log.e("value", "" + value.toString());
										locationcount.add(value.toString());
									} catch (JSONException e) {
										// Something went wrong!
									}
								}
								mChart.removeAllViews();
								mChart.setDrawBarShadow(false);
								mChart.setDrawValueAboveBar(true);
								mChart.setMaxVisibleValueCount(100);
								mChart.setPinchZoom(false);
								mChart.setDescription("");
								mChart.setDrawGridBackground(false);
								XAxis xAxis = mChart.getXAxis();
								xAxis.setPosition(XAxisPosition.BOTTOM);
								xAxis.setValues(locationname);
								xAxis.setDrawGridLines(false);
								xAxis.setDrawLabels(true);
								xAxis.setSpaceBetweenLabels(0);
								ValueFormatter custom = new MyValueFormatter();
								YAxis leftAxis = mChart.getAxisLeft();
								leftAxis.setLabelCount(5, true);
								leftAxis.setValueFormatter(custom);
								leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
								leftAxis.setSpaceTop(15f);
								YAxis rightAxis = mChart.getAxisRight();
								rightAxis.setDrawGridLines(false);
								rightAxis.setLabelCount(0, false);
								rightAxis.setDrawLabels(false);
								Legend l = mChart.getLegend();
								l.setPosition(LegendPosition.BELOW_CHART_LEFT);
								l.setForm(LegendForm.LINE);
								l.setFormSize(0);
								l.setTextSize(0);
								l.setXEntrySpace(0f);
								setData(locationcount.size(), 100);
								adapter = new LocationAdapter(getActivity(),
										locationname, locationcount);
								alllocation.setAdapter(adapter);
								filterdialog.setVisibility(View.GONE);
								ViewUtil.hideProgressDialog();
							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");
								filterdialog.setVisibility(View.GONE);
								ViewUtil.hideProgressDialog();
								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);

							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;
			try {

				List<NameValuePair> parameter = new LinkedList<NameValuePair>();

				parameter.add(new BasicNameValuePair("UserId", String
						.valueOf(params[0])));

				Log.e("parameter location", "" + parameter);
				result = NetworkConnector.GetLocationData(parameter);
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
			return result;
		}

	}

	private void setData(int count, float range) {

		ArrayList<String> xVals = new ArrayList<String>();
		for (int i = 0; i < count; i++) {
			xVals.add(locationname.get(i));
		}

		ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

		for (int i = 0; i < count; i++) {

			yVals1.add(new BarEntry(Integer.parseInt(locationcount.get(i)), i));
		}

		BarDataSet set1 = new BarDataSet(yVals1, "");
		set1.setBarSpacePercent(35f);

		ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
		dataSets.add(set1);

		BarData data = new BarData(xVals, dataSets);
		// data.setValueFormatter(new MyValueFormatter());
		data.setValueTextSize(10f);
		// data.setValueTypeface(mTf);

		mChart.setData(data);
	}

	@SuppressLint("NewApi")
	@Override
	public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

		if (e == null)
			return;

		RectF bounds = mChart.getBarBounds((BarEntry) e);
		PointF position = mChart.getPosition(e, AxisDependency.LEFT);

		Log.e("bounds", bounds.toString());
		Log.e("position", position.toString());

		Log.e("x-index", "low: " + mChart.getLowestVisibleXIndex() + ", high: "
				+ mChart.getHighestVisibleXIndex());
	}

	public void onNothingSelected() {
	};

}
