package com.appalmighty.ezefind.report.fgmt;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.DataBase.DatasourceHandler;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;

public class ReportAllFragment extends Fragment implements OnClickListener {
	TextView txtitem, txtmember, txtcategory, txtvalue, txtloc, txtdate,
			Clickedshow;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	DatasourceHandler DATABASE;
	String UserId;
	Cursor itemcount;
	SharedPreferences shared;
	static RelativeLayout filterdialog;
	ImageView filtering, cancel;
	CheckBox chkpackage, chkinventory, chkboth;
	String filtertype;
	RelativeLayout rlitem, rlmember, rlcategory, rlvalue, rlloc, rldate;
	Editor editor;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_view_report_all,
				container, false);
		DATABASE = new DatasourceHandler(getActivity());
		rlitem = (RelativeLayout) rootView.findViewById(R.id.inner_r_item_1);
		rlmember = (RelativeLayout) rootView.findViewById(R.id.inner_r_item_2);
		rlcategory = (RelativeLayout) rootView
				.findViewById(R.id.inner_r_item_3);
		rlvalue = (RelativeLayout) rootView.findViewById(R.id.inner_r_item_4);
		rlloc = (RelativeLayout) rootView.findViewById(R.id.inner_r_item_5);
		rldate = (RelativeLayout) rootView.findViewById(R.id.inner_r_item_6);
		Clickedshow = (TextView) rootView.findViewById(R.id.label_total_value);
		txtitem = (TextView) rootView.findViewById(R.id.txt_count_item);
		txtmember = (TextView) rootView.findViewById(R.id.txt_count_mem);
		txtcategory = (TextView) rootView.findViewById(R.id.txt_count_category);
		txtvalue = (TextView) rootView.findViewById(R.id.txt_count_val);
		txtloc = (TextView) rootView.findViewById(R.id.txt_count_loc);
		txtdate = (TextView) rootView.findViewById(R.id.txt_count_date);
		filtering = (ImageView) rootView.findViewById(R.id.id_report_filtering);
		chkpackage = (CheckBox) rootView.findViewById(R.id.checkbox_package1);
		chkinventory = (CheckBox) rootView
				.findViewById(R.id.checkbox_inventory1);
		chkboth = (CheckBox) rootView.findViewById(R.id.checkbox_both1);
		cancel = (ImageView) rootView.findViewById(R.id.btnDone);
		filterdialog = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter_all);
		internetcheck = new ConnectionDetector(getActivity());
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		UserId = shared.getString(AppConstant.KEY_USER_ID, "");
		filtertype = shared.getString(AppConstant.KEY_DEFAULT_Filter, "");
		if (filtertype.equals("package")) {
			chkpackage.setChecked(true);
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
		} else if (filtertype.equals("inventory")) {
			chkpackage.setChecked(false);
			chkinventory.setChecked(true);
			chkboth.setChecked(false);
		} else {
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			chkboth.setChecked(true);
		}
		itemcount = DATABASE.Getitemcount(UserId);
		isInternetPresent = internetcheck.isConnectingToInternet();
		if (isInternetPresent) {
			new AsyncGetAllDataTask().execute(filtertype);
		} else {
			ViewUtil.showAlertDialog(getActivity(), "ERROR",
					"No Internet Connection", false);
		}
		txtitem.setText("" + itemcount.getCount());
		rlitem.setOnClickListener(this);
		rlmember.setOnClickListener(this);
		rlcategory.setOnClickListener(this);
		rlvalue.setOnClickListener(this);
		rlloc.setOnClickListener(this);
		rldate.setOnClickListener(this);
		chkpackage.setOnClickListener(this);
		chkinventory.setOnClickListener(this);
		chkboth.setOnClickListener(this);

		filtering.setOnClickListener(this);
		cancel.setOnClickListener(this);

		return rootView;
	}

	public static Fragment newInstance() {
		ReportAllFragment fragment = new ReportAllFragment();
		return fragment;
	}

	public static void showdialog() {
		filterdialog.setVisibility(View.VISIBLE);
	}

	public class AsyncGetAllDataTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				ViewUtil.showProgressDialog(getActivity());
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result != null) {
						if (result.equals("error")) {
							// ViewUtil.showAlertDialog(getActivity(), "ERROR",
							// "SomeThing Wrong", false);
							ViewUtil.hideProgressDialog();
						} else {

							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("Response all", result);
								JSONObject phone = jObj
										.getJSONObject("message");
								txtitem.setText(phone.getString("Items_count"));
								txtmember.setText(phone
										.getString("Users_count"));
								txtcategory.setText(phone
										.getString("Category_count"));
								txtvalue.setText(phone.getString("Value_count"));
								txtloc.setText(phone
										.getString("Location_count"));
								txtdate.setText(phone.getString("Day_count"));
								Clickedshow.setText("0");
								filterdialog.setVisibility(View.GONE);
								ViewUtil.hideProgressDialog();
							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");
								filterdialog.setVisibility(View.GONE);
								ViewUtil.hideProgressDialog();
								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);

							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;
			try {

				List<NameValuePair> parameter = new LinkedList<NameValuePair>();

				parameter.add(new BasicNameValuePair("main_filter", String
						.valueOf(params[0])));

				Log.e("parameter all", "" + parameter);
				result = NetworkConnector.GetAllData(parameter);
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
			return result;
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.inner_r_item_1:
			Clickedshow.setText(txtitem.getText().toString());
			break;
		case R.id.inner_r_item_2:
			Clickedshow.setText(txtmember.getText().toString());
			break;
		case R.id.inner_r_item_3:
			Clickedshow.setText(txtcategory.getText().toString());
			break;
		case R.id.inner_r_item_4:
			Clickedshow.setText("$" + txtvalue.getText().toString());
			break;
		case R.id.inner_r_item_5:
			Clickedshow.setText(txtloc.getText().toString());
			break;
		case R.id.inner_r_item_6:
			Clickedshow.setText(txtdate.getText().toString());
			break;

		case R.id.id_report_filtering:
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {

				new AsyncGetAllDataTask().execute(filtertype);

			}
			break;
		case R.id.checkbox_package1:
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "package";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.checkbox_inventory1:

			chkpackage.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "inventory";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();

			break;
		case R.id.checkbox_both1:
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			filtertype = "both";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.btnDone:
			filterdialog.setVisibility(View.GONE);
			break;

		default:
			break;
		}
	}
}
