package com.appalmighty.ezefind.report.fgmt;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.filter.Approximator;
import com.github.mikephil.charting.data.filter.Approximator.ApproximatorType;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ValueFormatter;

public class ReportValueFragment extends Fragment implements OnClickListener,
		OnChartValueSelectedListener {
	TextView txtitemcount, txtloccount, txtcatogarycount, txttimecount,
			selectedvalue, txttotal;
	CheckBox chkpackage, chkinventory, chkboth, chkyear, chkmonth, chkweek,
			chkbag, chkbox, chksingleitem, chkbin;
	String filtertype = "both", userid;
	String filteritem = "year", filteritemtype = "bag", checkpoint, Response;
	ImageView filtering, filteringitem, filteringlocation, fileteringtime,
			cancel, cancelitemdialog, cancellocationdialog, canceltimedialog,
			bagfilter, boxfilter, singleitemfilter, binfilter;
	static RelativeLayout filterdialog, filteritemdialog, filtertimedialog,
			baglayout, boxlayout, singleitemlayout, binlayout;
	LinearLayout filterlocation;
	SharedPreferences shared;
	Editor editor;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	protected BarChart mChart;
	ArrayList<String> names;
	ArrayList<String> counts;
	ArrayList<String> list;
	MyAdapter adaptrer;
	ListView listview;
	String checkvalue;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_view_report_value,
				container, false);
		names = new ArrayList<String>();
		counts = new ArrayList<String>();
		list = new ArrayList<String>();

		selectedvalue = (TextView) rootView.findViewById(R.id.txt_day_val);
		txttotal = (TextView) rootView.findViewById(R.id.txt_week_val);
		txtitemcount = (TextView) rootView
				.findViewById(R.id.txt_count_item_bag);
		txtloccount = (TextView) rootView.findViewById(R.id.txt_count_item_box);
		txtcatogarycount = (TextView) rootView
				.findViewById(R.id.txt_count_item_single_item);
		txttimecount = (TextView) rootView
				.findViewById(R.id.txt_count_item_bin);
		baglayout = (RelativeLayout) rootView.findViewById(R.id.item_row_1);
		boxlayout = (RelativeLayout) rootView.findViewById(R.id.item_row_2);
		singleitemlayout = (RelativeLayout) rootView
				.findViewById(R.id.item_row_4);
		binlayout = (RelativeLayout) rootView.findViewById(R.id.item_row_3);
		bagfilter = (ImageView) rootView.findViewById(R.id.img_);
		boxfilter = (ImageView) rootView.findViewById(R.id.img_2);
		singleitemfilter = (ImageView) rootView.findViewById(R.id.img_4);
		binfilter = (ImageView) rootView.findViewById(R.id.img_3);
		filterlocation = (LinearLayout) rootView
				.findViewById(R.id.filerlocation);
		filterdialog = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter_all);
		listview = (ListView) rootView.findViewById(R.id.forboth);
		filteritemdialog = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter);
		filtertimedialog = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter_item);
		fileteringtime = (ImageView) rootView
				.findViewById(R.id.id_item_filtering);

		filtering = (ImageView) rootView.findViewById(R.id.id_report_filtering);
		filteringitem = (ImageView) rootView.findViewById(R.id.fikl);
		// filteringitem = (ImageView) rootView.findViewById(R.id.fikl2);
		cancelitemdialog = (ImageView) rootView.findViewById(R.id.btnDon);
		canceltimedialog = (ImageView) rootView.findViewById(R.id.btnDo);
		cancel = (ImageView) rootView.findViewById(R.id.btnDone);
		cancellocationdialog = (ImageView) rootView.findViewById(R.id.btnDone2);
		chkpackage = (CheckBox) rootView.findViewById(R.id.checkbox_package1);
		chkinventory = (CheckBox) rootView
				.findViewById(R.id.checkbox_inventory1);
		chkboth = (CheckBox) rootView.findViewById(R.id.checkbox_both1);
		chkbag = (CheckBox) rootView.findViewById(R.id.checkbox_bag);
		chkbox = (CheckBox) rootView.findViewById(R.id.checkbox_box);
		chksingleitem = (CheckBox) rootView
				.findViewById(R.id.checkbox_singleitem);
		chkyear = (CheckBox) rootView.findViewById(R.id.checkbox_year);
		chkmonth = (CheckBox) rootView.findViewById(R.id.checkbox_month);
		chkweek = (CheckBox) rootView.findViewById(R.id.checkbox_week);
		chkbin = (CheckBox) rootView.findViewById(R.id.checkbox_bin);
		mChart = (BarChart) rootView.findViewById(R.id.chart1);
		mChart.setOnChartValueSelectedListener(this);

		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);

		userid = shared.getString(AppConstant.KEY_USER_ID, "");
		Log.e("category", "" + shared.getString(AppConstant.KEY_CATEGORY, ""));
		Log.e("location", "" + shared.getString(AppConstant.KEY_LOCATION, ""));
		if (filtertype.equals("package")) {
			chkpackage.setChecked(true);
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
		} else if (filtertype.equals("inventory")) {
			chkpackage.setChecked(false);
			chkinventory.setChecked(true);
			chkboth.setChecked(false);
		} else {
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			chkboth.setChecked(true);
		}
		internetcheck = new ConnectionDetector(getActivity());
		isInternetPresent = internetcheck.isConnectingToInternet();
		if (isInternetPresent) {
			new AsyncGetValueDataTask().execute(userid);
		} else {
			ViewUtil.showAlertDialog(getActivity(), "ERROR",
					"No Internet Connection", false);
		}

		chkpackage.setOnClickListener(this);
		chkinventory.setOnClickListener(this);
		chkboth.setOnClickListener(this);
		chkbag.setOnClickListener(this);
		chkbox.setOnClickListener(this);
		chksingleitem.setOnClickListener(this);
		chkbin.setOnClickListener(this);
		chkyear.setOnClickListener(this);
		chkmonth.setOnClickListener(this);
		chkweek.setOnClickListener(this);
		filtering.setOnClickListener(this);
		filteringitem.setOnClickListener(this);
		fileteringtime.setOnClickListener(this);
		cancelitemdialog.setOnClickListener(this);
		cancellocationdialog.setOnClickListener(this);
		cancel.setOnClickListener(this);
		canceltimedialog.setOnClickListener(this);
		baglayout.setOnClickListener(this);
		boxlayout.setOnClickListener(this);
		singleitemlayout.setOnClickListener(this);
		binlayout.setOnClickListener(this);
		bagfilter.setOnClickListener(this);
		boxfilter.setOnClickListener(this);
		singleitemfilter.setOnClickListener(this);
		binfilter.setOnClickListener(this);

		return rootView;
	}

	public static void showdialog() {
		filterdialog.setVisibility(View.VISIBLE);
	}

	public static Fragment newInstance() {
		ReportValueFragment fragment = new ReportValueFragment();
		return fragment;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.id_report_filtering:
			baglayout.setBackgroundColor(Color.WHITE);
			boxlayout.setBackgroundColor(Color.WHITE);
			binlayout.setBackgroundColor(Color.WHITE);
			singleitemlayout.setBackgroundColor(Color.WHITE);
			if (filtertype.equals("package")) {
				Package(Response);
				Log.e("package", "package");
			} else if (filtertype.equals("inventory")) {
				Inventory(Response);
				Log.e("Inventory", "Inventory");
			} else {
				Both(Response);
				Log.e("Both", "Both");
			}

			break;
		case R.id.id_item_filtering:
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {
				TimeFioltering(Response);
			}
			break;
		case R.id.checkbox_package1:
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "package";
			editor = shared.edit();
			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.checkbox_inventory1:
			chkpackage.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "inventory";
			editor = shared.edit();
			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.checkbox_both1:
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			filtertype = "both";
			editor = shared.edit();
			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.checkbox_bag:
			chkbox.setChecked(false);
			chkbin.setChecked(false);
			chksingleitem.setChecked(false);
			filteritemtype = "bag";
			break;
		case R.id.checkbox_box:
			chkbag.setChecked(false);
			chkbin.setChecked(false);
			chksingleitem.setChecked(false);
			filteritemtype = "box";
			break;
		case R.id.checkbox_bin:
			chkbox.setChecked(false);
			chkbag.setChecked(false);
			chksingleitem.setChecked(false);
			filteritemtype = "bin";
			break;
		case R.id.checkbox_singleitem:
			chkbox.setChecked(false);
			chkbin.setChecked(false);
			chkbag.setChecked(false);
			filteritemtype = "singleitem";
			break;
		case R.id.btnDone:
			filterdialog.setVisibility(View.GONE);
			break;
		case R.id.btnDo:
			filtertimedialog.setVisibility(View.GONE);
			break;
		case R.id.item_row_1:
			baglayout.setBackgroundColor(Color.LTGRAY);
			boxlayout.setBackgroundColor(Color.WHITE);
			binlayout.setBackgroundColor(Color.WHITE);
			singleitemlayout.setBackgroundColor(Color.WHITE);
			ItemsFiltering(Response);
			selectedvalue.setText(txtitemcount.getText().toString());
			break;
		case R.id.item_row_2:
			baglayout.setBackgroundColor(Color.WHITE);
			boxlayout.setBackgroundColor(Color.LTGRAY);
			binlayout.setBackgroundColor(Color.WHITE);
			singleitemlayout.setBackgroundColor(Color.WHITE);
			LocationFiltering(Response);
			selectedvalue.setText(txtloccount.getText().toString());
			break;
		case R.id.item_row_3:
			baglayout.setBackgroundColor(Color.WHITE);
			boxlayout.setBackgroundColor(Color.WHITE);
			binlayout.setBackgroundColor(Color.LTGRAY);
			singleitemlayout.setBackgroundColor(Color.WHITE);
			TimeFioltering(Response);
			selectedvalue.setText(txttimecount.getText().toString());
			break;
		case R.id.item_row_4:
			baglayout.setBackgroundColor(Color.WHITE);
			boxlayout.setBackgroundColor(Color.WHITE);
			binlayout.setBackgroundColor(Color.WHITE);
			singleitemlayout.setBackgroundColor(Color.LTGRAY);
			CategoryFiltering(Response);
			selectedvalue.setText(txtcatogarycount.getText().toString());
			break;
		case R.id.img_:
			filteritemdialog.setVisibility(View.VISIBLE);
			break;
		case R.id.img_2:
			checkvalue = "location";
			new AsyncGetcataogaryTask().execute("49");
			break;
		case R.id.img_3:
			filtertimedialog.setVisibility(View.VISIBLE);
			break;
		case R.id.img_4:
			checkvalue = "category";
			new AsyncGetcataogaryTask().execute("49");
			break;
		case R.id.fikl:
			ItemsFiltering(filteritemtype);
			mChart.highlightValue(2, 0);
			filteritemdialog.setVisibility(View.GONE);
			break;
		// case R.id.fikl2:
		// filterlocation.setVisibility(View.GONE);
		//
		// break;
		case R.id.btnDon:
			filteritemdialog.setVisibility(View.GONE);
			break;
		case R.id.btnDone2:
			filterlocation.setVisibility(View.GONE);
			break;
		case R.id.checkbox_year:
			chkmonth.setChecked(false);
			chkweek.setChecked(false);
			filteritem = "year";
			break;
		case R.id.checkbox_month:
			chkyear.setChecked(false);
			chkweek.setChecked(false);
			filteritem = "month";
			break;
		case R.id.checkbox_week:
			chkyear.setChecked(false);
			chkmonth.setChecked(false);
			filteritem = "week";
			break;
		default:
			break;
		}
	}

	public class AsyncGetValueDataTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				ViewUtil.showProgressDialog(getActivity());
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				names.clear();
				counts.clear();
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						if (result != null) {
							if (result.equals("error")) {
								ViewUtil.showAlertDialog(getActivity(),
										"ERROR", "SomeThing Wrong", false);
								ViewUtil.hideProgressDialog();
							} else {

								JSONObject jObj = new JSONObject(result);

								if (jObj.getBoolean("success")) {
									Log.e("Response value", result);
									Response = result;

									if (filtertype.equals("package")) {
										Package(Response);

									} else if (filtertype.equals("inventory")) {
										Inventory(Response);

									} else {
										Both(Response);

									}
									ViewUtil.hideProgressDialog();

								}

								else {
									Log.e("Response", result);

									String Server_message = jObj
											.getString("message");
									filterdialog.setVisibility(View.GONE);
									ViewUtil.hideProgressDialog();
									ViewUtil.showAlertDialog(getActivity(),
											"Error", Server_message, true);

								}
							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;
			try {

				List<NameValuePair> parameter = new LinkedList<NameValuePair>();

				parameter.add(new BasicNameValuePair("UserId", String
						.valueOf(params[0])));

				Log.e("parameter value", "" + parameter);

				result = NetworkConnector.GetValueData(parameter);

			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
			return result;
		}

	}

	private void Chart() {
		mChart.removeAllViews();
		mChart.setDrawBarShadow(false);
		mChart.setDrawValueAboveBar(true);

		mChart.setMaxVisibleValueCount(100);

		mChart.setPinchZoom(false);

		mChart.setDescription("");

		mChart.setDrawGridBackground(false);

		XAxis xAxis = mChart.getXAxis();
		xAxis.setPosition(XAxisPosition.BOTTOM);
		xAxis.setValues(names);
		xAxis.setDrawGridLines(false);
		xAxis.setDrawLabels(true);

		xAxis.setSpaceBetweenLabels(0);

		ValueFormatter custom = new MyValueFormatter();

		YAxis leftAxis = mChart.getAxisLeft();
		// leftAxis.setTypeface(mTf);
		leftAxis.setLabelCount(5, true);
		leftAxis.setValueFormatter(custom);
		leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
		leftAxis.setSpaceTop(15f);

		YAxis rightAxis = mChart.getAxisRight();
		rightAxis.setDrawGridLines(false);
		// // rightAxis.setTypeface(mTf);
		rightAxis.setLabelCount(0, false);
		rightAxis.setDrawLabels(false);
		// // rightAxis.setValueFormatter(custom);
		// rightAxis.setSpaceTop(0);

		Legend l = mChart.getLegend();
		l.setPosition(LegendPosition.BELOW_CHART_LEFT);

		l.setForm(LegendForm.LINE);
		l.setFormSize(0);
		l.setTextSize(0);
		l.setXEntrySpace(0f);
		setData(counts.size(), 100);
	}

	private void setData(int count, float range) {

		ArrayList<String> xVals = new ArrayList<String>();
		for (int i = 0; i < count; i++) {
			xVals.add(names.get(i));
		}

		ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

		for (int i = 0; i < count; i++) {

			yVals1.add(new BarEntry(Integer.parseInt(counts.get(i)), i));
		}

		BarDataSet set1 = new BarDataSet(yVals1, "");
		set1.setBarSpacePercent(35f);

		ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
		dataSets.add(set1);

		BarData data = new BarData(xVals, dataSets);
		// data.setValueFormatter(new MyValueFormatter());
		data.setValueTextSize(10f);
		// data.setValueTypeface(mTf);

		mChart.setData(data);
	}

	@SuppressLint("NewApi")
	@Override
	public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

		if (e == null)
			return;

		RectF bounds = mChart.getBarBounds((BarEntry) e);
		PointF position = mChart.getPosition(e, AxisDependency.LEFT);

		Log.e("dataSetIndex", "" + dataSetIndex);
		Log.e("position", position.toString());

		Log.e("x-index", "low: " + mChart.getLowestVisibleXIndex() + ", high: "
				+ mChart.getHighestVisibleXIndex());
	}

	public void onNothingSelected() {
	};

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

		inflater.inflate(R.menu.bar, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.actionToggleValues: {
			for (DataSet<?> set : mChart.getData().getDataSets())
				set.setDrawValues(!set.isDrawValuesEnabled());

			mChart.invalidate();
			break;
		}
		case R.id.actionToggleHighlight: {

			if (mChart.isHighlightEnabled())
				mChart.setHighlightEnabled(false);
			else
				mChart.setHighlightEnabled(true);
			mChart.invalidate();
			break;
		}
		case R.id.actionTogglePinch: {
			if (mChart.isPinchZoomEnabled())
				mChart.setPinchZoom(false);
			else
				mChart.setPinchZoom(true);

			mChart.invalidate();
			break;
		}
		case R.id.actionToggleAutoScaleMinMax: {
			mChart.setAutoScaleMinMaxEnabled(!mChart.isAutoScaleMinMaxEnabled());
			mChart.notifyDataSetChanged();
			break;
		}
		case R.id.actionToggleHighlightArrow: {

			if (mChart.isDrawHighlightArrowEnabled())
				mChart.setDrawHighlightArrow(false);
			else
				mChart.setDrawHighlightArrow(true);
			mChart.invalidate();
			break;
		}
		case R.id.actionToggleStartzero: {
			mChart.getAxisLeft().setStartAtZero(
					!mChart.getAxisLeft().isStartAtZeroEnabled());

			mChart.notifyDataSetChanged();
			mChart.invalidate();
			break;
		}
		case R.id.animateX: {
			mChart.animateX(3000);
			break;
		}
		case R.id.animateY: {
			mChart.animateY(3000);
			break;
		}
		case R.id.animateXY: {

			mChart.animateXY(3000, 3000);
			break;
		}
		case R.id.actionToggleFilter: {

			Approximator a = new Approximator(ApproximatorType.DOUGLAS_PEUCKER,
					25);

			if (!mChart.isFilteringEnabled()) {
				mChart.enableFiltering(a);
			} else {
				mChart.disableFiltering();
			}
			mChart.invalidate();
			break;
		}
		case R.id.actionSave: {
			if (mChart.saveToGallery("title" + System.currentTimeMillis(), 50)) {
				Toast.makeText(getActivity(), "Saving SUCCESSFUL!",
						Toast.LENGTH_SHORT).show();
			} else
				Toast.makeText(getActivity(), "Saving FAILED!",
						Toast.LENGTH_SHORT).show();
			break;
		}
		}
		return true;
	}

	public class MyAdapter extends BaseAdapter {
		private LayoutInflater inflater;
		private Activity activity;
		ArrayList<String> text = new ArrayList<String>();

		public MyAdapter(FragmentActivity fragmentActivity,
				ArrayList<String> list) {
			// TODO Auto-generated constructor stub
			activity = fragmentActivity;
			text = list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return text.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return text.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final ViewHolder holder;
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.fragment_value_adapter_view, null);
				holder = new ViewHolder();
				holder.name = (TextView) convertView.findViewById(R.id.txt_);
				holder.check = (LinearLayout) convertView
						.findViewById(R.id.linear_);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {
				holder.name.setId(position);
				holder.check.setId(position);
				holder.name.setText(text.get(position));
				holder.check.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (checkvalue.equals("category")) {
							CategoryFiltering(text.get(position));
							mChart.highlightValue(position, 0);
							filterlocation.setVisibility(View.GONE);
						} else {
							LocationFiltering(text.get(position));
							mChart.highlightValue(position, 0);
							filterlocation.setVisibility(View.GONE);
						}

					}
				});

			} catch (Exception e) {
				e.printStackTrace();
			}
			return convertView;
		}

	}

	class ViewHolder {

		private TextView name;
		private LinearLayout check;
	}

	public class AsyncGetcataogaryTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ViewUtil.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			list.clear();

			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {
					if (result.equals("error")) {
						ViewUtil.showAlertDialog(getActivity(), "ERROR",
								"SomeThing Wrong", false);
						ViewUtil.hideProgressDialog();
					} else {

						JSONObject jObj = new JSONObject(result);

						if (jObj.getBoolean("success")) {
							Log.e("Response", result);
							JSONArray array = jObj.getJSONArray("message");
							Log.e("array", "" + array);
							for (int i = 0; i < array.length(); i++) {

								list.add(array.get(i).toString().toUpperCase());
							}
							Log.e("list:", "" + list);
							Log.e("listsize:", "" + list.size());

							// add elements to al, including duplicates
							HashSet hs = new HashSet();
							hs.addAll(list);
							list.clear();
							list.addAll(hs);
							adaptrer = new MyAdapter(getActivity(), list);
							listview.setAdapter(adaptrer);
							filterlocation.setVisibility(View.VISIBLE);
							ViewUtil.hideProgressDialog();

						} else {
							Log.e("Response", result);

							String Server_message = jObj.getString("message");
							ViewUtil.hideProgressDialog();
							filterlocation.setVisibility(View.GONE);
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									Server_message, true);
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("UserId", String
					.valueOf(params[0])));
			Log.e("parameter", "" + parameter);
			Log.e("checkvalue", "" + checkvalue);
			if (checkvalue.equals("category")) {
				result = NetworkConnector.GETCategory(parameter);
			} else {
				result = NetworkConnector.GETLocation(parameter);
			}
			return result;
		}

	}

	private void Package(String response2) {
		names.clear();
		counts.clear();
		JSONObject jObj;
		try {
			jObj = new JSONObject(response2);
			Log.e("jObj", "" + jObj);
			JSONObject overall_data = jObj.getJSONObject("overall_data");
			Log.e("overall_data", "" + overall_data);
			JSONObject Package = overall_data.getJSONObject("package");
			Log.e("Package", "" + Package);
			JSONObject item = Package.getJSONObject("item");
			Log.e("item", "" + item);
			txtitemcount.setText(item.getString("total_value"));
			JSONObject location = Package.getJSONObject("location");
			Log.e("location", "" + location);
			txtloccount.setText(location.getString("total_value"));

			JSONObject category = Package.getJSONObject("category");
			Log.e("category", "" + category);
			txtcatogarycount.setText(category.getString("total_value"));

			JSONObject time = Package.getJSONObject("time");
			Log.e("time", "" + time);
			txttimecount.setText(time.getString("total_value"));
			txttotal.setText(Package.getString("total"));
			names.add("ITEMS");
			names.add("LOCATIONS");
			names.add("CATEGORY");
			names.add("TIMES");
			counts.add(item.getString("total_value"));
			counts.add(location.getString("total_value"));
			counts.add(category.getString("total_value"));
			counts.add(time.getString("total_value"));

			filterdialog.setVisibility(View.GONE);

			Chart();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void Inventory(String response2) {
		names.clear();
		counts.clear();
		JSONObject jObj;
		try {
			jObj = new JSONObject(response2);

			JSONObject overall_data = jObj.getJSONObject("overall_data");
			JSONObject Inventory = overall_data.getJSONObject("inventory");
			JSONObject item = Inventory.getJSONObject("item");
			txtitemcount.setText(item.getString("total_value"));
			JSONObject location = Inventory.getJSONObject("location");
			txtloccount.setText(location.getString("total_value"));

			JSONObject category = Inventory.getJSONObject("category");
			txtcatogarycount.setText(category.getString("total_value"));

			JSONObject time = Inventory.getJSONObject("time");
			txttimecount.setText(time.getString("total_value"));

			names.add("ITEMS");
			names.add("LOCATIONS");
			names.add("CATEGORY");
			names.add("TIMES");
			counts.add(item.getString("total_value"));
			counts.add(location.getString("total_value"));
			counts.add(category.getString("total_value"));
			counts.add(time.getString("total_value"));
			txttotal.setText(Inventory.getString("total"));
			filterdialog.setVisibility(View.GONE);

			Chart();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void Both(String response2) {
		names.clear();
		counts.clear();
		JSONObject jObj;
		try {
			jObj = new JSONObject(response2);

			JSONObject overall_data = jObj.getJSONObject("overall_data");

			JSONObject Both = overall_data.getJSONObject("both");

			JSONObject item = Both.getJSONObject("item");

			txtitemcount.setText(item.getString("total_value"));
			JSONObject location = Both.getJSONObject("location");

			txtloccount.setText(location.getString("total_value"));

			JSONObject category = Both.getJSONObject("category");

			txtcatogarycount.setText(category.getString("total_value"));

			JSONObject time = Both.getJSONObject("time");
			txttimecount.setText(time.getString("total_value"));

			names.add("ITEMS");
			names.add("LOCATIONS");
			names.add("CATEGORY");
			names.add("TIMES");
			counts.add(item.getString("total_value"));
			counts.add(location.getString("total_value"));
			counts.add(category.getString("total_value"));
			counts.add(time.getString("total_value"));
			txttotal.setText(Both.getString("total"));
			filterdialog.setVisibility(View.GONE);

			Chart();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void ItemsFiltering(String response2) {
		names.clear();
		counts.clear();
		JSONObject jObj;
		JSONObject object;
		try {
			jObj = new JSONObject(response2);

			JSONObject overall_data = jObj.getJSONObject("overall_data");

			if (filtertype.equals("package")) {
				object = overall_data.getJSONObject("package");
			} else if (filtertype.equals("inventory")) {
				object = overall_data.getJSONObject("inventory");
			} else {
				object = overall_data.getJSONObject("both");
			}
			JSONObject item = object.getJSONObject("item");
			JSONObject data = item.getJSONObject("data");
			Iterator<String> iter = data.keys();
			while (iter.hasNext()) {
				String key = iter.next();

				names.add(key.toUpperCase());
				Object value = data.get(key);

				counts.add(value.toString());
			}
			Chart();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void LocationFiltering(String response2) {
		names.clear();
		counts.clear();
		JSONObject jObj;
		JSONObject object;
		try {
			jObj = new JSONObject(response2);

			JSONObject overall_data = jObj.getJSONObject("overall_data");

			if (filtertype.equals("package")) {
				object = overall_data.getJSONObject("package");
			} else if (filtertype.equals("inventory")) {
				object = overall_data.getJSONObject("inventory");
			} else {
				object = overall_data.getJSONObject("both");
			}
			JSONObject item = object.getJSONObject("location");
			JSONObject data = item.getJSONObject("data");
			Iterator<String> iter = data.keys();
			while (iter.hasNext()) {
				String key = iter.next();

				names.add(key);
				Object value = data.get(key);

				counts.add(value.toString());
			}
			Chart();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void CategoryFiltering(String response2) {
		names.clear();
		counts.clear();
		JSONObject jObj;
		JSONObject object;
		try {
			jObj = new JSONObject(response2);

			JSONObject overall_data = jObj.getJSONObject("overall_data");

			if (filtertype.equals("package")) {
				object = overall_data.getJSONObject("package");
			} else if (filtertype.equals("inventory")) {
				object = overall_data.getJSONObject("inventory");
			} else {
				object = overall_data.getJSONObject("both");
			}
			JSONObject item = object.getJSONObject("category");
			JSONObject data = item.getJSONObject("data");
			Iterator<String> iter = data.keys();
			while (iter.hasNext()) {
				String key = iter.next();

				names.add(key);
				Object value = data.get(key);

				counts.add(value.toString());
			}
			Chart();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void TimeFioltering(String response2) {
		names.clear();
		counts.clear();
		JSONObject jObj;
		JSONObject object;
		JSONObject filter;
		try {
			jObj = new JSONObject(response2);

			JSONObject overall_data = jObj.getJSONObject("overall_data");

			if (filtertype.equals("package")) {
				object = overall_data.getJSONObject("package");
			} else if (filtertype.equals("inventory")) {
				object = overall_data.getJSONObject("inventory");
			} else {
				object = overall_data.getJSONObject("both");
			}
			JSONObject time = object.getJSONObject("time");
			JSONObject data = time.getJSONObject("data");
			if (filteritem.equals("year")) {
				filter = data.getJSONObject("year");
			} else if (filteritem.equals("month")) {
				filter = data.getJSONObject("month");
			} else {
				filter = data.getJSONObject("week");
			}

			Iterator<String> iter = filter.keys();
			while (iter.hasNext()) {
				String key = iter.next();

				names.add(key);
				Object value = filter.get(key);

				counts.add(value.toString());
			}
			Chart();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
