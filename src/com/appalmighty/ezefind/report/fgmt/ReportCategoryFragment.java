package com.appalmighty.ezefind.report.fgmt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.filter.Approximator;
import com.github.mikephil.charting.data.filter.Approximator.ApproximatorType;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ValueFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

public class ReportCategoryFragment extends Fragment implements
		OnClickListener, OnChartValueSelectedListener {
	GridView catagoryview1;
	GridViewAdapter adapter;
	GraphView graph;
	BarGraphSeries<DataPoint> series;
	CheckBox chkpackage, chkinventory, chkboth;
	ImageView filtering, cancel;
	String filtertype = "both";
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;
	SharedPreferences shared;
	static RelativeLayout filterdialog;
	Editor editor;
	ArrayList<String> catagoryname;
	ArrayList<String> catagorycount;
	protected BarChart mChart;
	StaticLabelsFormatter staticLabelsFormatter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.fragment_view_report_category, container, false);
		catagoryview1 = (GridView) rootView.findViewById(R.id.catagoryview);
		// graph = (GraphView) rootView.findViewById(R.id.graph);
		filterdialog = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter_all);
		filtering = (ImageView) rootView.findViewById(R.id.id_report_filtering);
		cancel = (ImageView) rootView.findViewById(R.id.btnDone);
		chkpackage = (CheckBox) rootView.findViewById(R.id.checkbox_package1);
		chkinventory = (CheckBox) rootView
				.findViewById(R.id.checkbox_inventory1);
		chkboth = (CheckBox) rootView.findViewById(R.id.checkbox_both1);
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		mChart = (BarChart) rootView.findViewById(R.id.chart1);
		mChart.setOnChartValueSelectedListener(this);
		filtertype = shared.getString(AppConstant.KEY_DEFAULT_Filter, "");
		if (filtertype.equals("package")) {
			chkpackage.setChecked(true);
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
		} else if (filtertype.equals("inventory")) {
			chkpackage.setChecked(false);
			chkinventory.setChecked(true);
			chkboth.setChecked(false);
		} else {
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			chkboth.setChecked(true);
		}
		internetcheck = new ConnectionDetector(getActivity());
		isInternetPresent = internetcheck.isConnectingToInternet();
		if (isInternetPresent) {
			new AsyncGetCatogaryDataTask().execute(filtertype);
		} else {
			ViewUtil.showAlertDialog(getActivity(), "ERROR",
					"No Internet Connection", false);
		}
		catagoryname = new ArrayList<String>();
		catagorycount = new ArrayList<String>();
		chkpackage.setOnClickListener(this);
		chkinventory.setOnClickListener(this);
		chkboth.setOnClickListener(this);
		filtering.setOnClickListener(this);
		cancel.setOnClickListener(this);

		return rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

		inflater.inflate(R.menu.bar, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.actionToggleValues: {
			for (DataSet<?> set : mChart.getData().getDataSets())
				set.setDrawValues(!set.isDrawValuesEnabled());

			mChart.invalidate();
			break;
		}
		case R.id.actionToggleHighlight: {
			if (mChart.isHighlightEnabled())
				mChart.setHighlightEnabled(false);
			else
				mChart.setHighlightEnabled(true);
			mChart.invalidate();
			break;
		}
		case R.id.actionTogglePinch: {
			if (mChart.isPinchZoomEnabled())
				mChart.setPinchZoom(false);
			else
				mChart.setPinchZoom(true);

			mChart.invalidate();
			break;
		}
		case R.id.actionToggleAutoScaleMinMax: {
			mChart.setAutoScaleMinMaxEnabled(!mChart.isAutoScaleMinMaxEnabled());
			mChart.notifyDataSetChanged();
			break;
		}
		case R.id.actionToggleHighlightArrow: {
			if (mChart.isDrawHighlightArrowEnabled())
				mChart.setDrawHighlightArrow(false);
			else
				mChart.setDrawHighlightArrow(true);
			mChart.invalidate();
			break;
		}
		case R.id.actionToggleStartzero: {
			mChart.getAxisLeft().setStartAtZero(
					!mChart.getAxisLeft().isStartAtZeroEnabled());
			mChart.getAxisRight().setStartAtZero(
					!mChart.getAxisRight().isStartAtZeroEnabled());
			mChart.notifyDataSetChanged();
			mChart.invalidate();
			break;
		}
		case R.id.animateX: {
			mChart.animateX(3000);
			break;
		}
		case R.id.animateY: {
			mChart.animateY(3000);
			break;
		}
		case R.id.animateXY: {

			mChart.animateXY(3000, 3000);
			break;
		}
		case R.id.actionToggleFilter: {

			Approximator a = new Approximator(ApproximatorType.DOUGLAS_PEUCKER,
					25);

			if (!mChart.isFilteringEnabled()) {
				mChart.enableFiltering(a);
			} else {
				mChart.disableFiltering();
			}
			mChart.invalidate();
			break;
		}
		case R.id.actionSave: {
			if (mChart.saveToGallery("title" + System.currentTimeMillis(), 50)) {
				Toast.makeText(getActivity(), "Saving SUCCESSFUL!",
						Toast.LENGTH_SHORT).show();
			} else
				Toast.makeText(getActivity(), "Saving FAILED!",
						Toast.LENGTH_SHORT).show();
			break;
		}
		}
		return true;
	}

	public static Fragment newInstance() {
		ReportCategoryFragment fragment = new ReportCategoryFragment();
		return fragment;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.id_report_filtering:
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {

				new AsyncGetCatogaryDataTask().execute(filtertype);

			} else {
				ViewUtil.showAlertDialog(getActivity(), "ERROR",
						"No Internet Connection", false);
			}
			break;

		case R.id.checkbox_package1:
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "package";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.checkbox_inventory1:

			chkpackage.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "inventory";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();

			break;
		case R.id.checkbox_both1:
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			filtertype = "both";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.btnDone:
			filterdialog.setVisibility(View.GONE);
			break;
		}
	}

	public static void showdialog() {
		filterdialog.setVisibility(View.VISIBLE);
	}

	public class AsyncGetCatogaryDataTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				ViewUtil.showProgressDialog(getActivity());
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result != null) {
						if (result.equals("error")) {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"SomeThing Wrong", false);
							ViewUtil.hideProgressDialog();
						} else {

							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("Response category", result);
								JSONObject phone = jObj.getJSONObject("Data");

								Iterator<String> iter = phone.keys();
								while (iter.hasNext()) {
									String key = iter.next();
									try {
										Log.e("key", "" + key);
										catagoryname.add(key);
										Object value = phone.get(key);
										Log.e("value", "" + value.toString());
										catagorycount.add(value.toString());
									} catch (JSONException e) {
										// Something went wrong!
									}
								}

								// graph.removeAllSeries();
								// series = new BarGraphSeries<DataPoint>(new
								// DataPoint[] {
								//
								// });
								// graph.addSeries(series);
								//
								// staticLabelsFormatter = new
								// StaticLabelsFormatter(graph);
								//
								// staticLabelsFormatter.setHorizontalLabels(new
								// String[] {
								// "Bag", "Box", "Single Item", "Bin", " " });
								//
								// graph(staticLabelsFormatter);
								adapter = new GridViewAdapter(getActivity(),
										catagoryname, catagorycount);
								catagoryview1.setAdapter(adapter);
								filterdialog.setVisibility(View.GONE);
								ViewUtil.hideProgressDialog();
								mChart.removeAllViews();
								mChart.setDrawBarShadow(false);
								mChart.setDrawValueAboveBar(true);

								mChart.setMaxVisibleValueCount(100);

								mChart.setPinchZoom(false);

								mChart.setDescription("");

								mChart.setDrawGridBackground(false);

								XAxis xAxis = mChart.getXAxis();
								xAxis.setPosition(XAxisPosition.BOTTOM);
								xAxis.setValues(catagoryname);
								xAxis.setDrawGridLines(false);
								xAxis.setDrawLabels(true);
								xAxis.setSpaceBetweenLabels(0);

								ValueFormatter custom = new MyValueFormatter();

								YAxis leftAxis = mChart.getAxisLeft();
								// leftAxis.setTypeface(mTf);
								leftAxis.setLabelCount(5, true);
								leftAxis.setValueFormatter(custom);
								leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
								leftAxis.setSpaceTop(15f);

								YAxis rightAxis = mChart.getAxisRight();
								rightAxis.setDrawGridLines(false);
								// // rightAxis.setTypeface(mTf);
								rightAxis.setLabelCount(0, false);
								rightAxis.setDrawLabels(false);
								// // rightAxis.setValueFormatter(custom);
								// rightAxis.setSpaceTop(0);

								Legend l = mChart.getLegend();
								l.setPosition(LegendPosition.BELOW_CHART_LEFT);

								l.setForm(LegendForm.LINE);
								l.setFormSize(0);
								l.setTextSize(0);
								l.setXEntrySpace(0f);
								setData(catagorycount.size(), 100);

							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");
								filterdialog.setVisibility(View.GONE);
								ViewUtil.hideProgressDialog();
								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);

							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;
			try {

				List<NameValuePair> parameter = new LinkedList<NameValuePair>();

				parameter.add(new BasicNameValuePair("main_filter", String
						.valueOf(params[0])));

				Log.e("parameter category", "" + parameter);
				result = NetworkConnector.GetCatagoryData(parameter);
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
			return result;
		}

	}

	class GridViewAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;
		ArrayList<String> catagoryname1 = new ArrayList<String>();
		ArrayList<String> catagorycount1 = new ArrayList<String>();

		public GridViewAdapter(FragmentActivity activity2,
				ArrayList<String> catagoryname, ArrayList<String> catagorycount) {
			// TODO Auto-generated constructor stub
			this.activity = activity2;
			catagoryname1 = catagoryname;
			catagorycount1 = catagorycount;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return catagoryname1.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return catagoryname1.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			final ViewHolder holder;
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.fragment_report_catagoryview, null);

				holder = new ViewHolder();
				holder.catagoryname = (TextView) convertView
						.findViewById(R.id.label_pants);

				holder.catagorycount = (TextView) convertView
						.findViewById(R.id.txt_count_pants);

				holder.catagoryfilter = (ImageView) convertView
						.findViewById(R.id.img_1);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {
				holder.catagoryname.setId(position);

				holder.catagorycount.setId(position);

				holder.catagoryname.setText(catagoryname1.get(position));

				holder.catagorycount.setText(catagorycount1.get(position));

				holder.catagoryfilter.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// filtermemberdialog.setVisibility(View.VISIBLE);
						// selectedposition = position;
					}
				});

			} catch (Exception e) {
				e.printStackTrace();
			}
			return convertView;
		}

	}

	class ViewHolder {

		private TextView catagoryname, catagorycount;
		private ImageView catagoryfilter;

	}

	private void setData(int count, float range) {

		ArrayList<String> xVals = new ArrayList<String>();
		for (int i = 0; i < count; i++) {
			xVals.add(catagoryname.get(i));
		}

		ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

		for (int i = 0; i < count; i++) {

			yVals1.add(new BarEntry(Integer.parseInt(catagorycount.get(i)), i));
		}

		BarDataSet set1 = new BarDataSet(yVals1, "");
		set1.setBarSpacePercent(35f);

		ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
		dataSets.add(set1);

		BarData data = new BarData(xVals, dataSets);
		// data.setValueFormatter(new MyValueFormatter());
		data.setValueTextSize(10f);
		// data.setValueTypeface(mTf);

		mChart.setData(data);
	}

	@SuppressLint("NewApi")
	@Override
	public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

		if (e == null)
			return;

		RectF bounds = mChart.getBarBounds((BarEntry) e);
		PointF position = mChart.getPosition(e, AxisDependency.LEFT);

		Log.e("bounds", bounds.toString());
		Log.e("position", position.toString());

		Log.e("x-index", "low: " + mChart.getLowestVisibleXIndex() + ", high: "
				+ mChart.getHighestVisibleXIndex());
	}

	public void onNothingSelected() {
	};
}
