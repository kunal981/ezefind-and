package com.appalmighty.ezefind.report.fgmt;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appalmighty.ezefind.ConnectionDetector;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.net.NetworkConnector;
import com.appalmighty.ezefind.ui.ViewUtil;
import com.appalmighty.ezefind.util.AppConstant;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.PercentFormatter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ReportMemberFragment extends Fragment implements OnClickListener {
	private GridViewAdapter adapter1;
	ImageView filtering, filteringitem, cancel, cancelmemberdialog;
	CheckBox chkpackage, chkinventory, chkboth, chkbag, chkbox, chkbin,
			chksingleitem;
	static RelativeLayout filterdialog, filtermemberdialog;
	SharedPreferences shared;
	Editor editor;
	String filtertype, filtermember;
	private GridView gridView;
	private PieChart mChart;
	int centerpic = 0;
	ImageView center;
	Boolean isInternetPresent = false;
	ConnectionDetector internetcheck;

	ArrayList<String> Memberid;
	ArrayList<String> Memberimage;
	ArrayList<String> membername;
	ArrayList<String> itemcount;
	ArrayList<String> xVals;
	ArrayList<Entry> yVals1;
	ArrayList<String> typecount;
	int selectedposition;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_view_report_member,
				container, false);
		filterdialog = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter_all);
		filtermemberdialog = (RelativeLayout) rootView
				.findViewById(R.id.layout_filter);
		chkpackage = (CheckBox) rootView.findViewById(R.id.checkbox_package1);
		chkinventory = (CheckBox) rootView
				.findViewById(R.id.checkbox_inventory1);
		chkboth = (CheckBox) rootView.findViewById(R.id.checkbox_both1);
		chkbag = (CheckBox) rootView.findViewById(R.id.checkbox_bag);
		chkbox = (CheckBox) rootView.findViewById(R.id.checkbox_box);
		chkbin = (CheckBox) rootView.findViewById(R.id.checkbox_bin);
		chksingleitem = (CheckBox) rootView
				.findViewById(R.id.checkbox_singleitem);
		cancel = (ImageView) rootView.findViewById(R.id.btnDone);
		cancelmemberdialog = (ImageView) rootView.findViewById(R.id.btnDo);
		filteringitem = (ImageView) rootView
				.findViewById(R.id.id_member_filtering);
		filtering = (ImageView) rootView.findViewById(R.id.id_report_filtering);
		gridView = (GridView) rootView.findViewById(R.id.id_gridview);
		mChart = (PieChart) rootView.findViewById(R.id.donutchart);
		center = (ImageView) rootView.findViewById(R.id.centerimage);
		mChart.setUsePercentValues(true);
		mChart.setDescription("");
		shared = getActivity().getSharedPreferences(AppConstant.KEY_APP,
				Context.MODE_PRIVATE);
		Memberid = new ArrayList<String>();
		membername = new ArrayList<String>();
		Memberimage = new ArrayList<String>();
		itemcount = new ArrayList<String>();
		xVals = new ArrayList<String>();
		yVals1 = new ArrayList<Entry>();
		typecount = new ArrayList<String>();
		filtertype = shared.getString(AppConstant.KEY_DEFAULT_Filter, "");
		if (filtertype.equals("package")) {
			chkpackage.setChecked(true);
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
		} else if (filtertype.equals("inventory")) {
			chkpackage.setChecked(false);
			chkinventory.setChecked(true);
			chkboth.setChecked(false);
		} else {
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			chkboth.setChecked(true);
		}
		internetcheck = new ConnectionDetector(getActivity());
		isInternetPresent = internetcheck.isConnectingToInternet();
		if (isInternetPresent) {
			new AsyncGetAllMemberTask().execute(filtertype);
			new AsyncGetAllMemberReprtTask().execute(filtertype);
		} else {
			ViewUtil.showAlertDialog(getActivity(), "ERROR",
					"No Internet Connection", false);
		}
		center.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				centerpic++;

				if (centerpic == 1) {
					center.setImageResource(R.drawable.bin);
					new AsyncGetMemberReportfilterTask().execute(filtertype,
							"bin");
				} else if (centerpic == 2) {
					center.setImageResource(R.drawable.box);
					new AsyncGetMemberReportfilterTask().execute(filtertype,
							"box");
				} else if (centerpic == 3) {
					center.setImageResource(R.drawable.singleitem);
					new AsyncGetMemberReportfilterTask().execute(filtertype,
							"single item");
				} else if (centerpic == 4) {
					center.setImageResource(R.drawable.all);
					new AsyncGetMemberReportfilterTask().execute(filtertype,
							"all");
				} else if (centerpic == 5) {
					center.setImageResource(R.drawable.bag);
					new AsyncGetMemberReportfilterTask().execute(filtertype,
							"bag");
					centerpic = 0;
				}

			}
		});

		adapter1 = new GridViewAdapter(getActivity(), Memberid, membername,
				itemcount, typecount, Memberimage);
		gridView.setAdapter(adapter1);

		chkpackage.setOnClickListener(this);
		chkinventory.setOnClickListener(this);
		chkboth.setOnClickListener(this);
		chkbag.setOnClickListener(this);
		chkbox.setOnClickListener(this);
		chkbin.setOnClickListener(this);
		chksingleitem.setOnClickListener(this);
		filtering.setOnClickListener(this);
		cancel.setOnClickListener(this);
		cancelmemberdialog.setOnClickListener(this);
		filteringitem.setOnClickListener(this);
		return rootView;
	}

	public static Fragment newInstance() {
		ReportMemberFragment fragment = new ReportMemberFragment();
		return fragment;
	}

	private void setData(int count, float range) {

		float mult = range;

		PieDataSet dataSet = new PieDataSet(yVals1, "");
		dataSet.setSliceSpace(3f);
		dataSet.setSelectionShift(5f);

		// add a lot of colors

		ArrayList<Integer> colors = new ArrayList<Integer>();

		for (int c : ColorTemplate.VORDIPLOM_COLORS)
			colors.add(c);

		for (int c : ColorTemplate.JOYFUL_COLORS)
			colors.add(c);

		for (int c : ColorTemplate.COLORFUL_COLORS)
			colors.add(c);

		for (int c : ColorTemplate.LIBERTY_COLORS)
			colors.add(c);

		for (int c : ColorTemplate.PASTEL_COLORS)
			colors.add(c);

		colors.add(ColorTemplate.getHoloBlue());

		dataSet.setColors(colors);

		PieData data = new PieData(xVals, dataSet);
		data.setValueFormatter(new PercentFormatter());
		data.setValueTextSize(11f);
		data.setValueTextColor(Color.BLACK);

		mChart.setData(data);

		mChart.invalidate();
	}

	class GridViewAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;
		ArrayList<String> memberid1 = new ArrayList<String>();
		ArrayList<String> membername1 = new ArrayList<String>();
		ArrayList<String> itemcount1 = new ArrayList<String>();
		ArrayList<String> Memberimage = new ArrayList<String>();
		ArrayList<String> typecount1 = new ArrayList<String>();
		DisplayImageOptions displayImageOptions;

		public GridViewAdapter(FragmentActivity activity2,
				ArrayList<String> memberid, ArrayList<String> membername,
				ArrayList<String> itemcount, ArrayList<String> typecount,
				ArrayList<String> Memberimage) {
			// TODO Auto-generated constructor stub

			this.activity = activity2;
			memberid1 = memberid;
			membername1 = membername;
			itemcount1 = itemcount;

			typecount1 = typecount;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return memberid1.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return memberid1.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			final ViewHolder holder;
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {

				convertView = inflater.inflate(
						R.layout.layout_row_member_items, null);

				holder = new ViewHolder();
				holder.member = (TextView) convertView.findViewById(R.id.name);

				holder.typecount = (TextView) convertView
						.findViewById(R.id.item_num);
				holder.itemcount = (TextView) convertView
						.findViewById(R.id.item_count);
				holder.memberfilter = (ImageView) convertView
						.findViewById(R.id.img_bar);
				holder.memberPic = (ImageView) convertView
						.findViewById(R.id.img_user);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {
				holder.member.setId(position);

				holder.typecount.setId(position);
				holder.itemcount.setId(position);
				holder.member.setText(membername1.get(position));

				holder.typecount.setText(typecount1.get(position));
				holder.itemcount.setText(itemcount1.get(position));

				ImageLoader.getInstance().displayImage(
						Memberimage.get(position), holder.memberPic);

				holder.memberfilter.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						filtermemberdialog.setVisibility(View.VISIBLE);
						selectedposition = position;
					}
				});

			} catch (Exception e) {
				e.printStackTrace();
			}
			return convertView;
		}

	}

	class ViewHolder {

		private TextView typecount, itemcount, member;
		private ImageView memberfilter, memberPic;

	}

	public static void showdialog() {
		// TODO Auto-generated method stub
		filterdialog.setVisibility(View.VISIBLE);
	}

	public class AsyncGetAllMemberTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				ViewUtil.showProgressDialog(getActivity());
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				Memberid.clear();
				membername.clear();
				itemcount.clear();

				typecount.clear();
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result != null) {
						if (result.equals("error")) {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"SomeThing Wrong", false);
							ViewUtil.hideProgressDialog();
						} else {

							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("Response all member  task", result);
								JSONArray array = jObj.getJSONArray("Data");
								for (int i = 0; i < array.length(); i++) {
									JSONObject object = array.getJSONObject(i);
									Memberid.add(object.getString("UserId"));
									membername.add(object
											.getString("FirstName"));
									/*
									 * Memberimage.add(
									 * "http://beta.brstdev.com/yiiezefind"
									 * +object .getString("ProfileImage"));
									 */
									Memberimage.add(object
											.getString("ProfileImage"));
									itemcount.add(object
											.getString("Item_count") + " Item");

									typecount.add(object.getString("Bag_count")
											+ " "
											+ object.getString("Data_Type"));
								}

								Log.e("Memberimage", "" + Memberimage);
								filterdialog.setVisibility(View.GONE);
								adapter1.notifyDataSetChanged();
								ViewUtil.hideProgressDialog();
							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");

								ViewUtil.hideProgressDialog();
								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);

							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;
			try {
				List<NameValuePair> parameter = new LinkedList<NameValuePair>();

				parameter.add(new BasicNameValuePair("main_filter", String
						.valueOf(params[0])));
				 Log.e("parameter--member", "" +parameter);
				result = NetworkConnector.GetAllMemeber(parameter);
				
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
			return result;
		}

	}

	public class AsyncGetAllMemberReprtTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				ViewUtil.showProgressDialog(getActivity());
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				xVals.clear();
				yVals1.clear();
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result != null) {
						if (result.equals("error")) {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"SomeThing Wrong", false);
							ViewUtil.hideProgressDialog();
						} else {

							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("filter Response ", result);
								JSONArray array = jObj.getJSONArray("Data");
								for (int i = 0; i < array.length(); i++) {
									JSONObject object = array.getJSONObject(i);

									xVals.add(object.getString("FirstName"));
									yVals1.add(new Entry(Integer
											.parseInt(object
													.getString("Item_count")),
											i));

								}
								showchart();
								ViewUtil.hideProgressDialog();
							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");

								ViewUtil.hideProgressDialog();
								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);

							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;
			try {
				List<NameValuePair> parameter = new LinkedList<NameValuePair>();

				parameter.add(new BasicNameValuePair("main_filter", String
						.valueOf(params[0])));
				Log.e("parameter--member2", "" + parameter);
				result = NetworkConnector.GetAllMemeberReport(parameter);
				
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
			return result;
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.id_report_filtering:
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {

				new AsyncGetAllMemberTask().execute(filtertype);

			} else {
				ViewUtil.showAlertDialog(getActivity(), "ERROR",
						"No Internet Connection", false);
			}
			break;

		case R.id.id_member_filtering:
			isInternetPresent = internetcheck.isConnectingToInternet();
			if (isInternetPresent) {
				new AsyncfiltersingleMemberTask().execute(filtertype,
						Memberid.get(selectedposition), filtermember);
			} else {
				ViewUtil.showAlertDialog(getActivity(), "ERROR",
						"No Internet Connection", false);
			}
			break;
		case R.id.checkbox_package1:
			chkinventory.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "package";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.checkbox_inventory1:

			chkpackage.setChecked(false);
			chkboth.setChecked(false);
			filtertype = "inventory";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();

			break;
		case R.id.checkbox_both1:
			chkpackage.setChecked(false);
			chkinventory.setChecked(false);
			filtertype = "both";
			editor = shared.edit();

			editor.putString(AppConstant.KEY_DEFAULT_Filter, filtertype);
			editor.commit();
			break;
		case R.id.checkbox_bag:
			chkbox.setChecked(false);
			chkbin.setChecked(false);
			chksingleitem.setChecked(false);
			filtermember = "bag";
			break;
		case R.id.checkbox_box:
			chkbag.setChecked(false);
			chkbin.setChecked(false);
			chksingleitem.setChecked(false);
			filtermember = "box";

			break;
		case R.id.checkbox_bin:
			chkbag.setChecked(false);
			chkbox.setChecked(false);
			chksingleitem.setChecked(false);
			filtermember = "bin";

			break;
		case R.id.checkbox_singleitem:
			chkbag.setChecked(false);
			chkbox.setChecked(false);
			chkbin.setChecked(false);
			filtermember = "single item";

			break;
		case R.id.btnDone:
			filterdialog.setVisibility(View.GONE);
			break;
		case R.id.btnDo:
			filtermemberdialog.setVisibility(View.GONE);
			break;
		}
	}

	public class AsyncfiltersingleMemberTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				ViewUtil.showProgressDialog(getActivity());
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result != null) {
						if (result.equals("error")) {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"SomeThing Wrong", false);
							ViewUtil.hideProgressDialog();
						} else {

							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("Report Response ", result);
								JSONObject object = jObj.getJSONObject("Data");

								Memberid.set(selectedposition,
										object.getString("UserId"));
								membername.set(selectedposition,
										object.getString("FirstName"));
								itemcount.set(selectedposition,
										object.getString("Item_count")
												+ " Item");

								typecount
										.set(selectedposition,
												object.getString("Bag_count")
														+ " "
														+ object.getString("Data_Type"));
								ViewUtil.hideProgressDialog();
								adapter1.notifyDataSetChanged();
								filtermemberdialog.setVisibility(View.GONE);
							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");

								ViewUtil.hideProgressDialog();
								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);
								filtermemberdialog.setVisibility(View.GONE);
							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;
			try {
				List<NameValuePair> parameter = new LinkedList<NameValuePair>();

				parameter.add(new BasicNameValuePair("main_filter", String
						.valueOf(params[0])));
				parameter.add(new BasicNameValuePair("UserId", String
						.valueOf(params[1])));
				parameter.add(new BasicNameValuePair("Filter_by", String
						.valueOf(params[2])));
				Log.e("parameter single", "" + parameter);
				result = NetworkConnector.GetSingle_Memeber_filter(parameter);
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
			return result;
		}

	}

	public class AsyncGetMemberReportfilterTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				ViewUtil.showProgressDialog(getActivity());
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				xVals.clear();
				yVals1.clear();
				isInternetPresent = internetcheck.isConnectingToInternet();
				if (isInternetPresent) {

					if (result != null) {
						if (result.equals("error")) {
							ViewUtil.showAlertDialog(getActivity(), "ERROR",
									"SomeThing Wrong", false);
							ViewUtil.hideProgressDialog();
						} else {

							JSONObject jObj = new JSONObject(result);

							if (jObj.getBoolean("success")) {
								Log.e("filter Response ", result);
								JSONArray array = jObj.getJSONArray("Data");
								for (int i = 0; i < array.length(); i++) {
									JSONObject object = array.getJSONObject(i);

									xVals.add(object.getString("FirstName"));
									yVals1.add(new Entry(Integer
											.parseInt(object
													.getString("Item_count")),
											i));

								}
								showchart();
								ViewUtil.hideProgressDialog();
							}

							else {
								Log.e("Response", result);

								String Server_message = jObj
										.getString("message");

								ViewUtil.hideProgressDialog();
								ViewUtil.showAlertDialog(getActivity(),
										"Error", Server_message, true);

							}
						}
					}
				} else {
					ViewUtil.showAlertDialog(getActivity(), "ERROR",
							"No Internet Connection", false);
					ViewUtil.hideProgressDialog();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;
			try {
				List<NameValuePair> parameter = new LinkedList<NameValuePair>();

				parameter.add(new BasicNameValuePair("main_filter", String
						.valueOf(params[0])));
				parameter.add(new BasicNameValuePair("Filter_by", String
						.valueOf(params[1])));
				Log.e("parameter", "" + parameter);
				result = NetworkConnector.GetMemeberReportFilter(parameter);
			} catch (Exception e) {
				Log.e("crash", "" + e.getMessage());
			}
			return result;
		}

	}

	public void showchart() {
		mChart.setDragDecelerationFrictionCoef(0.90f);

		mChart.setDrawHoleEnabled(true);
		mChart.setHoleColorTransparent(true);
		mChart.setHoleRadius(50);
		mChart.setTransparentCircleColor(Color.WHITE);
		mChart.setTransparentCircleAlpha(1);

		mChart.setDrawCenterText(true);

		mChart.setRotationAngle(0);
		// enable rotation of the chart by touch
		mChart.setRotationEnabled(false);

		setData(xVals.size(), 100);

		Legend l = mChart.getLegend();
		l.setPosition(LegendPosition.RIGHT_OF_CHART);
		l.setXEntrySpace(20f);
		l.setYEntrySpace(2f);
		// l.setYOffset(0f);

		// l.setXOffset(0f);
	}
}
