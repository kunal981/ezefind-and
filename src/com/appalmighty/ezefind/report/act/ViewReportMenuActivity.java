package com.appalmighty.ezefind.report.act;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.appalmighty.ezefind.MainActivity;
import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.newentry.act.PickGoActivity;
import com.appalmighty.ezefind.setting.act.SettingActivity;
import com.appalmighty.ezefind.viewinventory.act.ViewInventoryActivity;

public class ViewReportMenuActivity extends FragmentActivity implements
		OnClickListener {

	private ImageView buttonNewEntry, buttonViewInventory, buttonSetting;
	private LinearLayout containerMenu;
	private RelativeLayout containerClick;

	private Button buttonPacking, buttonInventory;

	RelativeLayout relButtonAll, relButtonItem, relButtonMember,
			relButtonCategory, relButtonValue, relButtonLocation,
			relButtonDate;
	Button btnBack;
	private Button historyButton;
	private Button userButton;
	private Button fullMenuButton;
	String key = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_view_report_menu);
		key = getIntent().getStringExtra("key");
		historyButton = (Button) findViewById(R.id.id_btn_history);
		userButton = (Button) findViewById(R.id.id_btn_user);
		fullMenuButton = (Button) findViewById(R.id.id_btn_full_menu);
		btnBack = (Button) findViewById(R.id.back_button);
		buttonNewEntry = (ImageView) findViewById(R.id.id_img_new_entry);
		buttonViewInventory = (ImageView) findViewById(R.id.id_img_view_inventroy);
		buttonSetting = (ImageView) findViewById(R.id.id_img_setting);
		containerMenu = (LinearLayout) findViewById(R.id.rel_menu_bar);
		containerClick = (RelativeLayout) findViewById(R.id.r_outer_click_container);
		buttonPacking = (Button) findViewById(R.id.id_buton_pack);
		buttonInventory = (Button) findViewById(R.id.id_buton_inventory);

		historyButton.setOnClickListener(new MenuClickListner());
		userButton.setOnClickListener(new MenuClickListner());
		fullMenuButton.setOnClickListener(new MenuClickListner());

		btnBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				Intent intent = new Intent(ViewReportMenuActivity.this,
						MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		buttonPacking.setOnClickListener(this);
		buttonInventory.setOnClickListener(this);
		buttonNewEntry.setOnClickListener(this);
		buttonViewInventory.setOnClickListener(this);
		buttonSetting.setOnClickListener(this);
		containerClick.setOnClickListener(this);
	}

	class MenuClickListner implements OnClickListener {

		@Override
		public void onClick(View v) {
			int id = v.getId();
			Intent intentReport = null;
			switch (id) {
			case R.id.id_btn_history:
				intentReport = new Intent(ViewReportMenuActivity.this,
						ViewReportActivity.class);
				intentReport.putExtra("key", "history");
				startActivity(intentReport);
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
				finish();
				break;
			case R.id.id_btn_user:
				intentReport = new Intent(ViewReportMenuActivity.this,
						ViewReportActivity.class);
				intentReport.putExtra("key", "user");
				startActivity(intentReport);
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
				finish();
				break;
			case R.id.id_btn_full_menu:
				intentReport = new Intent(ViewReportMenuActivity.this,
						ViewReportActivity.class);
				intentReport.putExtra("key", "full_menu");
				startActivity(intentReport);
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
				finish();
				break;

			}

		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.id_img_new_entry:
			containerMenu.setVisibility(View.VISIBLE);
			break;
		case R.id.id_img_view_inventroy:

			Intent i = new Intent(ViewReportMenuActivity.this,
					ViewInventoryActivity.class);
			startActivity(i);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		case R.id.id_img_setting:
			Intent intentSetting = new Intent(ViewReportMenuActivity.this,
					SettingActivity.class);
			startActivity(intentSetting);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		case R.id.r_outer_click_container:
			if (containerMenu.isShown()) {
				containerMenu.setVisibility(View.INVISIBLE);
			}
			break;
		case R.id.id_buton_inventory:
			Intent intent1 = new Intent(ViewReportMenuActivity.this,
					PickGoActivity.class);
			intent1.putExtra("Pack_go", "inventory");
			startActivity(intent1);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		case R.id.id_buton_pack:
			Intent intent = new Intent(ViewReportMenuActivity.this,
					PickGoActivity.class);
			intent.putExtra("Pack_go", "pack_go");
			startActivity(intent);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		default:
			break;
		}
	}

}
